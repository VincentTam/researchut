#!/bin/bash

HOST="www.researchut.com"
WEBROOT="/home/rrs/researchut-hugo/"
LOCAL_PUBLIC="./public/"


echo -en "Generating web pages. Runnign hugo\n";
hugo || exit 1;

if ! [ -d $LOCAL_PUBLIC ]; then
    echo -en "$LOCAL_PUBLIC is not accessible. Aborting.\n\n";
    exit 1;
fi

echo -en "Syncing local folder $LOCAL_PUBLIC to $HOST:$WEBROOT\n";
rsync -avzP --delete $LOCAL_PUBLIC $HOST:$WEBROOT;

