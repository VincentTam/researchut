{
    "title": "Arjuna and Lord Shiva",
    "date": "2016-04-02T11:49:38-04:00",
    "lastmod": "2016-04-02T11:49:38-04:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "arjuna",
        "shiva"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/arjuna-and-shiva"
}

# Arjuna and Lord Shiva

Posted by [VIBHOR
MAHAJAN](https://vibhormahajan.wordpress.com/author/vibhormahajan/) on [MAY
10, 2012](https://vibhormahajan.wordpress.com/2012/05/10/arjuna-and-lord-
shiva/)

[![sivagivesweapon](/sites/default/files/Vibhor//sivagivesweapon.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/sivagivesweapon.jpg)

In Mahabharata, Duryodhana with help of his uncle Shakuni robbed the Pandavas
of their rightful share of the kingdom in a game of dice. As a result of this,
the Pandavas had to spend many years in the forest.  While they were in the
forest, Vedavyasa (Sage who wrote Mahabharata) came to visit the Pandavas.
Vedavyasa told them that they should pray to Lord Shiva. But since Arjuna was
the best suited amongst the Pandavas for worshipping Lord Shiva, Vedvyasa
taught Arjuna a special mantra (incantation). Then he asked Arjuna to go to
Mount Indrakila and pray to Shiva there.  Mount Indrakila  was on the banks of
the river Bhagirathi.

Arjuna went to Mount Indrakila. He made a linga (A linga is an image of Lord
Shiva) out of clay and started to pray to Lord Shiva. The news of Arjuna’s
wonderful tapasya spread everywhere. Arjuna stood on one leg and continually
chanted the mantra that Vedavyasa had taught him.

Suddenly, Arjuna saw a boar. Arjuna thought that this fierce boar might have
come to distract him from his tapasya. Alternatively, it might be a relative
of the several demons that he had killed and therefore might  wish him harm.
Thinking this, Arjuna picked up his bow and arrow and let fly an arrow at the
boar. Meanwhile, Lord Shiva had decided to subject Arjuna to a test and he had
also arrived at the spot disgusted as a hunter. When Arjuna shot an arrow at
the boar, so did Lord Shiva. Lord Shiva’s arrow struck the boar in its
hindquarters and Arjuna’s arrow struck the boar in its mouth. The boar fell
down dead.

A dispute started between Arjuna and the hunter about who had killed the boar.
Each claimed it for his own. They began to fight. But whatever weapons were
hurled by Lord Shiva were easily repelled all of Arjuna’s weapons. When all
the weapons were exhausted, the two started to wrestle.

After the fight had gone on for a while, Lord Shiva gave up his disguise of a
hunter and displayed his true form to Arjuna. Arjuna was ashamed that he had
been fighting with the very person to whom he had been praying.

Please forgive me, said Arjuna.

It is all right, replied Lord Shiva. I was just trying to test you. Your
weapons have been like offerings to me, you are my devotee. Tell me, what boon
do you desire?

Arjuna wanted the boon that he might obtain glory on earth. He said – “O lord,
if you are pleased with me, then I humbly ask that you bestow upon me your
irresistible personal weapon known as the  _Pashupata_. You destroy the
universe with this weapon at the end of creation, and with it I may be
victorious over   _Rakshasas_ ,  _Danavas_ , _Gandharvas_  (a class of
celestial),  _Nagas_ , ghosts and spirits. It will enable me to emerge
successfully from the battle I shall fight against  _Bhishma_ ,  _Drona_ ,
_Kripa_ , and the son of the suta,  _Karna_.

Lord Shiva replied – “O son of  _Kunti_ , I will give you this weapon. You are
capable of holding, throwing, and withdrawing it. Not even  _Indra_ ,  _Yama_
,  _Kuvera_ , or  _Varuna_  knows the mantras to this weapon – what to speak
of any Man. However, you must use it only against celestial fighters. The
_Pashupata_  should never be released at lesser enemies or else it may destroy
the creation. This weapon is discharged by the mind, eyes, words, or a bow. No
one in the three worlds of moving or nonmoving creatures can withstand its
force.”

Arjuna then bathed for purification and stood before Shiva to receive the
mantras. The god gave his weapon to Arjuna and it then waited upon him just as
it waited upon Lord Shiva himself. When the celestials saw the fearful weapon
standing in its embodied form by Arjuna’s side, the earth trembled and
terrible winds blew in all directions. Thousands of conches and trumpets were
heard resounding in the sky. This was such a divine weapon that its possession
made Arjuna invincible.

