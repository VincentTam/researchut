{
    "title": "Migrating from KDE3 to KDE 4.1",
    "date": "2008-07-31T07:51:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "KDE"
    ]
}

![](http://www.researchut.com/blog/images/kde41.png)

Yay!! First of all, **Thank you**.

I think now is the time most of the KDE users are going to migrate to **KDE
4.1**. KDE 4.1 looks so tempting that I really can't wait. So while I compose
this blog post, apt is doing all the needful to download the KDE4.1 packages
from [**Debian Experimental**](http://pkg-kde.alioth.debian.org). That doesn't
mean I wasn't following KDE 4.x. While, becuasae of resource constraints,
Debian made a wise choice of sticking to just a single KDE version (i.e. KDE3
in testing and KDE4 in unstable/experimental), there still are many ways to be
able to test KDE 4.x. And by _ways_ , I don't mean a VM or a Live CD. You can
definitely do a much better and realistic testing of KDE4 on your production
box without fearing of screwing your current KDE 3.x installation. At Debian
Wiki, there's a [cool howto](http://wiki.debian.org/Kde4schroot) describing
how to do it. If you follow the wiki howto properly, you should even be able
to test bling features like KWin's Composite and Plasma.

For me, I've been a native KDE user. I try to stick as much to KDE as
possible, for all my application needs. While my workplace depends on MS
Exchange for Calendaring, they're pretty open and have provided all the
standard access to exchange, i.e. **Webdav, IMAP, POP** et cetera. With
**kdepim** returning with KDE 4.1, I hoped I'd finally be able to do a
complete switch to KDE 4. But that wasn't the case when I did a KDE 4.x
testing during RC/Beta using the above mentioned cool howto. Thus this [KDE
Bugzilla](http://bugs.kde.org/show_bug.cgi?id=167167) entry. I was very happy
to hear that we'd soon be having an **akonadi native exchange resource**.

But still KDE 4.1 looks very tempting and I can't wait. So here's a small
howto on how to sync your exchange calendar while using KDE 4.1 (which doesn't
have any of the exchange resources)

  * On your KDE4 host, install an IMAP server. I use it for my personal mails that I sync to my hard drive. Using IMAP is good because that way I reliably store the Personal -  Contacts/Calendar/Birthdays/Journals/ToDo and more on my laptop 
  * Setup a Debian chroot of lenny. Follow the [**Cool HOWTO**](http://wiki.debian.org/Kde4schroot)
  * Install the kdepim suite into the lenny chrooted installation
  * Use **sux** on the host and fire up kontact. You should be able to see your chrooted kontact show up in the current X server desktop
  * Configure kmail to use the IMAP server. Make sure you **don't** subscribe any folder apart from INBOX which contains the PIM resource folders.
  * Under **Configuration = > Miscellaneous**, Enable **IMAP Groupware Resource Functionality**. Make sure to use the **KOLAB Format**. Resource folders should be in your local IMAP account.
  * Now sync your account. Done.
  * Now switch to Korganizer of the same chrooted installation. 
  * Go to its plugins page and enable **Exchange 2000 plugin**.
  * Now, first, add a resource. **Calendar of IMAP server**. Make it the **default** resource.
  * Now configure your Exchange 2000 Plugin.
  * Now Download your Calendar data. This data should now automatically be saved to the IMAP Calendar resource.
  * Just do a mail sync and quit from the chrooted environment.
  * ..........................
  * Now in KDE4 Host installation, fire KDE4's kdepim.
  * Do similar configs as above except, subscribe to all folders that you need.
  * For Korganizer, enable the **Calendar on IMAP Server resource**.
  * Bingo, You should be able to see your Exchange Calendar requests in KDE4.1 KDEPIM Calendar.
  * If all works, send me a Thank You email ![:-p](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_69.gif)

  
Some other things to keep in mind. If you are a Debian User like me, you'll
need to do some tweakings because the current KDE4 packages are modified to
store all settings in the **~/.kde4/** separately. With this, you'll end up
having **2 separate configs**. One option I think of is to create a **.kde4/
symlink to .kde/** . I believe that should work and inherit most of your
settings. I'm **yet to try** because my installation is still going on.

