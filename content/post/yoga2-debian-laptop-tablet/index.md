{
    "title": "Lenovo Yoga 2 13 running Debian with GNOME Converged Interface",
    "date": "2016-02-04T10:33:02-05:00",
    "lastmod": "2016-02-04T10:51:17-05:00",
    "draft": "false",
    "tags": [
        "debian",
        "gnome",
        "multitouch",
        "Touch",
        "gesture",
        "touchscreen"
    ],
    "categories": [
        "Debian-Blog"
    ],
    "url": "/blog/yoga2-debian-laptop-tablet"
}

I've wanted to blog about this for a while. So, though I'm terrible at
creating video reviews, I'm still going to do it, rather than procrastinate
every day.



In this video, the emphasis is on using Free Software (GNOME in particular)
tools, with which soon you should be able serve the needs for Desktop/Laptop,
and as well as a Tablet.

The video also touches a bit on Touchpad Gestures.



