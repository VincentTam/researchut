{
    "title": "Story Of Navratri",
    "date": "2016-01-10T12:05:27-05:00",
    "lastmod": "2016-01-10T12:05:27-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "hindu",
        "religion",
        "navratra"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/story-of-navratri"
}

![](/sites/default/files/jai-mata-di_thumb.jpg)The story of Navratra is the
symbolic message of the fact that however glorious and powerful the evil
become, at the end it is the goodness that wins over all of the evil. The
story is associated with Maa Durga and Mahisasura, the buffalo headed demon.



The story begins from the life of two sons of Danu called Rambha and Karambha
who performed austerities to gain extreme power and authority. When their
prayers became deeper and austerities became exceptional, the King of the
heaven God Indra got perturbed. Out of fear, he killed Karambha. Rambha, who
came to know about his brother's death, became more stubborn to win over the
Gods. He increased the intensity of his austerities and finally got several
boons from gods like great brilliance, beauty, invincibility in war. He also
asked a special wish of not being killed by either humans or Gods or Asuras.

He then considered himself immortal and started freely roaming in the garden
of Yaksha where he saw a female-buffalo and fell in love with her. To express
his love, Rambha disguised in the form of a male-buffalo and copulated with
the female buffalo. However, soon after that a real male buffalo discovered
Rambha mating with the she-buffalo and killed him. It was due to Rambha's
inflated ego that killed him, out of which he has not asked his death to be
spared from the wrath of animals. As the pyre of Rambha was organized, the
female-buffalo, who was copulated with him jumped into the funeral pyre of
Rambha to prove her love. She was pregnant at that time. Thus, demon came out
of the with the head of a buffalo and human body and he was named Mahishasura
(the buffalo headed demon).

Mahishasura was extremely powerful. He defeated the gods and the demons and
acquired power over the entire world. He even won over the heaven and threw
devas outside of it. He captured the throne of Indra and declared himself to
be the the lord of the gods. The gods led by Lord Brahma approached Lord
Vishnu and Lord Shiva and evaluated them of the situation. In order to save
the Gods, the three supreme deities emerged a light of anger, which combined
to the take the shape of a terrible form and this was Durga. All the gods then
granted this Goddess of power with all the supreme weapons they had. This is
why; Durga is called the brilliance of all the Gods.

When the goddess was seen by Mahishasura, he was mesmerized by her beauty. Her
then fell in love with her and proposed to marry her. The goddess said she
will marry him, if he defeated her in the battle. Then began a scary and
terrible battle between both of then which continued for nine days. Finally,
on the last day, Durga took the form of Chandika and stood over the chest of
Mahishasura and smashed him down with her foot. She then pierced his neck with
her spear and cut off his head off with her sword. It is the day when
Vijayadashmi is celebrated.



"Darbaar Tera Darbaaron Mein,  
Ik Khaas Ehmiyat Rakhta Hai…  
Usko Vaisa Mil Jata Hai,  
Jo Jaisi Niyat Rakhta Hai"

"Pyaara Saja Hai Tera Dwaar Bhawaani  
Badaa Nyaara Sajaa Hai Tera Dwaar Bhawaani  
Bhakhton Ki, Tere bhakton Ki Lagi Hai Kataar Bhawaani  
Badaa Pyaara Sajaa Hai Tera Dwaar Bhawaani"

 **" JAI MATA DI"**



 **Copyright (C) Vibhor Mahajan**

