{
    "title": "apt-offline 1.8.0 released",
    "date": "2017-05-21T17:17:37-04:00",
    "lastmod": "2017-05-27T23:29:25-04:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "apt-offline-gui",
        "python3",
        "pyqt5",
        "Offline APT Package Manager",
        "offline package manager"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Programming",
        "Tools"
    ],
    "url": "/blog/apt-offline-180"
}

I am pleased to announce the release of **apt-offline** , version **1.8.0**.
This release is mainly a forward port of apt-offline to Python 3 and PyQt5.
There are some glitches related to Python 3 and PyQt5, but overall the CLI
interface works fine. Other than the porting, there's also an important bug
fixed, related to memory leak when using the MIME library. And then there's
some updates to the documentation (user examples) based on feedback from
users.

Release is availabe from [Github](https://github.com/rickysarraf/apt-
offline/releases/tag/v1.8.0) and [Alioth](http://apt-
offline.alioth.debian.org)



![](/sites/default/files/Screenshot_from_2017-05-10_11-09-26.png)



![](/sites/default/files/Screenshot_from_2017-05-10_12-02-41.png)



What is apt-offline ?

    
    
    Description: offline APT package manager
    apt-offline is an Offline APT Package Manager.
    .
    apt-offline can fully update and upgrade an APT based distribution without
    connecting to the network, all of it transparent to APT.
    .
    apt-offline can be used to generate a signature on a machine (with no network).
    This signature contains all download information required for the APT database
    system. This signature file can be used on another machine connected to the
    internet (which need not be a Debian box and can even be running windows) to
    download the updates.
    The downloaded data will contain all updates in a format understood by APT and
    this data can be used by apt-offline to update the non-networked machine.
    .
    apt-offline can also fetch bug reports and make them available offline.
    
    

