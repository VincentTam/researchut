{
    "title": "apt-offline 1.8.1 released",
    "date": "2017-07-02T07:38:15-04:00",
    "lastmod": "2017-07-02T07:38:15-04:00",
    "draft": "false",
    "tags": [
        "apt",
        "apt-offline",
        "Offline APT Package Manager"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/apt-offline-181"
}

apt-offline 1.8.1 [released](https://github.com/rickysarraf/apt-
offline/releases/tag/v1.8.1).

This is a bug fix release fixing some python3 glitches related to module
imports. Recommended for all users.

apt-offline (1.8.1) unstable; urgency=medium

  * Switch setuptools to invoke py3  
  * No more argparse needed on py3  
  * Fix genui.sh based on comments from pyqt mailing list  
  * Bump version number to 1.8.1

 \-- Ritesh Raj Sarraf <rrs@debian.org>  Sat, 01 Jul 2017 21:39:24 +0545  


What is apt-offline

    
    
    Description: offline APT package manager
     apt-offline is an Offline APT Package Manager.
     .
     apt-offline can fully update and upgrade an APT based distribution without
     connecting to the network, all of it transparent to APT.
     .
     apt-offline can be used to generate a signature on a machine (with no network).
     This signature contains all download information required for the APT database
     system. This signature file can be used on another machine connected to the
     internet (which need not be a Debian box and can even be running windows) to
     download the updates.
     The downloaded data will contain all updates in a format understood by APT and
     this data can be used by apt-offline to update the non-networked machine.
     .
     apt-offline can also fetch bug reports and make them available offline.
    



