{
    "title": "Kalejee Daawat",
    "date": "2005-06-25T04:06:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "General",
        "Fun"
    ]
}

I wonder if Kalejee Daawat is something enjoyable only during the bachelor
days. Indeed, now, being in my bachelor days, I enjoy Kalejee Daawat a lot
with Ankit. Having a gulp of beer and then munching the spicy Kalejee in the
mouth reminds me of the great taste of North India.

Heartly thanks to Ankit and Nirmal for there awesome company and knowledge of
such exotic places in busy Bangalore.

