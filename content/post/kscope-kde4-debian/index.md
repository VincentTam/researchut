{
    "title": "Kscope 1.6",
    "date": "2012-04-07T13:46:59-04:00",
    "lastmod": "2014-11-12T06:01:32-05:00",
    "draft": "false",
    "tags": [
        "KDE",
        "kscope"
    ],
    "categories": [
        "Debian-Blog",
        "KDE",
        "Tools"
    ],
    "url": "/blog/kscope-kde4-debian"
}

In the 3.x days of KDE, there were some wonderful applications. One of them I
still admire, is kscope. Recently, I stumbled upon
[this](http://pl.atyp.us/wordpress/index.php/2010/01/they-killed-kscope/ "How
they killed kscope") blog entry and thought of sharing my living with kscope.

The move from KDE 3 to KDE 4 was a big one. During that move, the kscope
author decided to port kscope to a Qt only application. That is what we have
as the latest kscope, 1.9x version. But, on personal taste, it is not as good
as the 1.6x series.

With no viable replacement to my knowledge, making use of kscope 1.6x on
Debian (and Ubuntu) was the choice. Thanks to the way it is all packaged by
the KDE, it is easy.

Kscope depends on 2 packages for its functionlity: kdelibs4c2a and kate.
kdelibs4c2a is unsupported, but for the dire needs, you live with it. The
library can be easily pulled from the snapshot website, or just get the
package from the old distribution URLs. The same goes for kate and kscope
(1.6x)

Install the kdelibs4c2a package.



> 16:55:27 rrs@champaran:~$ apt-cache policy kdelibs4c2a

>

> kdelibs4c2a:

>

>   Installed: 4:3.5.10.dfsg.1-5

>

>   Candidate: 4:3.5.10.dfsg.1-5

>

>   Version table:

>

>  *** 4:3.5.10.dfsg.1-5 0

>

>         100 /var/lib/dpkg/status

You can't do the same with kate because the same package name is carried
forward. Simply unpack the old kate deb package and copy the following
libraries to /usr/local/:

> libkateinterfaces.so.0@     libkateutils.so.0@

>

> libkateinterfaces.so.0.0.0  libkateutils.so.0.0.0

Since the kscope package has a dependency defined on the kate pacakge, use
equivs to create a dummy package to satisfy kscope.

With that, it is all done. Just make sure to put your **kscope, kate  **and
**kdelibs4c2a** packages on hold.

For the eyes:

![kscope running on kde4 on debian](/sites/default/files/kscope-
kde4-debian.png)

