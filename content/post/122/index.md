{
    "title": "Professional Services",
    "date": "2005-07-04T04:47:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "General"
    ]
}

I'm available as a freelance consultant. Here's a
[list](http://www.researchut.com/services/ "Professional services offered.")
of services I offer. You can email me for details.

