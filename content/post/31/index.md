{
    "title": "The Taxi Wallah's Dream",
    "date": "2009-01-21T20:54:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Psyche"
    ]
}

It was about **2 AM** , some day in **June 2008** , when I caught the taxi
from [**Chinatown, Boston,
MA**](http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Chinatown,+Boston&sll=42.35106,-71.07029&sspn=0.023184,0.038624&g=Chinatown,+Boston&ie=UTF8&cd=1&ll=42.348173,-71.067123&spn=0.023185,0.038624&z=15&iwloc=addr)
to head to my hotel room in **[Waltham,
MA](http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Hilton,+Waltham,+MA&sll=42.389781,-71.239247&sspn=0.09268,0.154495&g=Waltham,+MA&ie=UTF8&ll=42.395921,-71.252689&spn=0.023168,0.038624&z=15&iwloc=A).**

It was the same day **Celtics** went victorious over **Lakers** in the
Basketball match. I was lucky to see the people cheering and celebrating the
victory. Everyone was shaking hands with others. I didn't feel that I was a
visitor there.

So as I said, I caught the taxi and was heading for my hotel in Waltham.
Suddenly, another taxi nearly hit the taxi I was in. My taxi driver and the
other taxi driver had a small conversation (yell really) with no outcome. The
other guy wasn't much interested in acknowledging his mistake. So anyway, we
proceeded. And the taxi driver, having been _pissed off_ of the just passed
incident, wasn't very happy. On the way, I started talking with him and
eventually it led to **politics**. I was interested to know what his opinion
was about the **Presidential candidates** ( **Barack H. Obama, John McCain**
).

He opined that **Barack** was a good contender and was very popular. But he
seemed more confident to say that Barack would **never** become the
**President** of the **United States**. (I won't get into the details on how
he backed his opinions with facts). He believed that **" Seeing Barack as the
President of the United States in like day dreaming"**.

But now that Barack Obama is the present of the United States, I'm sure that
the Taxi Wallah's **day dreams** have come true.

