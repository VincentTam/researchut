{
    "title": "Lord Vishnu – Dashavatharam (Part – I)",
    "date": "2016-02-25T06:25:39-05:00",
    "lastmod": "2016-02-25T06:25:39-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "vishnu"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/lord-vishnu-dashvatharam-1"
}

[![Visnu](/sites/default/files/Vibhor/visnu.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/visnu.jpg)

An avatara is an incarnation, which that a god adopts a human form to be born
on earth. The purpose is to destroy evil on earth and establish righteousness.
Lord Vishnu is regarded as the preserver of the universe and it is therefore
Lord Vishnu’s incarnations that one encounters most often. Lord Vishnu has
already had nine such incarnations and the tenth and final incarnation is due
in the future. These ten incarnations of Lord Vishnu are :-

**Matsya Avatara  (Fish Incarnation)**

[![MatsyaPurana_23159](/sites/default/files/Vibhor/matsyapurana_23159.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/matsyapurana_23159.jpg)

Many years ago, the whole world was destroyed. The destruction in fact
extended to all the three lokas (Worlds) of bhuloka (Earth), bhuvarloka
(Region between Earth and heaven) and svarloka (Heaven). All there worlds were
flooded with water. Vaivasvata Manu(Son of the sun-god) had spent ten thousand
years in prayers and tapasya (meditation) in the hermitage varrika, which was
on the banks of river Kritamala. Once Manu came to the river to perform his
ablutions. He immersed his hands in the water to get some water for his
ablutions. When he raised them, he found that there was a small fish swimming
in the water in the cup of his hands. Manu was about to throw the fish back
into the water then the fish said, “Don’t throw me back. I am scared of
alligators and crocodiles and  
big fishes. Save me.”

Manu found an earthen pot in which he could keep the fish. But soon the
fish,became too big for the pot and Manu had to find a larger vessel in which
the fish might be kept. But the fish became too big for this vessel as well
and Manu had to transfer the fish to a lake. But the fish grew and grew and
became too large for  
the lake. So Manu transferred the fish to the ocean. In the ocean, the fish
grew until it became gigantic. By now, Manu’s wonder knew no bounds. He
said,“Who are you? You must be the Lord Vishnu, I bow down before you. Tell
me, why are you tantalising me in the form of a fish?”

The fish replied, “I have to punish the evil and protect the good. Days from
now, the ocean will flood the entire world and all beings will be destroyed.
But since you have saved me, I will save you. When the world is flooded, a
boat will arrive here. Take the saptarishi’s (seven sages) with that boat.
Don’t forget to take the seeds of food grains with you. I will arrive and you
will then fasten the boat to my horn with a huge snake.” Saying this, the fish
disappeared.

Everything happened as the fish had promised it would. The ocean became
turbulent and Manu climbed into the boat. He tied the boat to the huge horn
that the fish had. He prayed to the fish and then fish related the Matsya
Purana to him. Eventually, when the water receded, the boat was anchored to
the topmost peak of the Himalayas and living beings were created once again.
An Asura (Demon) named Hayagriva had stolen the sacred texts of the Vedas. In
his form of a fish, Lord Vishnu also killed Hayagriva and recovered the Vedas.



## **Kurma Avatara  (Turtle Incarnation)**

[![kurma](/sites/default/files/Vibhor/kurma.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/kurma.jpg)

Many years ago there was a war between the devas (Gods) and the daityas
(Demons) and Gods lost this war. They prayed to Lord Vishnu to rescue them
from the oppression of the demons. Lord Vishnu told Lord Brahma and the other
gods that they should have a temporary truce with the demons. The two sides
should get together to churn the ocean. Lord Vishnu would ensure that the gods
would benefit more from this churning of the ocean than the demons. The truce
was agreed upon and the two sides got ready to churn the ocean. Mountain
Mandara was used as a churning rod and great snake Vasuki as the rope for
churning. Gods grasped Vasuki’s tail and demons grasped Vasuki’s head. But as
the churning began, the mountain Mandara which had no base, started to get
immersed in the ocean. Then Lord Vishnu came to the rescue. He adopted the
form of a turtle and the peak was balanced on the turtle’s back. As the
churning continued, terrible poison named Kalkuta emerged from the depths of
the ocean and was swallowed by Lord Shiva. Lord Shiva’s throat became blue
from this poison and he is therefore known as Neelkantha (Blue of throat).
Goddess Varuni (Goddess of wine) came out next, the gods readily accepted her.
She was followed by the Parijata tree, a beautiful tree that came to occupy
the pride of place in Lord Indira’s garden. A jewel named koustubha emerged
and was accepted by Lord Vishnu as his adornment. Three wonderful animals came
out next – the cow Kapila, the horse Ucchaishrava and the elephant Airavata.
They were followed by the Apsaras, who became the dancers of heaven. Goddess
Lakshmi or Shri came out next and was united with Lord Vishnu. Finally,
Dhanvantari (Originator of Ayurveda) emerged with a pot of amrita (the life
giving drink) in his hands. Demons led by Jambha gave half of the amrita to
gods and departed with the remaining half. But Lord Vishnu quickly adopted the
form of a beautiful woman. So beautiful was the woman that the demons were
charmed. “Pretty lady,” they said. “ Take the amrita and serve it to us. Marry
us.” Lord Vishnu accepted the amrita, but he had no intention of giving it to
the demons. He served it to the gods instead. There was only one demon named
Rahu who adopted the form of Chandra (Moon god), and succeeded in drinking
some of the amrita. Lord Vishnu there upon cut off Rahu’s head with his sword
but Rahu had drunk the amrita, so he could not die. He prayed to Lord Vishnu
and he granted him a boon that occasionally Rahu would be permitted to swallow
up the sun and moon (Solar and Lunar eclipse). Thus, gods defeated the demons
and regained heaven.



## Copyright (C) Vibhor Mahajan

