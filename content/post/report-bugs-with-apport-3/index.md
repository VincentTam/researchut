{
    "title": "Reporting bugs with Apport - III",
    "date": "2012-10-19T16:31:01-04:00",
    "lastmod": "2014-11-12T05:45:59-05:00",
    "draft": "false",
    "tags": [
        "apport"
    ],
    "categories": [
        "Debian-Pages",
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/report-bugs-with-apport-3"
}



Hello World. This is the follow-up to the last [2](http://blog/report-bugs-
with-apport) [updates](http://blog/report-bugs-with-apport-2) on the state of
apport in Debian.



A lot has changed since the last update on Apport. Currently, in Experimental,
we have version 2.6.1-2. With this version, and going forward, there will be
no hacks to make it work for Debian. Thanks to Martin Pitt, with his
assistance, Apport now has a very basic crashdb in place for Debian. The
Debian crashdb provides Apport the interface to interact with the Debian BTS.



This change is already upstream as part of the 2.6.1 release. So for Debian,
the packaging is a mere change of the crashdb from 'default' to 'debian'.
Being Just Another CrashDB inside Apport, it leverage full support of future
Apport releases, fixes and enhancements.



I would like to highlight some points, and some concerns, I have heard in my
previous blog posts.



  * [Direct email reports to the developer:](http://comment/960#comment-960) This was a concern from the last blog post. There will be no such pop-up anymore. It has been knocked off.
  * [Useless/Incomplete bug reports:](http://comment/934#comment-934) With no proper backtrace, it is worried that the bug report will be useless. Apport has intellignece to check if the backtrace is complete. If it is not, it will not report the bug.

[![Pop-up for Incomplete
backtrace](/sites/default/files/apport.jpeg)](https://lh4.googleusercontent.com/-uSpT56j_AhA/UH6ncujHnVI/AAAAAAAAB4E/nOsVpWG4ePY/s144/apport.jpeg)

  * [Opt-Out:](http://comment/935#comment-935) What if the maintainer is not interested in apport reports? The maintainer can ship a blacklist hook into /etc/apport/blacklist.d/. See /etc/apport/blacklist.d/README.blacklist for details.



