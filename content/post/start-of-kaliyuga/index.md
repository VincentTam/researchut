{
    "title": "End of Mahabharata and Start of Kali Yuga",
    "date": "2016-04-02T11:36:45-04:00",
    "lastmod": "2016-04-02T11:36:45-04:00",
    "draft": "false",
    "tags": [
        "hinduism"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/start-of-kaliyuga"
}

# End of Mahabharata and Start of Kali Yuga

Posted by [VIBHOR
MAHAJAN](https://vibhormahajan.wordpress.com/author/vibhormahajan/) on [MAY 9,
2012](https://vibhormahajan.wordpress.com/2012/05/09/end-of-mahabharata-and-
start-of-kali-yuga/)

_**“karmany evadhikaras te  
ma phalesu kadachana  
ma karma-phala-hetur bhur  
ma te sango ’stv akarmani”**  [(Bhagwat Gita: Chapter Two verse
47)](http://hellonetfriends.com/bhagwat_gita/Chapter_Two_Bhagwat_Gita_Verse_47.htm)_  
  
**“Sri Krishna said:** _  You have a right to perform your prescribed duty,
but you are not entitled to the fruits of action. Never consider yourself the
cause of the results of your activities, and never be attached to not doing
your duty.”_

[![mahabharata_23037](/sites/default/files/Vibhor/mahabharata_23037.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/mahabharata_23037.jpg)

Mahabharata was created and spoken by Vyaas jee, whereas scribed by Ganesh
jee. A glimpse of Mahabharata was shown in the Treta Yuga with a twenty three
days long battle between two of the greatest warriors of that time, that is,
Parshuram and Bhishma.

The actual story of Mahabharata started in the Dvapara Yuga also called as the
Third age of man. During the early days of the life of Draupadi, Vyaas jee
once visited her kingdom as a sadhu (sage). With her Dhai Ma (nurse-mother).
She visited the sadhu excited to know what her future holds. She knelt in
front of the sage, touching her head awkwardly to the ground. With a smile
glinted through the sage’s beard he said to darupadi –

_“You will marry the five greatest heroes of your time. You will be queen of
queens, envied even by goddesses. You will be a servant maid. You will be the
mistress of the most magical places and then lose it._

_But you will be remembered for causing the greatest war of your time. A
terrible war of Kurukshetra that will end the Third age of Man._

_You will bring about the deaths of evil kings – and your children’s, and your
brother’s. A million women will become widows because of you. But yes, indeed
you will leave a mark on history.”_

As the story of this epic unfolds, soon after the prediction by Vyaas ji, a
swayamwar was arranged for Draupadi. But before the wedding there was a test
of skill in which one had to pierce the eyes of a fish made of metal,
revolving high on the ceiling wall.

The Pandava’s and the Kaurava’s also came to the swayamwar to prove there
skills. It was Arjuna (Pandava Prince) who pierced the eye of the fish and
married Draupadi.

After marrying Draupadi, Pandava’s went to the place where they lived. Before
they were to enter there house, Bheem thought of playing a trick on there
mother, he said –“Ma, come see what we’ve brought home today.”

But without looking, Kunti (Pandava’s mother) replied – “Son, I can’t come
right now or the food will burn. But as always, whatever you brought should be
shared equally amongst all my sons.”

All through her(Kunti) life – even in the hardest of times – everything she
said was made sure to be done. She asked her sons to honour her words and all
five of them must marry Draupadi. And there forth she became the wife of five
husbands also known by the name “Panchaali”.

Later after some time The Pandava’s got a land named khandav from
dhritarashtra for themselves to build there kingdom. Maya who built palaces
for gods and asura’s built the most magnificent palace for them which they
named “The palace of Illusions”.

Pandava’s invited all the kings from Bharata to visit there palace. When the
kaurava’s prince Duryodhan visited them, dressed up all perfectly in a white
silk outfit, stepped into an illusionary bridge, floundering about in the
pool. Drapudai and her attendants burst into a pearl of laughter. One of the
attendants said “It seems the blind kings’s son is also blind.”

This insult of Duryodhan led to a game of dice against Yudhisthir, in which he
lost everything from his palace to himself and his brother’s and even his wife
Draupadi.

After he lost Draupadi, she was dragged trough the palace to the game room by
her hair and was take insulted by them. Before leaving the game room she said
– “I will not comb my hair untill the day i bathe in kaurava blood”

After the final game Pandava’s and Draupadi were banished to forests for
twelve years. During the course of twelve years they lived in forests and as
servants in other kings kingdoms

Once those twelve years were completed, Pandava’s raged for battle against the
Kaurva’s. The epic battle of Mahabharata was fought in the land of Kurukshetra
also to be called as “The war of Kurukshetra”

Shri Krishna was the charioteer of Arjuna, and it was before the war of
Kurukshetra, Shri Krishna explained Arjuna his duties as a warrior and prince
in the form of “Srimad Bhagavad Gita”.

After the long fought battle of the Kurukshetra, the Pandava’s defeated the
Kaurava’s. There was a pire of bodies, women jumping into fire with there
husband’s body, million women became widow and bodies were beyond
recognizable. But this was not the end of Third age of Man.

Kaurva’s mother (Gandhari) blamed Shri Krishna for the distruction of his sons
and ending the kaurva generation. She cursed him – “Because of what you have
done, your own clan will destroy itself in the span of one day. On that day,
your women will weep just as the women of Hastinapur are weeping now.”

After the span of few years a messenger (Yadu clan) from Dwarka (kingdom of
Shri Krishna) came to Hastinapur informing that – Overnight, Dwarka had turned
into a city of mourning, peopled – like Hastinapur after the war – with
children and widows. Balarama (Shri Krishna’s elder brother) was dead and no
one knew where Krishna was, but it was unlikely that he was alive.

After the death of his elder brother, Balarama, Shri Krishna gave up his body
using Yoga. He retired into the forest and started meditating under a tree.
The hunter Jara, mistook Krishna’s partly visible left foot for that of a
deer, and shot an arrow wounding and killing him mortally. Krishna’s soul then
ascended to heaven, while his mortal body was cremated by Arjuna. After
Krishna’s death it is known that the Kali Yuga has started.

After the death of Shri krishna, Pandava’s decided that it is time for all of
them also to die. Before going on there journey towards Heaven through the
pathway from Himalaya’s they gave over the kingship of Hastinapur to Parikshit
(Grandson of Arjuna and son of Abhimanyu).

Later, Parikshit was cursed by Shringi, the son of Rishi Shamik that – “Since
King Parikshit, out of arrogance of his powers, has put a dead snake on my
father’s neck, on the seventh day from now, Serpent king Takshak will bite
King Parikshit to death. The king will pay for his sins by this untimely
death.”

The death of Parikshit, was what Kali was waiting for. He had no fear now &
slowly, but surely engulfed the whole world with his negative influence. The
age of Kali Yuga had begun…..

