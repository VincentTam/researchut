{
    "title": "Sandhya and Kama (God of Love) – Shiva Puran",
    "date": "2016-01-11T05:12:03-05:00",
    "lastmod": "2016-01-11T05:12:03-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "shiv",
        "sati"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/sandhya-kama-shiv-puraan"
}

[![lord-brahma-hindu-god-of-creation](/sites/default/files/lord-brahma-hindu-
god-of-creation.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/lord-
brahma-hindu-god-of-creation.jpg)

**Birth of Sandhya and Kama**

Once Lord Brahma was holding his court when all of a sudden a beauteous female
popped out of his heart followed by a young man of incredible handsomeness who
had a bow and flower-head arrows in his quiver. The young man asked for some
specific duty. The creator, impressed by his beauty told him to romantically
charge all the creatures with flower arrows. Lord Brahma proclaimed that none,
not even a deity shall escape his arrows.  _Kama_ was the name young man got
formally but was also called  _Manmadha_ , the enchanter, because he bewitched
every mind anywhere.

[![url123](/sites/default/files/url123.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/url123.jpg)

To celebrate his empowerment Kama shot his flower arrows and everyone there in
Lord Brahma's court became infatuated and lustily ogled at the pretty female
who had preceded the young enchanter. The deity of righteousness (Dharma) did
not like this shameful scene, He prayed to Lord Shiva. Meanwhile, Lord Brahma
broke into sweat fearing rebuke from Lord Shiva. The sweat shed by him
produced  _64 pitriganas (progenitors)_  who come to be known as  _'
Agnisthomas'_.

[![Shiva](/sites/default/files/url.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/url.jpg)

The female that preceded Kama was called  _Sandhya_  being born of deep
concentration of Lord Brahma. In answer to the prayer of Dharma, Lord Shiva
materialised there and rebuked Lord Brahma for creating erotic scene in his
open court by empowering Kama. As Sandhya had happened to see Lord Shiva, she
became knower of Lord Shiva's mysticism by his grace. Sandhya was recognised
as the mother of Progenitors (Pitriganas)

Lord Brahma felt humiliated at being upbraided by Lord Shiva and as Kama had
caused that, he put a curse on latter to get burnt to ashes by the third eye
of Lord Shiva. It shocked Kama.

Trembling in fear Kama prayed to creator that he had acted at Lord Brahma's
own command. He was merely doing his duty, he claimed. There was some merit in
his claim but Lord Brahma reasoned that his empowerment as Kama was not meant
to create scenes of eroticism or orgy. Some discipline, moderation and modesty
was required. Although Lord Brahma admitted that the sexual urge could blind
anyone. Softening up, Lord Brahma said that Lord Shiva himself would find a
way to save him. As a divine coincidence, at the very same moment Daksha shed
sweat which transformed into a damsel of incredible beauty and grace.
Bewitching was she.  _Rati_  was her name, and at the very first sight Kama
fell in love with her passionately. Their union was solemnised by the express
will of Daksha and Lord Brahma.

 **Penance of Sandhya**

Having sighted Lord Shiva at the very time of her origin, Sandhya was
naturally motivated into making an intense penance to earn the grace of power
supreme. She was blaming herself for arousing the passions of males including
her brother (Kama) and father (Lord Brahma) in the court for that she had
taken a vow to burn herself to death.

Meanwhile, Daksha had given all his 27 daughters to Chandradeva in marriage.
They were all stars or constellations bearing the corresponding names like
Revati, Ashwani, Rohini etc. Chandra loved Rohini more than the others. This
discrimination was not liked by 26 others. They complained to Lord Brahma
about it. Lord Brahma took them to Sandhya who was engaged in her own penance
mission. At last Lord Shiva got propitiated and appeared to her and granted
her three boons as spelled out by her -

  1. Humans will become conscious of the sexual desire only after attaining the age of puberty, and not before that, and her husband not be a lecher.

  2. She should be rated as the greatest woman penance maker ever born.

  3. Anyone who casts an amorous or lustful glance at her, except her husband, shall turn into a eunuch.

[![Shiv
Sati](/sites/default/files/url4.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/url4.jpg)

After gaining the boons, she bowed to Lord Shiva. He advised her to go to the
bank of river Chandrabhaga where Sage Medhatidhi was holding a twelve year
long yajna to lift the curse put on Chandradeva by Daksha for subjecting his
daughters to injustice. In that holy fire Sandhya could offer her sacrifice
and redeem her vow. She would gain the husband whoever she thought of before
jumping, Lord Shiva revealed. By the grace of deity supreme she could do it
remaining invisible. In the holy fire pit of  _Jyotisthomayajna_ , Sandhya
jumped and out of flames emerged a girl having glowing body. Sage Medhatidhi
adopted her as his daughter and gave her the name  _' Arundhati'._

Later, she stayed with Guru Vashistha to gain the spiritual knowledge. She
adored him and got his image imprinted on her heart. Arundhati made it known
that she would accept only Vashistha as her husband. The sages, seers and Lord
Brahma made their union possible. In this way, Arundhati got a saintly
husband, she had prayed for to Lord Shiva. And she already was the greatest
penance maker and sacrificing woman as she had wished in her boon.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

