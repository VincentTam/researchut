{
    "title": "Boston, MA",
    "date": "2008-06-17T15:00:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "boston",
        "massachusetts"
    ],
    "categories": [
        "General"
    ]
}

I'm currently in [Boston,
MA](http://maps.yahoo.com/map?ard=1&q1=Boston%20MA#mvt=m&lat=42.358635&lon=-71.056699&zoom=14&q1=Boston%20MA)
to attend the [Red Hat Summit](http://www.redhat.com/promo/summit/2008/).

Boston is awesome. This city has beauty embedded to its roots. Historical,
Natural and Charming.

