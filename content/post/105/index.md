{
    "title": "What a lovely OS",
    "date": "2005-10-23T04:47:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages"
    ]
}

**_Really, What a lovely OS is Debian._**

I must admire its technical design. Recently I had to wipe out my hard drive
for some reasons. I had put a lot of effort into selection of packages and
configuration as per my taste.

And guess what, re-installation is a **cool breeze** in Debian. Every setting
and every package (from multiple sources) were back. I use a mixture of
**Testing + Unstable + Experimental + (Some other apt sources)** on my laptop.

  1. Backup your /home and /etc directory. I'll leave /etc/ upto you but /home/ is important. Right ? Also backup any non-standard path where you might have saved important data

  2.  **dpkg --get-selections > package-selection.txt**. This will make a database of the packages installed on your system. **This is important, don't forget to copy it to another reliable media**

  3. Also backup your /etc/apt/ folder. I back it up because that's where the core repository settings are.

  4. Wipe out your hard drive now.

  5. Do a new installation of Debian. Try to make it a bare minimum installation.

  6. Restore your /home back.

  7. Resotre your **/etc/apt/sources.list** , **/etc/apt/preferences** and other important ones that you might have.

  8. Obviously you'll do an ` **apt-get update** `

  9. Now the game to restore everything back as it was in the beginning. **`dpkg --set-selections <package-selection.txt`**

  10. That'll will do the rest of the job for you bringing back every package you had installed from different sources.

 _Just to note is that if you had custom built packages **(list Sun Java or
Pine or Qmail et cetera),** you'll have to re-build and re-install them
manually. If you also don't want to configure your system configurations you
might want to re-store your /etc/apt folder._

Hope this has helped you. Cheers!

