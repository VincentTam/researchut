{
    "title": "Chenchula’s Tale – Shiva Puran",
    "date": "2016-01-13T05:40:08-05:00",
    "lastmod": "2016-01-13T05:40:45-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/chenchulas-tale"
}

[![lord_shiva_by_mskumar-d365dpu](/sites/default/files/lord_shiva_by_mskumar-
d365dpu.jpg)](https://vibhormahajan.files.wordpress.com/2012/06
/lord_shiva_by_mskumar-d365dpu.jpg)

Once there lived a Brahmin called Binduga and his wife named Chenchula at a
place called Bashkala. Binduga fell to the fatal attraction of a woman of evil
ways and infamy. He could not go home or try to meet his wife. Chenchula felt
hurt and hard-pressed to make a living, she resorted to the oldest profession
as veil men were never far away.

Binduga learnt about scandalous act of his wife and returned home. He thrashed
her, but the unrepentant wife accused her husband of disloyalty and
dereliction forcing her into prostitution to stay alive.

A lengthy argument followed. At last an agreement was reached, according to
which each of them was to carry on one's evil ways. Chenchula would turn over
all the money she made to Binduga who could spend it on his own whores.
Chenchula always found customers who paid her money for sex. And Binduga
needed money. For the benefit of the world they continued to play husband-wife
roles.

In this way they lived and one day Chenchula became a widow. She continued
with her whoring way of life.

One day, she reached Gokarna, a holy place where recitation of Shiva Puran was
on in the Mahabaleshwara temple. Out of curiosity Chenchula heard some
snatches of it. She learnt that the sinners were condemmed to hell where
Yamaraja tormented them with horrific punishments. It frightened her. After
the recitation, Chenchula went to a Pauranic, the scholastic of Puran. She
confessed her sins to him. The scholastic advised her to hear the recitations
of Shiva Puran with her mind beamed into Lord Sublime. Chenchula faithfully
did so.

As a result she got salvaged in her death and her soul was taken to the domain
of Kailasha. There, Chenchula lived as a maid servant in the blissful company
of Ma Supreme, Gauri.

 **Chenchula Redeems Binduga Too**

Chenchula, one day relayed up a prayer to Ma Gauri revealing her desire to
know the whereabouts of the soul of her husband Binduga. She learnt that her
husband would suffer the tortures of hell for his sins. And currently he was
going through the woes of an unfulfilled spirit in the form of a wicked ogre
haunting Vindhya mountains. Chenchula prayed for his salvation from the ghost
life. The gracious Ma Supreme smiled and sent a gandharva named Tumbura with
her to the Vindhya's. Ma advised Tumbura to recite Shiva Puran to the
tormented soul to cleanse it of its sins. Accompanying Tumbura and Chenchula
also were two of the guards of Lord Shiva.

Tumbura was able to contact the evil spirit of Binduga on the Vindhya
mountain. But it would not hear Shiva Puran. Hence, the guards of Lord Shiva
ensnared it. Tumbura strummed his string instrument (Tumbura) and crooned the
glory of the great Lord Shiva. The sweet strains of melody wafted through the
air of the mountain side. Even angels and fairies descended to hear the
sanctifying melody revealing the glory and deeds of Lord Shiva.

it was time for the redemption of evil spirit of much sinned Binduga. The evil
got washed away and spotless soul shined out. Binduga had been salvaged.
Chenchula went back to Kailasha accompanied by her husband. There they lived
happily together ever after.

So efficacious was the grace of Lord Shiva.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

