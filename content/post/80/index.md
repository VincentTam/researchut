{
    "title": "root-tail on KDE desktop",
    "date": "2006-11-12T03:29:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools",
        "KDE"
    ]
}

For many who don't know, root-tail is a small utility which can write to your
Desktop's Screen (Often called the root window). It is mainly used to display
critical system messages.

I've been a root-tail fan for long. It like the idea of displaying syslog
messages on my desktop. It almost fixes 50% of my problems.

The problem with KDE is that it drwas its own Desktop image on top of the root
window because of which you can't see the messages drawn by root-tail. But KDE
is something I'm an addict of and can't live without. But recently I realised
that KDE already had the framework in place for applications like root-tail.

![](http://www.researchut.com/blog/images/root-tail.jpg)

 The Option " **Allow programs in desktop window** " is the option that needs
to be selected. And bingo, after that root-tail "Just Works".

