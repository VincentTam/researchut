{
    "title": "apt-offline 1.1",
    "date": "2011-04-08T07:36:50-04:00",
    "lastmod": "2013-01-31T13:33:34-05:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "apt-offline-gui",
        "Offline APT Package Manager"
    ],
    "categories": [
        "Debian-Pages",
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/apt-offline-advanced-options-gui"
}

I just completed the release of apt-offline version 1.1. This release has many
bug fixes, adds basic proxy support and has a new **Advanced Options** window
for the **Get** operation.

![apt offline advanced options](https://lh6.googleusercontent.com/__etqz-
yeP4s/TZ7xpWrqyeI/AAAAAAAABUU/fpZV_1oIn74/s640/apt-offline-advanced-
options.png)

