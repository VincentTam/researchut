{
    "title": "Lord Vishnu–Dashavatharam (Part – II)",
    "date": "2016-02-25T06:20:09-05:00",
    "lastmod": "2016-02-25T06:20:09-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "vishnu",
        "narasimha"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/lord-vishnu-dashvatharam-2"
}

## **Varaha Avatara  (Boar Incarnation)**

[![varaha_avatar](/sites/default/files/Vibhor/varaha_avatar.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/varaha_avatar.jpg)

  
Lord Vishnu’s third incarnation was in the form of a boar. Sage Kashyapa and
his wife Diti had a son named Hiranyaksha, who became the king of the asuras
(Demons). Hiranyaksha’s meditation pleased Lord Brahma and Lord Brahma granted
him the boon that he would be invincible in battle. Thus armed, Hiranyaksha
went out to fight with the devas (Gods). He comprehensively defeated the gods
and conquered heaven. He also defeated Lord Varuna (God of the ocean). Thus,
Hiranyaksha became the king of heaven, Earth and the underworld. But he was
not particularly fond of the earth and he himself had begun to live in Lord
Varuna’s palace under the ocean. So he hurled the earth into the depths of the
ocean. Gods went to Lord Vishnu and prayed that something should be done about
Hiranyaksha. They wished to be restored to heaven and they wished that Earth
should be brought back from the depths of the ocean. In response to these
prayers, Lord Vishnu adopted the form of a boar and entered the ocean.
Hiranyaksha did not know that this boar was none other than Lord Vishnu. He
thought that it was an ordinary boar and attacked it. The two fought for many
years. But finally, Hiranyaksha was gored to death by the boar’s tusks. The
boar raised the earth up once again with its tusks. Lord Vishnu thus saved the
gods and the principles of righteousness or dharma.

**4)  Narasimha Avatara (Incarnation in the form of half-man and half-lion)**

[![hindu-god-narasimha-photos](/sites/default/files/Vibhor/hindu-god-
narasimha-photos.jpg)](https://vibhormahajan.files.wordpress.com/2012/05
/hindu-god-narasimha-photos.jpg)

Hiranyaksha had a brother named Hiranyakashipu. Hiranyakashipu was furious to
know that his brother has been killed and pledged to kill Lord Vishnu. But
this could not be done unless he himself became powerful and invincible.
Therefore, he began to pray to Lord Brahma through difficult meditation. Lord
Brahma was pleased by his prayers and offered to grant a boon. Hiranyakashipu
said – “Please grant me the boon that I may not be killed at night or day, by
a man or a beast and that in the sky, the water or Earth.” Lord Brahma granted
the desired boon.

He went on to conquer all the three worlds. Hiranyakashipu had a son named
Prahlada, he was devoted to Lord Vishnu. Hiranyakashipu tried to persuade his
son but that did not work. He then tried to kill his son, but could not as
Lord Vishnu intervened to save Prahlada everytime. Gods had been driven off
from heaven by Hiranyakashipu. Gods went and prayed to Lord Vishnu, who
promised them that he would find a solution. One day, Hiranyakashipu called
Prahlada and asked – “How is it that you escaped each time I tried to kill
you?”Prahlada replied – “Because Lord Vishnu saved me, he is everywhere.”
“What do you mean everywhere?” retorted Hiranyakashipu. He pointed to a
crystal pillar inside the palace and asked – “Is Vishnu inside this pillar as
well?”

“Yes,”– replied Prahlada. “Very well, then I am going to kick the pillar,”
said Hiranyakashipu. When Hiranyakashipu kicked the pillar, it broke into two.
And from inside the pillar, Lord Vishnu emerged in his form of half-man and
half-lion. He caught hold of Hiranyakashipu and placed him across his thighs.
And with his claws, he tore apart his chest and killed him. Lord Brahma’s boon
had been that Hiranyakashipu would not be killed by a man or a beast. But then
Narasimha was neither a man nor a beast it was half-man and half beast. It
also said that the asura would not be killed in the sky, the water or Earth,
but Hiranyakashipu was killed on Lord Vishnu’s thighs. And finally, the boon
had promised that Hiranyakashipu would not be killed at night or day. Since
the incident took place in the evening, it was not night or day. After
Hiranyakashipu died, the gods were restored to their rightful places. Lord
Vishnu made Prahlada the king of asuras.





## Copyright (C) Vibhor Mahajan

