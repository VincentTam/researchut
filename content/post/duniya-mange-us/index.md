{
    "title": "Duniya Mange US, US mange us",
    "date": "2011-03-12T03:00:33-05:00",
    "lastmod": "2013-01-31T13:35:57-05:00",
    "draft": "false",
    "categories": [
        "Psyche"
    ],
    "url": "/articles/duniya-mange-us"
}

The following Cover Story was taken as it is from [Businnes World
Magazine](http://www.businessworldindia.com/) dated 5th January, 2004.  


## /* Begins

Why the Americans need Indians so desperately ?  

  
By Prosenjit Datta  
  

 Last evening, I received a singular lesson in Indo-American relations. And
why the two countries are coming closer despite minor irritants like Pakistan
and Iraq. The lesson was delivered by a young American who wished to remain
unnamed. The setting was one of those boring parties that features
businessmen, journalists and members of the diplomatic corps of various
countries. The immediate provocation that sparked off this most interesting
conversation was my stray remark -- naive as I see it now -- that America,
being the most owerful country in the world today, did not need anyone's help
to do whatever it wished. Or, at least, that Americans needed help from other
countries far less than the latter did from them.

  

       I was gently contradicted by this your American gentleman. "Oh, but you are so wrong," he said."Sure, we Americans do not need much help from other countries -- but we just cannot do without the help of India."

      

    I must confess that left me completely stumped. "Why does the US need India? Is is because we are both democracies and we both want to fight terrorism?" I asked. "Of course not. There are far more important reasons than that," he remarked. "It is because of your sheer numbers. Why, even George Bush remarked to former ambassador Blackwill once that what he liked about India was that it had a 'billion people..... Isn't that something?' "

  

          "So that is the reason -- India is a big market for the Americans, is that it?" I asked. "Oh, you are not really an attractive market. There are dozens of other markets which have better potential," he said.

  
By now I was getting thoroughly puzzled. So I waited for him to explain
further.  
  

"You see, we need you at the time of birth," he said. "We are facing a
terrible shortage of nurses back home -- we need almost 500,000 of them in a
hurry and fewer and fewer people are joining the nursing courses in the US. It
is a terrible job -- low wages, bad working hours, not many prospects of going
up in life. And, therefore, we need nurses from India.

              
             "And then we need teachers," he continued. "Now you Indians produce vast hordes of graduates and post-graduates who have no jobs. They jump at the thought of becoming teachers in our public schools. Now not too many people in America want to become teachers in these schools, what with the shootings and the bad behaviour of our kids. So we are looking at Indians to fill up the positions in at least the worst public schools in our country."

  
    "Then there is research on all the subjects -- genetics, software, telecommunication, pharmaceuticals -- where we Americans want to maintain our lead in the world. But because of our primary education system, we just aren't producing enough brilliant people. But your IITs churn out brains by the thousands. And we need them to do our research for us."

  

                         By now he was warming up to the subject. And I was more than happy to listen to his logic. "We will also shortly need you for our armed forces. As Iraq and Afghanistan showed us, even with our technological superiority in warfare, we still end up losing some American lives in battle," he said.

  

"But India doesn't want to fight American wars. I thought our prime minister
had made that amply clear when the US asked for troops for Iraq," I argued.

  

"Oh, India can stay out if it wishes. But suppose we give out visas and green
cards to people who want to join the American armed forces, I am sure we will
get millions of Indians applying overnight. In fact, I have already proposed
the idea to some higher ups," he said somewhat smugly.

                  
                    "And then we need you people to man all those irate calls that we get from people who have bought American products. Why do you think so many of these call centre jobs are being shifted to India these days?" he said.

  
"I thought that was because of the cost differential," I pointed out.  
  

"That is only a small part. The real reason was that we were simply too sick
of fielding all those calls from cranky customers who think that just because
they have bought a product, it gives them the right to also expect service,"
he said.

  
I must confess I had never thought of that aspect.  
  

"And then we will need more doctors and nurses from India to look after our
ageing baby boomers. We will need Indians to run our retirement homes. And we
will need them to take care of hazardous things like flights to space," he
said.

  
"So what will the Americans do?" I asked.  
  
"Oh, we will continue to do the really important thing --- RUN THE WORLD."  


## Ends. */

