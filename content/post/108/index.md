{
    "title": "Putting Debian packages on hold -- The dpkg way",
    "date": "2005-10-02T09:57:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages"
    ]
}

At times, for people like me who use a heterogeneous set of packages, it
becomes necessary to put certain packages on "hold" so that nothing breaks.

The plain dpkg way of putting packages on hold is very very simple.  
I'll be taking a package named "foo" as an example. Here are the steps:

**pre >1) dpkg --get-selections foo > selections.txt

**  
This will give you the package and its state on your system. The output
written to selections.txt would be something like this:  
**pre >foo install

**

2) **Replace the text "install" with "hold" in the selections.txt file.**

**pre >3) dpkg --set-selections < selections.txt

**  
This will change the state of package "foo" from "install" to "hold".

That's it.

Please leave a comment if you have any suggestion.

