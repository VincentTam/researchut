{
    "title": "Knowledge Of The Mysticism Of Lord Shiva – Part I",
    "date": "2016-01-11T05:27:57-05:00",
    "lastmod": "2016-01-11T05:27:57-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "shiv"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/knowledge-mysticism-shiva"
}

[![IMG_1783](/sites/default/files/img_1783.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/img_1783.jpg)

Lord Shiva is the mystery of all mysteries. Knowledge of Lord Shiva's
mysticism is neither within the scope of Lord Brahma nor in that of Lord
Vishnu who created Lord Brahma. Only Lord Shiva knows his own mysticism in
totality.

At the time of last doom everything drowned in a mega deluge. Day or night did
not exist. The time froze. None of the five elements (Panchabhootas) survived
or their compound products. An endless darkness engulfed all and a blind
nature prevailed. The only survivor was Lord Shiva, a nameless reality,
without any form or shape, eternal, immortal, beyond evolution, one full of
peaceful happiness of divine tranquility, self-aglow and the soul of the
light.

This absolute divine force inspired coming into existence of a symbolic figure
to recreate the cosmos. The figure got the name 'Sadashiva' , i.e. Shiva
Eternal. Coupling with him was his generative power - Shakti, the two
inseparables like a 'word' and its 'meaning' are.  Implied are they, in each
other.

[![pbaab268_fiveheaded_sadashiva](/sites/default/files/pbaab268_fiveheaded_sadashiva.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/pbaab268_fiveheaded_sadashiva.jpg)

  
 **Birth of Lord Vishnu**

Five faced was Sadashiva while Shakti had one face. Lord Shiva really is
Divine Luminant. He willed for a place to live with Shakti alias Sakaleshwari,
namely Shiva-Domain, Kashi in an earlier creation. The two did not leave this
place even during the doom. Then, the two thought of creating another divinity
to delegate the responsibilities of certain and evolution. A rub of nectar on
the left side produced a divinity of incredible beauty and attributes. All
aglow was his body. He prayed to Lord Shiva to give him a name. Lord Shiva
answered -'Vishnu, the omnipresent' and gave him the divine insight. At the
command of Lord Shiva, the divinity named Lord Vishnu made penance for twelve
celestial years. Streams of water jetted out of his body. The space was filled
up with their vapours and all the sins and evils got washed away. In the sea
formed in this process Lord Vishnu rested asleep tired of penance making. That
earned him the name 'Narayana' literally meaning 'Water asleep'. He generated
the creative elements like nature and the five elements (Panchabhootas) after
the three basic tendencies and ego. In all 24 elements were created. The
sensory organs and functional parts of the body followed. Thus by grace of
Lord Shiva, his willed one 'Vishnu' became the initiator of the new creation
before going back to sleep by his grace.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

