{
    "title": "Debian Developer",
    "date": "2010-05-05T07:02:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages"
    ]
}

Finally, I am proud to be officially part of one of the most successful and
well organized Free/Open project, the [Debian](http://www.debian.org) project.

I would like to thank everyone who have worked with me. I would also like to
thank my Advocate Giridhar and my AM Pablo. It was and is really awesome
working with you guys.

