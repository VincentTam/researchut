{
    "title": "About Me",
    "date": "2011-03-12T03:46:28-05:00",
    "lastmod": "2018-12-21T22:37:25+05:30",
    "draft": "false",
    "tags": [
        "about-me"
    ],
    "categories": [
        "General"
    ],
    "url": "/post/about-me"
}

I'm very grateful to the [Free Software Eco-System
Movement](http://www.gnu.org/philosophy/free-sw.html) that allowed access to
everyone to learn. I got involved with Debian as a user in the early 2000. I
became an official [Debian Developer](http://people.debian.org/%7Errs/) in
2010. Debian's [processes](http://www.debian.org/doc/debian-policy/ "Debian
Policy") and [structure](http://www.debian.org/devel/constitution "Debian
Constitution") still amazes me and I'm very proud to be part of this project.

![Me](/images/profile.jpeg)

My contributions to the Debian project can be seen
[here](https://contributors.debian.org/contributor/rrs@debian/). I wrote [apt-
offline](https://github.com/rickysarraf/apt-offline) (APT Offline Package Manager)
when stuck with real-world IT policies. I currently maintain [laptop-mode-
tools](https://github.com/rickysarraf/laptop-mode-tools), a tool that can assist your linux
kernel in conserving more power.

