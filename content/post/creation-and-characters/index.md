{
    "title": "Creation and Characters – Shiva Puran",
    "date": "2016-01-11T05:19:00-05:00",
    "lastmod": "2016-01-11T05:19:00-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "ardhnarishwar",
        "shiv",
        "sati"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/creation-and-characters"
}

[![shiva_wallpaper_12](/sites/default/files/shiva_wallpaper_12.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/shiva_wallpaper_12.jpg)

**Cosmic Creation**

After manifesting from Lingam of luminosity and enlightening Lord Brahma and
Lord Vishnu, Lord Shiva withdrew into his formless infinity. Lord Brahma
sought guidance from Lord Vishnu about creation. After briefing Lord Brahma,
Lord Vishnu went to live in the outer region of space making it his own
domain.

For the purpose of creation, Lord Brahma invoked Lord Shiva and Lord Vishnu.
Then, he threw up a handful of water which transformed into a great egg of
cosmic dimension that was a manifestation of 24 elements. But it was inert. He
prayed to beget life and consciousness for the cosmic egg. In reply Lord
Vishnu materialized there. He transformed into a shape having infinite
dimensions, thousand of heads, eyes and feet. In that form he entered into the
egg which became alive with that. Lord Shiva and Lord Vishnu built there own
domains inside.

Meanwhile Lord Brahma began to create. It began with three formats and five
elements, multiplied by their compounds. The eight were the primary tools of
creative exercise. The ninth format was  _kumara,_  which produced men of
spiritual wisdom, sages, holy persons and seers. Sanaka and Sananda were its
products. Lord Brahma wanted them to assist him in creative exercise, but they
made excuses and abandoned him. It hurt Lord Brahma.

Earlier, in the earth chapter of creation, ignorance and negative factors
(Tamas) were born followed by solid state living and non-living things like
mountains, plants, trees and other immovables. Beasts and birds came later
followed by humans at later stage. This part of creation immensely pleases and
satisfied Lord Brahma. But the betrayal of his own son-like Kumara products
was a great disappointment. To sad Lord Brahma, Lord Vishnu advised to seek
the grace of Lord Shiva. So, Lord Brahma made penance to propitiate Lord
Shiva.

 **Manifests Rudra**

As Prophesied earlier by Lord Shiva, from the central part of the forehead,
just above the eyes of Lord Brahma, Lord manifested  _Ardhanareeshwara  _(half
man - half woman figure) accompanied by a host of divine guards and servitors
called ganas. In this way the Rudra dimension of Lord Shiva materialized. It
was the destroyer aspect of Lord Shiva.

[![Ardhnarishwar](/sites/default/files/ardhnarishwar.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/ardhnarishwar.jpg)

Meanwhile, the creation of Lord Brahma was stagnating. No regeneration was
taking place, the creatures were just aging without dying or falling ill. Lord
Shiva had manifested in 'half man - half woman' form to convey the message
that regeneration was possible only through interaction between the two sexes.
He explained to Lord Brahma orally as well. Lord Brahma was advised to create
creatures in gender format and the species who would be subject to birth,
death and other calamities. So advising and duly blessing creator, lord Rudra
vanished.

 **Creation of Characters**

Freshly advised and empowered Lord Brahma began to create characters. By
mixing subjective elements of meaning, touch, taste, smell and beauty with
five objective elements of earth, fire, air and water plus compounds,
dimensions, statics, dynamics and calibration of time spans, Lord Brahma
shaped characters. Marichi was created from his eyes, heart produced Bhrigu,
Angirasa was born of his head, etc.

By the grace of Lord Shiva and inspiration provided by Lord Rudra, Lord Brahma
transformed half of his person into female anatomy. The interaction between
the  gender halves produced a male called Manu and a female in Shataroopa. the
two were paired by creator to beget two sons, Priyavrata and Uttanpada, and
three daughters Akooti, Devahooti and Prasooti. They were respectively coupled
with Ruchi, Kardama and Daksha.

Yajna and Dakshina were born to Akooti. Their pairing produced a dozen sons.
Devahooti and Prasooti begot large broods of daughters. Thirteen of them
namely Lakshmi, Shraddha, Dhriti, Tushti, Pusti, Medha, Kriya, Buddhi, Lajja,
Vasuva, Santhi, Siddhi and Kriti were all married to Dharma. The others;
Khyati, Neeti, Sambhuti, Samriti, Preeti, Kshama, Sannuti, Anuroopa, Voorja,
Swaha and Suddha were paired with Bhrigu, Dharma, Marichi, Angirasa,
Paulastya, Pulaha, Kratu, Atri, Vashistha, Agni and Poorvaja respectively. The
grooms were all either sages or deities. Those pairs gave birth to great many
brilliant characters and brightened up the world.

Sixty daughters Daksha had, out of which he gave thirteen to Kashyapa and one
'Sati' to Lord Shiva. Others went to various other legendary characters.
Kashyapa's spouses gave birth to broods that filled the earth world and
populated it in form of trees, plants and insects, devas, rakshasas, humans,
giants, kinnars, gandharvas, yakshas, men-o-snakes, demons, warriors, sages,
peers, holymen, kings, rishis, birds, mountains etc. That is why it is said,
the earth is kashyapised world.

Lord Shiva's consort Sati was manifestation of his latent power (Adi-Shakti)
called uma. Sati later reincarnated as Goddess Parvati when her Sati form got
immolated. Uma later manifested in several alternate or secondary forms. She
became Goddess Laxmi, Saraswati and Parvati to couple with Trinity members and
Goddess kaali to assist Lord Rudra.

Her secondary manifestations are : Goddess Durga, Bhagwati, Amba, Chamunda,
Jaya, Vijaya, Jayanti, Bhadrakaali, Kameshwari, Kaamada, Mridani and
Sarvamangla.

These primary legends remain the same through ages, only adding new episodes,
manifestations, villains and sub-legends with the passage of time.

The domain of Lord Shiva dominates all other domains. In his domain Supreme
Deity lives with his consort Power Latent (Adi-Shakti). Kailasha is his domain
that survives all dooms and remains resplendent as ever.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

