{
    "title": "Software Suspend 2 with root/swap on LVM",
    "date": "2006-07-07T06:17:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

_**Disclaimer: YMMV**_

I had always had failures with Software Suspend 2 (swsusp2) on my laptop
because of which I had a perception that it really wasn't ready for use for
beta users also. But recently after reading the comments of many users on LWN
I felt how wrong I actuall was.

Anyways, setting up swsusp2 with root/swap on LVM is a little tricky. From my
past experience  I've felt that to be the major reason for me to not able to
use this excellent feature.

We'll go straight.

[![](http://www.researchut.com/blog/pivotx/pics/icon_zip.gif)
](http://www.researchut.com/blog/images/config.gz.gz) the kernel config file
I've used for my kernel (2.6.17) with swsusp2. My setup is root/swap on LVM.

With this configuration you should have a kernel with swsusp2 working. What I
did find was that none of the present UI support swsusp2.

Hence I thought of writing a small utility which would be better for Desktop
users.

I also use a perl script which gets executed before swsusp2 is called. This
script was downloaded from LWN. This script basically does the job of freeing
up memory so that only the exact required data gets written to swap during
suspend. This script works both for swsusp and swsusp2 because technically it
has got nothing to do with software suspend.

rrs@learner:~ $ cat bin/swsusp.pl  
#!/usr/bin/perl -w  
use strict;  
use Time::HiRes qw(time);  
  
  
sub swapfree {  
open(PROC, "/proc/meminfo") or die "open: /proc/meminfo: $!";  
my ($swapfree) = grep(/^SwapFree:/, <PROC>);  
close(PROC);  
$swapfree =~ s/\D+//gos;  
print STDERR "swapfree=$swapfree\n";  
return $swapfree;  
}  
  
  
my $last_swapfree = swapfree;  
my @blobs;  
  
  
my $count = 0;  
my $total = 0;  
  
  
my $start_time = time;  
  
  
while ($last_swapfree <= (my $new_swapfree = swapfree)) {  
++$count;  
push(@blobs, ('.' x (1048576 * $count)));  
$total += $count;  
print STDERR "${total}M allocated\n";  
$last_swapfree = $new_swapfree;  
}  
system("ps m $$");  
print STDERR time - $start_time, " seconds\n";

rrs@learner:~ $ cat bin/my_hibernate.sh  
#!/bin/bash  
  
memory_free_perl()  
{  
        #First let's free up the memory.  
        echo -en "Executing the memory freeing perl script - swsusp.pl" | osd_cat --font '-misc-fixed-medium-r-semicondensed--*-240-*-*-c-*-*-*' \--colour=Green --shadow 1 --pos bottom --align center --offset 50 && ~/bin/swsusp.pl && echo -en "Execution completed - swsusp.pl" | osd_cat --font '-misc-fixed-medium-r-semicondensed--*-240-*-*-c-*-*-*' \--colour=Green --shadow 1 --pos bottom --align center --offset 50  
}  
  
suspend_to_disk()  
{  
  echo -en "Now starting Suspend2" | osd_cat --font '-misc-fixed-
medium-r-semicondensed--*-280-*-*-c-*-*-*' \--colour=Green --shadow 1 --pos
bottom --align center --offset 50 && sudo /usr/sbin/hibernate  
}  
  
suspend_to_ram()  
{  
  echo -en "Now starting Suspend2" | osd_cat --font '-misc-fixed-
medium-r-semicondensed--*-280-*-*-c-*-*-*' \--colour=Green --shadow 1 --pos
bottom --align center --offset 50 && sudo /usr/sbin/hibernate -F
/etc/hibernate/ram.conf  
}  
  
if [ $1 == "Disk" ]; then  
  
  # First free the memory  
  memory_free_perl  
  
  # Now Suspend to Disk  
  suspend_to_disk  
elif [ $1 == "RAM" ]; then  
  # First free the memory  
  memory_free_perl  
  
  # Now Suspend to RAM  
  suspend_to_ram  
elif [ $1 == "" ]; then  
  
  # Assume interactive invocation  
  memory_free_perl  
  
  # So suspend to disk  
  suspend_to_disk  
else  
  echo -en "Aborting!!! Invalid Suspend Task Given.\a" | osd_cat --font
'-misc-fixed-medium-r-semicondensed--*-280-*-*-c-*-*-*' \--colour=Green
--shadow 1 --pos bottom --align center --offset 50  
fi

rrs@learner:~ $ cat bin/my_hibernate_ui.sh  
#!/bin/bash  
  
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin/:/usr/local/sbin:  
  
DIALOG=`which dialog`;  
[ -n "$DISPLAY" ] && [ -x /usr/bin/Xdialog ] && DIALOG="Xdialog"  
set -vx  
which_suspend()  
{  
  Xdialog --radiolist "Suspend To ?" 10 25 2 "Disk" "Suspend" ON "RAM "
"Suspend" off 2>/tmp/susp_checklist.$$  
  retval=$?  
  choice=`cat /tmp/susp_checklist.$$`;  
  rm -f /tmp/susp_checklist.$$;  
  
  case $retval in  
    0)  
    ~/bin/my_hibernate.sh $choice;;  
    1)  
    echo -en "Cancel Pressed";;  
    255)  
    echo -en "Box Closed";;  
  esac  
}  
  
$DIALOG --wrap --title "Question"\  
  \--help "Contact Author Ritesh Raj Sarraf\nEmail: rrs@researchut.com"\  
  \--yesno "Do you want to execute the Suspend2 Script ?" 0 0  
  
case $? in  
  0)  
  which_suspend;;  
  255)  
  $DIALOG --wrap --title "Aborted" \--infobox "Cancelled"  
esac  


All these scripts together allow you to have a good UI for swsusp2 (They use
xosd, Xdialog et cetera)

After copying these 3 scripts to ~/bin/ you can add a shortcut on your Desktop
for my_hibernate_ui.sh which itself calls my_hibernate.sh and swsusp.pl

Let me know your comments and suggestions.

Cheers!

rrs

