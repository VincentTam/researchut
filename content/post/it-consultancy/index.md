{
    "title": "RESEARCHUT IT Consultancy",
    "date": "2016-10-20T13:10:23-04:00",
    "lastmod": "2016-12-03T05:48:55-05:00",
    "draft": "false",
    "url": "/it-consultancy"
}

I am available as a Freelancer providing Consultancy and AMCs. Support offering
includes specialization in **Free Software** ,  **GNU/Linux, and other Unix
like operating system** based solutions, providing **customized consulting** to
small and medium size businesses.

  * Integration and Interoperability with foreign products
  * Network Solutions: Web, Client/Server
  * Security Audit and Assessment
  * Server Maintenance
  * Long Term Support - Maintenance and Upgradation
  * Custom Solution

