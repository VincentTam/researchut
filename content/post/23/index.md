{
    "title": "Compressing Backups",
    "date": "2009-04-04T06:41:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "bzip2",
        "compression",
        "backups"
    ],
    "categories": [
        "Debian-Pages",
        "Tools",
        "Computing"
    ]
}

Once upon a time CPU power was low. In those days, what we have today, was
termed to be Super Computers.

Thanks to tough competition and great engineering, we now have CPU in the
range of Gigahertz and multiple cores. But on  Destkops/Laptops, do we really
have applications that utilize the ability of these processors ?

So I thought about making these powerful CPUs to do some work.

There are different views about Backups. My preference has always been that my
backup should be an identical image of my entire OS. That'd include the cache,
the packages installed, my personal data, my mail spool et cetera. Given the
requirements, my preferred choice of Backups has been [LVM
Snapshots](http://www.researchut.com/blog/archive/2009/03/04/Backups__Recovery).

And now to add to that is this

> rrs@champaran:~$ dd if=debian-500-amd64-i386-powerpc-netinst.iso | bzip2 -v
> /tmp/bzip.test  
>

>   (stdin): 965192+0 records in  
>  965192+0 records out  
>  494178304 bytes (494 MB) copied, 291.708 s, 1.7 MB/s  
>   1.058:1,  7.559 bits/byte,  5.51% saved, 494178304 in, 466933711 out.  
>  rrs@champaran:~$ du -h debian-500-amd64-i386-powerpc-netinst.iso
/tmp/bzip.test  
>  472M    debian-500-amd64-i386-powerpc-netinst.iso  
>  446M    /tmp/bzip.test  
>  rrs@champaran:~$ dd if=/tmp/bzip.test | bunzip2 -dc | dd
of=/tmp/bzip.uncompress  
>  911979+1 records in  
>  911979+1 records out  
>  466933711 bytes (467 MB) copied, 135.861 s, 3.4 MB/s  
>  965192+0 records in  
>  965192+0 records out  
>  494178304 bytes (494 MB) copied, 136.115 s, 3.6 MB/s  
>  rrs@champaran:~$ du -h debian-500-amd64-i386-powerpc-netinst.iso
/tmp/bzip.uncompress  
>  472M    debian-500-amd64-i386-powerpc-netinst.iso  
>  472M    /tmp/bzip.uncompress  
>  rrs@champaran:~$ md5sum  
>  md5sum            md5sum.textutils  
>  rrs@champaran:~$ md5sum debian-500-amd64-i386-powerpc-netinst.iso  
>  6c8cdaeaff65741a6fd37366a1ecc1b0  debian-500-amd64-i386-powerpc-netinst.iso  
>  rrs@champaran:~$ md5sum /tmp/bzip.uncompress  
>  6c8cdaeaff65741a6fd37366a1ecc1b0  /tmp/bzip.uncompress  
>

  
Now, the thing I need to verify is is wether D-I ships bzip2 utils on the CD.

