{
    "title": "iprint - Andrew \"Tridge\" Tridgell's iprint in Python",
    "date": "2006-02-05T20:33:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Programming"
    ]
}

Following code does the same thing that Tridge's iprint.c does.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

**#!/usr/bin/env python  
**

****

**import sys**

 ** "iprint - Andrew \"Tridge\" Tridgell's iprint in Python"**

 ****

**def toBinary(dec):  
"""  
This function does the coversion from integer to binary  
"""  
    bin = ''  
    while dec:  
        bin = (dec % 2 and '1' or '0') + bin  
        dec = long(dec/2)  
    return bin or '0'**

 ****



 **x = 1  
text = ''  
while len(sys.argv)  > 1 and x < len(sys.argv):  
    text += sys.argv[x]  
    x += 1**

 ****



 **for x in text:  
    print ord(x), "\t", hex(ord(x)), "\t", oct(ord(x)), "\t", toBinary(ord(x)), "\t", x  
    print "\n"**

