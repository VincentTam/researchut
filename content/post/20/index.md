{
    "title": "Father",
    "date": "2009-09-15T19:37:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "Dad",
        "Father",
        "Loss"
    ],
    "categories": [
        "General",
        "Psyche"
    ]
}

_" Be kind to thy father, for when thou wert young, Who loved thee so fondly
as he? He caught the first accents that fell from thy tongue, And joined in
thy innocent glee." \-- Margaret Courtney_





I miss you Dad.  

