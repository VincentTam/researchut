{
    "title": "The beauty of blogging",
    "date": "2005-10-02T10:24:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "General"
    ]
}

Today, I'm quite happily influenced by blogging. Different people have
different ways of defining and using this new trend.

For me, it was always difficult to store small documents for system
configuration and tips for making life simpler with computers. Earlier I ended
up with having multiple documents in my machine.

With blogging, I can dump all my small documents in my blog and have it
readily available when and where required. The other beauty is that it is well
open for others who could find it useful.

