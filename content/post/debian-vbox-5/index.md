{
    "title": "VirtualBox 5.x",
    "date": "2015-07-15T08:37:33-04:00",
    "lastmod": "2015-07-15T08:37:33-04:00",
    "draft": "false",
    "tags": [
        "VirtualBox"
    ],
    "categories": [
        "Debian-Blog",
        "Virtualization"
    ],
    "url": "/blog/debian-vbox-5"
}

We just pushed VirtualBox 5.0.0 into Debian Experimental. It should land up on
your mirrors very soon.

Since this is a major release, we are looking for some testing and feedback.
If you use VBox, or have your business depend on it, please give it a test.
For details about what is new in 5.x, please check out the [release
announcement](http://www.oracle.com/corporate/pressrelease/oracle-vm-
virtualbox-5-070915.html)

