{
    "title": "apt-offline 1.6",
    "date": "2015-01-14T05:32:07-05:00",
    "lastmod": "2015-01-14T05:32:07-05:00",
    "draft": "false",
    "tags": [
        "apt-offline"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/apt-offline-16"
}

I am pleased to announce the release of apt-offline - 1.6

This release is mostly a bug fix release, which every user should upgrade to.
It also fixes a major bug in the way we limited the validation of GPG
integrity, for the APT repository lists ( **Thank you Paul Wise** ).

Also, In the last release,  we migrated from custom magic library to the
python shipped ctype python-magic library. That allowed some bugs to creep,
and hopefully now, all those bugs should be fixed. A big thanks to **Roland
Summers** for his bug reports and continuous feedback.

 _ **What is apt-offline ?**_

    
    
    Description-en: offline APT package manager
     apt-offline is an Offline APT Package Manager.
     .
     apt-offline can fully update and upgrade an APT based distribution without
     connecting to the network, all of it transparent to APT.
     .
     apt-offline can be used to generate a signature on a machine (with no network).
     This signature contains all download information required for the APT database
     system. This signature file can be used on another machine connected to the
     internet (which need not be a Debian box and can even be running windows) to
     download the updates.
     The downloaded data will contain all updates in a format understood by APT and
     this data can be used by apt-offline to update the non-networked machine.
     .
     apt-offline can also fetch bug reports and make them available offline.
    

Debian changelog for the 1.6 release.

    
    
    apt-offline (1.6) experimental; urgency=medium
    
      * [2a4a7f1] Don't abuse exception handlers.
        Thanks to R-Sommer
      * [afc51b3] MIME type for a deb package.
        Thanks to R-Sommer
      * [ec2d539] Also include debian-archive-keyring.
        Thanks to Hans-Christoph Steiner (Closes: #748082)
      * [dc602ac] Update MIME type for .gpg
      * [c4f9b71] Cycle through possible apt keyrings.
        Thanks to Paul Wise (Closes: #747163)
      * [de0fe4d] Clarify manpage for install
      * [b5e1075] Update manpage with some doc about argparse positional
        values to arguments
      * [c22d64d] Port is data type integer.
        Thanks to Roland Sommer
      * [67edebe] autodetect release name
      * [5803141] Disable python-apt support
    
     -- Ritesh Raj Sarraf <rrs@debian.org>  Wed, 14 Jan 2015 15:34:45 +0530
    

[1] <https://alioth.debian.org/projects/apt-offline/>

