{
    "title": "Designed for Linux",
    "date": "2008-11-25T12:27:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Fun",
        "Technology"
    ]
}

A couple of years back (or maybe still), it was difficult to find Device
Support in Linux from the Hardware Vendor.

Recently I was at home on a small vacation. There something surprised me. I
bought a device which had a logo saying, "Designend for Linux". Cheers!

![](http://www.researchut.com/blog/images/dsc02032.jpg)

