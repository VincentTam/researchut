{
    "title": "Python Fun",
    "date": "2005-09-19T09:42:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Fun",
        "Programming"
    ]
}

Just think what all this small piece of code can do
![:-p](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_69.gif)

while x <= 100:  
… temp = urllib2.urlopen(address+prefix+str(x)+suffix)  
… data = open(prefix+str(x)+suffix, 'wb' )  
… data.write(temp.read())  
… data.close()  
… temp.close()  
… x +=1

