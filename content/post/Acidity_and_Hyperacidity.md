{
    "title": "Acidity and Hyperacidity",
    "date": "2018-11-14T18:40:06+05:30",
    "lastmod": "2019-01-02T11:40:06+05:30",
    "draft": "false",
    "tags": [
        "Ayurveda",
        "Acidity",
        "Hyperacidity",
        "Flatulence"
    ],
    "categories": [
        "Ayurveda",
        "General"
    ],
    "url": "/post/Acidity_and_Hyperacidity"
}

# Acidity and Hyperacidity

**Acidity** refers to the production of excessive hydrochloric acid from the gastric cells. Due to overproduction of the acid there is feeling of heartburn in the region of chest and a person may have sour eructation. There are different remedies available in the market for acidity and hyperacidity. Natural cure for acid reflux includes natural remedies that give quick relief from heartburn and sour eructation. It is easy to find acid reflux natural treatment. There are different natural remedies for the treatment of acid reflux.

There are different causes of acidity and hyperacidity and if a person makes some changes in lifestyle he can easily get rid of acid reflux symptoms naturally. Natural cure for acid reflux includes herbal remedies that stop the secretion of acid from the gastric cells and give quick relief. Acid reflux natural treatment may include herbal remedies and home remedies. People can get relief from acid reflux by avoiding certain foods. In this article a list of foods to avoid in acid reflux are given which may also help in acid reflux natural treatment.


## What are the symptoms of acidity/hyperacidity?

The main symptoms associated with acidity and hyperacidity is given below. A person having these symptoms may suffer from acid reflux or hyperacidity:

* There is continuous burning in the chest.
* There is sour eructation
* Pain in the abdomen
* Nausea
* Vomiting
* Feeling of uneasiness
* Tiredness
* Pain in the chest
* Feeling of tightness in the chest
* Decreased appetite


## What are the causes of Acidity/Hyperacidity?

There are different causes of acidity in different individuals. Some of the common causes of acidity/hyperacidity are as follow.

 * Diet: Diet is the main causes of acidity. Diet rich in spicy and fried foods is one of the major sources of hyperacidity. People are in a habit of eating too much spicy or oily food often complains of acidity. They quickly develop the symptoms of acidity such as pain in the chest, sour eructation, nausea, vomiting, etc.

 * Stress: This is another important cause of acidity. People who remain under mental stress often suffer from acidity due to imbalance of the endocrine hormones. There is increases level of stress hormones in the blood released at the time of mental stress which stimulate the gastric cells to secrete excessive amount of hydrochloric acid leading to hyperacidity.

 * Sedentary life: Many people lead a sedentary life. They do not perform any physical activity in life due to which they suffer from digestive problems such as hyperacidity and acidity.

 * Loss of sleep: Proper sleep is very essential for normal functioning of all the body organs. People who suffer from loss of sleep may also suffer from acidity. It is one of the major causes of acidity in people who sleep late at night or work in night shifts.

 * Anger: It is also a great cause for acidity. People who show recurrent episodes of anger may suffer from episodes of acidity and hyperacidity. Anger leads to the hyper secretion of acid from the gastric cells, thus leading to hyperacidity.

 * Improper meals: People who do not have a fixed time for meals also suffer from digestive ailments such as acidity and hyperacidity. Generally, people who eat their dinner late at night suffer from acidity because our stomach needs at least two hours for proper digestion and people who eat late at night do not get proper time for digestion of food.

 * Increased intake of prescription drugs: Sometimes people have to take prescription drugs due to systemic diseases and this leads to acidity. They take antacids that produce further problems. Therefore, it is best to take natural remedies for acid reflux.


## Home remedies for acidity

There are many home remedies for acidity. One may try these home remedies to get relief from symptoms of acid reflux. Some useful home remedies are given below:

 1. Drink lots of water every day to wash out the chemical substances from the body. Water helps in detoxification of the body and gives relief from acidity.
 2. Banana is a useful remedy for acidity. People suffering from acidity should eat one banana everyday to get rid of acidity and associated symptoms.
 3. Cold milk also helps in balancing the pH of the stomach and give relief from gastric disturbances such as acidity and heartburn.
 4. People suffering from acidity should clean the tongue and brush before going to sleep. This is a great home remedy to get rid of acidity.
 5. Aniseeds are found to be very helpful in the treatment of chronic acidity. You may crush aniseeds and make into powder. Take half tea spoon of this powder everyday with water to prevent acidity and hyperacidity.


## Foods to avoid in acid reflux

People suffering from acidity or acid reflux should avoid the following food items. A list of foods to avoid in acid reflux is given here:

 * One should avoid eating too much spicy and fried food as it is the main reason of acidity in most of the people. Light food including balanced nutrients should be eaten to get relief from acidity.
 * Excessive tea and coffee should also be avoided to get quick relief from long standing acidity and hyperacidity.
 * Alcohol should be avoided by the people who suffer from chronic acidity.
 * Cigarette smoking should be totally avoided in acidity.
 * Sour things such as lemon juice and other such drinks should be avoided as these may produce more acid secretion.


## Treatment

 Duration: 30 Days

 1. Divya Avipattikar Churna - 100 grams
 2. Divya Mukta Shukti Bhasma - 20 grams
 
 Way of Administration: Mix both these medicines together & take half an hour before or after breakfast & dinner.
 ½ teaspoon twice daily

## In case of Chronic and Hyper Acidity
 
 Duration: 30 days
 
 1. Divya Mukta Pishti - 4 grams
 2. Divya Kamdudha Ras - 20 grams
 3. Divya Mukta Shukti Bhasma - 10 grams
 
 Way of Administration: Mix all these three medicines together divide into 60 part and take each twice/thrice a day, i.e. one hour before breakfast, lunch & dinner either with honey or fresh water
 
 Notes: Avoid any and every consumable item that can cause Flatulence. Avoid spicy and fried items.
 
