{
    "title": "Lord Vishnu – Dashavatharam (Part – IV)",
    "date": "2016-02-25T05:59:21-05:00",
    "lastmod": "2016-02-25T05:59:21-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "hinduism"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/lord-vishnu-dashvatharam-4"
}

# Lord Vishnu – Dashavatharam (Part – IV)



[![OO2o4kTgOElXZx88CZ0u8-wlTqYK9XFcSRzr4aj zjy8pJ-uh7v5zu2
9p3gzt5A](/sites/default/files/Vibhor/oo2o4ktgoelxzx88cz0u8
-wltqyk9xfcsrzr4ajzjy8pj-
uh7v5zu29p3gzt5a.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/oo2o4ktgoelxzx88cz0u8
-wltqyk9xfcsrzr4ajzjy8pj-uh7v5zu29p3gzt5a.jpg)

**“JAI SHRI RAM”**  
”जय श्रीराम”

Lord Brahma came out of Lord Vishnu’s navel. Lord Brahma’s son was Marichi,
Marichi’s son Kashyapa, Kashyapa’s son Surya, Surya’s son Vaivasvata Manu,
Manu’s son Ikshvaku, Ikskhvakku’s son Kakutstha, Kakutstha’s son Raghu,
Raghu’s son Aja, Aja’s son Dasharatha, Dasharatha’s sons were Rama, Bharata,
Lakshmana and Shatrughna. Since Lord Rama was descended from Kakutstha and
Raghu, he was also called Kakutstha and Raghava. Since his father’s name was
Dasharatha, he was also called Dasharathi. Lord Rama’s story belongs to the
solar line (surya vansha), since one of his ancestors was Surya. Lord Vishnu
himself wished to destroy Ravana and the other rakshasas (demons). He
therefore divided himself into four parts and was born as Rama, Bharata,
Lakshmana and Shatrughna. Rama was Koushalya’s son, Bharata was Kaikeyi’s,
Lakshmana and Shartrughna were the sons of Sumitra.

Once, sage Vishvamitra came to Dasharatha and pleaded for Rama’s help in
defeating the rakshasas who were disturbing his yajanas. Lord Rama killed
these demons and Vishvamitra was so pleased that he taught Rama the use of all
divine weapons. Rama broke a bow of Lord Shiva that had been in the possession
of the king of Mithila, Janaka. This was the task that had been appointed for
marrying Sita, Janaka’s daughter. Lord Rama married Sita, Lakshmana married
Urmila, Bharata married Mandavi and Shatrughna married Shrutakirti.

As Lord Rama was the eldest of four sons and was to become king when his
father retired from ruling. His stepmother Kaikeyi, however, wanted to see her
son Bharata, become king. Remembering that the king had once promised to grant
her any two wishes she desired, she demanded that Rama be banished to forest
for fourteen years and Bharata be crowned as the king. The king had to keep
his word to his wife and ordered Lord Rama’s banishment. Rama accepted the
decree unquestioningly. “I gladly obey father’s command,” he said to his
stepmother.

When Sita, heard that Lord Rama was to be banished, she begged to accompany
him to his forest retreat. “As shadow to substance, so wife to husband,” she
reminded Lord Rama. “Is not the wife’s dharma to be at her husband’s side? Let
me walk ahead of you so that I may smooth the path for your feet,” she
pleaded. Lord Rama agreed, and Rama, Sita and his brother Lakshmana all went
to the forest.

When Bharata learned what his mother had done, he sought Lord Rama in the
forest. “The eldest must rule,” he reminded Lord Rama. “Please come back and
claim your rightful place as king.” Lord Rama refused to go against his
father’s command, so Bharata took his brother’s sandals and said, “I shall
place these sandals on the throne as symbols of your authority. I shall rule
only as regent in your place, and each day I shall put my offerings at the
feet of my Lord. When the fourteen years of banishment are over, I shall
joyously return the kingdom to you.” Lord Rama was very impressed with
Bharata’s selflessness. As Bharata left, Rama said to him, “I should have
known that you would renounce gladly what most men work lifetimes to learn to
give up.”

[![KishkindhaKanda_22484](/sites/default/files/Vibhor/kishkindhakanda_22484.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/kishkindhakanda_22484.jpg)

Lord Rama, Lakshmana and Sita lived in Dandaka forest. This forest was on the
banks of the river Godavari and there was a beautiful grove inside the forest
known as Panchavati. They built a hut there and resolved to live there.

One day a rakshasa princess named Shrupnakha tries to seduce Lord Rama, but
Lakshmana wounded her and drived her away. She returns to her brother Ravana,
the ten-headed ruler of Lanka, and tells her brother about Lord Rama and Sita.
Ravana asked demon Maricha to adopt the form of a golden deer and roam in
front of Lord Rama’s hut. Sita was so charmed by the deer that she asked Lord
Rama to capture it for her. Lord Rama took long time in returning,so Lakshmana
went to look for him. Taking advantage of Lord Rama and Lakshmana’s absence,
Ravana kidnapped Sita. Jatayu (King of the birds), did try to stop Ravana, but
he met his death at Ravana’s hand.

[![deer](/sites/default/files/Vibhor/deer.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/deer.jpg)

Lord Rama was broken-hearted when he returns to the empty hut and cannot find
Sita. In search of Sita, soon Ram and Laxman reached the foothills of
Rishyamuk mountains. Here the king Sugreev of the monkey clan was staying with
his ministers and friends. One of them was the mighty Lord Hanuman (Rudra
Avatar, Incarnation of Lord Shiva), the noblest devotee of Lord Rama.

On seeing the two foreigners coming to the mountain, Sugreeva asked Lord
Hanuman to see with what intentions these two had come to Kishkindha.

[![hanuman_meeting_ram_lakshma_original](/sites/default/files/Vibhor/hanuman_meeting_ram_lakshma_original.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/hanuman_meeting_ram_lakshma_original.jpg)

Accordingly, Lord Hanuman came down as a Brahmin priest, and inquired, “O
noble ones, who are you and what brings you here? It seems you are warriors
but you have taken to life of sanyasins. Why is it so? O delicate ones of
royal origin, your feet are having blisters due to this rough terrain. Please
tell me about yourselves.”

Lord Rama answered, “O Noble Monkey, we are the princes from Ayodhya. My name
is Rama and he is my brother Laxmana. As it happens, Ravana has kidnapped my
wife Sita and we are moving in search of her. And by the way who are you, who
speaks to us with such devotion and love?”

No sooner did Lord Rama speak thus, and then Lord Hanuman realized that he was
face to face with his Ishtha — Lord Rama. With tears in his eyes, he
prostrated at the lotus feet of Lord Rama and said, “O Lord, you have taken
such a long time to come to your devotee that this Hanuman has almost become
an ignorant fool not to recognize you. What a foolish question to ask – who
are you!”

Lord Rama lifted Lord Hanuman and put him to his chest, patting him on his
back. The tears flowed freely from the eyes of Lord Rama and Lord Hanuman.

[![Hanuman_Rama](/sites/default/files/Vibhor/hanuman_rama.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/hanuman_rama.jpg)

With the help of Sugriva, the monkeys were sent off in all the four directions
to look for Sita. The monkeys who had gone towards the south learnt that Sita
was in Lanka, across the ocean. Only Lord Hanuman could fly as his godfather
was Vayudev (God of wind), So he leapt over the ocean and arrived in Lanka. He
discovered the lonesome Sita in a grove of ashoka trees, the ashokavatika.
Lord Hanuman introduced himself and assured Sita that he would soon be back
with Lord Rama. Lord Hanuman caused some general havoc in Lanka and was
captured by Meghnaad or Indrajit (Ravana’s son). Ravana ordered that Lord
Hanuman’s tail should be set on fire. But Lord Hanuman used his burning tail
to set fire to all the houses of Lanka.

He then returned to Lord Rama with the news that Sita had been found. Lord
Rama, Lakshmana and the army of monkeys arrived at the shores of the ocean.
There they built a bridge with stones over the ocean so that they could cross
over into Lanka.

[![ramayana1](/sites/default/files/Vibhor/ramayana1.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/ramayana1.jpg)

Then, there was a terrible war in which Lord Rama killed the giant Kumbhakarna
(Ravana’s Brother). Lakshmana killed Indrajit. In the end, Lord Rama killed
Ravana with a powerful divine weapon, the brahmastra.

[![ram](/sites/default/files/Vibhor/ram.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/ram.jpg)

The fourteen years were by now over and Lord Rama, Lakshmana and Sita returned
to Ayodhya. There Lord Rama was crowned king and he treated his subjects as
his own sons. He punished the wicked and followed the path of dharma. During
Lord Rama’s rule there was no shortage of food grains anywhere and the people
were righteous. No one died an untimely death. Lord Rama and Sita had two sons
named Luv and Kush. Lord Rama went on to rule for eleven thousand years before
he died.

**“JAI SHRI RAM”**  
”जय श्रीराम”



## Copyright (C) VIbhor Mahajan

