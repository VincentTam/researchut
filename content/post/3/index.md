{
    "title": "apt-offline - 1.0",
    "date": "2010-11-08T09:55:00-05:00",
    "lastmod": "2011-01-29T10:16:08-05:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "offline package manager",
        "apt-offline GUI"
    ],
    "categories": [
        "Debian-Blog",
        "Programming"
    ]
}

Hello World.

I am very pleased to announce **apt-offline** , version **1.0**.

This release adds a Graphical User Interace to apt-offline.

![](http://www.researchut.com/blog/images/apt-offline-main.png)

Big thanks to **[Abhishek Mishra](http://blog.ideamonk.in/)** and [**Manish
Sinha**](http://milky.manishsinha.net/) who did all the development work to
make this GUI happen.

Help: I was wondering if there is a logo for APT that I could use in the big
blank space on the main window.

Apart from the GUI, there are a bunch of bug fixes in this release (which have
already been made available for the Squeeze release also).

![Generate Signature Window](http://www.researchut.com/blog/images/apt-
offline-signature.png)

![Download Window](http://www.researchut.com/blog/images/apt-offline-
download.png)

![Install Window](http://www.researchut.com/blog/images/apt-offline-
install.png)

I look forward to your feedback and comments on the GUI front.

Note: The GUI in its current form is basic and does not provide all the
features that are available in apt-offline.

