{
    "title": "Pivot at its best",
    "date": "2006-05-02T22:12:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools"
    ]
}

It really is amazing to see no more spam coming to my blog. Kudos to the Pivot
team who've done a great job integrating spam features into this great
software. Also the spam cleanup utility (integrated into the application) is
excellent. Thanks.

