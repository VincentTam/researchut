{
    "title": "Patanjali Research Foundation",
    "date": "2017-05-20T00:46:01-04:00",
    "lastmod": "2017-05-20T00:46:01-04:00",
    "draft": "false",
    "tags": [
        "Health",
        "Wellness",
        "ayurveda",
        "Patanjali"
    ],
    "categories": [
        "Debian-Blog",
        "General"
    ],
    "url": "/blog/patanjali-ayurveda-research"
}

**PSA: Research in the domain of Ayurveda**

[
http://www.patanjaliresearchfoundation.com/patanjali/](http://http://www.patanjaliresearchfoundation.com/patanjali/)  


I am so glad to see this initiative taken by the Patanjali group. This is a
great stepping stone in the health and wellness domain.

  
So far, Allopathy has been blunt in discarding alternate medicine practices,
without much solid justification. The only, repetitive, response I've heard is
"lack of research". This initiative definitely is a great step in that regard.

Ayurveda (Ancient Hindu art of healing) has a huge potential to touch lives.
For the Indian sub-continent, this has the potential of a blessing.

The **Prime Minister of India** himself
[inaugurated](https://www.youtube.com/watch?v=k9ySox1myXY) the research
centre.

