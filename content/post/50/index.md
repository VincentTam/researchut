{
    "title": "Random Thoughts.....",
    "date": "2008-04-05T07:25:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Psyche",
        "Rant"
    ]
}

Sometimes things can be so interesting.  
  
Yesterday midnight, after my colleague finished dinner from my place,
immediately called me back that he was:  
* Hit by an Auto-wallah  
And  
* The Auto-wallah was drunk  
* Didn't have any identification in the auto.  
* Instread was demanding money from my colleague.  
  
So trying to sort out matters we moved to the near by police station and let
the police-Gentle-man know that he was hit by a drunk auto-wallah.  
The actions of the police-Gentle-man are what were more interesting than
anything else. It really shows how much of faith one can have in our legal
system.  
And how much of value do people like us have, here, when our law makers and
law-care-takers act such..  
  
BTW, the response was,"There's nothing wrong in being drunk and driving at
night".  
So that's a new law I've been aware of; and one that can be leveraged.

