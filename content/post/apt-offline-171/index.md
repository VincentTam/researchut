{
    "title": "apt-offline 1.7.1 released",
    "date": "2016-09-12T06:41:50-04:00",
    "lastmod": "2016-09-12T08:13:07-04:00",
    "draft": "false",
    "tags": [
        "apt",
        "apt-offline",
        "debian"
    ],
    "categories": [
        "Debian-Pages",
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/apt-offline-171"
}

I am happy to mention the release of apt-offline, version 1.7.1.

This release includes many bug fixes, code cleanups and better integration.

  * Integration with **PolicyKit**
  * Better integration with **apt gpg keyring**
  * Resilient to failures when a sub-task errors out
  * New Feature: **Changelog**
    * This release adds the ability to deal with package changelogs (' **set** ' command option: _**\--generate-changelog**_ ) based on what is installed, extract changelog (Currently support with python-apt only) from downloaded packages and display them during installation (' **install** ' command opiton: _**\--skip-changelog**_ , if you want to skip display of changelog)
  * New Option: _**\--apt-backend**_
    * Users can now opt to choose an apt backend of their choice. Currently support: apt, apt-get (default) and python-apt



Hopefully, there will be one more release, before the release to **Stretch**.

apt-offline can be downloaded from its
[homepage](https://alioth.debian.org/projects/apt-offline/) or from
[Github](https://github.com/rickysarraf/apt-offline) page.



Update: The **PolicyKit** integration requires running the _apt-offline-gui_
command with _pkexec_ (screenshot). It also work fine with _sudo, su_ etc.

![](/sites/default/files/apt-offline-171-pkexec.png)



