{
    "title": "One week with the move",
    "date": "2010-12-03T15:45:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false"
}

It was [unfortunate](http://www.researchut.com/blog/archive/2010/06/06/fuck-
you-sony) when Sony decided to pull out the Other OS support from PS3. One of
the reasons of convincing myself to buy it was this feature. With that feature
gone, the PS3 stood as nothing much but mostly a media center and an
occasional game box.

But with the Move, I think they have compensated it. It is a greatly
engineered product with very good accuracy. It has been a week and I've been
thoroughly enjoying it. I liked the packaging too. One single compilation of
**Sports Center** contains like 5 games in it. Just the Table Tennis alone is
worth it. But there are more that I need to try.

Another surprising factor was pricing. United States pricing for the starter
pack is
[expensive](http://www.bestbuy.com/site/Sony+-+PlayStation+Move+Bundle+for+PlayStation+3/1051329.p?id=1218215123886&skuId=1051329)
than at what I got here. This was something I didn't expect at all.

**Thank you Sony for creating a good product.**

