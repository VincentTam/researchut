{
    "title": "Narada Curse Lord Vishnu – Part I",
    "date": "2016-04-02T12:01:40-04:00",
    "lastmod": "2016-04-02T12:01:40-04:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "vishnu",
        "narada"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/narada-curse-lord-vishnu"
}

# Narada Curse Lord Vishnu – Part I

Posted by [VIBHOR
MAHAJAN](https://vibhormahajan.wordpress.com/author/vibhormahajan/) on [MAY
14, 2012](https://vibhormahajan.wordpress.com/2012/05/14/narada-curse-lord-
vishnu-part-i/)

[![img_1120](/sites/default/files/Vibhor/img_1120.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/img_1120.jpg)  
Once Narada practiced severe austerities in the Himalayas. Indra (King of
Gods) became suspicious of Narada and decided to tempt him with worldly
pleasures to break his penance. With this in mind, Indra asked Kamadeva (God
of love) to break Narada’s penance.

Kama came to the spot where Narada was sitting, deep in meditation. As he shot
the first arrow the scene suddenly changed into a beautiful valley and a
beautiful damsel appeared before Narada. She began to dance before him. But
Narada’s eyes where closed to her charms.

She pleaded, “O sage, open your eyes and behold your slave.”

But Narada hardly heard her. Realizing, that she would never succeed in
distracting the sage, the damsel left for her heavenly abode. Kamadeva had to
acknowledge his defeat. Narada then opened his eyes and asked that who sent
him here. “Lord Indra”, replied Kamdeva.

Narada said – “Go and tell Indra that Narada has conquered all desires, that
he is above temptation,”

Narada gloated over his achievement and thought, that he has defeated Kama.
So, Lord Shiva is no longer the only conqueror of this invincible god. He must
go and tell Lord Shiva about it. He must accept him as his equal.

He went to Kailasa, paid his salutations to Lord Shiva and said, “I have
conquered Kamadeva. Indra sent him to tempt me. But Kamadeva failed.”

Lord Shiva replied, “I am glad to know that. But keep the matter to yourself.
In any case never brag about it to Lord Vishnu.”

Narada left Kailasa and pondered, he should speak about his success to Lord
Vishnu who love him so dearly. He went to Vishnu’s abode and paid his respect
to the lord. Vishnu invited the sage warmly and said – “Come Narada! I am so
pleased to see you.”

Narada said, “You will be more so my lord, when you learn that I, your
devotee, have conquered Kamadeva. Shiva is no longer the only conqueror of
Kamadeva. I, your devotee, am above temptation too.”

Vishnu laughed and said, “Is that so? But never cease to be on your guard. You
never know…”

Narada left from there and wondered –  “Vishnu didn’t seem to pleased about my
achievements either. I am no weakling. Doesn’t he know that? Why should he
warn me to be on my guard?”

