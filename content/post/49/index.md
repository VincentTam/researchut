{
    "title": "Amarok Magnatune Last.FM",
    "date": "2008-04-16T04:53:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "amarok"
    ],
    "categories": [
        "KDE"
    ]
}

I'm not a music fanatic. While I see many of my friends and colleagues
working, with their headphone on and music playing, for me, it is not at all
possible. It is just the opposite. I can't work with music (or any kind of
distracting sound) on.

So when I sometimes run Amarok, I used to think what a pile of crap it really
is. I mean, look at the resource consumption. Amarok alone took around 20% of
CPU cycles when just playing music. This was disappointing as I am a KDE
addict and prefer KDE applications over any other. Trying to run other
applications led to additional libraries being installed/loaded.

But last night when I was bored and was just running through some of Amarok's
features after reading an article about Amarok and Magnatune, I was surprised.
Amarok is a music beast. It is a massive wolf. It rocks........ Along with the
superb services provided by the folks at Magnatune and Last.FM, Amarok is a
wonderful music player to use. I have always advocated about software and
related services being tightly integrated. **Amarok + Magnatune + Last.FM** is
just that.

![Amarok](http://www.researchut.com/blog/images/shot7_thumb.png)

![Magnatune](http://www.researchut.com/blog/images/magnatune_logo.gif)

![LastFM](http://www.researchut.com/blog/images/red_logo.jpg)  

**Magnatune** , what to say about it. It is just awesome. You want to buy an
album of a band but aren't sure if the album is good enough. Magnatune is the
answer. Magnatune allows you to listen to the music online (using Amarok) and
then purchase at a nominal price in all major formats (ogg, mp3, flac et
cetera). You can also order a CD to be delivered to you if you don't prefer to
download the music. This is awesome. Wonderful. Sweet.

**Last.FM**. Another wonderful service. Last.FM is an online community of
music listeners. You create an account with Last.FM and then feed the
credentials to Amarok. Then, as you keep playing your collection in Amarok,
Amarok keeps updating the details at Last.FM. This has many benefits. It helps
you find people and music, like-minded. You can then listen to music from your
**Neighbours** (Last.FM) which would be similar to what you've been listening.
This is again awesome. Wonderful. Sweet.

**Podcasts**. Another thing I wasn't much aware of. I said it, I'm not a music
fanatic. So that led me to not even explore features in Amarok which are
beyond music. Amarok is awesome in tracking Podcasts and playing them for you.
Now I have my KDE Radio and Linux-Foundation podcast added to Amarok. Amarok
informs me of new podcasts, when released. Simple. Wonderful. Sweet.

There must be many more features in Amarok that I've yet not explored. But the
above mentioned features alone make Amarok a great player for me.   Thank you
Markey and the team. **Amarok Rocks.**

