{
    "title": "apt-offline backports for Debian Jessie",
    "date": "2016-04-14T08:57:22-04:00",
    "lastmod": "2016-04-14T08:59:10-04:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "Jessie",
        "Stable",
        "Backports",
        "debian"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/apt-offline-debian-jessie-backports"
}

For Debian Jessie, the version of apt-offline available is: 1.5.1. This
version has had some [issues](https://bugs.debian.org/cgi-
bin/pkgreport.cgi?pkg=apt-offline;dist=unstable).

My very 1st backports is available in the form of [apt-offline 1.7 for
](https://packages.debian.org/source/jessie-backports/apt-
offline)[Debian](https://packages.debian.org/source/jessie-backports/apt-
offline) Jessie Backports. For Debian Jessie users, this should fix most of
the issues.

