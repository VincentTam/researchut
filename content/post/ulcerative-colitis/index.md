{
    "title": "Ulcerative Colitis",
    "date": "2017-02-25T14:20:12-05:00",
    "lastmod": "2018-11-14T19:28:08-05:00",
    "draft": "false",
    "tags": [
        "ayurveda",
        "ulcer",
        "ulcerative colitis"
    ],
    "categories": [
        "General",
	"Ayurveda"
    ],
    "url": "/post/ulcerative-colitis"
}

Ulcerative colitis is a disease that affects the intestines. There is
inflammation of the colon and rectum. The main cause of ulcerative colitis may
be recurrent infections. Bacteria or virus is the most important cause of
ulcerative colitis. It may result due to infection in other parts of the body.
It is a chronic disease. It usually occurs during the young age. There are
small ulcers present in the colon or rectum.

Symptoms of ulcerative colitis may aggravate after eating spicy food. It is
very important to learn about ulcerative colitis to prevent flare ups. People
suffering from ulcerative colitis can manage their symptoms effectively by
knowing the disease properly. It is the inflammation of the lining. Small
ulcers secrete pus, which may cause discomfort. It is believed to occur due to
weak immune system. The proteins that protect you from infections become
foreign substances for your body and cause inflammation. Some people confuse
between crohn’s disease and ulcerative colitis. The difference between the two
diseases is that crohn’s disease affects any part of the gastrointestinal
tract but ulcerative colitis only affects the lining of the colon.

**Causes of ulcerative colitis**  
The real cause of ulcerative colitis is not known. Immune system is found to
be responsible for producing ulcerative colitis. There are certain other
factors that may aggravate the condition.  
Stress is a common factor that aggravates the symptoms of ulcerative colitis.
It may occur at any age but most often it is seen in people with ages between
15-30 years.  
People having a family history of ulcerative colitis are prone to suffer. It
may occur in individuals of the same family.  
Environmental factors may also cause ulcerative colitis. People who are eating
fried and spicy food and suffer from recurrent inflammations are prone to
suffer from ulcerative colitis.

**Ulcerative colitis Symptoms**  
Symptoms of ulcerative colitis begin slowly and progress. The symptoms may
vary in severity in different individuals. Many people suffer from mild
symptoms of ulcerative colitis. Some people suffer from severe symptoms of
ulcerative colitis. Some common ulcerative colitis symptoms are:

  * Abdominal pain is the most common symptom that occurs in the people suffering from ulcerative colitis.
  * There is gurgling sound in the abdomen.
  * Stools may be bloody and may also contain pus due to inflammation of the colon.
  * Diarrhoea is the common symptom of ulcerative colitis.
  * Fever may be present due to infection of the colon.
  * Due to excessive passage of watery stools people may lose weight.


## Home remedies for ulcerative colitis

Home remedies provide quick relief from the symptoms of ulcerative colitis. Home remedies are safe and may be taken any time to prevent the flare ups of ulcerative colitis. Some useful home remedies are:
 * People suffering from ulcerative colitis should limit the intake of dairy products. It will reduce the episodes of diarrhoea and also gives relief from pain and flatulence in the stomach.
 * One should consume high fibre diet to get rid from diarrhoea.
 * Avoid eating foods that may cause flatulence and pain in abdomen such as broccoli, cabbage, etc.
 * Drink lots of fluids that help to remove chemicals from the body.
 * Avoid any kind of mental stress because stress is the common triggering factor and it aggravates the symptoms of ulcerative colitis and can make the condition worse.
 * Exercise is important for keeping fit and healthy. One should do milk exercise and also do meditation to get relief from stress.


**Ulcerative colitis natural treatment**  

Herbal remedies for ulcerative colitis help in the natural treatment. Package
for ulcerative colitis consists of herbal remedies for ulcerative colitis.
This is an ulcerative colitis natural treatment because all the herbal
remedies in this package are natural. The remedies found in this package are
safe and provide quick relief from the ulcerative colitis symptoms. The
package for ulcerative colitis consists of the following herbal remedies for
ulcerative colitis:

**Divya moti pisti** : This is an excellent remedy for the treatment of
gastrointestinal disorders. It is a natural remedy that helps to boost up the
immune system and prevent recurrent attacks of infection. It gives quick
relief from abdominal pain and diarrhoea. This natural treatment for
ulcerative colitis provides nutrition to the colon and help in quick healing.

**Divya sankha bhasma** : This is also a wonderful herbal remedy for
ulcerative colitis. People suffering from ulcerative colitis should take this
herbal remedy in combination with other herbal remedies for ulcerative
colitis. It helps in preventing recurrent inflammation of the gastric cells
and gives relief from pain and diarrhoea.

**Divya kapardaka bhasma** : This helps to control ulcerative colitis symptoms
naturally without producing any negative effects on other parts of the body.
It is ulcerative colitis natural treatment. It is traditionally believed to be
one of the best herbal remedies for ulcerative colitis. This remedy may be
taken regularly to prevent recurrent attacks of gastrointestinal infection.

**Divya mukta-sukti bhasma** : This herbal remedy provides relief from all the
symptoms of ulcerative colitis. It boosts up the immune system to prevent
bacterial and viral infections. It also nurtures the cells of colon and rectum
to prevent inflammation. It brings down the fever and other symptoms of
inflammation quickly. It is a natural tonic for human body.

**Divya udaramarta vati** : This is known to be an excellent remedy for the
treatment of gastrointestinal inflammation. It may be taken regularly to get
rid of recurrent attacks of inflammation. It prevents flare ups by boosting
the immune system. This herbal remedy works on the gastro intestinal system of
the body to enable proper functioning of all the parts.

**Divya sarpa-kalpa kvatha** : This herbal remedy is useful for people who are
prone to suffer from gastro intestinal infections. It is a natural energy
booster and provides nourishment to the cells of the colon and rectum. People
who do not want to take conventional treatment for ulcerative colitis should
start taking this herbal remedy along with other remedies to get quick relief
from pain and diarrhoea.  


**Treatment for ULCERATIVE COLITIS:**

**30 day medicine chart** :

  * Divya Mukta Pishti - 5 gm
  * Divya Shank Bhasm - 10 gm
  * Divya Kapardak Bhasma - 10 gm
  * Divya MUKTA SHUKTI BHASMA - 10 gm

**Way of Administration** \- Mix these four medicines together, divide into
sixty part  & take each in the morning & evening on empty stomach, with honey.
(or as advised by the physician)

**Divya Udramrit Vati** \- 20 gm

**Way of Administration** \- 1 Tab., Take twice after food with water. (or as
advised by the physician)

**Divya SARVAKALP KWATH**  \- 300 gm

**Way of Administration** \- Prepare decoction of one teaspoon of this
medicine, and take twice a day i.e. in the morning  & evening on empty
stomach. (or as advised by the physician)

