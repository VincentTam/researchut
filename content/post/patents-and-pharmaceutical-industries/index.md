{
    "title": "Patents and the Pharmacy Industry",
    "date": "2011-02-26T05:49:30-05:00",
    "lastmod": "2013-01-31T13:37:06-05:00",
    "draft": "false",
    "tags": [
        "patents",
        "intellectual property",
        "pharmacy",
        "pharmaceutical industry"
    ],
    "categories": [
        "Debian-Blog",
        "Jurisprudence"
    ],
    "url": "/blog/patents-and-pharmaceutical-industries"
}

Mosf of us would be well versed with the Patent system in general. We have
patents in every sector - Technology, Agriculture, Pharmacy etc. Most readers
in our profession must already be well versed of the Pros/Cons of the Patent
system in Software/Technology. Patents in software are more like ammunition.
The more you have, the stronger you are. We don't seem much cat fight in
Software/Technology because not one organization own all the patents for a
product. An IBM ThinkPad might be using a cool track pad feature which might
be a patent of Dell. Since the finished product comprises of many patents from
different owners in the same industry, it is better to play good with each
other. You scratch my back, I scratch yours.



But what about the Pharmaceutical Industry? There, one single patent could
comprise the product. I was reading
[this](http://www.earth.columbia.edu/cgsd/documents/lehman.pdf) document from
Bruce Lehman which touches upon Patents and Pharmaceutical Industries, but
couldn't find an answer to my question.

Say, Glaxo spends a billion in research and comes up with an invention. They
patent the idea and market the product. Given that the medical "products" are
of a special kind they need to be disclosed. To quote from the linked
document:

The pharmaceutical industry has an important characteristic that sets it apart
from other  
industries that rely on patent protection. In many technology-based industries
it is  
possible to keep inventions a secret until the moment they are marketed. This
enables  
inventors to delay patent filings until the last possible moment and,
therefore, to  
maximize the effect of the 20 year patent term which runs from filing of the
patent  
application. The culture of medical research, however, emphasizes very early
disclosure  
of inventions, usually long before a resulting product can be placed on the
market. This is  
because scientists working in the field of human pathology have an obligation
to share  
their findings as soon as possible with their peers so that those peers will
be able to  
benefit from the new knowledge in their own research. And, unlike industries
such as  
computers and software, the pharmaceutical industry is heavily regulated by
government  
agencies to assure the safety and efficacy of products which will be sold to
consumers. In  
the United States the Food and Drug Administration performs this function.
Much of the  
investment in new drugs is in the clinical trials which are necessary to
satisfy safety and  
efficacy regulators. The tolerance for a "buyer beware" philosophy in the
pharmaceutical  
industry is extremely low compared to other industries.

The challenge is, Glaxo spends a billion in discovering a solution. Glaxo
would want to make returns out of its investment in the product. The reality
is, Glaxo launches the product at price N. Pharmaceutical companies in
developing counties, like India, manufacture an equivalent product at price
N/10. And that product also gets US FDA approved, which would imply that it is
perfectly legal to sell the very same product.

So how does the patent system work in this scenario? How would the system
ensure that the inventor gets the returns on the investment made in the
research? Or, in other ways, what good is it to acquire a patent for a medical
enhancement?

