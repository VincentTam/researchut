{
    "title": "B0rken Car - Don't Panic",
    "date": "2010-06-01T19:15:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "alto",
        "maruti"
    ],
    "categories": [
        "General",
        "Fun"
    ]
}

So this past weekend, me and my friend, went on a drive to **Yellagiri
Hills**. It was exciting driving on the highway in an entry level Indian car,
[Maruti Suzuki Alto](http://www.marutialto.com/).

Unfortunately, we couldn't make it to the destination. _5_ kilometers before
the destination, while we were uphill, the car b0rke down. On one of the hair-
pin bends where we had to cut down the speed to zero, switch to the _1st_ and
_2nd_ gear failed. Hmmm!! At that point, we were on a very steep road, with
the car stalled and the gear box (for _1st_ and _2nd_ ) broken down. Nice!!

Somehow we did manage to turn the car back (Don't ask me how) and reach down
the hill with the slope. That's when we started "thinking". Now what. Still
another _200+_ kilometers to go.

Called up the Maruti "On the Road" pick-up service. Being Sunday, the contact
was unavailable. Nice!!

We took the plunge and drove the broken car. How ?? The _3rd, 4th, 5th_ and
_Reverse_ gears still work. So we somehow managed to reach near to " **Vanyan
Badi** " at a restaurant and asked for help. Luckily we did get a resource to
help us out. Unfortunately, the resource was not resourceful enough. He
recommended us drive down the b0rken car all the way back home. Nice!!

With not much choice left, and with all the Cats & Dogs rain (ah!! Yes. I will
talk more about it in a while), we decided to move on with the b0rken car.
Plan was: worst case, we dump the car and catch a cab (hopefully).

As soon as we started, we were welcomed with a very nice and heavy rainfall.
Heavy enough that the water was near to on the road, the speed breakers were
not visible and a very good amount of water was clogged at the sides of the
road. Well! Fuck happens. We continue.

We did hit a big breaker which went unnoticed because of the water clog. Apart
from that, otherwise, there weren't any issues. We covered _3_ toll gates full
of traffic with out b0rken car and reached back home safely. Then hunted down
for food/booze, ate/drank and CRASHED.

So, when your car goes b0rke and you are just a bunch of dudes stuck, don't
worry. Fuck it. It ain't gonna screw you that bad.
![;-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_121.gif)

