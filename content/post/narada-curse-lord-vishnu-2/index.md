{
    "title": "Narada Curse Lord Vishnu–Part II",
    "date": "2016-04-02T12:05:36-04:00",
    "lastmod": "2016-04-02T12:05:36-04:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "vishnu",
        "narada"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/narada-curse-lord-vishnu-2"
}

# Narada Curse Lord Vishnu–Part II

Posted by [VIBHOR
MAHAJAN](https://vibhormahajan.wordpress.com/author/vibhormahajan/) on [MAY
14, 2012](https://vibhormahajan.wordpress.com/2012/05/14/narada-curse-lord-
vishnupart-ii/)

[![narada_and_shrimati](/sites/default/files/Vibhor/narada_and_shrimati.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/narada_and_shrimati.jpg)

  
As Narada moved on from Lord Vishnu’s adobe, he suddenly saw a beautiful city
like he had never seen before. Curiously he entered the city and asked a
passerby, “Who is the ruler of this big and charming city?”

“It belongs to the glorious King Sheelanidhi”– replied the passerby.

Hearing that Narada had come to visit him, the king came out to receive him
and welcomed him. Just then a beautiful girl came and bowed before Narada. The
beauty of the girl took Narada. She was King’s daughter Shrimati. A Swayamvara
was being planned for her.

Narada said to the king – “Your daughter is the incarnation of goddess
Lakshmi. No less than Hari (Lord Vishnu) in glory and power shall be her
husband.”

Narada was so taken by the princess that he wished to make her his wife.
Narada prayed fervently to Lord Vishnu. At last when Lord Vishnu appeared
before him Narada requested, “Lord let my face resemble Hari’s.”. Lord Vishnu
said – “So be it. You shall certainly have the face of the Hari,”

When Narada made his request he had forgotten that the word Hari also meant “A
Monkey” and he could not see his own face. Narada reached the court of that
King full of confidence sure of his victory. The princess entered the hall of
marriage with a flower garland in hand. Narada was sure that the princess
would choose him as the companion of life. But to his utter dismay, the
princess did not even look twice at his face. As Shrimati passed him Narada
could not resist, he stood up and said, “You must be looking for me beautiful
one. Here I am.” Rest of the crowd burst into laughter and said, “For you?
Monkey face… a very handsome one, no doubt, but still a monkey-face?

Narada was left wondering, “Do I really have the face of a monkey?”

Suddenly Lord Vishnu also appeared in the court and before Narada could ask
him why he had got a monkey face the princess put the garland around the neck
of Lord Vishnu.

Narada filled with rage, cursed Vishnu proclaiming that Vishnu, during his
sojourn on earth, would have to bear the pain of his wife’s forcible
separation from him and only a monkey would be able to relieve him of his
sufferings.

Later Narada asked – “Why he got monkey’s face! Why?” , to which Lord Vishnu
replied, “My dear Narada you are a scholar of Sanskrit. Don’t you know, Hari
also means monkey? You didn’t specify which Hari you meant.” Narada asked
angrily, “Am Istuck with this face forever?” Vishnu calmly replied, “Narada,
calm down, and lookaround you.”

Narada was dumb struck when he looked around for everything had vanished.
Vishnu laughed and replied, “There was no city, no king, no Swayamvara.Your
monkey face and all this was an illusory creation of mine to humble your
pride; because you thought you were above temptation.”

Narada gratefully prostrated before Vishnu and said, “I am grateful to you, my
lord! You have opened my eyes.”

In his next incarnation, that is his seventh incarnation, Lord Vishnu was born
as Lord RamChandra and because of this curse Lord Hanuman helped him in
Ramayana. Lord Hanuman was a Rudraroop that is incarnation of Lord Shiva.

