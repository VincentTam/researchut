{
    "title": "Sony DSC-M1",
    "date": "2005-06-22T20:23:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "tags": [
        "sony",
        "dsc m1"
    ],
    "categories": [
        "General",
        "Fun"
    ]
}

![](http://www.researchut.com/blog/images/dscm1.jpg)

Purchased a Sony DSC-M1 recently.  
Man, proprietary stuffs are real costly. The storage device alone (1 GB) costs
$200.  
But in total the camera is good to have. It's compact, 5.1 Megapixel lens and
blah blah blah.  
For an ignorant photographer like me, having a smart digital camera helps
capture moments in good pictures to save them for future.

Learnings,  
if possible try going for commodity stuff which isn't proprietary unless you
have the passion of spending money on "Love at first sight at $omething". Yes,
proprietary stuff are unique which increase the temptation.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

