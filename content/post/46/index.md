{
    "title": "Beauty in Debian",
    "date": "2008-06-06T08:16:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages"
    ]
}

While this is not really specific to [Debian](http://www.debian.org), but
somehow it does contribute to it.

One of the reasons why I like [Debian](http://www.debian.org) a lot is because
of its package organization and availability. I take full leverage of the good
work the Debian Developers have done packaging various dictionary packages in
Debian.

While recently doing a re-installation (Don't ask me why
![;-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_121.gif),
searching back for the Debian dict packages, yielded in the following:

> rrs@learner:~$ apt-cache search dict | grep dict  
>

>

> dict-freedict-hin-eng

This is something really nice in Debian. Searching through the large amount of
packages is just plain easy. I wasn't aware of this package at all.

Here's what it turned out to be:

> rrs@learner:~$ apt-cache show dict-freedict-hin-eng  
>  Package: dict-freedict-hin-eng  
>  Priority: optional  
>  Section: text  
>  Installed-Size: 2316  
>  Maintainer: KÄstutis BiliÅ«nas <kebil@kaunas.init.lt>  
>  Architecture: all  
>  Source: freedict  
>  Version: 1.3-4  
>  Provides: dictd-dictionary  
>  Suggests: dict | opendict | kdict, dictd | serpento  
>  Filename: pool/main/f/freedict/dict-freedict-hin-eng_1.3-4_all.deb  
>  Size: 1473648  
>  MD5sum: 1c698c7ef611d7c5cd878bb785b00ae8  
>  SHA1: 697b9d904b990640ba98dd9499522f989570627e  
>  SHA256: 64988f08b0b1bf0cc19d4cbefd0939ecc978ad79d7ce7d471ebdf7b22a82e213  
>  Description: Dict package for Hindi-English Freedict dictionary  
>   This is a package of the Hindi-English Freedict dictionary, formatted  
>   for the dictionary server and client which uses the DICT Protocol.  
>  Homepage: http://freedict.org/  
>  Tag: culture::hindi, made-of::data:dictionary, role::app-data  
>

Amazing! And the results
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

> rrs@learner:~$ mydict limousine  
>

>

> From WordNet (r) 3.0 (2006) [wn]:  
>  
>    limousine  
>        n 1: large luxurious car; usually driven by a chauffeur [syn:  
>             {limousine}, {limo}]  
>  
>  From English-Hindi Freedict Dictionary [fd-eng-hin]:  
>  
>    limousine <N.>  
>  
>    1\. à¤²à¤¿à¤®à¥à¤à¥à¤¨  
>         "She called a limousine to go to the ball."

And then back from Hindi->English

> rrs@learner:~$ dict à¤²à¤¿à¤®à¥à¤à¥à¤¨  
>  1 definition found  
>  
>  From English-Hindi Freedict Dictionary [reverse index] [fd-hin-eng]:  
>  
>    limousine <N.>  
>  
>    1\. à¤²à¤¿à¤®à¥à¤à¥à¤¨  
>         "She called a limousine to go to the ball."  
>

>  Sweet!
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

