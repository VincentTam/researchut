{
    "title": "Reporting bugs with Apport",
    "date": "2012-07-12T07:43:34-04:00",
    "lastmod": "2014-11-12T05:53:58-05:00",
    "draft": "false",
    "tags": [
        "apport"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/report-bugs-with-apport"
}

So yet another bug reporting tool. :-)

When I prepared Apport for Debian, I wasn't sure how it will look going
forward. If you look at the current one lying in experimental, it just detects
your crashes and pops up in your systray. It doesn't have the mechanism to
interact with BTS.

So, like the title of this post says, this is the first worthy feature for
Apport in the context of Debian.

![Apport Detail Report](/sites/default/files/apport-bug.png)

Nothing special here. You would have seen a similar window before if you have
installed apport.



What changes with this release is, that now, when you check the **Send an
error report to help fix this problem** , it will really file a bug report on
the BTS.

Here's what the emailed bug report will look like:



    
    
    Package: leafnode
    Version: 2.0.0.alpha20090406a-1
    
    
    =============================
    
    ProblemType: Crash
    Architecture: amd64
    Date: Tue Jul  3 00:08:02 2012
    Dependencies:
     adduser 3.113+nmu3
     base-passwd 3.5.24
     cron 3.0pl1-123
     debconf 1.5.44
     debianutils 4.3.1
     dpkg 1.16.4.3
     gcc-4.7-base 4.7.1-2
     libbz2-1.0 1.0.6-3
     libc-bin 2.13-33
     libc6 2.13-33
     libclass-isa-perl 0.36-3
     libdb5.1 5.1.29-4
     libfile-copy-recursive-perl 0.38-1
     libgcc1 1:4.7.1-2
     libgdbm3 1.8.3-11
     liblzma5 5.1.1alpha+20120614-1
     libpam-modules 1.1.3-7.1
     libpam-modules-bin 1.1.3-7.1
     libpam-runtime 1.1.3-7.1
     libpam0g 1.1.3-7.1
     libpcre3 1:8.30-5
     libpopt0 1.16-7
     libselinux1 2.1.9-5
     libsemanage-common 2.1.6-6
     libsemanage1 2.1.6-6
     libsepol1 2.1.4-3
     libswitch-perl 2.16-2
     libustr-1.0-1 1.0.4-3
     libwrap0 7.6.q-23
     logrotate 3.8.1-4
     lsb-base 4.1+Debian7 [modified: lib/lsb/init-functions]
     multiarch-support 2.13-33
     netbase 5.0
     openbsd-inetd 0.20091229-2
     passwd 1:4.1.5.1-1
     perl 5.14.2-12
     perl-base 5.14.2-12
     perl-modules 5.14.2-12
     sensible-utils 0.0.7
     tar 1.26-4
     tcpd 7.6.q-23
     update-inetd 4.43
     zlib1g 1:1.2.7.dfsg-13
    Disassembly:
     => 0x7f8e69738475 <*__GI_raise+53>:	cmp    $0xfffffffffffff000,%rax
        0x7f8e6973847b <*__GI_raise+59>:	ja     0x7f8e69738492 <*__GI_raise+82>
        0x7f8e6973847d <*__GI_raise+61>:	repz retq 
        0x7f8e6973847f <*__GI_raise+63>:	nop
        0x7f8e69738480 <*__GI_raise+64>:	test   %eax,%eax
        0x7f8e69738482 <*__GI_raise+66>:	jg     0x7f8e69738465 <*__GI_raise+37>
        0x7f8e69738484 <*__GI_raise+68>:	test   $0x7fffffff,%eax
        0x7f8e69738489 <*__GI_raise+73>:	jne    0x7f8e697384a2 <*__GI_raise+98>
        0x7f8e6973848b <*__GI_raise+75>:	mov    %esi,%eax
        0x7f8e6973848d <*__GI_raise+77>:	nopl   (%rax)
        0x7f8e69738490 <*__GI_raise+80>:	jmp    0x7f8e69738465 <*__GI_raise+37>
        0x7f8e69738492 <*__GI_raise+82>:	mov    0x34e97f(%rip),%rdx        # 0x7f8e69a86e18
        0x7f8e69738499 <*__GI_raise+89>:	neg    %eax
        0x7f8e6973849b <*__GI_raise+91>:	mov    %eax,%fs:(%rdx)
        0x7f8e6973849e <*__GI_raise+94>:	or     $0xffffffff,%eax
        0x7f8e697384a1 <*__GI_raise+97>:	retq
    DistroRelease: Debian 7.0
    ExecutablePath: /usr/sbin/fetchnews
    ExecutableTimestamp: 1265584779
    Package: leafnode 2.0.0.alpha20090406a-1
    PackageArchitecture: amd64
    ProcCmdline: /usr/sbin/fetchnews
    ProcCwd: /
    ProcEnviron:
     LANGUAGE=en_US:en
     LC_TIME=en_IN.UTF-8
     LC_MONETARY=en_IN.UTF-8
     PATH=(custom, no user)
     LC_ADDRESS=en_IN.UTF-8
     LANG=en_US.UTF-8
     LC_TELEPHONE=en_IN.UTF-8
     LC_NAME=en_IN.UTF-8
     SHELL=/bin/sh
     LC_MEASUREMENT=en_IN.UTF-8
     LC_NUMERIC=en_IN.UTF-8
     LC_PAPER=en_IN.UTF-8
    ProcMaps:
     00400000-00421000 r-xp 00000000 08:06 4464934                            /usr/sbin/fetchnews
     00621000-00622000 rw-p 00021000 08:06 4464934                            /usr/sbin/fetchnews
     00622000-00623000 rw-p 00000000 00:00 0 
     00be4000-00c05000 rw-p 00000000 00:00 0                                  [heap]
     7f8e68edc000-7f8e68eef000 r-xp 00000000 08:06 1179776                    /lib/x86_64-linux-gnu/libresolv-2.13.so
     7f8e68eef000-7f8e690ee000 ---p 00013000 08:06 1179776                    /lib/x86_64-linux-gnu/libresolv-2.13.so
     7f8e690ee000-7f8e690ef000 r--p 00012000 08:06 1179776                    /lib/x86_64-linux-gnu/libresolv-2.13.so
     7f8e690ef000-7f8e690f0000 rw-p 00013000 08:06 1179776                    /lib/x86_64-linux-gnu/libresolv-2.13.so
     7f8e690f0000-7f8e690f2000 rw-p 00000000 00:00 0 
     7f8e690f2000-7f8e690f7000 r-xp 00000000 08:06 1180036                    /lib/x86_64-linux-gnu/libnss_dns-2.13.so
     7f8e690f7000-7f8e692f6000 ---p 00005000 08:06 1180036                    /lib/x86_64-linux-gnu/libnss_dns-2.13.so
     7f8e692f6000-7f8e692f7000 r--p 00004000 08:06 1180036                    /lib/x86_64-linux-gnu/libnss_dns-2.13.so
     7f8e692f7000-7f8e692f8000 rw-p 00005000 08:06 1180036                    /lib/x86_64-linux-gnu/libnss_dns-2.13.so
     7f8e692f8000-7f8e692fa000 r-xp 00000000 08:06 1183392                    /lib/libnss_mdns4_minimal.so.2
     7f8e692fa000-7f8e694f9000 ---p 00002000 08:06 1183392                    /lib/libnss_mdns4_minimal.so.2
     7f8e694f9000-7f8e694fa000 rw-p 00001000 08:06 1183392                    /lib/libnss_mdns4_minimal.so.2
     7f8e694fa000-7f8e69505000 r-xp 00000000 08:06 1180055                    /lib/x86_64-linux-gnu/libnss_files-2.13.so
     7f8e69505000-7f8e69704000 ---p 0000b000 08:06 1180055                    /lib/x86_64-linux-gnu/libnss_files-2.13.so
     7f8e69704000-7f8e69705000 r--p 0000a000 08:06 1180055                    /lib/x86_64-linux-gnu/libnss_files-2.13.so
     7f8e69705000-7f8e69706000 rw-p 0000b000 08:06 1180055                    /lib/x86_64-linux-gnu/libnss_files-2.13.so
     7f8e69706000-7f8e69883000 r-xp 00000000 08:06 1179700                    /lib/x86_64-linux-gnu/libc-2.13.so
     7f8e69883000-7f8e69a83000 ---p 0017d000 08:06 1179700                    /lib/x86_64-linux-gnu/libc-2.13.so
     7f8e69a83000-7f8e69a87000 r--p 0017d000 08:06 1179700                    /lib/x86_64-linux-gnu/libc-2.13.so
     7f8e69a87000-7f8e69a88000 rw-p 00181000 08:06 1179700                    /lib/x86_64-linux-gnu/libc-2.13.so
     7f8e69a88000-7f8e69a8d000 rw-p 00000000 00:00 0 
     7f8e69a8d000-7f8e69a95000 r-xp 00000000 08:06 1180031                    /lib/x86_64-linux-gnu/libcrypt-2.13.so
     7f8e69a95000-7f8e69c94000 ---p 00008000 08:06 1180031                    /lib/x86_64-linux-gnu/libcrypt-2.13.so
     7f8e69c94000-7f8e69c95000 r--p 00007000 08:06 1180031                    /lib/x86_64-linux-gnu/libcrypt-2.13.so
     7f8e69c95000-7f8e69c96000 rw-p 00008000 08:06 1180031                    /lib/x86_64-linux-gnu/libcrypt-2.13.so
     7f8e69c96000-7f8e69cc4000 rw-p 00000000 00:00 0 
     7f8e69cc4000-7f8e69cc6000 r-xp 00000000 08:06 1180047                    /lib/x86_64-linux-gnu/libdl-2.13.so
     7f8e69cc6000-7f8e69ec6000 ---p 00002000 08:06 1180047                    /lib/x86_64-linux-gnu/libdl-2.13.so
     7f8e69ec6000-7f8e69ec7000 r--p 00002000 08:06 1180047                    /lib/x86_64-linux-gnu/libdl-2.13.so
     7f8e69ec7000-7f8e69ec8000 rw-p 00003000 08:06 1180047                    /lib/x86_64-linux-gnu/libdl-2.13.so
     7f8e69ec8000-7f8e69ed5000 r-xp 00000000 08:06 1179893                    /lib/x86_64-linux-gnu/libpam.so.0.83.0
     7f8e69ed5000-7f8e6a0d4000 ---p 0000d000 08:06 1179893                    /lib/x86_64-linux-gnu/libpam.so.0.83.0
     7f8e6a0d4000-7f8e6a0d5000 r--p 0000c000 08:06 1179893                    /lib/x86_64-linux-gnu/libpam.so.0.83.0
     7f8e6a0d5000-7f8e6a0d6000 rw-p 0000d000 08:06 1179893                    /lib/x86_64-linux-gnu/libpam.so.0.83.0
     7f8e6a0d6000-7f8e6a112000 r-xp 00000000 08:06 1182916                    /lib/x86_64-linux-gnu/libpcre.so.3.13.1
     7f8e6a112000-7f8e6a312000 ---p 0003c000 08:06 1182916                    /lib/x86_64-linux-gnu/libpcre.so.3.13.1
     7f8e6a312000-7f8e6a313000 rw-p 0003c000 08:06 1182916                    /lib/x86_64-linux-gnu/libpcre.so.3.13.1
     7f8e6a313000-7f8e6a333000 r-xp 00000000 08:06 1180134                    /lib/x86_64-linux-gnu/ld-2.13.so
     7f8e6a503000-7f8e6a507000 rw-p 00000000 00:00 0 
     7f8e6a530000-7f8e6a532000 rw-p 00000000 00:00 0 
     7f8e6a532000-7f8e6a533000 r--p 0001f000 08:06 1180134                    /lib/x86_64-linux-gnu/ld-2.13.so
     7f8e6a533000-7f8e6a534000 rw-p 00020000 08:06 1180134                    /lib/x86_64-linux-gnu/ld-2.13.so
     7f8e6a534000-7f8e6a535000 rw-p 00000000 00:00 0 
     7fffbc06e000-7fffbc08f000 rw-p 00000000 00:00 0                          [stack]
     7fffbc1ff000-7fffbc200000 r-xp 00000000 00:00 0                          [vdso]
     ffffffffff600000-ffffffffff601000 r-xp 00000000 00:00 0                  [vsyscall]
    ProcStatus:
     Name:	fetchnews
     State:	S (sleeping)
     Tgid:	6872
     Pid:	6872
     PPid:	6871
     TracerPid:	0
     Uid:	9	9	9	9
     Gid:	9	9	9	9
     FDSize:	64
     Groups:	9 
     VmPeak:	   21440 kB
     VmSize:	   21276 kB
     VmLck:	       0 kB
     VmPin:	       0 kB
     VmHWM:	     984 kB
     VmRSS:	     984 kB
     VmData:	     380 kB
     VmStk:	     136 kB
     VmExe:	     132 kB
     VmLib:	    2132 kB
     VmPTE:	      64 kB
     VmSwap:	       0 kB
     Threads:	1
     SigQ:	0/23227
     SigPnd:	0000000000000000
     ShdPnd:	0000000000000000
     SigBlk:	0000000000000000
     SigIgn:	0000000000000000
     SigCgt:	0000000000000000
     CapInh:	0000000000000000
     CapPrm:	0000000000000000
     CapEff:	0000000000000000
     CapBnd:	ffffffffffffffff
     Cpus_allowed:	f
     Cpus_allowed_list:	0-3
     Mems_allowed:	00000000,00000001
     Mems_allowed_list:	0
     voluntary_ctxt_switches:	6
     nonvoluntary_ctxt_switches:	1
    Registers:
     rax            0x0	0
     rbx            0x0	0
     rcx            0xffffffffffffffff	-1
     rdx            0x6	6
     rsi            0x1ad8	6872
     rdi            0x1ad8	6872
     rbp            0x0	0x0
     rsp            0x7fffbc08d4f8	0x7fffbc08d4f8
     r8             0x7f8e6a504700	140249645729536
     r9             0x6d6f642064656966	7885631562835126630
     r10            0x8	8
     r11            0x246	582
     r12            0x0	0
     r13            0x7fffbc08d920	140736348084512
     r14            0x0	0
     r15            0x0	0
     rip            0x7f8e69738475	0x7f8e69738475 <*__GI_raise+53>
     eflags         0x246	[ PF ZF IF ]
     cs             0x33	51
     ss             0x2b	43
     ds             0x0	0
     es             0x0	0
     fs             0x0	0
     gs             0x0	0
    Signal: 6
    SourcePackage: leafnode
    Stacktrace:
     #0  0x00007f8e69738475 in *__GI_raise (sig=<optimized out>) at ../nptl/sysdeps/unix/sysv/linux/raise.c:64
             pid = <optimized out>
             selftid = <optimized out>
     #1  0x00007f8e6973b6f0 in *__GI_abort () at abort.c:92
             act = {__sigaction_handler = {sa_handler = 0x7f8e69850d3d, sa_sigaction = 0x7f8e69850d3d}, sa_mask = {__val = {140736348083740, 140249634747968, 0, 0, 140736348084512, 140249631083424, 140249645738440, 0, 4294967295, 206158430232, 1, 6427272, 0, 0, 0, 0}}, sa_flags = 1781664242, sa_restorer = 0x1}
             sigs = {__val = {32, 0 <repeats 15 times>}}
     #2  0x0000000000416292 in ?? ()
     No symbol table info available.
     #3  0x0000000000411c80 in ?? ()
     No symbol table info available.
     #4  0x0000000000406952 in ?? ()
     No symbol table info available.
     #5  0x00007f8e69724ead in __libc_start_main (main=<optimized out>, argc=<optimized out>, ubp_av=<optimized out>, init=<optimized out>, fini=<optimized out>, rtld_fini=<optimized out>, stack_end=0x7fffbc08d918) at libc-start.c:228
             result = <optimized out>
             unwind_buf = {cancel_jmp_buf = {{jmp_buf = {0, 498162418289285118, 4206480, 140736348084512, 0, 0, -498021567843461122, -435434157313288194}, mask_was_saved = 0}}, priv = {pad = {0x0, 0x0, 0x418360, 0x7fffbc08d928}, data = {prev = 0x0, cleanup = 0x0, canceltype = 4293472}}}
             not_first_call = <optimized out>
     #6  0x0000000000402fb9 in ?? ()
     No symbol table info available.
     #7  0x00007fffbc08d918 in ?? ()
     No symbol table info available.
     #8  0x000000000000001c in ?? ()
     No symbol table info available.
     #9  0x0000000000000001 in ?? ()
     No symbol table info available.
     #10 0x00007fffbc08ee96 in ?? ()
     No symbol table info available.
     #11 0x0000000000000000 in ?? ()
     No symbol table info available.
    StacktraceTop:
     ?? ()
     ?? ()
     ?? ()
     __libc_start_main (main=<optimized out>, argc=<optimized out>, ubp_av=<optimized out>, init=<optimized out>, fini=<optimized out>, rtld_fini=<optimized out>, stack_end=0x7fffbc08d918) at libc-start.c:228
     ?? ()
    ThreadStacktrace:
     .
     Thread 1 (LWP 6872):
     #0  0x00007f8e69738475 in *__GI_raise (sig=<optimized out>) at ../nptl/sysdeps/unix/sysv/linux/raise.c:64
             pid = <optimized out>
             selftid = <optimized out>
     #1  0x00007f8e6973b6f0 in *__GI_abort () at abort.c:92
             act = {__sigaction_handler = {sa_handler = 0x7f8e69850d3d, sa_sigaction = 0x7f8e69850d3d}, sa_mask = {__val = {140736348083740, 140249634747968, 0, 0, 140736348084512, 140249631083424, 140249645738440, 0, 4294967295, 206158430232, 1, 6427272, 0, 0, 0, 0}}, sa_flags = 1781664242, sa_restorer = 0x1}
             sigs = {__val = {32, 0 <repeats 15 times>}}
     #2  0x0000000000416292 in ?? ()
     No symbol table info available.
     #3  0x0000000000411c80 in ?? ()
     No symbol table info available.
     #4  0x0000000000406952 in ?? ()
     No symbol table info available.
     #5  0x00007f8e69724ead in __libc_start_main (main=<optimized out>, argc=<optimized out>, ubp_av=<optimized out>, init=<optimized out>, fini=<optimized out>, rtld_fini=<optimized out>, stack_end=0x7fffbc08d918) at libc-start.c:228
             result = <optimized out>
             unwind_buf = {cancel_jmp_buf = {{jmp_buf = {0, 498162418289285118, 4206480, 140736348084512, 0, 0, -498021567843461122, -435434157313288194}, mask_was_saved = 0}}, priv = {pad = {0x0, 0x0, 0x418360, 0x7fffbc08d928}, data = {prev = 0x0, cleanup = 0x0, canceltype = 4293472}}}
             not_first_call = <optimized out>
     #6  0x0000000000402fb9 in ?? ()
     No symbol table info available.
     #7  0x00007fffbc08d918 in ?? ()
     No symbol table info available.
     #8  0x000000000000001c in ?? ()
     No symbol table info available.
     #9  0x0000000000000001 in ?? ()
     No symbol table info available.
     #10 0x00007fffbc08ee96 in ?? ()
     No symbol table info available.
     #11 0x0000000000000000 in ?? ()
     No symbol table info available.
    Title: fetchnews crashed with SIGABRT in __libc_start_main()
    Uname: Linux 3.4-trunk-amd64 x86_64
    UserGroups: 
    



The first 2 lines should be enough for the BTS server to file the bug report
to the correct package and against the correct version.

If you paid attention in the screen shot and the email report, you will notice
that the email report does not include the **CoreDump** section. This was
knocked off intentionally as we currently might not need it. If there is a
need, the data is always available on the user 's apport database, and the
dev/user and seek it on a need basis.



But in case, if a day comes when we really want it, that too is possible. The
same email report will have the CoreDump seciton with a section like the
following:

    
    
    CoreDump: base64 
    
    
    H4sICAAAAAAC/0NvcmVEdW1wAOx9C0AU1f7/7LLAggirouF7VDQ0xcVXWGqLoq4Kur6KMmV5CoqwwqJYVquggYqhlVm3B3mt7OWlbnXtZeubHtfobc9Lb6xUemiU5f7PmfkcmBl3VMzbvff3Px89fPb7nfOeM2fOOfOdMzeNT5pgNBgEBpMwRmiRBMEmnA6bEC8MbfYvI6eN4BePBfvX0zQC6Q9RJyULY6NKpuGC6A8rO56uE07wH44lI7YynINlc6cmnFE3n1L5nNB7j+Ssv8lPelbDaeEkuBCu8ag6HENjz9PCmZTh6sNz/aanVz4PS6+V4aoQTohQhxM1rK2XGoQTI/yn5xD814sX4VyacGerFxbOObh15atj6Z17OKl89Qjn0Qkn6pSvEeGqB59z+YKU4aqmtS6fQgDS0wlXo5NPC8I5HK07DyyczdW68yCy9FytK58V4Zw64eou8l8+G8JZy/2Xj51Abfmaw21Th7Np+LTrD+Fc21p5/SGcRxPOoX8dye0T4ep00vPotU92Hh5u3Xln4Wwvt6587A7jaGU4C8I5X/afz3qd/pqFs73WuutWZOm91rr20hzuu9aVz4pwru9adz3YEM6jE65Op3wOdh6OtO68s3C2tgtaVT4nS+/cw0nlc7H61AnnDfBfPg/Cie0WnOv5C1KFi2tdPqsQzqoTTtAZ91SzcI4Frepf6tlIbeaCc71PyyECcXxm686fBeGsrQwnIpytleGsCFcX9IJPVT6jmrXnwRHM2q863NnOn5MNbJ0+irOFo2EcBjn8uGkzxgvN/ZuMID8t7lAXQXiPuHfhLjTE983qMmswAVVmhpydm5UpZhYsEiaq254PCFLkW8nfQF9cViT1HUM18Te9LefjEhahJv6zgV3DtI7LMaJhdSzMwMFo+cIIgdPWbXaWOyMnP2tpEbwPLi4qHFyUnps/uPmI+EfqOhBD/SBF3nq19J1S+VmT8Z389iVVm8CBdhAzwQHNl 7xNNReLV6SpPH/NUzH0OWZNHkPAVYPl+Npo9GEaua1GDtfIF2ni7wz+5QP5fOOOIXxxTJYjWPgP/bfLQHSLRkUd3hTgv749PWm7FP7r0FcDP15U/cmO9fL4jLFNgzkaiBr4zwWL//QaYtey3vH/NtBrxEbbBHFJk6bOTuFt4v9cmwj4N/vn4ODg4ODg4ODg4OD4/wE3aZ7/G/H8n60B2aDfFmVs9kOf/5vJ325CV2n+HSgol59tKvYiasZmxRzNJCeIhG0q7gY1Y4OCA1UlsKl4b5hRxWxhu2Vdmq0Dp6vYi8UxS5SgCmdEuBiEi4F
    
    
    
    
    
    Now, **Is this going to be useful? Will we get flooded with bug reports?**
    
    
     
    
    
     Usefulness: I think this will uncover many bugs that might be getting unnoticed today. Take this example itself. This bug was detect against the leafnode package's fetchnews binary. That binary does a quiet job in the background, fetching news. I never was aware that it had been crashing. As a system-wide monitoring tool, apport was able to detect, trap and inform the user of the crash. So I think this will be useful and improve the overall quality.
    
    
    Abuse/Flood: This is something I can't predict. Perhaps it will bring in challenges if people just blindly click on the **Send Report** button. One option can be to have the  "Send Report" checkbox unchecked by default. That'll hopefully lower down the possibilities.
    
    
     
    
    
    **What do you think?** Let me know your comments.
    
    
    This change will soon be pushed to apport in experimental. Hopefully this weekend.
    
    
     
    
    
    I want to wrap up this post with a **Thank You** note to **Martin Pitt** and his team who created Apport, and with such simplicity. It took me 30 lines of code to adapt it to Debian. My intent with Apport is to keep the changes to the minimum so that we can always leverage new features and fixes that Apport brings in. So, as far as I 'll try, there will be no drift from its original shape.





