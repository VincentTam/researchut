{
    "title": "Fixing Hardware Bugs",
    "date": "2017-04-04T07:02:35-04:00",
    "lastmod": "2017-04-04T07:43:49-04:00",
    "draft": "false",
    "tags": [
        "Lenovo",
        "linux",
        "usb",
        "Realtek"
    ],
    "categories": [
        "Debian-Blog",
        "Rant",
        "Computing"
    ],
    "url": "/blog/fixing-realtek-sdcard"
}

Bugs can be annoying. Especially the ones that crash or hang and do not have a
root cause. A good example of such annoyance can be kernel bugs, where  a
faulty hardware/device driver hinders the kernel's suspend/resume process.
Because, as a user, while in the middle of your work, you suspend your machine
hoping to resume your work, back when at your destination. But, during
suspend, or during resume, randomly the bug triggers leaving you with no
choice but a hardware reset. Ultimately, resulting in you losing the entire
work state you were in.



Such is a situation I encountered with my 2 year old, **Lenovo Yoga 2 13**.
For 2 years, I had been living with this bug with all the side-effects
mentioned.

    
    
    Mar 01 18:43:28 learner kernel: usb 2-4: new high-speed USB device number 38 using xhci_hcd
    Mar 01 18:43:54 learner kernel: usb 2-4: new high-speed USB device number 123 using xhci_hcd
    Mar 01 18:44:00 learner kernel: usb 2-4: new high-speed USB device number 125 using xhci_hcd
    Mar 01 18:44:11 learner kernel: usb 2-4: new high-speed USB device number 25 using xhci_hcd
    Mar 01 18:44:16 learner kernel: usb 2-4: new high-speed USB device number 26 using xhci_hcd
    Mar 01 18:44:22 learner kernel: usb 2-4: new high-speed USB device number 27 using xhci_hcd
    Mar 01 18:44:22 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:22 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:22 learner kernel: usb 2-4: new high-speed USB device number 28 using xhci_hcd
    Mar 01 18:44:23 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:23 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:23 learner kernel: usb 2-4: new high-speed USB device number 29 using xhci_hcd
    Mar 01 18:44:23 learner kernel: usb 2-4: Device not responding to setup address.
    Mar 01 18:44:23 learner kernel: usb 2-4: Device not responding to setup address.
    Mar 01 18:44:23 learner kernel: usb 2-4: device not accepting address 29, error -71
    Mar 01 18:44:24 learner kernel: usb 2-4: new high-speed USB device number 30 using xhci_hcd
    Mar 01 18:44:24 learner kernel: usb 2-4: Device not responding to setup address.
    Mar 01 18:44:24 learner kernel: usb 2-4: Device not responding to setup address.
    Mar 01 18:44:24 learner kernel: usb 2-4: device not accepting address 30, error -71
    Mar 01 18:44:24 learner kernel: usb usb2-port4: unable to enumerate USB device
    Mar 01 18:44:24 learner kernel: usb 2-4: new high-speed USB device number 31 using xhci_hcd
    Mar 01 18:44:24 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:25 learner kernel: usb 2-4: new high-speed USB device number 32 using xhci_hcd
    Mar 01 18:44:30 learner kernel: usb 2-4: new high-speed USB device number 33 using xhci_hcd
    Mar 01 18:44:30 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:31 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:31 learner kernel: usb 2-4: new high-speed USB device number 34 using xhci_hcd
    Mar 01 18:44:36 learner kernel: usb 2-4: new high-speed USB device number 35 using xhci_hcd
    Mar 01 18:44:36 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:36 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:37 learner kernel: usb 2-4: new high-speed USB device number 36 using xhci_hcd
    Mar 01 18:44:37 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:37 learner kernel: usb 2-4: device descriptor read/64, error -71
    Mar 01 18:44:37 learner kernel: usb 2-4: new high-speed USB device number 37 using xhci_hcd
    Mar 01 18:44:37 learner kernel: usb 2-4: Device not responding to setup address.
    Mar 01 18:44:37 learner kernel: usb 2-4: Device not responding to setup address.
    Mar 01 18:44:38 learner kernel: usb 2-4: device not accepting address 37, error -71
    Mar 01 18:44:38 learner kernel: usb 2-4: new high-speed USB device number 38 using xhci_hcd
    Mar 01 18:44:38 learner kernel: usb 2-4: Device not responding to setup address.
    



    
    
    Mar 02 13:34:05 learner kernel: usb 2-4: new high-speed USB device number 45 using xhci_hcd
    Mar 02 13:34:05 learner kernel: usb 2-4: new high-speed USB device number 46 using xhci_hcd
    Mar 02 13:34:05 learner kernel: usb 2-4: New USB device found, idVendor=0bda, idProduct=0129
    Mar 02 13:34:05 learner kernel: usb 2-4: New USB device strings: Mfr=1, Product=2, SerialNumber=3
    Mar 02 13:34:05 learner kernel: usb 2-4: Product: USB2.0-CRW
    Mar 02 13:34:05 learner kernel: usb 2-4: Manufacturer: Generic
    Mar 02 13:34:05 learner kernel: usb 2-4: SerialNumber: 20100201396000000
    Mar 02 13:34:06 learner kernel: usb 2-4: USB disconnect, device number 46
    Mar 02 13:34:16 learner kernel: usb 2-4: new high-speed USB device number 47 using xhci_hcd
    Mar 02 13:34:21 learner kernel: usb 2-4: new high-speed USB device number 48 using xhci_hcd
    Mar 02 13:34:26 learner kernel: usb 2-4: new high-speed USB device number 49 using xhci_hcd
    Mar 02 13:34:32 learner kernel: usb 2-4: new high-speed USB device number 51 using xhci_hcd
    Mar 02 13:34:37 learner kernel: usb 2-4: new high-speed USB device number 52 using xhci_hcd
    Mar 02 13:34:43 learner kernel: usb 2-4: new high-speed USB device number 54 using xhci_hcd
    Mar 02 13:34:43 learner kernel: usb 2-4: new high-speed USB device number 55 using xhci_hcd
    Mar 02 13:34:49 learner kernel: usb 2-4: new high-speed USB device number 57 using xhci_hcd
    Mar 02 13:34:55 learner kernel: usb 2-4: new high-speed USB device number 58 using xhci_hcd
    Mar 02 13:35:00 learner kernel: usb 2-4: new high-speed USB device number 60 using xhci_hcd
    Mar 02 13:35:06 learner kernel: usb 2-4: new high-speed USB device number 61 using xhci_hcd
    Mar 02 13:35:11 learner kernel: usb 2-4: new high-speed USB device number 63 using xhci_hcd
    Mar 02 13:35:17 learner kernel: usb 2-4: new high-speed USB device number 64 using xhci_hcd
    Mar 02 13:35:22 learner kernel: usb 2-4: new high-speed USB device number 65 using xhci_hcd
    Mar 02 13:35:28 learner kernel: usb 2-4: new high-speed USB device number 66 using xhci_hcd
    Mar 02 13:35:33 learner kernel: usb 2-4: new high-speed USB device number 68 using xhci_hcd
    Mar 02 13:35:39 learner kernel: usb 2-4: new high-speed USB device number 69 using xhci_hcd
    Mar 02 13:35:44 learner kernel: usb 2-4: new high-speed USB device number 70 using xhci_hcd
    Mar 02 13:35:50 learner kernel: usb 2-4: new high-speed USB device number 71 using xhci_hcd
    Mar 02 13:35:50 learner kernel: usb 2-4: Device not responding to setup address.
    Mar 02 13:35:50 learner kernel: usb 2-4: Device not responding to setup address.
    Mar 02 13:35:50 learner kernel: usb 2-4: device not accepting address 71, error -71
    Mar 02 13:35:50 learner kernel: usb 2-4: new high-speed USB device number 73 using xhci_hcd
    Mar 02 13:35:51 learner kernel: usb 2-4: new high-speed USB device number 74 using xhci_hcd
    Mar 02 13:35:56 learner kernel: usb 2-4: new high-speed USB device number 75 using xhci_hcd
    Mar 02 13:35:57 learner kernel: usb 2-4: new high-speed USB device number 77 using xhci_hcd
    Mar 02 13:36:03 learner kernel: usb 2-4: new high-speed USB device number 78 using xhci_hcd
    Mar 02 13:36:08 learner kernel: usb 2-4: new high-speed USB device number 79 using xhci_hcd
    Mar 02 13:36:14 learner kernel: usb 2-4: new high-speed USB device number 80 using xhci_hcd
    Mar 02 13:36:20 learner kernel: usb 2-4: new high-speed USB device number 83 using xhci_hcd
    Mar 02 13:36:26 learner kernel: usb 2-4: new high-speed USB device number 86 using xhci_hcd
    



Thanks to the Linux USB maintainers, we tried
[investigating](https://www.spinics.net/lists/linux-usb/msg146092.html) the
issue, which resulted in uncovering other bugs. Unfortunately, this bug was
concluded as a **possible hardware bug**. The only odd bit is that this
machine has a Windows 8.1 copy still lying on the spare partition, where the
issue was not seen at all. It could very well be that it was not a hardware
bug at all, or a hardware bug which had a workaround in the Windows driver.

But, the results of the exercise weren't much useful to me because I use the
machine under the Linux kernel most of the time.



So, this March 2017, with 2 years completion on me purchasing the device, I
was annoyed enough by the bugs. That led me trying out finding other ways to
taming this issue.

Lenovo has some variations of this device. I know that it comes with multiple
options for the storgae and the wifi component. I'm not sure if there are more
differences.

The majority of the devices are connected over the xHCI bus on this machine.
If a single device is faulty, or has faulty hardware; it could screw up the
entire user experience for that machine. Such is my case. Hardware
manufacturers could do a better job if they could provide a means to disable
hardware, for example in the BIOS. HP shipped machines have such an option in
the BIOS where you can disable devices that do not have an important use case
for the user. Good example of such devices are Fingerprint Readers, SD Card
Readers, LOMs and mabye Bluetooth too. At least the latter should apply for
Linux users, as majority of us have an unpleasant time getting Bluetooth to
work out of the box.

But on my Lenovo Yoga, it came with a ridiculous BIOS/UEFI, with very very
limited options for change. Thankfully, they did have an option to set the
booting mode for the device, giving the choices of **Legacy Boot** and
**UEFI.**

Back to the topic, with 2 years of living with the bug, and no clarity on if
and whether it was a hardware bug or a driver bug, it left me with no choice
but to open up the machine.

  
![](/sites/default/files/IMG_20170320_135207_HDR.jpg)Next to the mSATA HDD
sits the additional board, which houses the **Power, USB, Audio In** , and the
**SD Card** reader.



Opening that up, I got the small board. I barely use the SD Card reader, and
given the annoyances I had to suffer because of it, there was no more mercy in
killing that device.

So, next was to unsolder the SD Card reader completely.

![](/sites/default/files/IMG_20170320_144554_HDR.jpg)

Once done, and fitted back into the machine, everything has been working
awesomely great in the last 2 weeks. This entire fix costed me र् 0. So
sometimes, fixing a bug is all that matters.

In the Hindi language, a nice phrase for such a scenario remnids me of the
great [Chanakya](https://en.wikipedia.org/wiki/Chanakya), **"साम, दाम, दंड और
भेद"**.

