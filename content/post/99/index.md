{
    "title": "Secure P2P Sharing",
    "date": "2006-03-30T21:50:00-05:00",
    "lastmod": "2013-01-31T13:34:49-05:00",
    "draft": "false",
    "categories": [
        "Computing",
        "Technology",
        "Virtualization"
    ]
}

First,

 **DISCLAIMER: I 'm not a guru, I'm just a learner. So there might be a chance
that the whole article might be fundamentally wrong.**

I thought of this because I use P2P network a lot.

On my laptop, which is the repository of everything I have, it is necessary
for me to make sure that it is secure enough.

To firewall it I use the single rule from iptables:

 _` **/sbin/iptables -A INPUT -i eth0 -m state --state NEW,INVALID -j DROP**
`_

This command, as I know, helps me keep my machine secure.

The only problem is that with this command most of the P2P applications (I use
gnutella) don't work to the fullest features.

In this article I'll try to explain about how to keep a secure machine along
with a full featured P2P application working.

To accomplish the goal I used UML. For people who aren't aware of UML can
visit it [here.](http://user-mode-linux.sf.net/)

We'll use a UML installation of say 500MB. I won't go into mentioning how to
install UML but a good place to look at is
[Alyz](http://www.alyz.com/uml.xhtml)

Once you have a workng UML installation, we need to make it network aware.

We'll use UML installation OS as the **Guest OS** and the base OS running on
the laptop as the **Host OS.**

Once network support is enabled into the Guest OS, it needs to be able to
access the net.

On the host OS, where say eth0 is your interface to the internet, the
following command will allow access to the Guest OS to the internet.

` **iptables -t nat -A POSTROUTING -s 172.16.1.2 -i eth0 -j MASQUERADE** `

I'm assuming that the ip address for the Guest OS's network interface is
172.16.1.2

Check and see if you can talk to the internet. Also make sure that the
/etc/resolv.conf has proper dns entries.

Cool! So now you have a working Guest OS which can access the internet.

I use [Debian](http://www.debian.org/), so once your Guest OS is setup, use
the following commands:

 **apt-get update; apt-get upgrade;**

So now your UML Debian Installation is complete and up to date.

Install gnutella - ` **apt-get install gtk-gnutella** `

We need more configurations to be done so that gnutella can function properly.
With the present configuration, gnutella won't work full featured because
connections initiated from outside will get dropped because of the firewall
rule we earlier used. To get rid of it, following is one solution:

 **/sbin/iptables -A INPUT -i eth0 -d ! 172.16.1.2 -m state --state
NEW,INVALID -j DROP**

Modify the original firewall rule to the above mentioned rule. This'll allow
all connections, excluding for machine with ip address 172.16.1.2, initiated
from outside to be dropped. This way the Host OS (my laptop) will still be
secure where as the Guest OS (Debian UML Installation) will have full access
to the internet.

Now we need to execute the gnutella program and download our **stuff.** The
ssh program has **X Forwarding** capabilities using which we can execute
applications of remote machines and get thier display on the local machine.

First setup the Guest OS with all required packages for **ssh** , and basic
**X utilities.**

On the Host OS, execute the following command:

 **xhost +Guest-OS-Hostname**

On the Guest OS, execute the following command:

 **export DISPLAY=Host-OS-Hostname:0.0**

In the first command, the Guest OS will be granted access to receive X
server's display.

The second command says the Guest OS to use the specified host for its
display.

Wait! We're still not done.

We need to add the keys to both the hosts so that they really trust eachother.

On the Host OS:

 **xauth list**

It'll give you a list of hosts with a secure key like:

 **wxp-8m8th1s.blr.foo.bar.com:0 MIT-MAGIC-COOKIE-1
de1782496508133705bfdd868dd12fb7debian/unix:0 MIT-MAGIC-COOKIE-1
de1782496508133705bfdd868dd12fb7**

You'll need to copy the relevant host line of your Guest OS and add it to your
Guest OS as follows:

 **xauth add wxp-8m8th1s.blr.foo.bar.com:0 MIT-MAGIC-COOKIE-1
de1782496508133705bfdd868dd12fb7**

Repeat the same on the Guest OS and add the key to the Host OS.

Now ssh into the Guest OS as follows:

 **ssh Guest-OS -X**

Make sure you use the **-X** option. When logged in execute the gnutella
program and you should see the output in your Host OS's desktop.

There's one more point to note.

Your **Debian UML Gust OS** installation is just a 500MB installation. If you
download stuffs like ISO images it would fill up the 500MB file image.

 **User Mode Linux** has an elegant solution to it, **hostfs**

Add the following entry to /etc/fstab in your Guest OS:

 **none /mnt/host_os_tmp hostfs rw,/tmp 0 0**

The above entry in /etc/fstab will mount the Host OS's /tmp folder to
/mnt/host_os_tmp on the Guest OS with read, write permissions. This way you
can make gnutella use /mnt/host_os_tmp as its download folder and use your
actual harddrive for saving data.

Cheers!

    
    
    rrs

