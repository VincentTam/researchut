{
    "title": "Debian SAN",
    "date": "2012-04-07T13:10:00-04:00",
    "lastmod": "2013-01-31T13:42:45-05:00",
    "draft": "false",
    "tags": [
        "debian",
        "SAN"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/state-of-san-in-debian"
}

Many of the SAN components in Debian have me involved. So I think out to give
an update on what the state of SAN would be in Debian Wheezy.



 **Open-iSCSI** : Just uploaded a newer version to experimental which
eventually will crawl into testing. It should fix all release goals for
Wheezy. Please give it a good test

 **iscsitarget** : Same goes here. This one is already in testing. Please
test. It has some bugs reported, all in needinfo, as they haven't been easily
reproducible or reported complete.

 **multipath-tools** : It is in pretty good shape. Some minor bugs. But still,
please test. :-)

 **Open-FCoE** : Being dependent on hardware, for which I couldn't manage
resource commitment, this one is literally untested. It lies in testing,
untested.

 **LIO Target** : The newest target implementation for Linux. It is also
available in testing, has had some minor bugs reported by users, and fixed. If
you use, or anticipate to use it in the Wheezy life cycle, now is the time to
test.

