{
    "title": "Laptop Mode Tools 1.69 Released",
    "date": "2016-03-07T08:20:20-05:00",
    "lastmod": "2016-03-07T09:42:14-05:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools",
        "shiva",
        "Laptop Mode Tools"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-1-69"
}

Today is an auspicious day. For those who know (or follow) the Hindu religion
will be familiar; Today is [Maha Shivaratri![](/sites/default/files/LMT-Maha-
Shivaratri.jpeg)](https://en.wikipedia.org/wiki/Maha_Shivaratri)

On this day, It is great delight for me to be able to release **Laptop Mode
Tools, version 1.69**

This release adds on many bug fixes and some enhancements. There is a new
module (disabled by default) for **cpuhotplug**. The release tarball also
includes a basic **PolicyKit**  file for convenience, that packagers can use
for the Laptop Mode Tools Graphical Configuration Interface. Apart from the
policykit file, the graphical invocation script has been slightly fine tuned
to work under pkexec and sudo. Some defaults have been tuned based on user
requests - This should improve in situations where your **External USB
Mouse/Keyboard** used to suspend after idle time periods.



In January this year, I had the pleasure of meeting Bart Samwel in person at
his office in Amsterdam. For those who don't know, Bart started off Laptop
Mode Tools around 2004, and I took over maintenance around 2008. Meeting in
person has been a delight, especially with the ones you work over email for
years; This is something I cherished last year at Debconf 15 too.

IMPORTANT:-  Until now, Laptop Mode Tools project was hosted on Bart's
webserver. Now, as you read, the homepage and mailing lists have changed. I'd
urge all users to subscribe to the new mailing list and update their
bookmarks.

Homepage: https://github.com/rickysarraf/laptop-mode-tools/wiki

Mailing List: https://groups.google.com/d/forum/laptop-mode-tools

Note: For users who are not comfortable with creating a google a/c for mailing
list subscription, you should still be able to subscribe with your personal
email address. Please follow the steps in the mentioned homepage.

Since last couple releases, I've also been providing RPM packages for Opensuse
Tumbleweed and Fedora. The same should be available on the github release
page. The Debian package will follow shortly in the Debian repository.



Thank you and a Happy Maha Shivaratri. _**Har Har Mahadev**_.

    
    
    1.69 - Mon Mar  7 17:44:42 IST 2016
        * Wait for all forked modules to complete
        * Add new module: cputhotplug
        * CPU online/offine is reverse here
        * Fix shell syntax
        * Install policykit file
        * Detach polling daemon from main process
        * Do NOT touch speed if throttling is not set
        * Restore to MAX speed when back to AC Power
        * Fix manpage about DISABLE_ETHERNET_ON_BATTERY setting
        * Update documentation about ENABLE_LAPTOP_MODE_ON_AC setting
        * Change powersaving default for USB class devices
        * Drop usbhid from default (black)list
        * Add usb keyboard driver to the list default list
        * Be consistent with passing args to LMT from different invokers
        * Honor device plug/unplug events on a per device basis;
          like how Chromium initially submitted this patch
        * Also be consistent with option in the event parser
        * Update links in README.md
        * Update new github homepage location
        * Add lmt-config-gui shell script



![](/sites/default/files/pkexec-lmt.png)

 ![](/sites/default/files/lmt-sudo.png)

