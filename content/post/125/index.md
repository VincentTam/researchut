{
    "title": "Is it the way you blog ?",
    "date": "2005-06-26T23:51:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "General"
    ]
}

Today, blogging is one of the hottest topic.

Day in day out people are blogging. Statistics on the net show that every 7
seconds an article is posted to a blog.

But what exactly is a blog ?

A search at my laptop's local dictionary server yielded two definitions :

rrs@laptop:~ $ dict blog

2 definitions found

From WordNet ® 2.0 (August 2003) [wn]:

blog n : a shared on-line journal where people can post diary entries about
their personal experiences and hobbies [syn: {web log}]

From Jargon File (4.4.4, 14 Aug 2003) [jargon]:

blog n. [common] Short for weblog, an on-line web-zine or diary (usually with
facilities for reader comments and discussion threads) made accessible through
the World Wide Web. This term is widespread and readily forms derivatives, of
which the best known may be {blogosphere}.  

So in desi terms I understand it as a diary in electronic form with an add-on
of **commenting** option available. I find it awkward looking at websites
where people have converted the whole site into a blog.

In my opinion it's better to follow the good old Unix philosophy - **KISS -
"Keep it simple, stupid"**

