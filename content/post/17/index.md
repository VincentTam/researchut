{
    "title": "Debian PS3 Installation",
    "date": "2009-12-31T01:10:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "ps3",
        "playstation",
        "Other OS"
    ],
    "categories": [
        "Debian-Pages"
    ]
}

It is holiday time and so finally time to installing Debian onto the Sony PS3
box. The installation was smooth as most of the stuff is already documented.
The Debian Installer has all the support needed, to install onto the Sony PS3.
As of now the only thing not working perfectly is the boot loader. It seems to
be taking default is the kernel that was booted last. Even if I change the
default in /etc/kboot.conf, it still boots into the kernel that was booted
last time.

