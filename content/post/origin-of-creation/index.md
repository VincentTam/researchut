{
    "title": "Origin Of Creation",
    "date": "2016-01-10T12:33:25-05:00",
    "lastmod": "2016-01-10T12:33:25-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "hinduism",
        "religion",
        "origin"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/origin-of-creation"
}

[![url5](/sites/default/files/url5.jpg)](https://vibhormahajan.files.wordpress.com/2012/08/url5.jpg)

The basic element of manifest and the un-manifest, Power Eternal started the
creation in manifested Lord Brahma form. First, water came into existence. By
the will of power eternal, Lord Shiva, a divinity got vested in the water and
was called "Narayana" (Water borne). Then an egg appeared in the water and out
of it came Lord Brahma who split the egg in two halves horizontally. The upper
half became celestial world and the lower half the base. The space in between
was turned into sky and fourteen domains by Lord Brahma.

[![neutron-star-magnetar_1118_610x343](/sites/default/files/neutron-star-
magnetar_1118_610x343.jpg)](https://vibhormahajan.files.wordpress.com/2012/08
/neutron-star-magnetar_1118_610x343.jpg)

He created earth on water. Ten directions came into being in the sky. Then
creator made elements of mind, speech, angst, beauty and love. A seven sage
group made up of Marichi, Atri, Angira, Pulastya, Pulaha, Ritu and Vashishtha
materialised as willed by the mind of creator. The sages were the material and
sources for the creation of Purans.

Then, Lord Brahma revealed four Vedas, the book form repositories of wisdom
and knowledge of religious exercises. He worshipped Deity Supreme before
producing the gods from his chest, humans from his thighs and demons from
lower parts. He rested for a long time to assess his creations. But it was a
static world with no scope of evolution. To correct this fault he created body
and its nature. More creations followed in that format. It was also flawed
being non-generative and non-re-creative.

Lord Brahma created a host of his mind born sons who came to be known as
Manus. In the same way Manus created more Manus as their own lines of sages,
seers and holy ones also came into existence in large numbers. Creator and
Manus shaped various other creatures, four legged, two legged, water borne and
winged ones. But it still made no sense. Lord Brahma and his Manus were
creating like potters make pots and had no generative power of their own. The
created world was stagnating. There was no variety, no evolution and no self
generation. It frustrated Lord Brahma.

[![Lord Brahma](/sites/default/files/lord-
brahma.jpg)](https://vibhormahajan.files.wordpress.com/2012/08/lord-
brahma.jpg)

He Prayed to Lord Shiva to guide him into right and lively kind of creation
that did not stagnate. Deity Supreme answered the prayer after a long penance
of Lord Brahma by manifesting in his Ardhanareeshwara form which was made up
of "Half-man + Half-woman" vertically. In this way, he revealed that self-
generating and evolutionary creation was possible only through gender based
male-female pairing. Lord Brahma got the message.

[![androgyny-Ardhanarisvara](/sites/default/files/androgyny-
ardhanarisvara.jpg)](https://vibhormahajan.files.wordpress.com/2012/08
/androgyny-ardhanarisvara.jpg)

Then Lord Brahma's mind projected gender based Manu and Shatroopa pair. Their
mating produced sexually reproductive Manus who bred their own lines of males
and females. It led to the explosion of the life forms by evolution, sons,
daughters, couplings, species and dynastic. King Uttanpada was born who sired
the great faithful Dhruva.

Dhruva had two sons, Pushti and Dhanya. They bred Ripu, Ripunjaya, Vipra,
Nrikala, Vrisha and Teja to further the blood line in Chaksuta, Varuna, Bena,
Prithu (The incarnation of Lord Vishnu),  Vijitashwa, Prajapati, Haryavya and
his ten thousand sons who became ascetic upon hearing the sermon of Sage
Narada who said the mundane life was not worth living being full of woes and
miseries. Angered Daksha (Prajapati) put a curse on Narada to be foot loose
and not to stay at one place for long. Then, Daksha sired thirteen girls who
were married to Sages, Kashyapa, Angira, Krishnashwa and Chandra. As sexual
wave spread, Daksha sired more girls said to be 28 in total. The earlier
married ones also got new wives besides the girls getting new husband. They
all produced a number of deity figures like Vishwa, Sadhya, Marutwana, eight
Vasus, twelve Bhanus, Mahurtaja, Ghosa, Naga, Prithvi etc. Then arrived
Sankalp, Ava, Dhurva, Soma, Dhara, Anila, Pratyusha and Prabhasha. Sankalpa
was their mother.

Prabha produced Vishwakarma Prajapati. He was a craftsman who crafted various
masterpieces for the gods like aerial craft, homes, ornaments, embellishments
etc. He became deity of craftsmen and skilled workers. Sarupa produced Rudras
and they all multiplied into millions of Rudras but eleven are main Rudras
namely, Raivata, Aja, Bheema, Bhava, Ugra, Bhama, Vrisha, Kapi, Ankpada,
Ahirbungna, Bahurupa and Mahana. They are masters of the world.

In Chakshusha's blood line Vena was born who was an evil character. He
harassed all in many ways. So much so that even sages fled to the forests. At
last the seers and sages worked up the death of Vena by invoking a killer
mantra. When his mother wept bitterly, the sages of Saraswata line churned the
dead Vena's right hand and out of it Prithu was born. Prithu became the first
sole king of the earth. He turned earth into a cow and made it yield the milk
of herbal plants, food grain crops, fruit bearing trees, vegetable plants etc.
He had four sons. One of them sired ten enunciate sons called Prachatas. They
made penance to propitiate Lord Shiva for thousands of years. Lord booned them
power to save earth from over vegetation. Prachetas returned to normal life
and married Anubhuti. the daughter of Chandra.

 **Kashyapa Bloodline**

Aditi, Diti, Sursa, Arishta, Ila, Dhanu, Surabhi, Vinita, Tabra, Krodha,
Voshi, Kadru etc. were the wives of Sage Kashyapa. Aditi gave birth to twelve
deities of Tushita order. She was the mother of Lord Vishnu (manifested) and
Indra also. Diti produced giants (demonic) and Dhanu bore demons. Vinita
produced asura, garuda and other species of birds and animals. Sursa became
mother of reptiles. That is why she is known as Nagmata also. Her sons include
even the divine serpents like Adishesha, Vasuki and Takshaka. Krodha produced
offspring with fangs and claws. Surabhi gave birth to rabbits, hare and
buffaloes. Ila was the mother of trees, plants, creepers and other greens. One
of them produced females only who became the celestial beauties known as
apsaras. Kadruva gave birth to pythons, dragons and boas. Arishta gave birth
to men-o-snake.

The offspring's of Khasha made up the orders of Yakshas and Rakshasas. Tabra
produced eight daughters in Keki, Syeni Bhasi, Sujrevi, Shuki, Gridharika,
Ashani and valooki besides sons. Further on Keki bore crows, Syeni bore hawks
and kites and Bhasi bore ducks and other game birds. Gridharika produced
eagles and vultures. Ashmi begot camels, horses and all kinds of asses.

Thus the wives of Kashyapa filled up the world with greens, plants, forests,
birds, animals, reptiles, insects and other living creatures. The world was
came to be known as Kashyapi.

