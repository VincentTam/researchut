{
    "title": "Who says GUI Programming in shell is difficult ?",
    "date": "2005-07-07T05:31:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "tags": [
        "shell"
    ],
    "categories": [
        "Programming"
    ]
}

Today, I wanted a quick utility to help me enter passwords for encfs during
mount.  
I was looking for something plain, simple and userfriendly.  
At the end I ended up to use the shell for it.

Yes, using dialog/Xdialog you can be a GUI programmer in minutes.

If the text is scrambled, you can download the script
[here.](http://www.researchut.com/repository/encfsmount)

Cheers!

![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

#!/bin/bash

  1. encfsmount - A simple frontend for encfs
  2. (C) Ritesh Raj Sarraf July 2005

  1. Available only under the GPL

PATH="/bin:/sbin:/usr/bin:/usr/sbin:/home/rrs/bin"  
export PATH

XDIALOG_HIGH_DIALOG_COMPAT=1  
export XDIALOG_HIGH_DIALOG_COMPAT

  1. && exec sudo "$0" "$@"

ASKPASS=`which ssh-askpass`;  
CRYPT="/mnt/windoze/.linuxrc";  
TARGET="/var/tmp/.linuxrc";  
FUSERMOUNT=`which fusermount`;  
ENCFS=`which encfs`;  
ARGS="-u";

#set -vx

DIALOG=`which dialog`;  
[ -n "$DISPLAY" ] && [ -x /usr/bin/Xdialog ] && DIALOG="Xdialog"

mount()  
{ # This is one good way to use dialog to accept passwords # But for some
reason encfs isn't able to read the password # supplied by dialog # Hence, I'm
using ssh-askpass to get the password #$ENCFS $CRYPT $TMP --extpass=`$DIALOG
--title "Password" --password --inputbox "Enter Password" 18 45 2`;

# This uses ssh-askpass $ENCFS $CRYPT $TARGET --extpass=$ASKPASS;

}

check_mount()  
{ # Since encfs isn't returning good exit status # I'm using this hack to
figure out if the mount # passed or failed. grep -i encfs /etc/mtab;
retval=$?;

case $retval in 0) $DIALOG --wrap --title "Information"\ --msgbox
"Successfully Mounted!" 10 41;; 1) $DIALOG --wrap --title "Information"\
--msgbox "Mount Failed!" 10 41;;  
esac  
}

prelim_mount_check()  
{ grep -i encfs /etc/mtab; retval=$?;

case $retval in 0) $DIALOG --wrap --title "Caution"\ --yesno "ALREADY
MOUNTED!\nDo you want to umount ?" 0 0

case $? in 0) umount; esac;; 1) #$DIALOG --wrap --title "Information"\
#--yesno "Do you want to mount crypt ?"\ #case $? in # 0) mount; check_mount;;
#esac;; esac }

umount()  
{ $FUSERMOUNT -u $TARGET;  
}

ask_passwd()  
{ $DIALOG --wrap --title "Password"\ --help "Contact Author Ritesh Raj
Sarraf\nEmail: rrs@researchut.com"\ --inputbox "Please enter your password" 18
45 2 > /tmp/passwd.$$;

retval=$?;  
$INPUT=`cat /tmp/passwd.$$`; #rm -f /tmp/passwd.$$;

case $retval in 0) mount;; #echo "Password is '$input'";; 1) echo
"Cancelled";; 255) echo "Box Closed";;  
esac  
}

main()  
{ $DIALOG --wrap --title "Question"\ --help "Contact Author Ritesh Raj
Sarraf\nEmail: rrs@researchut.com"\ --yesno "Do you want to mount your
encrypted filesystem ?" 0 0

case $? in 0) prelim_mount_check;; 255) $DIALOG --wrap --title "Aborted"\
--infobox "Cancelled"  
esac  
}

main

