{
    "title": "Why Debian",
    "date": "2004-04-25T02:55:40-05:00",
    "lastmod": "2011-03-12T02:55:40-05:00",
    "draft": "false",
    "tags": [
        "debian",
        "ILUGD",
        "Talks"
    ],
    "categories": [
        "Debian-Pages",
        "Computing"
    ],
    "url": "/post/why-debian"
}

The following talk is all about [Debian](http://www.debian.org/) for which I
was made to speak at the [ILUGD](http://www.linux-delhi.org/) meet held on the
18th of April 2004. Most of the references in this talk has been taken from
[Manoj Srivastava's](mailto:%20srivasta@debian.org) (Lead Debian Developer)
talk.

  
/* Begins  

 ![Debian OpenLogo](http://www.debian.org/logos/openlogo-100.png)  


  
Why Debian  
  
Debian -- Philosophy, Merits and Key Features  
  



Philosophy ** **

Philosophy is the most durable differentiating criterion between the operating
systems we are considering. Performance numbers change. Ease of use,
reliability, availability of software -- all these characteristics change over
time, and you have to go out and re-evaluate them over time.

But philosophy doesn’t change.

I must confess that philosophy and community is what lead me to Debian; and I
think these are still the most important criteria, and are often underrated.

  
 Why is free software a good thing ?

The popular answer seems to be:-

  * because it is cool,
  * because it is zero cost.
  * because it gives you a geekish image.

The motivations of the authors also are varied, but the coin that they get
paid in is often recognition, acclaim in the peer group, or experience that
can be traded in in the work place.

But all this is missing the critical motto of why Free Software was designed.
I’d like to give an example to the manner in which academic research is
conducted. If researchers were doomed to reinvent the wheel, handle, brakes,
axle only; then everything beyond that and other innovations (may be the
motorbike) progress in the research community would be stunted. People start
in research by doing literature searches, looking for interesting
investigations, and perhaps correlating unrelated papers, building on the
ideas and techniques of other researchers in the field. The secrecy shrouding
research in most labs exists only till the moment of publication -- and then
people share their techniques, and ideas, and results -- indeed,
reproducibility is a major criteria of success.  
                    Contrast this with proprietary software, where mostly all begins again -- from scratch.

People could soar and grow if only we could freely share and build upon the
ideas and labours of others. This would lower the time, effort, and cost of
innovation, allow for best practices and design patterns to develop and
mature, and reduce the grunt programming that raises the barrier to developing
solutions in house.

====  
We just have to ensure that the incentive for achievement still exists (and it
need not be purely a profit motive).  
====

This belief leads us to choose the GPL, and free software foundation view of
things, as opposed to the BSD licence, which are also free software licenses,
and at the end lead eventually to choosing Debian. In my personal opinion, the
BSD license has been more about personal pride in writing free software, with
no care as to where the software went;

Debian is an exercise in community barn building; together, we can achieve far
more than we could on our own. The Debian social contract is an important
factor in my choice of Debian, with its blend of commitment to free software.

Features:-

 What leads an average computer user to a good OS ?

  * Ease of use
  * Reliability
  * Availability of software (Software Packages)
  * Cost
  * Utility and Usability

Utility, of course, depends on what our goals/requirements are.

There is more to an operating system than a kernel with a hodge-podge of
software thrown on the top -- systems integration is a topic usually given
short shift when discussing the merits of a system. But a well-integrated
system -- where each piece dovetails with and accommodates other parts of the
system -- has greatly increased utility over the alternative.Debian, in my
experience, and the experience of a number of other users, is the best
integrated OS out there. Debian packages trace their relationships to each
other not merely through a flat dependency/conflicts mechanism, but a richer
set of nuanced relationships –

  * Pre dependencies,
  * Oordinary dependencies,
  * Recommendations,
  * Suggestions,
  * Conflicts, and 
  * Enhanced relationships.

Apart from this, packages are categorized according to priority (Essential
through extra), and their function. This richness of the relationships, of
which the packaging system is aware and pays attention to, indicates the level
at which packages fit in with each other. Debian is developed by about 1000
volunteers (Most of which are SysAdmins). That means that every developer is
free to maintain programs he is interested in or he needs for his special
tasks in real life. That's why Debian is able to cover different fields of
specializations -- its developers just want to solve their own special
problems. This broad focus is different from commercial distributions which
just try to cover mainstream tasks.

It is said that Debian machines at work:-

  * Take less hand holding, 
  * Are easier to update,
  * And just plain don't break as often as the Red Hat and Mandrake boxes.

One of the reasons for selecting Debian over other distributions is its sheer
size of the project which strongly suggests that Debian won't suddenly
disappear and one is suddenly left without any support. Debian can't go
bankrupt. Its social contract doesn't allow the project to abruptly decide not
to support non enterprise versions of the distribution. I do not want my OS to
be held hostage to anyones business plans!

You can fine-tune the degree of risk you want to take, since Debian has three
separate releases:

  * Stable,  \-- Woody
  * Testing, -- Sarge and 
  * Unstable. – Sid

On some of the machines people run `stable'. Some of the other systems
(individual work-stations) run various combinations of testing/unstable. (Note
that there are no security updates for testing). What's great is the ability
to make finely graded decisions for different machines serving different
functions. But even the more adventurous choices are solid enough that they
virtually never break. And ` _stable'_  just never breaks ;-).

Large number of Supported Architectures.

Supported architectures are:

  * Intel x86/ IA-32 (i386)
  * Motorola 68K (m68k)
  * Sun SPARC (sparc)
  * Alpha (alpha)
  * Motorola/IBM Power PC (powerpc)
  * ARM (arm)
  * MIPS CPUs (mips and mipsel)
  * HP PA-RISC (hppa)
  * IA-64 (ia64)
  * S/390 (s390)
  * Debian GNU/Hurd (i386)
  * Debian GNU/NetBSD (netbsd-i386 and netbsd-aplha)
  * Debian GNU/FreeBSD (freebsd-i386)





Debian provides a great deal of feedback upstream. For example, the XFree86
project does not itself maintain or debug X on all the architecture Debian
supports -- it relies on Debian for that. This attention to detail is hard for
any other Linux distribution to match.

 **  
Is it just apt-get ?**

People often say how they came to Debian because of apt-get, or that apt is
the killer app for Debian. But apt-get is not what makes the experience so
great: apt-get is a feature readily reproduced (and, in my opinion, never
equalled), by other distributions -- call it urpmi, apt4rpm, yum, or what have
you. The differentiating factor is  **Debian policy** , and the stringent
package format QA process (look at things like apt-listchanges, apt-list-bugs,
dpkg-builddeps, pbuilder, pbuilder-uml -- none of which could be implemented
so readily lacking a policy (imagine listchangelog without a robust changelog
format)). It is really really easy to install software on a Debian box.

So the killer app is really Debian policy, the security team, the formal bug
priority mechanisms, and the policy about bugs  **(namely: any binary without
a man page is an automatic bug report. Any interaction with the user not using
debconf is a bug).**

 **  
** A small reading from the Wiki Page of “Why Debian Rocks”:

 / *

This is the crux, the narthex, the throbbing heart of Debian and what makes it
so utterly superior to all other operating systems. Policy is defined. It is
clear. It is enforced through the tools you use every day. When you issue
`apt-get install foo`, you're not just installing software. You're enforcing
policy - and that policy's objective is to give you the best possible system.  
                               What Policy defines are the bounds of  _Debian_ , not your own actions on the system. Policy states what parts of the system the package management system can change, and what it can't, how to handle configuration files, etc. By limiting the scope of the distribution in this way, it's possible for the system administrator to make modifications outside the area without fear that Debian packages will affect these changes. In essence, Policy introduces a new class of bugs, policy bugs. Policy bugs are release-critical -- a package which violates policy will not be included in the official stable Debian release.

  */  

The evaluation process each package has to undergo in the unstable
distribution before it makes it into testing adds to the quality of the
finished product. Once a package has not shown any important problem for a
certain time(14 days) period it goes into the testing distribution. This
distribution is the release candidate for the future stable distribution which
is released only when all release critical bugs are resolved. This careful
testing process is the reason why Debian has a longer release cycle than other
distributions. However, in terms of stability this is an advantage. (Note: RH
Enterprise Linux is apparently shooting for 12 - 24 month release cycles.
Closer to what Debian's historically had.)



The fact that Debian supports as many architectures as it does also feeds into
the quality of packages: Porting software often uncovers flaws in the
underlying code. Add to the fact that all software in Debian goes though 10 or
so automatic build daemons, and needs be bug free when building on these
different environments, requires that the build and install scripts be very
robust, and requires a very strict tracking of build time dependencies. Add
source archive mirrors and version tracking, and you have a fairly robust
system (snapshot.debian.net provides for easy rollbacks) .The Debian bug
tracking system is a key to the quality of the distribution. Since releases
are linked to the numbers of release critical bugs in the system, it ensures
that the quality of the release is better than any proprietary UNIX. The
Release Manager is fairly ruthless about throwing out any non essential
package with RC bugs if they do not get fixed -- or delaying the release if it
is a critical package with the bug. Compared to commercial Linux
distributions, Debian has far higher developer to package ratios. Added to the
lack of business cycle driven deadlines, Debian tends to do things right,
rather than do things to get a new version out in time for Christmas.  

 **  
Features Set and Selection of Packages**

Debian has over 10000 packages now(13000 + in SID). The chances are that
anything you need is already packaged and integrated into the system, with a
person dedicated to keeping it (and a small number of other packages) upto
date, integrated, and bug free.

Debian has a huge internationalization effort, translating not only the
documentation but also the configuration and install scripts (all debconf
interaction can be fully internationalized). It helps to have a massively
geographically distributed community -- there are native speakers in tonnes of
languages.The internationalization effort in Debian matches that for Gnome and
KDE.

Other notables, to pay a little attention to, are:

  * The Debian documentation project,
  * Alioth,
  * Debian installer,
  * Debian CD,
  * Lintian, and
  * The package tracking system.

 Some other things which will keep me using Debian until they're supported by
something else:

  * debconf and the ability to prepopulate the database
  * make-kpkg with all the install-time prompts turned off
  * /usr/share/doc/{Changelog.Debian,changelog,copyright,README.Debian
  * apt and friends
  * Large package database
  * It's policy of Free Software

 **  
Debian VS BSD**

The BSD kernels, from all accounts, seem to be stabler, and of better quality
than Linux kernels seem to be. On the flip side, Linux kernels more feature
rich, and the quality has improved significantly, seem to perform much better,
and better hardware support than the BSD kernels do. Indeed, I've heard
comments that when it comes to driver support, the BSD's are where Linux was 5
years ago. Personally, the supposed added bugginess of the Linux kernels have
not exceeded my threshold of acceptability. And, overall, I don't think that a
Debian box feels any less robust and stable than, say, a FreeBSD box. Of
course, the recent spate of holes in Linux kernels are beginning to strain
that. (However, we should keep in mind that having more features is a
contributry factor: the two latest holes were in the mremap(2) call that is
not available for any of the *BSD.) ** **

Of course, [Debian Gnu/FreeBSD](http://www.debian.org/ports) may provide the
best of both worlds.  

 **  
Maintainence and Administration**

Upgrades have been said to be the killer advantage for Debian. More than most
other OS's, the network is the distribution and upgrade mechanism for Debian.
Policy, the thought that has gone into the maintainer scripts, and the ways in
which they can be called, the full topographical sorting over the dependency
web done by apt and friends, all work together to ensure that upgrades in
place work smoothly. Reinstalls are not unheard of in an recommended BSD
upgrade path (Since 2.8 or 2.9, OpenBSD said at least two times to i386 users
"upgrade not supported / not recommended, do a fresh install").

This ease of upgrades also plays into security of the system; security
upgrades are far more convenient on Debian than they are on other systems,
thanks to the Security team. For us mere mortals not on vendor-sec, having
security.debian.org in our sources list ensures that our boxes get updated
conveniently, and quickly, after any exploit is made public -- since the
security team was already working on a fix before the details went public.
This means that systems get updated in minutes, whereas the recommended way to
do an upgrade on a BSD OS involves recompiling the entire system (at least,
the "world").

Debian attempts to ensure smooth upgrades skipping a major release - which is
not something that I have seen supported elsewhere. I keep coming back to
quality of packaging.  
Even downgrades are possible. Experience and talks show that Debian can be
downgraded to a previous release too. But isn't recommended/Encouraged anyway.

Administering Debian is the primary reason most people stay with it. I know no
other distribution where you can type in apt-get install sendmail, and walk
away with a fully functional mail server, complete with SASL and TLS, fully
configured, complete with certificates. All administration can be done over
SSH given only dialup speeds.

The Debian guarantee that user changes to configuration files shall be
preserved, and that all configuration files shall live in /etc (as opposed to
being all over the file system) makes for easier backups.Debian is compliant
with the FHS, and LSB compliance is a release goal. The distributed nature of
Debian development and distribution makes it really easy to set up a separate
repository of custom packages that can then be distributed in house; and the
policy and build mechanisms ensure that third parties can build the system
just as easily in a reproducible fashion.



 **  
Portability and Hardware Support.**

Linux tends to support more of the esoteric hardware than BSD does. Whether
that is a problem, depends on your needs. Support for the high quality
hardware is mostly the same. IBM's assurance of Linux support on all their
hardware, and that of HP, is also an advantage for Linux. Multiple journaling
file systems that have come into the Linux kernel recently are also a vital
addon. For desktop, the killer factor is drivers. And Linux leaves all the
other X86 Unixes behind by a mile. When it comes to portability, NetBSD is
supposed to be the byword. I googled to find out, what is suported by NetBSD,
and Debian: I found that debian supports ibm s/390 (IBM) and ia64, while
NetBSD has support for sun2 (m68010), PC532 (whatever that is), and VAX. Note
that what NetBSD call architectures are often labelled sub-architectures by
Debian, and thus do not count in the 11 supported architecture count.

 **  
SOURCE BUILDS**

There are a lot of things told about the ports mechanism of BSD, and the
portage systems of gentoo. I have also heard about how people have problems
actually getting things to compile in the ports system. Apart from the fact
that compiling everything rapidly gets old.

It is not as if you can't do a port like auto build of Debian -- there are
auto-builders on 11 architectures that do that, continuously, every single day
-- the question is why would one want to? I have yet to see a single,
replicable test demonstrating any palpable performance improvement by local,
tailored optimized compilations -- and certainly none that justifies, in my
eyes, the time spent tweaking and build the software all over.

Someone said that when they were younger and felt like playing a prank they
would adjust some meaningless parameters on someone's computer and tell them
"this will make it run about 5% faster, but you probably won't notice it".
With such a challenge they usually responded by becoming totally convinced
that their machines had been improved considerably and that they could feel
the 5% difference!

Conventional wisdom seems to indicate overall system performance increases are
less than 1%. Specific programs can benefit greatly, though, and you can
always tweak a critical app for your environment in Debian. Whatever time is
saved by running an optimized system is more than compensated for by the time
spent building the system, and building upgrades of the system (I've heard of
people running doing their daily update in the background while doing other
things in the foreground.)

Not to mention how integration suffers by not having a central location where
interoperability of the pieces can be ever tested well, since every system
would differ wildly from the reference.

A source build system is also far more problematic when it comes to major
upgrades -- There are anecdotal evidence of it not being as safe and sane as
the Debian upgrade mechanisms.

Anyway, if we do want to build packages from source on Debian, we can use:

  *  **apt-get source –b packagename,**
  *  **apt-src, or any of a number of tools**.

The real point here is that Gentoo is a distro for hobbyists and hard-core
linux users, who can spare the time building their apps. I know Gentoo also
provides pre compiled binaries -- but does that not defeat their supposed
advantage? For an enterprise environment where downtime does cost money this
is simply inadmissible and Debian provides the best solution. Those of you who
administer more than a handful machines can really appreciate how convenient
it is to be able to issue ` **apt-get update && apt-get upgrade**` at once
instead of having to go downloading, configuring, compiling and installing
software machine per machine, without any sort of automated help ( I am not
completely doing justice to emerge / portage here, but the point is clear, I
hope ). I can emphasize this enough: for "serious"/production usage, binary
distros are the best and only viable solution; Amongst them, Debian ( not only
because of APT but also because of all the hard work done by Debian Developers
to ensure correctness of the packaging ) is the best [I have tried SuSE,
RedHat and Mandrake, and I wouldn't prefer going back ]



 **Security And Reliability**

There is always a trade off between security and convenience -- the ultimately
secure computer is one that is never turned on. Secure, but not very useful.
You have to decide where your comfort zone lies.

What does one think of when one says Security and Unix like OS?  
    OpenBSD, with some justification. It is audited and has the small size, small system requirments AND the pure text based install. If you stick to the core install, you get an audited system, with no services turned on by default and an assurance that there are no holes in the default install that can lead to a remote root compromise. However, you tend to end up with old software, and the default install really does very little. Most people agree that the secure and audited portion of OpenBSD does not provide all the software they require. Also, OpenBSD's performance numbers are, umm, poor, compared to SELinux on a 2.6.3 kernel.

OpenBSD's secure reputation is justified - but only when you know the project,
when you are familiar with what does it really cover. OpenBSD may be a great
firewall, maybe even mail or static Web server - As long as you keep out of
the ports tree, you do have an audited, security-conscious system. The OpenBSD
userland ports break more often than stable Debian -- but, in OpenBSD, ports
are officialy not part of the system, and should a security problem appear in
one of them, you are on your own.



The Debian GNU/Linux distribution has a strong focus on security and
stability. We have an Security team, automated build systems to help the
security team quickly build versions across all the architectures that are
supported, and policy geared towards those goals. Debian handles binary
package distribution much better. One can have his own aptable archive and
feed all productive servers from it, using Debian's native apt mechanisms.Even
without SELinux, I find the rock solid stability of Debian stable, with the
peace of mind that comes from back ported security fixes provided by the
Security team, very persuasive. It is easy for an untrained recipient to keep
up to date with security; and reduces the likelihood of compromise. This is
very important in a commercial environment with a large number of computers,
where is it important that the software NOT be upgraded every few months.  

  
Latest Development In Debian



Most of the complains that I've heard about Debian, are from the newbies
complaing about it's installer. The hurdles that most of the people feel is,
Installing Debian. The blue screened, console based installer seems ultra
technical and ugly to them. The installer could be an issue upto some extent,
but I think, SPECIFICALLY to newbies. Experience users often find the
installer quite easy and simple to use. It's just the trend of using a GUI
based fancy installer that has landed up into the mind of the people
resembling Debian as an ultra technical GNU/Linux distribution. The next -
generation Debian Installer, scheduled to ship with Debian Sarge promises to
fulfill many of the problems for newbies. Also the anaconda installer from Red
Hat has been ported to Debian and can be found at Progeny.

 **  
Conclusion**

There is no other OS or distribution that I know of which has just this mix of
properties (ease of maintenance, affordability, stability, size,
customizability, strong support). For the most part, I do not want to tinker
with and Debug my workstation, I want to get my job done, easily, safely, and
with minimal concern about the infrastructure I use. Debian helps me
accomplish that.And that's still the primary reason I use it today, from a
technical standpoint. Software installation and upgrade. The packages are top-
notch, they as a rule install and upgrade perfectly. Software maintenance is
still a really large part of any sysadmin's job, and with Debian it's simply
trivial. It's a non-issue. Don't even bring it up when talking about any
problems with Debian, it's not worth the effort.

Ends */  

