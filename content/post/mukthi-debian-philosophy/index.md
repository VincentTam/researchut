{
    "title": "Mukthi 11.04 - Free Software/Debian - Philosophy, Design, Merits",
    "date": "2011-05-07T11:56:15-04:00",
    "lastmod": "2013-01-31T13:51:11-05:00",
    "draft": "false",
    "tags": [
        "debian",
        "Talks",
        "Mukthi 11.04",
        "MSRIT",
        "Free Software"
    ],
    "categories": [
        "Debian-Pages",
        "Debian-Blog"
    ],
    "url": "/blog/mukthi-debian-philosophy"
}

I was invited to talk at the [Mukthi 11.04](http://mukthi.vrglinug.org/) event
hosted at the **M. S. Ramaiah Institute of Technology**.

I talked about **Free Software/Debian - Philosophy, Design, Merits**. The
focus was more on the philosophical aspects of why Free Software/Debian. The
slides are available
[here](http://people.debian.org/~rrs/slides/MSRIT%20-%20Mukthi%202011.pdf).

