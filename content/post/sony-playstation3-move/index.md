{
    "title": "Console Gaming - Sony Playstation 3",
    "date": "2011-07-06T02:24:21-04:00",
    "lastmod": "2014-11-12T06:13:14-05:00",
    "draft": "false",
    "tags": [
        "sony",
        "ps3",
        "playstation",
        "move",
        "the fight",
        "sports champions",
        "game reviews"
    ],
    "categories": [
        "Debian-Blog",
        "Fun",
        "Computing"
    ],
    "url": "/blog/sony-playstation3-move"
}

I have been thinking of this topic for some time. I am a happy owner of the
old **Phat PS3 Model.** At one time, I use to do a fair amount of gaming on my
computers. So when Sony marketed the PlayStation 3 product, it was cool to see
a machine that did:

  1. Gaming
  2. Movies, Music, Pictures
  3. Blu-Ray
  4. Ran Linux

I convinced myself to buy a PlayStation 3, that I'd use it for playing with
Linux apart from gaming. The regular gaming did not excite any more, perhaps
it had to do with the limitation of time. On running Debian on it, was when I
realized that the Linux support was merely a joke. The Linux support included
running a Linux distribution in a VM with not much access to the full hardware
resources. **Not a Great Experience**

Soon, as many would be aware, Sony did the meanest thing. They pulled off the
Linux support citing security concerns. To ensure further updates, most users
were left with no choice but to update the firmware. This was **Not A Good
Thing** as they had advertised the Linux feature during marketing of the
product. Losing the Linux support made the PlayStation pile up more dust, as I
am an occasional gamer.

But then came the good thing. Sony announced the **PlayStation Move** product,
its answer to the Motion Gaming. This is the best thing they did and they did
it good. The PS Move product was well priced and is an awesome product. The
level of precision in the PS Move is told to be the best by most of the
reviewers that I follow.

The 2 games that make the PS Move shine are **[Sports
Champions](http://us.playstation.com/games-and-media/games/sports-ps3.html)**
and **[The Fight.](http://us.playstation.com/games-and-media/games/the-fight-
lights-out-ps3.html)**

The _Sports Champions_ demo came bundled with the PS Move kit. The disc houses
5 - 6 games on it of which Table Tennis, Sword Fight and Archery are very
good.

![Sports Champions](/sites/default/files/Sports_Champions.png)

The good part about these games is that they start off with easy levels but
eventually (the Champions Cup) get tougher. Playing the Champions Cup level is
going to be both, Fun and Tiring. It is surprising that this game was given
just a [7.5
rating](http://asia.gamespot.com/ps3/sports/sportschampions/index.html), I
think it deserves a 10/10
![Cool](/site/sites/all/libraries/tinymce/jscripts/tiny_mce/plugins/emotions/img
/smiley-cool.gif) The folks at [Zindagi Games](http://www.zindagigames.com/)
did an awesome job.





 _The Fight_  started off with a different impression. First of all, Sony did
not push a demo for this game on the PlayStation Network. Even with low
ratings, I took the plunge to buy the game. The game was not responsive. It
wouldn't react to the moves I made physically and that'd lead to the opponent
beating me. Game development rules are very simple: Don't make a game which
people can't win, you'll lose your gamers. Also don't make a game which is
just too easy to win, you'll still lose your gamers. A good example is the
**Prince of Persia** game.  Was disappointed with **The Fight** until I read
this well done [review by Pace J
Miller](http://pacejmiller.wordpress.com/2010/12/28/game-review-the-fight-
lights-out-ps3-move/).

![The Fight](/sites/default/files/fightlights-thumb-640xauto-17716.jpg)

Both Sony and [Coldwood Interactive](http://www.coldwood.com/) did a bad job
in marketing this game. I think Sony should pay Pace for doing the right
review. _The Fight_  is a good game. It expects the user to train in the
system. A user cannot just walk-in into the game and start beating the
opponents. _The Fight_ forces the users to earn points (which can be used to
improve health, stamina and fighting skills) before they can be a fighter fit
enough for fighting the in-game opponents. Points are earned through sparring
sessions and working out in the gym.

I am glad I bought _The Fight_. After reading Pace 's review, I have been
thoroughly enjoying this game. It is a great game and at the same time a very
great way to get away from lethargy. Be prepared to sweat like hell when
playing _The Fight_

Despite all the debacle Sony went through (1. pulling off the Linux support 2.
PS3 Compromised 3. PSN Compromised), they still have the best product in the
market for the Motion Gaming experience

