{
    "title": "Glory of Eternal Uma-7 Manifestations (Part-II)",
    "date": "2016-01-10T12:18:37-05:00",
    "lastmod": "2016-01-10T12:18:37-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "hinduism",
        "religion",
        "Uma"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/glory-uma-part2"
}

[![sri_lalita](/sites/default/files/sri_lalita.jpg)](https://vibhormahajan.files.wordpress.com/2012/09/sri_lalita.jpg)

  
  
**Gauri (Kaushiki) Tale**

[![Gauri](/sites/default/files/gauri.jpg)](https://vibhormahajan.files.wordpress.com/2012/09/gauri.jpg)

Once there existed two demon brothers called Shumbha and Nishumbha. They were
mighty and very big trouble makers. All the three worlds and their populaces
were living under the fear of those two demons. They had let loose a rule of
tyranny. The gods and other celestial races had fled to the caves and forests
to hide. That made the demon duo more arrogant and cruel.

The gods were advised by Brihaspati and Lord Brahma to seek the help of
Goddess Parvati (Uma manifest). The gods went to the forest where Goddess
Parvati was making penance to sort out her own problems. To help the gods she
created her alter-ego out of her call. This new manifestation of her was
called Gauri (The white one) since she had a spotless milk white skin. She was
also known as Kaushiki for being born out of a cell (Kaushika). She astride a
tiger armed with many fangled divine weapons. An incredible beauty she was and
drank wine from a silver flask that dangled from her waist. Her skin was so
clear that the gods could see red liquid going down her throat. Like a big cat
she growled angrily. The over whelmed gods sang paeans to her as she made the
universe shudder. She needed no briefing as she had heard the woes of the gods
revealed wen she was a part of Goddess Parvati.

She flashed to a garden outside the palace of the evil demon pair of Shumba
and Nishumbha. The demon commanders called Chanda and Munda saw her there. The
beauty of Gauri stunned them. They ran to their demon pair of masters, Shumbha
and Nishumbha with the news of the arrival of a unique beauty in the garden
that deserved to the grace the palace of the demons. They had no words to
explain her beauty correctly.

It excited Shumbha and Nishumbha. They sent some messengers to Gauri proposing
her to marry one of the two and enjoy all the luxuries and splendours of the
palace. Gauri revealed that she was avowed to marry only a person who would
defeat her in a battle. The demon lords thought it was below there dignity to
fight a tender girl of the weaker sex. It was some joke, they thought. So they
sent their commander, Dhoomralochana to reason with her and if it did not work
he was asked to bring her to palace by force. The demon got burnt to ashes by
an angry gaze of Gauri. Then the demon duo sent their warriors Chanda and
Munda with a host. A growl of Gauri blew away the demon host. She grabbed the
fleeing Chanda and Munda and killed them. For killing Dhoomralochana, Devi got
the epithet of 'Dhoomravati' and for slaying Chanda - Munda she gained the
adjective name of 'Chandika'.

Then, demon lords sent Raktabeeja demon to teach a lesson to the irrepressible
Gauri. The every drop of blood of that demon fallen on ground could produce
another demon like him. It created a problem for Gauri. To counter it Gauri
produced many dark manifestations of her called Kaalis who drank the blood of
Raktabeeja by collecting it in coconut shells to prevent its being spilled on
the ground. Thus Gauri was able to slay demon Raktabeeja. Now Shumbha and
Nishumbha had to come to the battle.

The demon duo again tried to entreat her to stop battling and marry them to
make them her slaves for ever. Gauri challenged them to defeat her in battle
if they were real men and prove that they were worth marrying. To impress
their lords, warriors came forward to challenge Gauri who rained arrows at
them killing most of them and making the rest fall back. Shumbha was dismayed,
Nishumbha moved forward and spoke - "O beauty, why should you kill our poor
soldiers with arrows when you can kill us with your one askance glance?" Gauri
gave him a treat of a volley of her deadly arrows. Nishumbha had to engage in
a battle with her. Devi Gauri's arrows destroyed all his demonic weapons and
then felled him to ground as Shumbha watched in shock.

The death of his brother angered Shumbha and he moved forward to challenge
Gauri. Finding him in front of her made her let out great roar that sounded
like a thunder clap followed by a rumble. It put scare into the demon host
that trembled. Shumbha launched a fiery weapon on Gauri which the latter cut
to pieces with her arrows. Then, Devi threw her glimmering trident at the
demon that went through latter's rib cage to protrude its dents from the back.
The wounded Shumbha staggered towards his female foe with his sword raised.
Gauri released her deadly chakra and beheaded the evil demon. The bulk of the
demon collapsed the ground. Devi's tiger devoured the dead demons.

The gods rained down flowers and sounded bugles and kettle drums to hail the
victory of Goddess Gauri. The celestial rhapsodists sang her paeans.

 **Uma Manifests as Yaksharoopa**

[![AdiShakti_rollover](/sites/default/files/adishakti_rollover.jpg)](https://vibhormahajan.files.wordpress.com/2012/09/adishakti_rollover.jpg)

Once again yet another battle was fought by the gods and the demons against
each other. This time the gods won and the defeated demons had to flee to the
bottom world of Patala.

This victory was secured without seeking the help or blessings of The Trinity.
It filled the hearts and the minds of the gods with arrogance. Big egos they
grew. They got so overblown that they talked of nothing but their valour,
endeavour, battle skills, courage, grit, will power, divinity, wisdom etc. The
unmanifest power eternal, Uma felt the gods needed to be corrected and some
sense put back into them. In the domain of the gods she mysteriously
manifested in the form of a giant and radiant female figure of Yaksha. The
gods saw it wondered who or what it was. No one had any idea. Yaksha herself
did not bother to reveal anything deepening the enigma.

Mystified Indra sent Vayu, the wind god to find out who the intruder was. To
Vayu's query about her identity she did not reply and ask who he was. The wind
god introduced himself and extolled his own divine powers of creating storms,
tornadoes, typhoons etc. The Yaksharoopa put a straw on the ground and asked
him to blow it away. Vayu used all his powers but the straw remained inert at
its place. Yaksharoopa smiled mockingly. Embarrassed Vayu returned to Indra in
defeat.

Then, Indra sent Agni, the fire god to investigate. When asked to introduce
himself Agni boasted about his incendiary powers. Yaksharoopa again put straw
in front of him and asked him to demonstrate his burning powers. Agni used all
his powers but straw remained there unaffected. He too went back defeated.
Indra sent other powerful gods to try their powers but all returned with long
faces and bent heads.

It greatly surprised the celestial lord, Indra. He himself went to see the
mysterious figure. But Yaksharoopa had vanished. He stood puzzled. Then,
Saraswati, the goddess of knowledges whispered into his mind to use common
sense to know who it could be? It was primordial Eternal Force! The truth
suddenly dawned on Indra. He at once prayed to the Force Eternal. It
manifested in form of Uma to speak," Look, I am the divinity of Brahma,Vishnu
and Mahesh. Even they are nothing without my grace. I provide to you all the
powers you have that earn you victories. If I withdraw those powers all your
gods will be reduced to nothing. So, you must not become self conceited.
Humility should be your asset. One who grows ego he works his own downfall."

Indra promised that he and other gods would never again fall pray to ego. He
prayed for her grace.

 **" JAI MATA DI"**

 **Copyright (C) Vibhor Mahajan**

