{
    "title": "Apport in Debian",
    "date": "2015-01-14T05:47:57-05:00",
    "lastmod": "2015-01-14T05:47:57-05:00",
    "draft": "false",
    "tags": [
        "apport"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/debian-apport-help"
}

Looking at the [PTS](https://packages.qa.debian.org/a/apport.html) entries, I
realized that it has been more than 2 yrs, since I pushed the first Apport
packages into Debian.

We have talked about it [in the past](http://www.researchut.com/blog/report-
bugs-with-apport-3), and do not see a direct need for apport yet. That is one
reason why it still resides (and will continue to) in Experimental.

Even though not used as a bug reporting tool, Apport can still be a great tool
for (end) users to detect crashes. It can also be used to find further details
about program crashes and pointers to look further.

This post is a call for help if there is anybody, who'd be interested to work
on maintaining Apport in Debian. Most work include maintaining new upstream
releases, and porting the Debian CrashDB to newer versions, as and when
necessary.

As said above, it is not going to be a bug reporting tool, but rather a bug
monitoring tool.

