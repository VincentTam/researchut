{
    "title": "Laptop Mode Tools 1.67",
    "date": "2015-07-02T08:11:06-04:00",
    "lastmod": "2015-07-02T08:11:44-04:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-167"
}

I am pleased to announce the release of [Laptop Mode
Tools](http://samwel.tk/laptop_mode), version
[1.67](https://github.com/rickysarraf/laptop-mode-tools/releases/tag/1.67).

This release has many important bug fixes, and everyone is recommended to
upgrade. Of the many, one important fix is to, more reliably check for Device
Mapper based devices, which is common these days with Crypt and LVM.

For the summary of changes to quote from git log:

    
    
    1.67 - Thu Jul  2 17:05:07 IST 2015
        * Relax minimum window size to accomodate low res screens
        * Fix variable name to comply with our "constants" assuptions
        * Get more aggressive in power saving for Intel HD Audio
        * Account Device Mapper devices
        * Add swsusp freeze support
        * Switch battery-level-polling default to True
        * Detect ethernet carrier, early and relibaly
        * changes the boolean setting *_ACTIVATE_SATA_POWER to a customizable
          *_SATA_POLICY, with backward-compatible defaults and documentation
          Thanks Yuir D'Elia
    

PS: On a side note, over the years, Linux's power savings functionality has
improved a lot, all thanks to its use in the mobile worlds. At the same time,
because of more companies shipping drivers depending on external firmware,
stability has become less reliable. And to add to that, bare functionality of
devices typically ask for disabling, you know what, LPM.

So, at the end, the result is the same.

