{
    "title": "Evolution Newsgroup UI",
    "date": "2009-05-29T05:34:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "KDE",
        "knode",
        "evolution",
        "gnome"
    ],
    "categories": [
        "Tools",
        "KDE",
        "Rant",
        "Computing"
    ]
}

I've been using KDE for a while now, probably 9yrs. I've also been an early
adopter of KDE 4.x. While KDE 4 is still far way behind in proving its worth
of the radical core changes it made (take for example: **Nepomuk, Strigi,
Phonon, Decibel** \- I still wonder when they are going to be ready for the
*user*), I still find KDE apps far far ahead of GNOME.

Probably, many would disagree. Possibly, they might flame me too.I mean
everyone supports GNOME as the default - Red Hat/Novell/Ubuntu. And I always
wonder WHY.

Was it the licensing ? I can't think of anything else. Anyways, why I still
think that the GNOME design sucks, I'll give an example.

**Evolution** : Many call it the real killer app for GNOME. Maybe. There's one
small feature in Evolution, that I've tried many times and I just feel that it
is a UI design with stupidity at its best. The **NewsGroup** plugin of
Evolution. Ever tried ? How fast can you subscribe to newsgroups there. Take
[these](http://www.ensode.net/evolution_newsgroups.html) [examples](http://www
.go-evolution.org/Newsgroup). I'd be interested to know how Evolution users
subscribe to newsgroups.

I personally use leafnode to cache the news contents. In  Kontact Knode, when
the Gmane newsgroup listing appears, it appears with thousands of newsgroups.
But KDE allows me to run a search on that list and find for the relevant
newsgroup that I need.

Such a basic feature and I wonder what was there in the mind of the **GNOME
Evolution** devs when they were designing that interface.

