{
    "title": "FuseCompress",
    "date": "2007-03-09T03:54:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools"
    ]
}

So recently I tried to look at what all projects I monitored using a RCS.

The list I ended up with was:

  * Open iSCSI
  * Beryl
  * Linux
  * LVM
  * Device Mapper  

  * KDE

Wow!! That itself is huge. I then looked at the space consumption for these
projects and that led to 4.1 GB. That's too much of space taken away.

Now, since most of the contentt is text in these projects, I thought why not
find out a way to use them with compression beneath. And that is where
[FuseCompress](http://www.miio.net/fusecompress/) came to the rescue. Like
many other excellent implementations (ntfs-3g, sshfs), fusecompress does
exactly what I was looking for. It keeps the data compressed (gz, bz2, lzo)
and provides a transparent interface for the applications (through FUSE). With
this in place, I get full comprerssion, with saving disk space, while not
breaking any of the applications. Now that is what I call full utilization of
your CPU cycles (Most n00bs claim that full CPU cycle utilization is by using
Gentoo which keeps compiling on n on n on....You often might have heard
someone saying -mcpu=.., -fblah-makes-it-fast)

Ritesh

