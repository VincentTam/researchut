{
    "title": "Gunanidhi and Kubera’s Tale – Shiva Puran",
    "date": "2016-01-11T05:15:06-05:00",
    "lastmod": "2016-01-11T05:15:06-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "shiv"
    ],
    "categories": [
        "General"
    ],
    "url": "/gunanidhi-kuber-shiv-puraan"
}

[![Shiv](/sites/default/files/0.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/0.jpg)

**Gunanidhi 's Tale**

A pious and faithful Brahmin called Yajnadutta lived in Kampilya, a long time
ago. That Brahmin was blessed with a promising son whom the happy father had
named Gunanidhi, literally meaning 'repository of fine attributes'. After the
boy's sacred thread ceremony (Upanayana) he was sent to a guru to learn
letters and gain wisdom.The boy progressed into youth but unfortunately fell
in a bad company and picked up all bad and vice habits. Yajnadutta had been
too busy to notice the degeneration of his son. The mother was blind in
affection for her son to see anything going wrong.

Spoil rotten, Gunanidhi took gambling and womanising. All the money he laid
his hands on went into those vices. Then, needing more money he began to
thieve and sell the stolen goods. Once he gambled away all he had made.
Meanwhile, his father, while going to river to take his bath entrusted his
precious gold ring to his wife. The wife placed it in a niche of a wall. The
son, Gunanidhi saw her doing that and stole the ring. He sold it to a man and
gambled away the proceeds. Incidentally, Yajnadutta ran into the person who
was wearing the ring his wife had lost. Upon enquiry the man revealed that he
had bought the ring from a young man named Gunanidhi.

Yajnadutta now realised that what lately he had been hearing bad things about
his son were true. The son learnt that his father had come to know of his
misdeeds. Gunanidhi fled to avoid being confronted by his anguished father.

Gunanidhi went hungry for a couple of days. He flopped under a tree famished
and exhausted. Suddenly flavours of richly cooked food wafted into his
nostrils. A faithful was carrying some food offering to the nearby Lord Shiva
temple. He waited at some distance for an opportunity to steal the offering.
He saw the worship and prayers being sung. When the devotees fell asleep he
sneaked into the sanctum where in front of Lord Shiva idol offerings were
placed. The wick of the lamp had slipped into the oil and flame was about to
flicker off. Gunanidhi quickly tore a strip off his kurta, dipped in oil, lit
it and put it into the lamp to serve as substitute wick. Then he tried to
hurry out with the bundle of stolen food offering. But he happened to stumble
against a sleeping devotee who screamed in fright. The devotees woke up and
grabbed the thief. Gunanidhi got beaten up and a blow of a thick stick smashed
his skull. He died instantly.

The agents (dootas) of death arrived to despatch the sinful soul of Gunanidhi
to burning hell. But ganas (elite guards of Lord Shiva) intervened saying that
Gunanidhi had earned a place in the divine domain of Lord Shiva by
participating in Lord Shiva worship, lighting the lamp after fasting for two
days before his death, although done unwittingly yet the reward stood granted
by the grace of lord. he had been salvaged.

 **Rebirth of Gunanidhi as Kubera**

As the son of the king of Utakala, Gunanidhi took rebirth by a new name
Damana. Damana lived a life of piety, devotion and nobility. In his next birth
he was  born as the son of the grandson of Brahma. He had brought forward
goodness of his previous life and by the grace of Lord Maheshwara, remembered
all about his past lives.

An ardent devotee of Lord Shiva he remained all his life. On the bank of Ganga
he planted a holy lingam and penance on empty stomach. His shrivelled skin
draped his skeletal emaciated body. Propitiated with his intense devotion Lord
Shiva appeared to him along with his divine consort. The blessed their
faithful with the boon of a divine body and renamed  him _' Kubera'. _By the
grace of divine couple, Kubera later gained the rule of Alkapuri. Lord Shiva
went to live near Alkapuri when Kubera wished for his proximity on a boon.

In his new domain Lord Shiva rattled his  _' hand tambour'_ (Dumroo). It's
fierce beat echoed through the cosmos. All celestial beings rushed to Lord
Shiva to feat their eyes on him. So did sages, holy men, faithful's and hosts
of his own ganas led by their chiefs with folded hands praying and making
their obeisance. the celestial builder instantly created palaces to host them
all. Lord Shiva duly settled in his adobe. Later, all the guests returned to
their own adobes. Thus, Lord Shiva granted his proximity to his favoured
faithful Kubera who became the lord of the divine treasury.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

