{
    "title": "Glory of Eternal Uma–7 Manifestations (Part-I)",
    "date": "2016-01-10T12:27:09-05:00",
    "lastmod": "2016-01-10T12:27:09-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "hinduism",
        "religion",
        "Uma"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/glory-uma-part1"
}

[![Uma1](/sites/default/files/uma1.jpg)](https://vibhormahajan.files.wordpress.com/2012/09/uma1.jpg)

The eternal force Mahamaya, an eternal part of Lord Shiva. Also known as Ma-
Uma or Adi-Shakti, manifested in seven different avatars as Mahamaya, Yogmaya,
Mahakali, Mahalaxmi, Gauri, Durga and Yaksharoopa, since the very beginning
of creation.

 **Mahakali and Yogmaya Manifestations**

[![250px-Mahakali](/sites/default/files/250px-
mahakali.jpg)](https://vibhormahajan.files.wordpress.com/2012/09/250px-
mahakali.jpg)

At the very beginning of the creation water came into existence by the will of
force eternal Lord Shiva. In water, Lord Vishnu vested. A long penance tired
him out and he went to deep sleep of yoganidra. From the ear wax of Lord
Vishnu got two mighty demons created who were called Madhu and Kaitabha. For a
long period they made penance and got the blessings of Power Eternal Mahamaya.
Meanwhile, a lotus stalk had emerged from the navel of sleeping Lord Vishnu
and Lord Brahma appeared atop the flower. The demons spotted him and began to
harass him. The frightened Brahma tried to seek protection of Lord Vishnu but
he found the latter withdrawn into yoganidra. In desperation, Lord Brahma
prayed to Mahamaya. She answered his prayer by manifesting as Mahakali on the
twelfth day of the first fortnight of Phaluguna. She woke up Lord Vishnu by
exiting from his eyes as Yogmaya.

[![Yogmaya](/sites/default/files/yogmaya.jpg)](https://vibhormahajan.files.wordpress.com/2012/09/yogmaya.jpg)

To defend Lord Brahma, Lord Vishnu challenged the demon duo and fought with
them for five thousand years but failed to subdue them. Then, Lord Vishnu
invoked Mahamaya and prayed for her help. Mahamaya befuddled the minds of
demon duo and made them tell Lord Vishnu how they could be accounted for. So
Lord Vishnu put their heads on his widened thighs and killed them as that was
the only way possible.

 **Mahalaxmi (Jagdamba) Manifestation**

[![Jagdamba](/sites/default/files/jagdamba.jpg)](https://vibhormahajan.files.wordpress.com/2012/09/jagdamba.jpg)

The demon lord Rambhasura was a mighty one who had become a scourge of the
gods. His son Mahishasura was even mightier than him and was a repository of
more evils. He was a very ambitious demon and to realise his demonic dreams he
made a hard penance to propitiate Lord Brahma. From Lord Brahma he got a boon
that no man or god shall ever kill him. After gaining that boon he went
berserk tormenting all the three worlds. He raided heaven and banished gods
from there. The deities of the directions were under his constant threat and
punishment. Then he targeted all the people who used to worship deities and
celestial beings. He banned all religious rites and ceremonies. No yajna no
havana and no worship was his monstrous order.

The deities and the gods went to Lord Brahma seeking deliverance from the
tyranny of Mahishasura. Lord Brahma took them to Lord Vishnu as the former had
himself booned the demon. Lord Vishnu advised that they must all go to Lord
Shiva to seek his help. They all paid to Deity Supreme. Lord Shiva revealed
that the demon could be killed only by a female character as he could not die
at the hands of a male due to his boon. Lord Brahma, Lord Vishnu and Lord
Rudra shot out light balls of their divinity which fused in one radiant body.
The other gods shot into it their own respective beams. Thus amplified ball of
light transformed into a goddess figure of Mahalaxmi, also called 'Jagdamba'.
She was a picture of valour. Her face radiated the divinity of Lord Shiva and
the other parts of her body represented the divinity of other gods. She had
lord Vishnu arms and thighs and the legs of Lord Brahma. Yama was in her hair
and she rode a lion.

The divine lords lent her their powers. She got the trident of Lord Shiva,
Sudarshan Chakra of Lord Vishnu, Thunderbolt of Indra, Armour from
Vishwakarma, Heat from Agni, Carrying Bowl of Lord Rama, Time scale and shield
from Yama, Storm from Pavana, Liquidity from Varuna, Rays from Surya and the
other gods empowered her according to their own divine capacities. Thus power
packed Mahalaxmi rode that reverberated through the universe. It elated the
hearts of the gods.

They all prayed to deliver them from the torments of evil Mahishasura. She
assured them of her protection and the independence from the demonic tyranny.
She went to the ground outside the palace of the demon and let out a mighty
roar which shook the entire universe.

Angry demons came out to meet the challengers. There were great demonic
battlers in Chiksura, Chandra, Udagara, Kerala, Bhaksha etc. besides
Mahishasura.

The two armies clashed. To help Mahalaxmi, the host of the gods had also
followed her but the most of the fighting was being done by the goddess
herself. She killed several renowned demon warriors in no time. Very soon all
the big names of the demons except Mahishasura had become history. It angered
the demon lord. The demons of the order of Mahishasura were basically wilder
beasts who could transform into any animal shape.

Angry Mahishasura charged in his bull buffalo form at Mahalaxmi and herarmy.He
played havoc with the gods who prayed to the goddess to deal with the demon.
She tossed a lasso at the demon buffalo and seized it. To get off the noose
the demon transformed into a lion and tried to clobber the lion the goddess
was riding. Mahalaxmi raised her sword to slay the enemy lion but he instantly
reverted to his demonic form. The goddess attacked with her sword, spear and
arrows to confuse her enemy.

Mahishasura transformed into an elephant and trumpeted violently. Mahalaxmi
cut it's tongue in a swift move. The demon again changed his form and resorted
to demonic poltergy creating great many illusions. The goddess took a swing
from her divine flagon of wine gifted to her by Kubera. Then she blew her
conch shell in fury creating shock waves and let out a thunderous laughter
that made Mahishasura tremble. With a beastly roar she jumped at Mahishasura
and stomped him down to earth. Her foot was on the demon neck and her trident
impaled the face of her enemy and she lifted him up in the air as if raising
the flag of her victory.

[![Jagdamba
\(2\)](/sites/default/files/jagdamba-2.jpg)](https://vibhormahajan.files.wordpress.com/2012/09/jagdamba-2.jpg)

The Scene frightened the demon army and it fled in all directions in fright.
The delighted gods rained down flowers from the heaven to celebrate the
victory and kettle drums were beaten. The gods assembled around Mahalaxmi and
sang her prayers requesting her to keep her protection extended to the noble
forces forever.

 **" Jai Mata Di"**

 **Copyright (C) Vibhor Mahajan**

