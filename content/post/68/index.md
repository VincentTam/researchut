{
    "title": "NetworkManager Firewall",
    "date": "2007-05-30T21:09:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

Currently, there's no simple framework in NetworkManager for Firewalling
support.

 Here's one way you can provide a simple and minimal firewall setup for your
box controlled by NM.

Create the following files:

> rrs@learner:~$ cat /etc/network/if-up.d/firewall  
>  #!/bin/bash  
>  
>  if [ $IFACE == "lo" ]; then  
>      echo;  
>  else  
>      /sbin/iptables -A INPUT -i $IFACE -m state --state NEW,INVALID -j DROP;  
>  fi  
>  rrs@learner:~$ cat /etc/network/if-down.d/firewall  
>  #!/bin/bash  
>  
>  if [ $IFACE == "lo" ]; then  
>      echo;  
>  else  
>      /sbin/iptables -D INPUT -i $IFACE -m state --state NEW,INVALID -j DROP;  
>  fi

With these scripts in place you'll have a basic firewall where no requests
initiated from outside will be dropped. NM will make sure to execute the
script on the active interface basis.

I've also filed a wishlist suggesting the packagers to add something like this
into the Debian package.

http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=426683

