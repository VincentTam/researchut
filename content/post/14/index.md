{
    "title": "apt-offline 0.9.8",
    "date": "2010-05-11T03:46:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

I just uploaded [apt-offline](http://apt-offline.alioth.debian.org) version
0.9.8 to sid. Hopefully this is the one that would be part of squeeze. For
those who still arent aware of apt-offline, it is an Offline Package Manager
for apt.

I wrote a small [howto](http://www.debian-
administration.org/article/Offline_Package_Management_for_APT) that can give
more details about it.

