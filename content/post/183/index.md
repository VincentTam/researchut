{
    "title": "MMA",
    "date": "2011-08-07T10:41:13-04:00",
    "lastmod": "2014-11-12T06:10:12-05:00",
    "draft": "false",
    "tags": [
        "playstation",
        "game reviews",
        "MMA"
    ],
    "categories": [
        "Debian-Blog",
        "Fun"
    ]
}

Wrestling (WWE) is entertaining. The way the ww[ef] superstars (Especially the
ones like Undertaker, Triple H) are presented is fun to watch. Or the kind of
high flying maveuveurs the wwf superstars are able to perform, it really is
super cool. The only catch is - it is not a sport. The feuds are all pre-
defined to juicen things up.

I recently came to know of UFC. I hope some day they start airing it in my
part of the world. I'm still getting acquianted to MMA, but it really is an
awesome sport. This is one forum which does show size does not matter. Was
watching Royce Gracie's matches from UFC 4. Awesome matches clearly showing
the skill with which one can beat any size or power. The final was in between
a pro-jiu jitsu vs pro-wrestler. Gracie won it but was given a tough fight by
the wrestler.

That brings me to [EA Sports MMA](http://www.ea.com/mma).
![](/sites/default/files/MMA_Logo.png)

I downloaded this demo some time back but never played it. Only after the
recent interest in MMA I looked back at this demo. The demo was good enought
to convince for the full version. The gameplay primarily comes in 2 modes:
**1 x 1 fighter mode** and a **Career Mode.**

If you start with the 1x1 fighter mode, you'll very soon be beaten out. Just
like [The Fight](/blog/sony-playstation3-move), this game does need some
practising and getting a hang of the game controls. Uncommon - Attacks are
triggered by the R1 key, with L3 as the modifier for kicks.

In career mode, you can create your own custom character and start from
scratch to becoming a fighter. You start off with the basics, learn the
standards tactics on how to **Clinch** , **Take Down** , **Block** and
**Submit** your opponent. With these skills learnt, the game starts to
becoming a real fun. But the real gem about the game is in the **Stamina.**
The developers ensured that this is not just another _" who's the better
button masher"_ game. You need to be careful to not run out of stamina. The
stamina is the biggest key to winning in this game, just like in real life
fighting. If you run out of stamina, you can't even submit your opponent even
if you have a full body mount. To beat down the tough fighters you will need
to master the key combinations with great speed. Be fast enough to **Jab + Jab
+ Hook + Side Kick,** all of this with minimal stamina spent.



Overall, a great game to have if you like Mixed Martial Arts.

