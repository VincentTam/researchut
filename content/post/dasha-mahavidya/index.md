{
    "title": "Dasha (Ten) Mahavidya – Part I",
    "date": "2016-04-02T12:13:02-04:00",
    "lastmod": "2016-04-02T12:13:02-04:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "dasha",
        "mahavidya"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/dasha-mahavidya"
}

# Dasha (Ten) Mahavidya – Part I

Posted by [VIBHOR
MAHAJAN](https://vibhormahajan.wordpress.com/author/vibhormahajan/) on
[JANUARY 6, 2016](https://vibhormahajan.wordpress.com/2016/01/06/dasha-ten-
mahavidya-part-i/)

![Mahavidya](/sites/default/files/Vibhor/mahavidya1.png)

There is a deep quintessential need in the human psyche for a mother. It is
said; the Goddess was humankind’s earliest conception of divinity. Among the
Shakthas who worship Mother Goddess, the source of all existence is female.
God is woman. She is the principle representation of Divinity. She is that
power which resides in all life as consciousness, mind, matter, energy,
silence, joy as also disturbance and violence. She is the vibrant energy that
makes everything alive, fascinating and wonderful. She is inherent in
everything and at the same time transcends everything. Her true nature is
beyond mind and matter; she is not bound by any limitation. She is Arupa. When
she is represented in a form, her intense representation is a Bindu the
dimensionless point about to expand immensely. The Bindu symbolizes her most
subtle micro form as the universal Mother, womb, yoni, creator, retainer and
also the receiver of the universe.

The Goddess in Tantra as “Bindu” denotes what is hidden; the secret, the
subtle and the most sensitive. She represents the source of all that is to be
known, to be searched and to be attained with dedication and effort; she is
Durga. The seeker is drawn by a fascination to know her. She is the mystery
and allure of all knowledge. She is at once the inner guiding power, the
knowledge and its comprehension. She is Vidya. Ordinarily, Vidya stands for
knowledge, learning, discipline and a system of thought. But, in the context
of Tantra it has an extended meaning. Here, it variously refers to a female
deity, to the personification of her consciousness; or to the manifestation of
her wide variety of powers in specific forms at different times for different
purposes. Her varied forms-dynamic and static-   are interpreted as explicit
instances of her absolute nature. And each of her Vidya is an illustration of
her primordial energy as Adi prakrithi or Adi parashakthi.

The Devi, in the Tantra represents consciousness functioning at different
levels of the universe -inward and outward. She also is the source of diverse
principles, energies and faculties which make the manifested and unmanifested
universe. When the countless diversity that occurs in nature, in humans and in
all existence, is personified they are visualized by the Tantric through
idioms that are familiar to him. He views each of that as a specific
manifestation of the Devi. He recognizes each expression of her as a Vidya.
Those symbolic Tantric visualizations are named Mahavidya, in awe and
reverence. Though Her Vidya is infinite, for the purpose of Tantric Sadhana,
they are usually classified as being ten: Dasha Mahavidya. Each tradition of
Tantra has its own set of Dasha Mahavidyas. Generally, the ten important
Mahavidyas enumerated in the shaktha upa-puranas, Maha Bhagavata Purana and
Brahaddharma Purana are taken as standard forms  _(Kali, Tara, Tripura Sundari
(Sodashi),  Bhuvaneshwari, Bhairavi, Chinnamasta, Dhumavati, Bagalamukhi,
Matangi and Kamala_ _)_. They are described as the ten great gupta ( secret )
Mahavidyas.  Further, each of the ten has within itself many layers; each
carries many names; and, each form has its own sub-variations. Whatever is
their numbers, names, order of appearance or their diverse forms, all
represent the natures of one and the same reality. Kali is included in all the
enumerations and she is regarded the Adi (primary) Mahavidya. Each of the
other Mahavidya shares some of her characteristics.

* * *

**The Origin of Mahavidya’s**

The origin of Mahavidyas as a group is unclear. There are various explanations
based in mythologies of the Mahadevi the Great Goddess and in the Tantra
texts. But all explanations seem to suggest that the Mahavidyas, as a group of
ten, is of comparatively recent origin. The Mahavidyas is a combination of
three well established deities –Kali, Tara and Kamala; couple of deities that
already had marginal presence; and other deities, perhaps of local origin, who
figure exclusively in the Tantric Mahavidya cult. All the Mahavidyas, whatever
might be their origins and individual dispositions, are associated with the
Shiva cult. As a rule, they are depicted as dominating over Shiva, the male.

Among the Mahavidyas,  **Kali  **is the foremost. Though Kali makes her
specific appearance in the Devi-Mahatmya as an emanation of Durga, she
combines in herself the virtues and powers of many Vedic deities. She inherits
the all – pervasive sovereign power and splendor of Devi, the mystery and
darkness of Rathri, dark as the bright starlit night who is Mayobhu
(delighting), Kanya (virgin), Yosha Yuvathi (youthful) , Revathi (opulent),
Bhadra Shiva (auspicious)  and Pashahasta (holding a noose); the mercy of
Durga who transports her devotee over all the difficulties; the occult power
and delusion of Viraj  the Maha-Maya , the goddess of heaven (divi maayeva
devata) and the Dhirgajihvi (long tongued) ; the death, destruction and
dissolution of Nirrti; and the timelessness of Kala. Kali is also one of the
seven tongues of Agni (Kali, Karali, Manojava, Sulohita, Sudhumravarna,
Suphulingini and Visvaruchi). Kali is thus associated with darkness, night,
time, mystery, fire, and immense power of attraction. She is also the source
and the residue of all energies.  

**Tara  **the savior (Taarini) is as potent as Kali. She is said to be the
form that Mahadevi took in order to destroy the thousand-headed –Ravana. Tara
has strong presence in the Buddhism (especially the Tibetan Buddhism) and in
Jain pantheons also. Among the Mahavidyas, Tara is next only to Kali; and she
resembles Kali in appearance more than any other Mahavidya. Tara as Mahavidya
is not entirely benign; she could be fierce and horrifying.

Among the Mahavidyas,  **Kamala**  is the best known and adored even outside
the cult. Kamala of the Mahavidya is a reflection of Shri for whom a Suktha of
fifteen riks is devoted in the khilani attached to the fifth Mandala of Rig
Veda. The Devi Mahatmya which is a part of the Markandeya Purana celebrates
Mahalakshmi as the immense potential (sarva-sadhya) and the mighty Shakti of
Devi, the destroyer of Mahisha. However, as Mahavidya, Kamala is not endowed
with all those powers nor does she enjoy the same prestige as Mahalakshmi in
Tantra or Lakshmi in the orthodox tradition. Kamala is invoked mainly in
rituals seeking wealth, power and hidden treasures. Kamala in her Mahavidya
form is associated with Shiva and not with Vishnu.

**Sodashi**  as Mahavidya is also referred to as Tripura Sundari the most
beauteous in all the three worlds. She along with Kali and Tara is reckoned as
Adi (primordial) Mahavidya. She is associated with sixteen phases of the moon
or sixteen modifications of desire. Sodashi as Tripura Sundari, Lalita and
Rajarajeshwari are the important goddess in the Sri Vidya tradition. But, as
Mahavidya her belligerent aspect as Tripura Bhairavi is stressed.

**Bhuvaneshwari  **is related to Prithvi (the Mother Earth). In the Puranas
she is associated especially with Varaha Avatar of Vishnu. Broadly,
Bhuvaneshwari, whose extension is the world, represents substantial forces of
the material world. The other Mahavidyas:  **Chinnamasta, Bagalamukhi,
Dhumavati and Matangi**  are rarely mentioned except as Mahavidyas. These
along with  **Bhairavi  **are primarily tantric deities of funeral pyres and
graveyards.

* * *

**Appearances and Characteristics**

Kali is Adi Mahavidya, the primary Mahavidya. She is the first and the
foremost among the Mahavidyas. She is not only the first but the most
important of the Mahavidyas. It is said, the Mahavidya tradition is centered
on Kali and her attributes. Kali is the epitome of the Mahavidyas. The rest of
the Mahavidyas emanate from Kali and share her virtues and powers in varying
shades.

The Mahavidyas, as a group, form a most wonderful assimilation of contrasting
elements and principles in nature. They all are intensely feminine, asserting
the supremacy of the female. One could say they are the ‘anti-model’ of
traditional docile housewife. They totally reject every authority and any type
of dominance. They aggressively put down and overpower male ego and its
arrogance. Their agitation often transforms into dreadful wrath. And that
truly underlines the nature of the Mahavidyas. Though the Mahavidyas are
female they are not depicted as a wife. In the hymns devoted to some
Mahavidyas their male spouses are mentioned. But, that minor detail is never
stressed, as that is a weak and an insignificant aspect of their
individuality. Mahavidyas are also not associated with Motherhood or
fertility.

Mahavidyas are symbols of female independence; symbols of the ‘other’ ways of
being feminine; the way that threatens the male. They are highly independent,
rebellious, and stubborn; and over-domineering as if possessed of ferocious
obsession to pulverize and grind the male ego into abject submission. Their
wrath burns down every type of male arrogance. Incidentally, it is said, each
Mahavidya is so independent and exclusive that she relates only to just a few
that are close to her, but not to all the Mahavidyas in the group. By all
accounts, the Mahavidyas as a group and as individual deities are the most
bizarre set of goddesses in any religion or in any culture.  The Mahavidyas
have fierce forms; terrifying demeanor; agitating minds; strange and exotic
characters   ; untidy habits; shocking behaviors; and destruction-loving
nature. They enjoy strong association with death, violence and pollution. Some
of the Mahavidyas are ghoul like deities of cremation grounds and corpses,
sporting wild disheveled hair, hideous features, dancing naked and sometimes
copulating with an inert male stretched flat under them. In most cases they
preside over strange tantric-magical rituals. It is their outrageous aspects
that set them apart from the other deities.

Mahavidyas are a distinctive group of deities, and far different from the
deities worshipped in the polite society. The Mahavidyas giving way to violent
emotional expressions are shown as performing loathsome, socially despicable
roles, indulging in all that is forbidden in a normal society. And in fact,
they challenge the normally accepted concepts and values in an established
social order. They bring into question the very notions of beauty, goodness,
honor, respect, decency, cleanliness and physical comfort. There is another
way of looking at their forms that are often disturbing and difficult to bear.
This perhaps was the way they were intended to look. Mahavidyas are not meant
to be pleasant or comforting. Their ambiguous, enigmatic, contradictory and
paradoxical nature and behaviors are intended to shock, jolt and challenge our
conceited way of looking at the world that keep us in bondage. They kick hard
to awaken us; and point out that the world is really much different from what
it appears to be; and it surely is not designed to satisfy our comforting
fantasies. Mahavidyas are ‘anti-models’, provocative energies urging us to
shed our inhibitions, to discard our superficial understanding of beauty,
cleanliness, goodness or the proper way of doing things. They, in their own
weird ways, challenge us to look beyond; and to look deeper and experience
what lies beneath the façade of the ordinary world. From an aesthetic point of
view Mahavidyas suggest a flight from reality and take you to a totally
different world, which is poignant, restless and aggressive. The interesting
aspect of the Mahavidyas is that the images seem to have sprung from intuition
or from a non-rational source, and yet they bring home the realities of life.
Pointing out to reality is in the nature of Tantra outlook. Tantra takes man
and the world as they are and not as they should be. It is said; the images
should be viewed in the light of the meanings which underlie and generate the
image. In case, the image and its meaning are disassociated, it then becomes a
mere repulsive picture.

The Mahavidyas in general are a strange amalgam of contradictions: death and
sex; destruction and creation. In her creative aspect, Mahavidya is an
enchantress – ‘the fairest of the three worlds’, radiating her   benign
powers. In her negative aspect her intensely fierce nature is made explicit by
her terrifying features. But at the same time, every Mahavidya is neither
totally negative nor totally positive. Each is a combination of many awe
inspiring virtues and magical powers. From the gross descriptions of ferocious
deities Kali and Dhumavati it might appear they are devoid of pleasing,
benevolent and such other positive virtues. But, their namavalis (strings of
one thousand and eight names) sing and praise them as oceans of mercy. In
contrast, Kamala, given her association with Shri, surprisingly, carries
within her demeanor a few fierce or terrible aspects. The Mahavidyas cannot
easily be classified as those that are strictly of fierce (raudra) or benign
(saumya) nature. A couple of the Mahavidyas are pictured as beauteous, amorous
and benevolent. But in the ambience of death and destruction in which they are
placed and in the overall context of the Mahavidya tradition, they are meant
to be fearsome, demanding submission of the male. For instance, the beautiful
goddess Tripura Sundari’s terrible form as Tripura Bhairavi is taken as her
authentic Mahavidya aspect. The Mahavidyas, when pleased, might bless an
adept; but that is   often by destroying or harming or suppressing the adept’s
enemies or opponents. Thus destruction is at times the Mahavidyas mode of
blessing.

Each of the Vidya’s is great in its own right. The notions of superiority and
inferiority among them should never be allowed to step in. All are to be
respected alike. The differences among them are only in their appearances and
dispositions. And yet they all reflect various aspects of the Devi. The might
of Kali; the sound-force (sabda) of Tara; the beauty and bliss of Sundari; the
vast vision of Buvaneshwari; the effulgent charm of Bhairavi; the striking
force of Chinnamasta; the silent inertness of Dhumavati; the paralyzing power
of Bagalamukhi; the expressive play of Matangi; and the concord and harmony of
Kamalatmika are various characteristics, the distinct manifestations of the
Supreme consciousness of the Devi that pervades the Universe. The tantric text
Mundamala-tantra, however, makes a sub classification in three levels.  

  * _Maha Vidya_  –  the extraordinary Vidyas, consisting Kali and Tara;
  * _Vidya  _– the normal Vidyas consist deities Shodashi (or Tripura), Bhuvaneshwari, Bhairavi, Chinnamasta and Dhumavati; and
  * _Siddha Vidya_  – the Vidya for adepts refers to Kamala, Matangi and Bagalamukhi.

It is also said that Mahavidyas are indeed various expressions of the Mother:
Kali is Time; Bhuvaneshwari is space; the piercing word is Tara; the flaming
word is Bhairavi; and expressed word is Matangi. Chinnamasta combines light
and sound in her thunderclap; Bagalamukhi stuns and stifles the flow free flow
of things. The luminous desire is Sundari; and the delightful beauty is
Kamala. The Sadhaka prays to Kali to grant him virtues of : the generosity of
Chinnamasta; the valor in battle of Bagalamukhi; the wrath of Dhumavati; the
majestic stature of Tripura Sundari; the forbearance of Bhuvaneswari; and
control over enemies like Matangi.

Kali is said to represent unfettered absolute reality; Tara an expanded state
but yet bound by the physical; Bagalamukhi the fierce concentration; Kamala
and Bhairavi with satisfaction of physical well being and worldly wealth;
while the other Mahavidyas symbolize  the worldly needs and desires that
eventually draws into Kali. Also, Kali, Chinnamasta, Bagalamukhi and Dhumavati
are characterized by their power and force – active and dormant. Tara has
certain characteristics of Kali and certain others of Sundari. And she is also
related to Bhairavi, Bagalamukhi and Matangi in aspects of sound-force (sabda)
express or implied. Whereas Sundari, Bhuvaneshwari, Bhairavi, Matangi and
Kamalatmika have qualities of light, delight, and beauty. The Tantras speak of
Kali as dark, Tara as the white; and Sundari as red.

* * *

**Worshiping** **  the ** **Mahavidyas**

The Mahavidyas are not goddesses in the normal sense of the term. The worship
of Mahavidyas – as a group- is generally not temple oriented; and, there is no
pilgrim center (Tirtha) associated with the Dasha Mahavidyas- group. They are
also not associated with prominent geographical features such as hills,
rivers, river-banks or trees.  The worship of one Mahavidya might differ from
that of the others.The Tantra texts specify which path should be taken in
worshipping a particular Mahavidya. For instance, the worship of Kali,
Kamakhya , Tara, Bhairavi, Chinnamasta, Matangi and Bagala involve strongly
individualized left-handed tantric rituals, rooted in their specific Mantras
and Yantras, conducted in secrecy. The worship of these divinities requires
great rigor, austerity, devotion, persistence and a sort of ruthless
detachment. The left-handed worship practice of Mahavidya is very difficult
and is filled with risks and dangers. Its practice is not considered either
safe or suitable for common householders, as it involves rituals that cannot
be practiced normally. The text also mentions that Sodasi, Kamala and
Bhuvaneshwari prefer right-handed worship practices. And at the same time, the
texts clarifies that both the paths are appropriate . And Mahavidya could be
worshipped in either manner depending upon the inclination and the nature of
the worshipper.

Though some of the Mahavidyas are worshipped in their temples, the private
places marked out in the cremation grounds seem to be favored places for
tantric rituals, especially in the case of Kali, Tara, Bagalamukhi,
Chinnamasta and Dhumavati. In the extreme forms of this class of worship
(vamachara) the deities, the Sadhakas and the ritual practices are associated
with blood and corpses. Their worship is characterized by the pancha tattva or
pancha makaara (five ‘M’); rituals performed employing five forbidden or
highly polluting elements: madya (liquor) ,mamasa (meat), matsysa (fish),
mudraa (ritual gestures or parched grains causing hallucinations) and maithuna
(sex). By partaking the polluted and forbidden things the Sadhaka affirms his
faith that there is nothing in this world that is outside the goddess; she
pervades all; and within her there are no distinctions of ‘pure’ or ‘impure’.
He attempts to erase the artificial or man made distinctions and be one with
his goddess. These five elements carry various esoteric interpretations
according to the nature of worship undertaken: tamasika (pashvachara),
rajasika (vichara), or divya (sattvika sadhana).

* * *

![Maa](/sites/default/files/Vibhor/maa.jpg)

This mind, like a firefly, flashes into existence,  
Just to call lovers of the Dark One!  
Having been summoned into my beloved Kali,  
Her Blackness so mystical, so absorbing,  
the mind disappears into Her,  
like a candle dropping into the midnight ocean,  
even the cry of ecstasy is lost in Her depths!  
Yet those who gaze into Her unfathomable Void  
take on a glow that those with sight can see!  
How sublime to reflect Her Dark Light!  
How sublime to share Her grace-filled gifts!  
Extinguish yourself in Her this very instant!  
She has waited so long, so patiently to embrace you – Her beloved.

~ Kalidas

