{
    "title": "Temple of Lord Mahadev, Bangalore",
    "date": "2008-11-25T12:14:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "General"
    ]
}

Having been in Bangalore for more than 4+ years, it was a regret when I
visited the temple of Lord Mahadev, that why didn't I visit it earlier.

This past weekend I had the opportunity to be there with my parents and I must
say, it is awesome. You need to be there.

![Lord Mahadev Temple,
Bangalore](http://www.researchut.com/blog/images/imag0015.jpg_ib4872.jpg)

