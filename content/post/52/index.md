{
    "title": "ZippoBlu Butane Gas Ligher",
    "date": "2008-02-21T00:33:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "tags": [
        "zippo"
    ],
    "categories": [
        "General",
        "Fun"
    ]
}

I received my [ZippoBlu](http://www.zippoblu.com) Butane Gas Lighter
yesterday. And today, I'm using it.

I had ordered it through eBay. Initially I was worried about the delivery of
the item as it might get damaged during shipping to India.  But  the [seller
(123khaly)](http://myworld.ebay.com/123khaly)is awesome.  He did a good
packing before the shipping and the lighter reached safely to me. Keep in mind
that the gas cannot be shipped because it is flammable. These are
international regulations. So I inquired with Zippo India if they have
imported Butane Gas or not. Unfortunately they haven't. This got me
disappointed. It was like I have a Plasma TV with no Cable connection. So I
moved down to M.G Road, Bangalore and was able to find Chinese Butane Gas.
While Zippo doesn't recommend non-Zippo Butane, I didn't have a choice.

But he lighter is awesome. It has some of the innovative [Zippo
technologies](http://www.google.com/patents?id=7iwWAAAAEBAJ&dq=d502285). And
also the infamous Zippo Click sound. The Blue Flame is awesome.

This lighter is a must-have..

![](http://www.researchut.com/blog/images/30028.jpg)

