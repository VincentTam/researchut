{
    "title": "Linux-Nepal",
    "date": "2005-07-04T05:23:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "tags": [
        "Linux Nepal"
    ],
    "categories": [
        "General"
    ]
}

  
  

[Linux Nepal](http://groups.yahoo.com/group/linux-nepal) is a non-profit
organization dedicated to promoting GNU/Linux in the kingdom of the Himalayas,
Nepal.

