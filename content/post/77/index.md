{
    "title": "Using latest Beryl in Debian",
    "date": "2006-12-07T03:19:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

Most GNU/Linux desktop users today are aware of the new 3D enhancements
brought to the desktop. Namely **Compiz** and **Beryl**.

Compiz is already present in Debian but Beryl isn't and will not be included
until Etch is released, which probably is expected to be released in another 3
months. Beryl looks to be primarily focused on Ubuntu which IMO is not good
but I can't comment much because I haven't contributed to it. In fact, as per
the recent announcement on the Beryl blog, their release too seems to be
focused on Ubuntu.

Anyway, This small HOWTO is about how to use the latest Beryl snapshot on
Debian based systems. We'll be using the latest code from Beryl SVN and with a
good source install management utility called stow. Let's begin....

  * Make sure stow is installed. If installed you should be having a folder named **/usr/local/stow/**   

  * Check out the latest code from Beryl SVN. **svn co svn://svn.beryl-project.org/beryl/trunk/ beryl**  

  * You'll notice a new folder in your current working directory named beryl. Under it would be present a shell-script named **makeall**
  * Replace that particular shel-script with the one provided here. This is a slightly modified script which automates the installation using **stow**.
  * Once the installation is done you should have the latest Beryl working on your Debian box.

Safety Tips:

After the installation is complete, cd to the /usr/local/stow/ directory and
make a backup copy of the beryl-svn folder. This way, when, the next time you
do an update and Beryl is broken you can revert back to your older working
Beryl installation.

[makeall script](http://www.researchut.com/blog/images/makeall)

