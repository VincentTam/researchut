{
    "title": "Lonely Nights",
    "date": "2005-12-21T01:37:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Psyche"
    ]
}

Recently after my vacation to home I regained my usual life, **The Lonely
Nights**

![](http://www.researchut.com/blog/images/blog1.jpg)

Almost every ~~night~~ morning while I try to sleep I get weird feelings.
Feelings like there's a snake crawling on my legs over the quilt. The feeling
like it's moving towards me and will bite me right at my neck so that I have
no possibilities of fighting back to death. Also the scorpion that's stuck
under my pillow, comes out in anguish and hits me at the neck.

I wondered how'd it look if I was really not to wake up again after being
asleep. Thanks to my recently bought **Sony Ericsson K700i** mobile phone. I
was able to make out how'd it actually look.

![Dead Man Inc](http://www.researchut.com/blog/images/blog.jpg)

[![Video](http://www.researchut.com/blog/pivotx/pics/icon_file.gif)
](http://www.researchut.com/blog/images/clip1.3gp.file "Video") Here's a video
about how'd it look at the last moment.

And yes, more pics are available at the gallery.

