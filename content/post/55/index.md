{
    "title": "KDE4 with KDE3",
    "date": "2008-01-25T06:37:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "KDE"
    ]
}

While the Debian KDE Team has done a great job of packaging KDE4 in time, with
quality, it is not always possible to fulfill the request of every odd user.

Unfortunately, I fell into the Odd user category because I still want to have
KDE4 and KDE3 installed together. For many reasons.

So here's one way to do it.

  * Install KDE 3 Packages from Debian Repositories
  * Install KDE 4 from SVN
  * Create a X session file like this. 

> rrs@learner:~$ cat /usr/share/xsessions/kde4-svn.desktop  
>  [Desktop Entry]  
>  Encoding=UTF-8  
>  Type=XSession  
>  Exec=/opt/kde4-svn/kde/bin/startkde  
>  TryExec=/opt/kde4-svn/kde/bin/startkde

  * Add the following to your .bashrc file

>  

> if [ $DESKTOP_SESSION == "kde4-svn" ]; then  
>  # KDE  
>  export KDE4DIR=/opt/kde4-devel  
>  export KDEDIR=$KDE4DIR/kde  
>  export KDEHOME=$HOME/.kde4-devel  
>  export KDETMP=/tmp/$USER-kde4  
>  mkdir -p $KDETMP  
>  export KDEVARTMP=/var/tmp/$USER-kde4  
>  export KDEDIRS=$KDEDIR  
>  export PKG_CONFIG_PATH=$KDEDIR/lib/pkgconfig:$PKG_CONFIG_PATH  
>  export PATH=$KDEDIR/bin:/usr/local/bin:$PATH  
>  export LD_LIBRARY_PATH=$KDEDIR/lib:$LD_LIBRARY_PATH

> unset XDG_DATA_DIRS # to avoid seeing kde3 files from /usr  
>  unset XDG_CONFIG_DIRS

> # This will make the debug output prettier  
>  export KDE_COLOR_DEBUG=1  
>  export QTEST_COLORED=1

>

> # Make  
>  # Tell many scripts how to switch from source dir to build dir:  
>  export OBJ_REPLACEMENT="s#$KDE_SRC#$KDE_BUILD#"  
>  # Use makeobj instead of make, to automatically switch to the build dir.  
>  # If you don't have makeobj, install the package named kdesdk-scripts or  
>  # kdesdk, or check out kdesdk/scripts from svn, or just don't set the alias  
>  # yet.  
>  alias make=makeobj  
>  fi

  * And then restart KDM and select KDE4-SVN from the listi of available Window Managers.

This will ensure that your KDE3 Data and Settings are  safe and intact while
you can still play  with all of  KDE4 and help the KDE team with bug reports.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

[KDE4 + KDE3](/blog/tag/kde4___kde3 "Entries tagged with: KDE4 + KDE3")

>  
>

