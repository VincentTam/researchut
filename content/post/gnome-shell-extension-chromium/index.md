{
    "title": "GNOME Shell Extensions and Chromium",
    "date": "2016-10-01T07:04:16-04:00",
    "lastmod": "2016-10-01T07:07:28-04:00",
    "draft": "false",
    "tags": [
        "GNOME Shell Extensions",
        "Chromium",
        "Chrome",
        "Browser"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/gnome-shell-extension-chromium"
}

Most GNOME users may be using one or more extensions for the GNOME Shell.
These extensions allow extending functionality for the shell, or modify
default behavior, to suit the taste of many users, who may want more than the
default. Having flexibility to customize the desktop to ones personal need is
a great feature, and the extensions help achieve them.

The GNOME Shell Extensions distribution mechanism is primarily through the
web. I think they aspire to achieve something similar to Chrome's Web Store.
Up till recently, the ability to install those Shell Extensions was a) Through
your distribution channel, where your distribution may have packaged some of
the extensions. b) Through the Firefox web browser. GNOME Shell Extensions
installation from the web, to most of what I'm aware of, until recently, only
worked with Firefox browser.

With the [chrome-gnome-shell](https://tracker.debian.org/pkg/chrome-gnome-
shell) package, which is now available in Debian, Debian GNOME users should be
able to use the Chromium browser for managing their GNOME Shell Extensions.

  1. Install package chrome-gnome-shell
  2. Open Chromium Browser and go to Web Store
  3. Install Chrome Shell Integration extension for Chromium 
    1. .![](/sites/default/files/Screenshot%20from%202016-10-01%2016-21-08.png)
  4. Point your browser to: https://extensions.gnome.org/local 
    1. ![](/sites/default/files/Screenshot%20from%202016-10-01%2016-25-09.png)



In future releases, the plan is to automate the installation of the browser
extension (step 3), when the package is installed. This feature is Chromium
specific and will be achieved using a system-wide chromium browser policy,
which can be set/overridden by an administrator.



