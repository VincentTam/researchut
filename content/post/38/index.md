{
    "title": "SVN Ignore",
    "date": "2008-10-31T06:17:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "subversion"
    ],
    "categories": [
        "Tools"
    ]
}

Ignoring files in subversion during checkout.

> You can't directly ignore folders on a checkout, but you can use sparse
checkouts in svn 1.5. For example:

>

> $ svn co http://subversion/project/trunk my_checkout --depth immediates

>

> This will check files and directories from your project trunk into
'my_checkout', but not recurse into those directories. Eg:

>

> $ cd my_checkout && ls  
>  bar/ baz foo xyzzy/

>

> Then to get the contents of 'bar' down:  
>  $ cd bar && svn update --set-depth infinity

