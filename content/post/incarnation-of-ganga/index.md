{
    "title": "Incarnation of Goddess Ganga as river Ganga",
    "date": "2016-04-02T11:57:28-04:00",
    "lastmod": "2016-04-02T11:57:28-04:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "ganga",
        "shiva"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/incarnation-of-ganga"
}

# Incarnation of Goddess Ganga as river Ganga

Posted by [VIBHOR
MAHAJAN](https://vibhormahajan.wordpress.com/author/vibhormahajan/) on [MAY
11, 2012](https://vibhormahajan.wordpress.com/2012/05/11/incarnation-of-
goddess-ganga-as-river-ganga/)

_[![ganges_over_shiva-
222x300](/sites/default/files/Vibhor/hindu8-10.jpg)](https://vibhormahajan.files.wordpress.com/2012/05
/ganges_over_shiva-222x300.jpg)_

King Sagara the ruler of Ayodhya and an ancestor of Lord Rama successfully
performed the Ashwamedha Yagya (Horse sacrifice) for 99 times. Each time, he
sent the horse around the Earth it returned to the kingdom unchallenged. He
decided to perform the Ashvamedha Yagna for the 100th time, that would make
him emperor of Earth.

When Indra (King of Gods) saw that happen, he became worried by Sagar’s
increasing power. So Indra stole Sagar’s horse before the ritual could be
completed and tied it to the tree under which Sage Kapila was meditating.

King Sagar’s courtiers discovered the sacrificial horse had gone missing. The
king therefore ordered his 60,000 sons to go find the horse. The arrogant
princes went about burning and destroying places in their quest to find the
horse. And presently they came to Sage Kapila’s hermitage. They found him in
meditation and the horse tied next to him. They mistook the sage to be the
abductor and attacked him. Feeling disturbed and insulted, Sage Kapila burned
them to ashes with yogic fire that came out of his third eye.

When Ansuman, the last remaining son of King Sagar came to know the fate of
his brothers, he asked Sage Kapila how to liberate their souls from the ashes.
The Rishi advised Ansuman to please Lord Brahma with penances so that he would
release Ganga from his kamandula and bring Ganga to Earth to purify the ashes
and liberate his brothers. But,this task was undertaken for many generations.

In the seventh generation of King Sagar, Bhagiratha was born. King Bhagirath
left his kingdom and began to mediate for the salvation of the souls of his
ancestors. Bhagirath observed a penance to Lord Brahma for a thousand years,
requesting Ganga to come down to earth from heaven and wash over his
ancestor’s ashes to release them from a curse and allow them to go to heaven.
Pleased with the devotion, Lord Brahma granted Bhagirath’s wish but told him
to pray to Lord Shiva, so that he would accept to hold the force of Ganga in
his hairs, lest she would overwhelm the entire Earth.

Thus Bhagiratha undertook further penances to please Lord Shiva. When Ganga
descended from the heavens, Lord Shiva covered the sky with his hair locks, so
that not a drop would fall on Earth. When he had fully captured Ganga, Lord
Shiva released a small part of Ganga’s force and told her to follow
Bhagiratha. This gave Lord Shiva the name of Gangadhar, he who holds the river
Ganga.

On origin Ganga flooded the ashram of Saint Janu, he swallowed her as a lesson
in respect and only released Ganga out of pity for Bhagiratha. Wherever Ganga
would flow in following Bhagiratha, people came in large numbers to bath and
obtain purification.

Finally Ganga reached the ashram of Sage Kapila and freed the 60.000 souls
from their ashes.

