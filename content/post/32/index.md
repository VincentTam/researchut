{
    "title": "pypt-offline goodness",
    "date": "2008-12-29T22:15:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "apt-offline"
    ],
    "categories": [
        "Tools",
        "Programming"
    ]
}

I created [**pypt-offline**](http://pypt-offline.sf.net) with the hope that
it'd be useful for people who don't have an internet connection but would
still like to enjoy [**Debian**](http://www.debian.org). Enjoying Debian is
about enjoying its Package Manager, APT. I don't have any data to show if
pypt-offline is in use by anybody. I too, use it rarely, when at my hometown.
But I hope people who use it, find it useful.

At my hometown, I use internet service from my friend's ISP. I have a 10kb/s
connection at home. That's slow.

And I've been using [KDE 4.2 in Debian](http://pkg-kde.alioth.debian.org). The
Debian KDE team decided to defer public availability of KDE 4.2 Beta packages
for a good reason. But I did want to use it and find more bugs, sooner. So I
ended up with the unofficial sources. There, it comes packaged almost every 2
days. That ends up being a **600-700 mb** download quite often. (This includes
the debug packages).

Being at home and downloading 700 megs of deb is painful. So I rushed to my
friend's ISP. There I can get **500kb/s** bandwidth. But apt didn't want to
play good. The problem is that apt is not threaded. So from a single source,
only a single download can be initiated. That ended up me using on **120kb/s**
bandwitdth to download. That was time consuming. I wanted it to be downloaded
faster so that I could spend more time at home.

That's when I thought of using pypt-offline. When I designed pypt-offline, one
of my requirements set, was threads. pypt-offline is threaded. This ended up
being very helpful for me. I was able to utilize the bandwidht upto **600
kb/s** by using 5 threads. This has yielded in faster download for me, while
it must be choking the bandwidth for the server
![;-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_121.gif)

I wish if apt was threaded. But I think it isn't threaded for good reasons.
Having a threaded apt will end up with excess load on Debian servers. But
then, if you really want fast downloads and do have the bandwidth, do give
pypt-offline a try. And yes, you can try it on **Windows/Linux/Mac.**

It makes me happy today to see a good use of pypt-offline.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

