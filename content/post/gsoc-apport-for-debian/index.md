{
    "title": "Apport Integration with Debian - GSoC Update",
    "date": "2015-07-06T03:49:52-04:00",
    "lastmod": "2015-07-06T03:50:59-04:00",
    "draft": "false",
    "tags": [
        "GSoC",
        "Google Summer of Code",
        "apport",
        "debian"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Technology",
        "Tools"
    ],
    "url": "/blog/gsoc-apport-for-debian"
}

For this year's Google Summer of Code, I have been mentoring Yuru Roy Shao, on
[**Integrating Apport with
Debian**](https://wiki.debian.org/SummerOfCode2015/Projects#SummerOfCode2015.2FProjects.2FApportForDebian.Apport_for_Debian).
Yuru is a CS student studying at University of Michigan, USA completing his
PhD.

For around 2+ years, Apport was packaged for Debian, but remained in
Experimental. While we did have a separate (Debian BTS aware) crashdb, the
general concerns (bug spam, too many duplicates etc) were the reason we held
its inclusion.

With this GSoC, Yuru has been bringing some of the missing integration pieces
of Debian. For example, we are now using debianbts to to talk to the Debian
BTS Server, and fetch bug reports for the user.

While apport's Bug Report data collection itself is very comprehensive, still
for Debian, it will have the option to use native as well as reportbug. This
will allow us to use the many hooks people have integrated so far with
reportbug. Both Bug Report data collectors will be available.

Yuru has blogged about his GSoC progress so far,
[here](http://blog.yurushao.info/2015/07/Debian-Apport-GSoC/). Please do have
a read, and let us know your views. If the travel formalities work out well, I
intend to attend Debconf this year, and can talk in more detail.

