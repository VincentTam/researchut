{
    "title": "Laptop Mode Tools 1.72",
    "date": "2018-02-01T12:45:06-05:00",
    "lastmod": "2018-02-01T23:30:55-05:00",
    "draft": "false",
    "tags": [
        "Laptop Mode Tools",
        "power saving",
        "linux"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-1_72"
}

What a way to make a gift!

I'm pleased to announce the 1.72 release of Laptop Mode Tools. Major changes
include the port of the GUI configuration utility to Python 3 and PyQt5.
![](/sites/default/files/LMT-172.png)Some tweaks, fixes and enhancements in
current modules. Extending {black,white}list of devices to types other than
USB. Listing of devices by their devtype attribute.

A filtered list of changes is mentioned below. For the full log, please refer
to the git repository.

Source tarball, Feodra/SUSE RPM Packages available at:  
<https://github.com/rickysarraf/laptop-mode-tools/releases>

Debian packages will be available soon in Unstable.

Homepage: <https://github.com/rickysarraf/laptop-mode-tools/wiki>  
Mailing List: <https://groups.google.com/d/forum/laptop-mode-tools>





    
    
    1.72 - Thu Feb  1 21:59:24 IST 2018
        * Switch to PyQt5 and Python3
        * Add btrfs to list of filesystems for which we can set commit interval
        * Add pkexec invocation script
        * Add desktop file to upstream repo and invoke script
        * Update installer to includes gui wrappers
        * Install new SVG pixmap
        * Control all available cards in radeon-dpm
        * Prefer to use the new runtime pm autosuspend_delay_ms interface
        * tolerate broken device interfaces quietly
        * runtime-pm: Make {black,white}lists work with non-USB devices
        * send echo errors to verbose log
        * Extend blacklist by device types of devtype



### **What is Laptop Mode Tools**

    
    
    Description: Tools for Power Savings based on battery/AC status
     Laptop mode is a Linux kernel feature that allows your laptop to save
     considerable power, by allowing the hard drive to spin down for longer
     periods of time. This package contains the userland scripts that are
     needed to enable laptop mode.
     .
     It includes support for automatically enabling laptop mode when the
     computer is working on batteries. It also supports various other power
     management features, such as starting and stopping daemons depending on
     power mode, automatically hibernating if battery levels are too low, and
     adjusting terminal blanking and X11 screen blanking
     .
     laptop-mode-tools uses the Linux kernel's Laptop Mode feature and thus
     is also used on Desktops and Servers to conserve power
    

PS: This release took around 13 months. A lot of things changed, for me,
personally. Some life lessons learnt. Some idiots uncovered. But the best of
2017, I got married. I am hopeful to keep work-life balanced, including time
for FOSS.

