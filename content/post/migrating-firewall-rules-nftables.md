{
    "title": "Migrating Firewall from iptables to nftables",
    "date": "2018-11-18T19:36:06+05:30",
    "lastmod": "2018-11-18T19:36:06+05:30",
    "draft": "true",
    "tags": [
        "iptables",
        "firewall",
        "nftables"
    ],
    "categories": [
        "Debian-Blog",
        "Computing"
    ],
    "url": "/post/Migrating-Firewall-to-nftables"
}


## Firewall
My firewall service is very basic. It has had some iptables rules that
I have learnt and extended over the years.

The rules are all basic *iptables* syntax rules and are all written in a single
file, which is revision controlled. That further gets invoked through the
`/etc/network/if-pre-up.d` scripts.

Recently, turns out I hadn't paid attention that my firewall scripts weren't being loaded.
That is because very recently the newer **iptables** were packaged, which now default to
the **nf_tables** kernel backend. So this part is something that happened because I
ignored some of the *changelogs* and *NEWS* items that I usually do read about.

The other part of the problem was the following because I always assumed that the
command would succeed. So, in all, a *double failure*

```
rrs@chutzpah:~/tidBits/chutzpah (master)$ cat /etc/network/if-pre-up.d/firewall 
#!/bin/sh

/sbin/iptables-restore < /home/rrs/tidBits/chutzpah/firewall.rules
/sbin/ip6tables-restore < /home/rrs/tidBits/chutzpah/firewall.rules

18:42 ♒♒♒   ☺ 😄    
```
So I fixed this first to `return` the exit status for the command.

<br>
Then was to migrate my old rules from the `legacy iptables`

```
rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo iptables-legacy-restore < firewall.rules 
19:09 ♒♒♒   ☺    

rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo ip6tables-legacy-restore < firewall.rules 
19:09 ♒♒♒   ☺    

rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo iptables-legacy-save > /tmp/firewall.legacy
19:10 ♒♒♒   ☺    

rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo ip6tables-legacy-save > /tmp/firewall6.legacy
19:10 ♒♒♒   ☺    

rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo iptables-nft-restore < /tmp/firewall.legacy
19:11 ♒♒♒   ☺    

rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo ip6tables-nft-restore < /tmp/firewall6.legacy
19:11 ♒♒♒   ☺    

rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo iptables-nft-save > chutzpah-iptables-nft.txt
# Warning: iptables-legacy tables present, use iptables-legacy-save to see them
19:12 ♒♒♒   ☺    

rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo ip6tables-nft-save > chutzpah-ip6tables-nft.txt
# Warning: ip6tables-legacy tables present, use ip6tables-legacy-save to see them
19:12 ♒♒♒   ☺    
```

<br><br>
And then restore the same way, but through the newer **iptables-nft** tool. Once done, the
listing could be done, either with **iptables-nft -L** or **nft list ruleset**

```
rrs@chutzpah:~/tidBits/chutzpah (master)$ sudo nft list ruleset
table ip mangle {
        chain PREROUTING {
                type filter hook prerouting priority -150; policy accept;
        }

        chain INPUT {
                type filter hook input priority -150; policy accept;
        }
.....snipped.....
```
