{
    "title": "World Class Company, Third Class Ethics",
    "date": "2006-08-30T22:29:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "tags": [
        "Dell",
        "Dell India"
    ],
    "categories": [
        "Rant"
    ]
}

**World Class Company - Third Class Ethics**

Well!! After almost around 3 months after my exit from my previous employer (
**Dell R &D Center, Bangalore - India**), I've now had some time to rant about
the experiences I had.

Hereforth, unless explicitly mentioned, When **Dell** is quoted, I mean as
**Dell R &D Center, Bangalore - India, **and **not** any other Dell facility
because it is not fair for me to comment about the other centers since I 've
not worked there.

I worked for Dell's Linux Engineering Team for around 21 months. Dell being my
debut Multi National Company, it was great and exciting to work.

  * Dell is a great and renowned brand.   

  * It's a huge company with a huge userbase.
  * And on top of all, it is a very successful company

The couple of things which I experienced at Dell were very frustrating.
Interestingly, companies makes most such issues as company policies, often
making no options for an individual to give a thought to it.

Though being in the Linux Engineering team and expected to work to all the
fullest extents, you aren't given any resource for it. Well! Ah!! That's not
within the company policy is what you hear.

Public Mailing List archives are blocked. Well! Company Policy. But still you
hear, "We understand Linux, We support it."

Probably it was me, an odd, who had the feeling of being closed in a jail
environment, working at Dell. It felt more to me like I'm again being ruled by
the British Empire (East India Company).

With a belief in myself that not every company would be the same, the thought
to quit Dell came to my mind after 21 months. A tough decision to make, but I
had to.

Truly speaking, the whole idea of taking the decision to quit Dell was because
I didn't believe the way Dell treated its Engineers. My immediate managers
often kept telling me that I hadn't seen the real world and that the outside
world was much more worse. Well well well, one fine day, (after being rejected
in an interview from a search giant. I did screw my skills a lot in those 21
months) I realised that if I had to follow what I believed, I had to take the
risk. I didn't like the idea of ignoring what I belived, for say 5 years, and
then realise that what **I** believed was correct. And after 5 years, that
would have been wrong and I'd have been royall screwed up to be a slave. With
this belief, I finally concluded to take the tough decision to quit. And quit
did I. And this is where the real drama starts.

Just about a month before I quit, Dell started facing high attrition rate. And
one fine day, a sweet e-mail was sent stating that the notice period to have
changed from one to two months. Hmm!! Maybe that is one of the ways to control
attrition.

I quit at the time just recently after my reviews, where I was:

  * Graded an EP
  * Had Promotion
  * Had a good pay hike

And when I resigned, I was told to be **incompetent.** Interestingly.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

Anyway, I didn't have much to care about it.

My statement till I quit was, " **I believe the way Dell defines engineering
is not the case. I want to go out and see the world now. I don 't want to wait
for 5 years and then come to know that I was right. I want to risk now. And if
I feel my thoughts were in-correct, I'd be gratefully willing to return back
to Dell.**" which has now changed after all the cheap drama that they did, to,
" **I really didn 't expect you people to be of such cheap attitude. I never
expected this, at least not from Dell. Forget me joining, I'd not even think
of refering a fresher to Dell."**

Upon resignation, I mentioned to my manager that I'd like to be relieved in 30
days time period. The manager, from the team's work prespective, had no issues
as I wasn't owning anything which needed to be transitioned. He was okay if
the HR was comfortable relieving me.

The HR was okay to relieve me if my manager confirmed that I wasn't holding
anything which needed transition. Thus the HR **confirmed** that I would be
relieved in **30 days**.

But the grand daddy for both, the senior manager wasn't happy with it. This
was the first time for him in his 2-3 year tenure with his team to have a high
attrition rate. So one fine evening I was asked by my Sr. Manager to have a
talk where he clearly mentioned that he'd not relieve me. I told him that the
HR had confirmed me that I'll be relieved in 30 days if I am not owning any
responsibility, which I wasn't owning any.

And Wow!! This was his reply, " **Take it clear. No matter what you do, If you
leave in 30 days, I will never give you the relieving letter. And if the HR
has said so, he 'll lose his job**". The explanations he gave was that it was
difficult to find a qualified replacement so early and that work would be
affected. But interestingly, 2 days before I was supposed to complete my
notice period, my email was removed from all important distribution lists. I
wonder how he'd define me working without access to emails.

That's when I decided, well you can suck the letter, that's what best is in
your reach. I don't need it. If someone is to hire me, he's to hire me for my
skills. Not for the pieces of paper.

And next day, the HR was just another lier. More to note is that, people there
make statements like, " **Who confirmed it to you ? Do you have a proof that
someone has committed to you ? Do you have it in written ?** "

I believed people were valued more by their words. But no, at least not there.
And it really made me more stronger on my decision to quit in 30 days. I was
also threatened by the HR that **they wouldn 't pay me, they wouldn't do my
final settlement**. Well! Now I was already prepared for such statement.
That's the cheapest level upto which they can go. That's all they can do.

But I did service the number of days in the notice period I had mentioned even
though they had declared that they won't pay me.

Today, I'm very happy of me and my beliefs. I'm happy and proud that I made
the correct decision. And that, what and the way I thought, was correct.

For people who plan to join Dell, I have some suggestions:

  * Don't go by name (They recently changed to R&D Center). Hint: Remember a little old Sprite ad.
  * Don't trust people there by words. Never. They are not worth it. Communicate with them only in written. 
  * Don't blindly believe in what is shown there in slides. That's plain wrong. Do give it a second thought.
  * Keep yourself updated with your surroundings also. You might not be given the right news. Often news is filtered.

  
And all this is not that I hate Dell. Here, I'm refering Dell as an
international company and not the Bangalore - India Center. I'm referring Dell
as an international brand. Hell! NO. I like it. In fact I myself am a happy
and proud owner of a Dell XPS M1210 machine. Yeah! I do like Dell products.

