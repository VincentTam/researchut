{
    "title": "NetworkManager Debian Network Interfaces",
    "date": "2007-05-30T18:49:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

So many of you might be aware of NetworkManager. The big new tool from RedHat
does simplify Network Configuration on your Linux box.

But there lies a small problem with NetworkManager. It is very picky about
what it is going to touch and what not.

Here's a paragraph from the README.Debian file:

> Configuration of wireless and ethernet interfaces  
>  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
>  Only devices that are *not* listed in /etc/network/interfaces or which have  
>  been configured "auto" and "dhcp" (with no other options) are managed by
NM.  
>  
>  This way you can setup a custom (static) configuration for a device and NM  
>  will not try to override this setting.  
>  
>  After modifying /etc/network/interfaces you have to restart NM with the  
>  command "/etc/dbus-1/event.d/25NetworkManager restart".  
>  
>  Examples:  
>  
>  1.)  
>  auto wlan0  
>  iface wlan0 inet dhcp  
>  -> This device is managed by NM.  
>  
>  1.a)  
>  allow-hotplug eth0  
>  iface eth0 inet dhcp  
>  -> This device is managed by NM  
>  
>  2.)  
>  auto wlan0  
>  iface wlan0 inet dhcp  
>  wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf  
>  -> This devices is *not* managed by NM because it has additional options.  
>  
>  3.)  
>  iface wlan0 inet dhcp  
>  -> This device is *not* managed by NM because it is not set to "auto".  
>  
>  4.)  
>  iface eth0 inet static  
>  address 192.168.1.10  
>  netmask 255.255.255.0  
>  gateway 192.168.1.1  
>  -> This device is *not* managed by NM because it is configured as "static"
and  
>  has additional options.  
>  
>  5.)  
>  Device is not listed in /etc/network/interfaces.  
>  -> Device is managed by NM.

But most Debian users will have many configurations which will have some or
the other combination because of which it doesn't qualify to be handled by NM.

There's one trick which can be used to fool NM and still make it use those
devices.

Here's what my /etc/network/interfaces file looks like:

> auto lo  
>  iface lo inet loopback  
>  
>  auto eth2=eth2office  
>  iface eth2office inet dhcp  
>  wireless-essid netapp  
>  up /sbin/iptables -A INPUT -i eth2 -m state --state NEW,INVALID -j DROP;
/usr/sbin/vpnc-connect; #/usr/local/bin/vpnclient connect NetApp  
>  pre-down /usr/sbin/vpnc-disconnect; #/usr/local/bin/vpnclient disconnect  
>  down /sbin/iptables -D INPUT -i eth2 -m state --state NEW,INVALID -j DROP;

With this setup, I can still have NM handle my eth2 device (which is a
Wireless Device). And when I feel NM is not good enough in handling, I can
switch it to Offline mode and ifup this interface manually.

