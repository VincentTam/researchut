{
    "title": "Blog Update",
    "date": "2010-05-04T22:04:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "General"
    ]
}

As you must have noticed, the interface of the blog has changed. I just am in
the middle of migrating to [PivotX](http://www.pivotx.net).

I started blogging around 5 yrs back. Having had my domain, I wanted to have
my own blogging software. Most of the blogging tools had dependency on
databases and stuff where as I was looking for something very simple while at
the same time easy to use, feel and administer.

Pivot was my decision then. And it was the right one. Today, migration to
PivotX was not that very difficult. All my blog posts were easily migrated to
the new PivotX tool.

One of my pre-req was to have my blogging software _not_ use a db. It should
provide a flat file interface. Pivot[X] was and is the same way today. It
still supports flat file databases without compromising on any of the
features.

Thank you to the Pivot[X] team from an old time user.

