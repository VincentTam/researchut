{
    "title": "Lord Shiva Curse Lord Brahma",
    "date": "2016-02-25T06:32:12-05:00",
    "lastmod": "2016-02-25T06:32:12-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "shiva",
        "vishnu",
        "brahma",
        "ketaki"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/shiva-curse-brahma"
}

[![url78](/sites/default/files/Vibhor/url78.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/url78.jpg)

Once while travelling around the universe Lord Brahma reached the abode of
Lord Vishnu. He saw Lord Vishnu resting on Shesh-Nag and being attended by
Garuda and other attendants.

[![Lord vishnu wallpapers-20012012 \(2\)](/sites/default/files/Vibhor/lord-
vishnu-
wallpapers-20012012-2.jpg)](https://vibhormahajan.files.wordpress.com/2012/05
/lord-vishnu-wallpapers-20012012-2.jpg)

Lord Vishnu did not get up to receive Lord Brahma. This act of Lord Vishnu got
him angry. Very soon, verbal dual erupted between them to determine who is
superior. It became so severe that a battle was fought between them, which
continued for very long time.

All the deities arrived from heaven to watch the battle. They became very
worried when they saw no sign of battle coming to an end. They decided to go
to Lord Shiva, to seek his help.

[![url456](/sites/default/files/Vibhor/url456.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/url456.jpg)

Even though in meditation Lord Shiva knew everything, but still feigning
ignorance, he asked about the well beings of the world. The deities told him
about the battle, fought between Lord Brahma and Lord Vishnu. Lord Shiva then
sent his one hundred Ganas to pacify both of them, but still the war still did
not stop.

Then Lord Shiva himself went there. When Lord Shiva reached there, he saw that
Lord Brahma and Lord Vishnu were about to use their deadly weapons- Brahma
Astra and Narayanastra respectively. Fearing the destruction, which these
deadly weapons would cause, Lord Shiva manifested himself in the form of
‘Analstamba’ (pillar of fire) between them. Lord Brahma and Lord Vishnu were
very surprised to see the pillar of fire, which was so enormous in size that
it reached the sky and penetrated down the Earth.

Lord Vishnu transformed himself into a boar and went to the ‘Patal’
(Underworld) to find the base of that ‘Pillar of fire’. But he was
unsuccessful in his attempt and came back. Similarly Lord Brahma transformed
himself into a swan and flew up in the sky to find its limit. While going
through the aerial route he met a withered ‘Ketaki’ flower, which had still
some freshness and fragrance left in it. Lord Shiva smiled at the futile
attempts of Lord Brahma and Lord Vishnu. As a result of his smile the Ketaki
flower fell down from the branch. Ketaki flower told Lord Brahma that he had
been present there since the beginning of the creation, but was unable to know
about the origin of that ‘Pillar of fire’. The flower also advised Lord Brahma
against making any effort in that direction, as it would be of no use. Lord
Brahma then sought the help of Ketaki flower to give a false witness before
Lord Vishnu, that he (Lord Brahma) had been successful in seeing the limit of
that pillar of fire. Ketaki flower agreed. Both of them went to Lord Vishnu
and Lord Brahma told him that he had seen the limit of that Pillar of fire.
Ketaki flower gave a witness. Lord Vishnu accepted the superiority of Lord
Brahma

Lord Shiva became very angry with Lord Brahma, but was pleased with Lord
Vishnu and accorded him the same status as that of his own. He proceeded to
punish Lord Brahma for his falsehood

After according same status to Lord Vishnu as that of his own, Lord Shiva
opened his third eye and from it manifested ‘Bhairav’.

[![Shiva comic virgin](/sites/default/files/Vibhor/shiva-comic-
virgin.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/shiva-comic-
virgin.jpg)

Lord Shiva ordered Bhairav to kill Lord Brahma. Bhairav severed the fifth head
of Lord Brahma with his sword. Lord Brahma became very terrified, he was
trembling in fear. Lord Vishnu felt pity on his condition and requested Lord
Shiva to forgive him. Lord Shiva then stopped Bhairav, but told Lord Brahma
–“You spoke untruth with a desire to become worship able. It is my curse that,
you will not be worshipped by anybody. You will posses only four heads.” Lord
Brahma begged for forgiveness. Lord Shiva feeling pity on him gave him a boon
of being the presiding deity of all the yagya (Yajna). Similarly the Ketaki
flower was also prohibited from being used during worship. But when Ketaki
flower tendered his apology Lord Shiva gave blessing that it would be
fortunate to be offered to Lord Vishnu during the worship.

**“Har Har Mahadev”**



### Copyright (C) Vibhor Mahajan

