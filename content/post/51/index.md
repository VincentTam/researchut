{
    "title": "distcc",
    "date": "2008-03-16T13:40:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "gcc"
    ],
    "categories": [
        "Tools"
    ]
}

When Rusty was here during FOSS.IN, he mentioned about utilities without which
how difficult life would be.

  * Mercurial
  * distcc
  * ccache

I have been using Mercurial and really love it. It is wonderful. I always
thought of spending some time with distcc and see what all it can help.

I am amazed. distcc is exactly what it says. And the setup is so much simple.

I had been fucking my laptop by building KDE4 on it every week. Now I can give
her some time to soothe and cool down. She'd gone very hot when building on
her
![;-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_121.gif)

BTW, KDE4 + distcc doesn't require much effort. Just modify the cmakekde() as
mentioned on Techbase and customize it to include CC=distcc CPP=distcc
CXX=distcc just before the keyword cmake

