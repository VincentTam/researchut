{
    "title": "Lord Vishnu – Dashavatharam (Part – V)",
    "date": "2016-02-25T05:49:07-05:00",
    "lastmod": "2016-02-25T05:49:07-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "krishna"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/lord-vishnu-dashvatharam-5"
}

## Lord Vishnu – Dashavatharam (Part – V)



[![lord-krishna4263jpeg](/sites/default/files/Vibhor//lord-
krishna4263jpeg.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/lord-
krishna4263jpeg.jpg)

**“Jai Shri Krishna”  
“जय श्री कृष्णा”**

Lord Krishna was born during turbulent times in Mathura. Kansa, son of
Ugrasena, audaciously disbanded the Yadava ruling council and declared himself
dictator of Mathura with the support of his father-in-law, Jarasandha, the
powerful king of Magadha. All those who protested were either killed or
imprisoned.

Kansa’s younger sister, Devaki had married Vasudeva. On the wedding day,
oracles foretold that the eighth child born of this union would be the killer
of Kansa. A terrified Kansa wanted to kill his sister, but was persuaded to
let her live on condition that Vasudeva would present to him their eighth
child as soon as it was born.

When Devaki bore her first child, Kansa became nervous. He stormed into her
chambers, grabbed her firstborn by ankles and smashed its head against the
stony floor. And so it came to pass, Devaki kept producing children and Kansa
kept killing them as soon as they were born.

Thus were killed six children of Devaki and Vasudeva. The Rishis revealed
–“Your children suffer the pain of dying at birth because in their past life
they angered sages with their misbehaviour. And you suffer the pain of
watching them die because in your past you angered sages by stealing cows for
your yagna. All suffering has its roots in karma. But fear not, the seventh
and eighth child will bring you joy. The seventh will be the herald of God.
The eighth will be God himself.”

When Devaki conceived the seventh child. A goddess called Yogamaya magically
transported the unborn child into the womb of Vasudeva’s other wife Rohini,
who lived with her brother Nanda in the village of cowherds, Gokul, across the
river Yamuna. The child thus conceived was Balarama, fair as the moon, strong
as a herd of elephants. He was an incarnation of Adi-Ananta-Shesha, the
serpent with thousand hoods in whose coils rests Lord Vishnu.

[![balram](/sites/default/files/Vibhor/balram.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/balram.jpg)

When Lord Vishnu plucked a dark hair from his chest and placed it in Devaki’s
womb, thus was conceived her eighth child. He slipped out of his mother’s womb
nine months later on a dark and stormy night. He was as dark as the darkest
night and as charming as the sun is to a lotus flower.

Yoagamaya caused the whole city to sleep and advised Vasudeva to place the
child in a basket and take it out of the city, across the river, to Gokul.

[![23023377_BG_Vasudeva_Krishna](/sites/default/files/Vibhor/23023377_bg_vasudeva_krishna.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/23023377_bg_vasudeva_krishna.jpg)

At Gokul, in the cattle sheds, he found Yashoda (Nanda’s wife), sleeping with
a newborn girl beside her. Instructed by Yogamaya, Vasudeva exchanged the
babies and returned to Mathura.

The Next day, Kansa strode into Devaki’s chamber and picked up the eighth
child with an intent of dashing her head to the ground. But the child slipped
out of his hands, flew into the sky, and transformed into a splendid goddess
with eight arms, each bearing magnificent weapons and announced that the
killer of Kansa was still alive and that Kansa would die as foretold.

Lord Krishna meanwhile grew up among gopas (Cowherds) and gopis (Milkmaids) of
Gokul. The arrival of Lord Krishna changed everything in Gokul. His life,
right from his birth, was an adventure.

Kansa sent Putana, a wet-nurse who had poison in her breasts, to kill all the
newborns around Mathura. But when Lord Krishna sucked on her breasts, he
sucked out not only the poison but also Putana’s life.

Such incidents with the demons so frightened Yashoda that she insisted that
the entire village move from Gokul to a more auspicious place on the banks of
river Yamuna, next to Tulsi plants, at the base of the Govardhan hill. It came
to be known as Vrindavan.

Here Lord krishna grew up with a fondness for butter. Nothing gave him greater
pleasure than raiding the dairies of milkmaids and stealing all that had been
churned ans stored in pots hanging from the rafters.

[![TA-043_600](/sites/default/files/Vibhor/ta-
043_600.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/ta-
043_600.jpg)

As Lord Krishna grew he opposed the blind rituals of the Rishis; he preferred
acts of charity and devotion. Every year. Kansa performed a great yagna where
ghee was poured into fire to please Indra, the rain God. Lord Krishna opposed
this practice. When the villagers decided not to send ghee from the village
for the yagna, Indra got angry and caused torrential rain to fall flooding the
village.

It was then that Lord Krishna picked up the Govardhan mountain with his little
finger and raised it, turning it into a giant parasol that protected the whole
village from the downpour. It was enough to tell Indra that Lord Krishna was
no ordinary man, he was God on Earth.

[![4](/sites/default/files/Vibhor/4.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/4.jpg)

Every night, outside the village, in forest, on the banks of Yamuna, in a
meadow known as Madhuvan that was full of fragrant flowers, Lord Krishna would
stand and play the flute. All the milkmaids would leave their homes while the
rest of the family slept, and come to this meadow to dance around Lord
Krishna. This was their special pleasure.

[![Radha, Gopis and
Krishna](/sites/default/files/Vibhor/pasttime.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/pasttime.jpg)

This wonderful relationship of Lord Krishna with the milkmaids came to an end
when Kansa sent a chariot to Vrindavan ordering Lord Krishna to come to
Mathura and participate in a wrestling competition. Balarama accompanied him.

No sooner Lord Krishna entered the city of Mathura and won the hearts of the
Yadavas with his strength and beauty. Fearlessly, Lord Krishna killed Kansa’s
washerman who had showered abuse on him. He then broke the royal bow on
display and subdued the royal elephant who tried to block his path to the
arena. Lord Krishna and Balarama then defeated all the wrestlers of Mathura.
The audience cheered the two making Kansa angrier than ever. Kansa ordered
that Lord Krishna and all those who had cheered him be killed. In response,
Lord Krishna pounced on Kansa and smote him to death.

[![DeathofKansa_22669](/sites/default/files/Vibhor/deathofkansa_22669.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/deathofkansa_22669.jpg)

Having killed Kansa, Lord Krishna was hailed as the liberator of the Yadavas.
His true identity as the son of Vasudeva and Devaki was revealed. He was
recognized as a Kshatriya, a decendant of Yadu.

**“Jai Shri Krishna”  
“जय श्री कृष्णा”**



# Copyright (C) Vibhor Mahajan

