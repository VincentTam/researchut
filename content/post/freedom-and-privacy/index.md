{
    "title": "Freedom, Privacy and Our Choices",
    "date": "2015-12-04T14:05:31-05:00",
    "lastmod": "2015-12-04T14:05:31-05:00",
    "draft": "false",
    "tags": [
        "KDE",
        "gnome",
        "Yoga 2 13",
        "Yoga",
        "Lenovo",
        "Touch",
        "UI",
        "Design",
        "Freedom",
        "Privacy"
    ],
    "categories": [
        "Debian-Blog",
        "Technology"
    ],
    "url": "/blog/freedom-and-privacy"
}

When I bought my [Lenovo Yoga 2 13](http://www.researchut.com/blog/lenovo-
yoga-2-13-debian), I had great plans. I wanted a device, that I could use
both, as a laptop, and also as a tablet. The path hasn't been easy. But then,
no path in Free Software against Freedom and Privacy has been easy. The choice
is left to us, the users.

I had been a long time KDE User. With KDE's decision to have different UIs for
different form factors (plasma active, plasma netbook, plasma desktop), it was
now time to try something different. I had 2 choices to explore: **Unity** and
**GNOME**. But given that Unity is not feasible in Debian, the obvious first
option to explore was GNOME. And I guess my timing was perfect. I started with
3.14 which showed great promises about touch support, followed by 3.16 and now
3.18. There 's still a long way to go, but GNOME definitely is in the right
direction and improves impressively with each release.

I would like to thank the Debian GNOME Team. You guys just are amazing. The
pace at which the packages are prepared and pushed, it is a tough job. So,
thank you.

Now that I use GNOME, I also follow some GNOME development, file bugs and read
User Feedback. While (many of) the design decisions that GNOME makes today is
in line with what I desire, there are other users who aren't happy. Many don't
like the new UI design GNOME is heading to, i.e. the fusion of the UI such
that it is usable on both form factors: _Desktop and Tablet_. People find the
Toolbars bloated. GNOME developer Allan Day has a good [blog
post](https://blogs.gnome.org/aday/2014/08/27/gnome-design-saving-you-space-
since-2009-or-so/) showing the facts.

But I think there's the other aspect which needs to be emphasized. By
improving the same tools, the GNOME devs are making them capable to be used
both, on Laptops and Tablets. This is important to me as a user. I still have
the freedom of the tools I use.. And those tools don't sniff. By using the
same tools in both modes, I also can reliably build a workflow. I remember
when I was moving from IceDove to Evolution. It was painful, but once done, I
now have an email client I can still use with basic touch workflow.

If you take current GNOME out of the picture, what would a user do. Live with
a Laptop and a Tablet, 2 separate device. The laptop running Debian and the
Tablet ???

In the direction GNOME is in, it allows me to be liberated for both the use
cases. My most valuable assets: __data__ and __time__ , both are efficient in
use. And not to forget the reason for this blog entry: **Freedom** and
**Privacy**.

Sure you can choose not to buy/use a tablet at all, and live in a world 10yrs
behind.

By improving the same tools for both use cases, GNOME is also ensuring that
they don't throw away learnings from the past. Today Gtk is touch ready and
the same libs also are running when on Laptop. A feature addition/enhancement
gets leveraged by both. The Search Bar addition into File Dialog is available
both, in Laptop and Tablet mode. And the list could go on....

On the other hand, I'm not sure what the KDE User Experience is today. But if
I have to log out of my plasma desktop session, to use in Tablet Mode (plasma
active), that workflow is already a broken one.

I do miss the flexibility KDE used to provide, where in a substantial part of
the user flow could be heavily customized. This is one gripe I have with the
GNOME Project in general. They just don't balance things at all. Elegance is
not about cutting out features.

There are simple things you'll miss in GNOME if you come from KDE. Like Ctrl+R
in Kwrite. You can't do that in GEdit.

Today, with companies like Google, who are developing tools that are cross-
platform, stand as good examples as to what GNOME/KDE should do.

 _ _ **Don 't bloat the UI, but then again don't strip it off either.**__

The Chromium Browser's UI is a great example. The Chromium UI Devs are pretty
strict on what new visible field can be added. But under one Wrench button,
they provide a world for everyone to be happy. Power Users can run through the
option available, and Super Power Users can use things like chrome://settings
and chrome://flags

I the new world today, where without a proprietary stack running on your mobil
device, it is literally useless; Efforts like that of from the GNOME Project,
gives some hope.

