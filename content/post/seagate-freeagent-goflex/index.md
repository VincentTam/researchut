{
    "title": "Seagate FreeAgent GoFlex",
    "date": "2011-11-07T09:36:36-05:00",
    "lastmod": "2013-01-31T13:46:36-05:00",
    "draft": "false",
    "tags": [
        "scsi",
        "usb",
        "external hdd"
    ],
    "categories": [
        "Debian-Blog",
        "Computing"
    ],
    "url": "/blog/seagate-freeagent-goflex"
}

I recently purchased a Seagate FreeAgent GoFlex 1 TB hard drive. It is a 2.5"
compact disk that you connect over USB. It draws its power from the USB
controller. My previous Extern HDDs were all 2.5" laptop drives, for which I
bought a USB enclosure. Those devices always worked perfect, as in:

  * They were automatically detected
  * Partitions/File Systems understood
  * Desktop systems would prompt for actions

The typical kernel messages I got for these devices were:

[56063.268107] usb 1-1: new high speed USB device number 13 using
ehci_hcd[56063.401635] usb 1-1: New USB device found, idVendor=14cd,
idProduct=6116[56063.401645] usb 1-1: New USB device strings: Mfr=1,
Product=3, SerialNumber=2[56063.401652] usb 1-1: Product: USB 2.0  SATA BRIDGE
[56063.401658] usb 1-1: Manufacturer: Super Top   [56063.401663] usb 1-1:
SerialNumber: M6116018VF16[56063.402857] scsi8 : usb-storage
1-1:1.0[56064.400896] scsi 8:0:0:0: Direct-Access     WDC WD50 00BEVT-24A0RT0
PQ: 0 ANSI: 0[56064.401576] sd 8:0:0:0: Attached scsi generic sg2 type
0[56064.402102] sd 8:0:0:0: [sdb] 976773168 512-byte logical blocks: (500
GB/465 GiB)[56064.402615] sd 8:0:0:0: [sdb] Write Protect is off[56064.402618]
sd 8:0:0:0: [sdb] Mode Sense: 03 00 00 00[56064.403101] sd 8:0:0:0: [sdb] No
Caching mode page present[56064.403105] sd 8:0:0:0: [sdb] Assuming drive
cache: write through[56064.404861] sd 8:0:0:0: [sdb] No Caching mode page
present[56064.404864] sd 8:0:0:0: [sdb] Assuming drive cache: write
through[56064.419657]  sdb: sdb1[56064.421850] sd 8:0:0:0: [sdb] No Caching
mode page present[56064.421854] sd 8:0:0:0: [sdb] Assuming drive cache: write
through[56064.421857] sd 8:0:0:0: [sdb] Attached SCSI disk



But for my Seagate FreeAgent GoFlex HDD, things were different. The kernel
would detect but then it would not mount. The error reported is that the
device is busy. My FreeAgent's kernel messages look similar to what the
regular one has:



[  168.520140] usb 1-1: new high speed USB device number 5 using ehci_hcd[
168.657424] usb 1-1: New USB device found, idVendor=0bc2, idProduct=5021[
168.657433] usb 1-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3[
168.657439] usb 1-1: Product: FreeAgent GoFlex[  168.657444] usb 1-1:
Manufacturer: Seagate[  168.657449] usb 1-1: SerialNumber: NA0C1BML[
168.659079] scsi5 : usb-storage 1-1:1.0[  169.657136] scsi 5:0:0:0: Direct-
Access     Seagate  FreeAgent GoFlex 0148 PQ: 0 ANSI: 4[  169.708786] sd
5:0:0:0: Attached scsi generic sg3 type 0[  169.709079] sd 5:0:0:0: [sdc]
1953525167 512-byte logical blocks: (1.00 TB/931 GiB)[  169.709954] sd
5:0:0:0: [sdc] Write Protect is off[  169.709963] sd 5:0:0:0: [sdc] Mode
Sense: 1c 00 00 00[  169.710567] sd 5:0:0:0: [sdc] Write cache: enabled, read
cache: enabled, doesn't support DPO or FUA[  169.759942]  sdc: sdc1[
169.762050] sd 5:0:0:0: [sdc] Attached SCSI disk



As the Device Mapper Multipath Debian Maintainer, I have multipath-tools
installed on my laptop. Turns out, for some reason, the  device is consumer by
the device mapper multipath stack.

20:12:58 rrs@champaran:~$ ls /dev/mapper/1Seagate@  1Seagate FreeAgent GoFlex
N  1Seagate FreeAgent GoFlex            Np1  control  LocalDisk-ROOT@
LocalDisk-SWAP@



Also the block device ID listing is enumerated as type SCSI.





19:56:44 rrs@champaran:~$ ls /dev/disk/by-id/ata-
HITACHI_HTS723216L9SA60_091220FC1220NCJASEVG@
scsi-SATA_HITACHI_HTS7232091220FC1220NCJASEVG@ata-
HITACHI_HTS723216L9SA60_091220FC1220NCJASEVG-part1@
scsi-SATA_HITACHI_HTS7232091220FC1220NCJASEVG-part1@ata-
HITACHI_HTS723216L9SA60_091220FC1220NCJASEVG-part2@
scsi-SATA_HITACHI_HTS7232091220FC1220NCJASEVG-part2@ata-
HITACHI_HTS723216L9SA60_091220FC1220NCJASEVG-part3@
scsi-SATA_HITACHI_HTS7232091220FC1220NCJASEVG-part3@ata-HL-DT-
ST_DVDRAM_GU10N_M189CNI1127@                                         **scsi-
SSeagate_FreeAgent_GoFle_NA0C1BML@** ata-ST1000LM010-9YH146_W1000ZD8@
**usb-WDC_WD12_00BEVE-11UYT0_ST_Killer-0:0@** **dm-name-1Seagate@
usb-WDC_WD12_00BEVE-11UYT0_ST_Killer-0:0-part1@**dm-name-LocalDisk-ROOT@
wwn-0x5000c5003d19c8c2@dm-name-LocalDisk-SWAP@
wwn-0x5000cca586e112bc@dm-uuid-LVM-
buywwzKkpfKG2RegankA2nPkmFBBPFe3D5DepV8w8nLrHfoAjIIQVnakOQJZEqJX@  wwn-
0x5000cca586e112bc-part1@dm-uuid-LVM-
buywwzKkpfKG2RegankA2nPkmFBBPFe3k9aJfc9B7wRmVIwfoagffHUZjuN6c4cM@  wwn-
0x5000cca586e112bc-part2@ **dm-uuid-mpath-1Seagate@**
wwn-0x5000cca586e112bc-part3@ **raid-1Seagate@**





But I'm not sure why the same doesn't happend for my external laptop drive. It
gets properly tagged as device type USB.



So Laptop or Server, if you have a FreeAgent that you want to connect to your
machine, and see the device busy error when accessing the device['s] directly,
do the following:

First, the ID of the device.



20:17:43 rrs@champaran:~$ /lib/udev/scsi_id --whitelisted --page=0x83
--device=/dev/sdc1Seagate FreeAgent GoFlex            N



Add the ID to /etc/multipath.conf under blacklist section



blacklist {       wwid "1Seagate FreeAgent GoFlex*"        wwid "1Seagate FA
GoFlex Desk*"#       devnode "^(ram|raw|loop|fd|md|dm-|sr|scd|st)[0-9]*"#
devnode "^hd[a-z][[0-9]*]"#       device {#               vendor DEC.*#
product MSA[15]00#       }}



Run _mulitpath -F_ to flush the unused maps.



Run _multipath -v3_ to ensure that now the device is blacklisted.

Nov 03 20:19:02 | sdc: (Seagate:FreeAgent GoFlex) wwid blacklisted



The front cause for the mis-behavior is:





# Coalesce multipath devices before multipathd is running (initramfs, early#
boot)ACTION=="add|change", SUBSYSTEM=="block", RUN+="/sbin/multipath -v0
/dev/$name"





But something else, maybe the default blacklist table, needs the actual fix.

