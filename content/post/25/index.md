{
    "title": "Backups && Recovery",
    "date": "2009-03-04T20:25:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools",
        "Computing"
    ]
}

**Backups**

> Most of the users using computers have a very high dependency on it. Day-by-
Day, our data is getting digitized. Everything is getting into electronic
formats ( **Movies/Pictures/Music** et cetera). If you are one, you know how
important it is to have a backup.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

> Lately, I haven't been using Microsoft Windows on a daily basis. So I'll
comment on Linux here.

> The definition of **Backup**  can be different. People like backing-up only
the **Important** data. The problem is that the term **Important**  is very
volatile. What is important to X is not necessarily important to Y.

> For my backup solutions, in the past, I've relied on a KDE Backup tool,
**Keep**. It internally uses **rdiff-backup**. It was good. It allowed
incremental backups. There were some hiccups here and there but overall it was
pretty good. But it was very difficult to tell the application about  What all
it should backup. And then, it needed to do a diff verification for every file
that was a candidate.

>  

> Restoration from rdiff-backup was not always great. Especially the
incremental backups. If something was messed up, it was tough to recover. For
example, assume that the backup was in progress. And you needed to rush for
home immediately aborting your backup. Keep wouldn't play good there.

>  

> Then came **LVM**. I've used LVM for years but never really looked at it as
a backup option. To start with, I'd say, LVM is the **Best Backup Tool** for
my needs. I described **Important**  above. For me, **Important** is my
**HOME** dir, my **/var/tmp/kdecache-rrs** dir, and my **/tmp/kde-rrs** dir.
Apart from that, my **/etc/** dir, my **/usr/loca/** dir. And many more that I
can't recollect. If there was one simple tool to backup all without worrying
of **Permission, Security Labels** et cetera, that'd be LVM.

> And LVM allows **Online Backups**. So I can have my **/** volume online and
still go ahead with the backup while I'm working.

> And depending on I want, I can do a **File** or a **Block** based backup.

>  

 **Recovery**

> So we know how important our data has become for us, depending on how
dependent you are on computers for your day-to-day life. I hate thinking about
it but increasing dependency on computers gets me worried about security.
Yeah!! You'd say Linux is more secure. (Wouldn't want to discuss in that
direction)

> I really like the **SELinux Security Features**. Most of the people
(including Enterprise Customers) I know,   disable **SELinux** on their
machines. Currently SELinux doesn't see a widespread integration into the
entire **Application Stack**. Thus apps just fail as SELinux restricts their
access.

>  

> On my Debian Box, SELinux is miles away from the kind of integration
packages have through apt. That sometimes makes me run to **Red Hat** based
distributions to see what their state is, on SELinux.

> So yesterday, I wiped off my Debian setup and installed **Fedora**. Used it
for a couple of hours and decided to go back to Debian again (More of a
personal taste).

> That's what I do once in a year.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

>  

> The thing I have the most, is to lose data. **Data** being - **My Settings**
and other stuff I mentioned above.

> So to recover Debian was just a couple of hours. Just had to do a **Block
Restoration** of the **Block Backed ROOT LV** to the new **LV**. And then, a
minor grub and kenrel installation. And voila, everything was back just as it
was yesterday.

>  

 **Nothing much to say. Thank you Alasdair and Team Device Mapper @ Red Hat
**

