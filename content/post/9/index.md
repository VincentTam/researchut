{
    "title": "SystemTap in Debian",
    "date": "2010-07-08T18:08:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "Mandatory Access Controls",
        "mac"
    ],
    "categories": [
        "Debian-Pages",
        "Tools",
        "Computing"
    ]
}

The latest kernel upload (2.6.32-16) brings goodies to SystemTap in Debian.
This version has added support for kprobes, on which systemtap has a major
dependency, for many of its features.

Most of the systemtap instrumentation should work now and all of this will be
part of the Squeeze release. Instrumenting the kernel modules still needs some
work (DBTS: #555549) but can be done.

