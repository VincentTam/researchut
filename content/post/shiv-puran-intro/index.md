{
    "title": "Shiva Puran – Introduction",
    "date": "2016-01-13T05:46:18-05:00",
    "lastmod": "2016-01-13T05:46:18-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "shiv"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/shiv-puran-intro"
}

[![lord-shiva-chalisa](/sites/default/files/lord-shiva-
chalisa.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/lord-shiva-
chalisa.jpg)

  
**The Glory Of Shiva Puran**

The Puran itself is the creation of Lord Shiva himself, the mystical wisdom
crafted in words, molded into one hundred thousand couplets or quartets
(Shlokas), divided into twelve chapters (Samhitas); and first revealed in
grace to the creator Lord Brahma who relayed it to his favoured son, Narada.
Then, it was relayed on in succession to Sanat Kumara and Ved Vyasa. The
latter condensed the massive volume into 24,000 shlokas categorised under
seven chapters namely:

 **Vidyeshwara Samhita, Rudra Samhita, Shatarudra Samhita, Kotirudra Samhita,
Uma Samhita, Kailasha Samhita and Vayaviya Samhita.**

The enlightenment with the mysticism of Shiva-Jnan was truly possible only for
one Sage Ved Vyasa, a literati of the sacred texts as he as. He could abridge
the original text of Shiva Puran to make it sharp and focused with distilled
mysticism. By the grace of Lord Shiva, the inspiration behind his endeavour
was to remove the diluting impression a voluminous text carried, out of
compassion to all and to impart the enlightenment to others in digest form. A
man of great literary talent he was. Later, he visualized the entire epic of
Mahabharata in his mind. Upon the suggestion of Lord Brahma, Ved Vyasa
dictated Mahabharata to Lord Ganesha who penned it down non-stop.

Learned Ved Vyasa, then imparted the knowledge of Shiv Puran to other
scholarly sages like Soota and Shuka. Sage Soota revealed the Mysticism and
glory of Shiva Puran to other holy men through a discourse. Soota was the son
of Romaharshana and a favoured disciple of learned Ved Vyasa.

 **Revelations by Soota**

A great many sages and seers had converged on the holy centre of Prayaga,
situated at the confluence of Ganga, Yamuna and mythical Saraswati by the will
of Lord. Sages headed by Shaunaka approached Sage Soota and requested him to
reveal the glory of mysticism of Shiva Puran to them. Soota gladly accepted to
oblige the holy group.

Spoke great Sage Soota:

"O accomplished  sirs! You are seized by the desire to know the glory of
divinely supreme Lord Shiva. Hear this, there is nothing more of value and
blessing full than reciting or hearing Shiva Puran. Even hearing one Katha
(Tale) contained therein, a part of it, an episode of just half of it earns
one Lord's blessings, redemption, salvation and deliverance from
transmigration."

Speaking thus, Sage Soota began the Katha's of Shiva Puran.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

