{
    "title": "Kernel Prayer",
    "date": "2005-07-27T22:07:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Fun"
    ]
}

Our kernel who art in the protected memory areas hallowed  
be thy interfaces (syscalls). thy init(8)/init(1M) come.  
thy will (memory protection and processes scheduling) be  
done on the userspace as it is done where thou reside.  
and give us our regular time quanta and forgive us our  
memory trespasses (violations) as we forgive to the  
meatware (and the software developers). and lead us  
not into a fault but deliver us from the SIGSEGV. amen.

