{
    "title": "CM7",
    "date": "2011-05-07T11:46:06-04:00",
    "lastmod": "2013-01-31T13:51:04-05:00",
    "draft": "false",
    "tags": [
        "Samsung Galaxy S",
        "CyanogenMod",
        "Android",
        "GingerBread"
    ],
    "categories": [
        "Debian-Blog",
        "Computing"
    ],
    "url": "/blog/cm7-samsung-galaxy-s"
}

CyanogenMod does list Samsung Galaxy S in its list of devices, but given that
it is marked **experimental** I was not very confident of trying it out. But
yesterday, I took the plunge. Even though experimental, the ROM is pretty
good. And the Cyanogen goodness just makes it awesome. This ROM (also being
AOSP GingerBread) is Super Fast.

So in a day of use, my impressions:

  * Battery performance has turned out to be better than what I was used to.
  * Cyanogen's enhancements are pretty impressive.
  * The CyanogenGalaxyS team's work is of very good quality, even though they've marked it experimental. (Thanks codeworkx, coolya and the team)

The CyanogenMod experience is also good because it makes me feel like home,
just like our Debian project.

  * Everything is AOSP Build
  * Non-Free Add-Ons are available from other sources pretty easily
  * Device support strategy is just like our architecture support strategy. If it qualifies QA standards, it gets listed as Officially Supported. Otherwise Experimental Support
  * Suffers the same proprietary drivers problem.

For anyone who hasn't tried it on the Galaxy S, give it a
[shot](http://forum.cyanogenmod.com/topic/17020-all-models-cyanogenmod-7-for-
samsung-galaxy-s-phones-experimental/). It is good.

