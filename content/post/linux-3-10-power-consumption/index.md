{
    "title": "Power consumption on Linux 3.10",
    "date": "2013-07-23T16:08:19-04:00",
    "lastmod": "2013-07-23T17:54:23-04:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools",
        "linux"
    ],
    "categories": [
        "Debian-Blog",
        "Computing"
    ],
    "url": "/blog/linux-3-10-power-consumption"
}

The power consumption on the Linux kernel 3.10 is pretty bad.

On kernel 3.10, with the follwing config, the PowerTop results are:



 _#_

 _# Timers subsystem_

 _#_

 _CONFIG_TICK_ONESHOT=y_

 _CONFIG_NO_HZ_COMMON=y_

 _# CONFIG_HZ_PERIODIC is not set_

 _CONFIG_NO_HZ_IDLE=y_

 _# CONFIG_NO_HZ_FULL is not set_

 _CONFIG_NO_HZ=y_

 _CONFIG_HIGH_RES_TIMERS=y_



PowerTOP v2.0     Overview   Idle stats   Frequency stats   Device stats
Tunables  



The battery reports a discharge rate of _**28.0 W**_

The estimated remaining time is 23 minutes



Summary: 1785.5 wakeups/second,  0.0 GPU ops/second, 0.0 VFS ops/sec and 22.1%
CPU use



Power est.      Usage       Events/s    Category       Description

  16.3 W     2915 rpm                   Device         Laptop fan

  5.11 W    100.0%                      Device         USB device: WALTON
Primo-X1 Primo-X1

  1.70 W     33.3%                      Device         Display backlight

  849 mW     33.3%                      Device         Display backlight

  425 mW     86.0 ms/s     330.7        Process        /usr/bin/konsole

  316 mW     63.9 ms/s      66.1        Process        /usr/bin/plasma-desktop

  142 mW     28.6 ms/s     396.8        Process        /usr/bin/X :0 -auth
/var/run/lightdm/root/:0 -nolisten tcp vt7

 64.1 mW     13.0 ms/s     198.4        Process        kwin -session
101261418fe3000136103713100000053880000_13746081

 53.6 mW     10.8 ms/s      0.00        Process        powertop

 35.9 mW      7.3 ms/s      66.1        Process
/usr/lib/chromium/chromium --type=plugin --plugin-path=/usr/li

 24.3 mW      4.9 ms/s     396.8        Interrupt      PS/2 Touchpad /
Keyboard / Mouse

 6.92 mW      1.4 ms/s      0.00        Interrupt      [48] i915

 5.94 mW      1.2 ms/s      66.1        Interrupt      [9] RCU(softirq)

 3.98 mW      0.8 ms/s      0.00        kWork          flush_to_ldisc

 3.78 mW      0.8 ms/s      66.1        Process        [ksoftirqd/2]

 3.33 mW    673.3 us/s      66.1        Process        [rcu_sched]

 1.80 mW    363.1 us/s      66.1        Interrupt      [1] timer(softirq)

 1.79 mW    363.0 us/s      0.00        Process        [ksoftirqd/4]



Where as on the 3.9 kernel:





The battery reports a discharge rate of _**13.2 W**_

The estimated remaining time is 43 minutes



Summary: 611.5 wakeups/second,  0.0 GPU ops/second, 0.0 VFS ops/sec and 14.2%
CPU use



Power est.      Usage       Events/s    Category       Description

  14.0 W     2722 rpm                   Device         Laptop fan

  1.72 W     33.3%                      Device         Display backlight

  862 mW     33.3%                      Device         Display backlight

  255 mW     65.7 ms/s      58.0        Process        /usr/bin/plasma-desktop

 91.9 mW     23.7 ms/s      27.5        Process
/usr/lib/chromium/chromium --type=renderer --lang=en-US --forc

 60.1 mW     15.5 ms/s      96.1        Process
/usr/lib/chromium/chromium --type=plugin --plugin-path=/usr/li

 25.0 mW      6.4 ms/s      25.1        Process        kwin -session
101261418fe3000136103713100000053880000_13746094

 21.5 mW      5.6 ms/s      34.2        Process        /usr/bin/X :0 -auth
/var/run/lightdm/root/:0 -nolisten tcp vt7

 13.1 mW      3.4 ms/s       5.6        Process        /usr/bin/konsole

 9.82 mW      2.5 ms/s      53.7        Process        [irq/48-iwlwifi]

 9.11 mW      2.4 ms/s       2.2        Process        /usr/bin/knemo

 8.62 mW      2.2 ms/s      12.3        Process
/usr/lib/chromium/chromium --password-store=detect

 8.32 mW      2.1 ms/s      45.5        Interrupt      [48] iwlwifi

 6.96 mW      1.8 ms/s      35.3        Interrupt      [7] sched(softirq)

 5.13 mW      1.3 ms/s      57.1        Interrupt      [47] i915

 4.24 mW      1.1 ms/s       0.4        Process        powertop

 3.38 mW      0.9 ms/s       1.5        Timer          tcp_keepalive_timer

 2.85 mW      0.7 ms/s      11.9        Process        /usr/sbin/mysqld
--basedir=/usr --datadir=/var/lib/mysql --plu

