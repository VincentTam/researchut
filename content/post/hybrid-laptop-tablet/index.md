{
    "title": "Linux Tablet-Mode Usability",
    "date": "2017-01-17T09:02:36-05:00",
    "lastmod": "2017-01-17T13:30:38-05:00",
    "draft": "false",
    "tags": [
        "calibre",
        "gnome",
        "multitouch",
        "touchscreen",
        "tablet",
        "goldendict",
        "onboard"
    ],
    "categories": [
        "Debian-Blog",
        "General",
        "KDE",
        "Computing",
        "Technology"
    ],
    "url": "/blog/hybrid-laptop-tablet"
}

In my ongoing quest to get **Tablet-Mode** working on my Hybrid machine,
here's how I've been living with it so far. My intent is to continue using
Free Software for both use cases. My wishful thought is to use the same
software under both use cases.  

  * **Browser:** On the browser front, things are pretty decent. **Chromium** has good support for Touchscreen input. Most of the Touchscreen use cases work well with Chromium. On the **Firefox** side, after a huge delay, finally, Firefox seems to be catching up. Hopefully, with Firefox 51/52, we'll have a much more usable Touchscreen browser.
  * **Desktop Shell:** One of the reason of migrating to GNOME was its touch support. From what I've explored so far, GNOME is the only desktop shell that has touch support natively done. The feature isn't complete yet, but is fairly well usable. 
    * Given that GNOME has touchscreen support native, it is obvious to be using GNOME equivalent of tools for common use cases. Most of these tools inherit the touchscreen capabilities from the underneath GNOME libraries.
    * **File Manager:** Nautilus has decent support for touch, as a file manager. The only annoying bit is a right-click equivalent. Or in touch input sense, a long-press.
    * **Movie Player:** There's a decent movie player, based on GNOME libs; **GNOME MPV.** In my limited use so far, this interface seems to have good support. Other contenders are: [![](/sites/default/files/smplayer-tablet.png)](/sites/default/files/smplayer-tablet.png)
      * **SMPlayer**  is based on Qt libs. So initial expectation would be that Qt based apps would have better Touch support. But I'm yet to see any serious Qt application with Touch input support. Back to SMPlayer, the dev is pragmatic enough to recognize tablet-mode users and as such has provided a so called "Tablet Mode" view for SMPlayer (The tooltip did not get captured in the screenshot).
      * **MPV**  doesn't come with a UI but has basic management with OSD. And in my limited usage, the OSD implementation does seem capable to take touch input.
  * **Books / Documents:** _GNOME Documents/Books_ is very basic in what it has to offer, to the point that it is not much useful. But since it is based on the same GNOME libraries, it enjoys native touch input support.   **Calibre** , on the other hand, is feature rich. But it is based on (Py)Qt. Touch input is told to work for Windows. For Linux, there's no support yet. The good thing about Calibre is that it has its own UI, which is pretty decent in a Tablet-Mode Touch workflow.
  * **Photo Management:** With compact digital devices commonly available, digital content (Both Photos and Videos) is on the rise. The most obvious names that come to mind are Digikam and Shotwell. 
    * **Shotwell**  saw its reincarnation in the recent past. From what I recollect, it does have touch support but was lacking quite a bit in terms of features, as compared to Digikam.
    * **Digikam** is an impressive tool for digital content management. While Digikam is a KDE project, thankfully it does a great job in keeping its KDE dependencies to a bare minimum. But given that Digikam builds on KDE/Qt libs, I haven't had any much success in getting a good touch input solution for Tablet Mode. To make it barely usable in Table-Mode, one could choose a theme preference with bigger toolbars, labels and scrollbars. This helps in making a touch input workaround use case. As you can see, I've configured the Digikam UI with **Text alongside Icons**  for easy touch input.[![](/sites/default/files/Digikam-GNOME-Use.jpg)](/sites/default/files/Digikam-GNOME-Use.jpg)
  * **Email** : The most common use case. With Gmail and friends, many believe standalone email clients are no more a need. But there always are users like us who prefer emails offline, encrypted emails and prefer theis own email domains. Many of these are still doable with free services like Gmail, but still. 
    * **Thunderbird** shows its age at times. And given the state of Firefox in getting touch support (and GTK3 port), I see nothing happening with TB.
    * **KMail** was something I discontinued while still being on KDE. The debacle that KDEPIM  was, is something I'd always avoid, in the future. Complete waste of time/resource in building, testing, reporting and follow-ups.
    * **Geary** is another email client that recently saw its reincarnation. I recently had explored Geary. It enjoys similar benefits like the rest applications using GNOME libraries. There was one touch input [bug](https://bugzilla.gnome.org/show_bug.cgi?id=773051) I found, but otherwise Geary's featureset was limited in comparison to Evolution.
    * Migration to **Evolution** , when migrating to GNOME, was not easy. GNOME's philosophy is to keep things simple and limited. In doing that, they restrict possible flexibilities that users may find obvious. This design philosophy is easily visible across all applications of the GNOME family. Evolution is no different. Hence, coming from TB to E was a small unlearning + newLearning curve. And since Evolution is using the same GNOME libraries, it enjoys similar benefits. Touch input support in Evolution is fairly good. The missing bit is the new Toolbar and Menu structure that many have noticed in the newer GNOME applications (Photos, Documents, Nautilus etc). If only Evolution (and the GNOME family) had the option of customization beyond the developer/project's view, there wouldn't be any wishful thoughts.[![](/sites/default/files/Evoltution-GNOME-flex.png)](/sites/default/files/Evoltution-GNOME-flex.png)
      * Above is a screenshot of 2 windows of Evoluiton. In its current form too, Evolution is a gem at times. For my RSS feeds, they are stored in a VFolder in Evolution, so that I can read them when offline. RSS feeds are something I read up in Tablet-mode. On the right is an Evolution window with larger fonts, while on the left, Evoltuion still retains its default font size. This current behavior helps me get Table-Mode Touch working to an extent. In my wishful thoughts, I wish if Evolution provided flexibility to change Toolbar icon sizes. That'd really help easily _touch_ the delete button when in Tablet Mode. A simple button, _**Tablet Mode**_ , like what SMPlayer has done, would keep users sticky with Evolution.

My wishful thought is that people write (free) software, thinking more about
usability across toolkits and desktop environments. Otherwise, the year of the
Linux ~~desktop~~ ,  ~~laptop~~ , ~~tablet;~~ in my opinion, is yet to come.
And please don't rip apart tools, in porting them to newer versions of the
toolkits. When you rip a tool, you also rip all its QA, Bug Reporting and
Testing, that was done over the years.

Here's an example of a tool ( **Goldendict** ), so well written. Written in
Qt, Running under GNOME, and serving over the Chromium
interface.[![](/sites/default/files/Goldendict.jpg)](/sites/default/files/Goldendict.jpg)



In this whole exercise of getting a hybrid working setup, I also came to
realize that there does not seem to be a standardized interface, yet, to
determine the current operating mode of a running hybrid machine. From what we
[explored](https://bugs.launchpad.net/onboard/+bug/1366421) so far, every
product has its own way to doing it. Most hybrids come pre-installed and
supported with Windows only. So, their mode detection logic seems to be
proprietary too. In case anyone is awaer of a standard interface, please drop
a note in the comments.

