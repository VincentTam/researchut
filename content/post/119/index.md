{
    "title": "Basic RAM Information",
    "date": "2005-07-04T05:30:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "tags": [
        "Compaq",
        "RAM"
    ],
    "categories": [
        "Computing"
    ]
}

[A small amount of research I did before adding more RAM into my laptop. By
the end I had ended up with some information which I thought was worth putting
up on my website.](http://www.researchut.com/docs/meminfo.html "Basic RAM
Information")

