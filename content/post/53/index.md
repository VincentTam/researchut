{
    "title": "KDE on Windows - Be Free",
    "date": "2008-02-13T04:51:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "KDE"
    ]
}

Many of my friends, when watching me work on my laptop, think that I'm using
Windows. But now it is going to be real.

  
  

They'll soon be using, what I use, on Windows. Yes, I'm talking of KDE. [KDE
on Windows](http://windows.kde.org).

  
  

The recent builds of KDE for Windows have been awesome. Most of the
applications, **" Just Work"**

That to Qt and KDE, to road to Freedom has just begun.

  
  

Most of the applications from KDE, even though they are under Beta currently,
work perfect on Windows.

Konqueror, Dolphin, Amarok, KDE Games - You name it - **" Just Works"**

This will lead KDE to attract more developers/users and help enchance KDE
more.

Here are previews of some of the applications available for Windows.

![](http://www.researchut.com/blog/images/amarok.jpg)

Amarok. The killer app. When my colleagues saw it, they were just
flabbergasted. No time to wait. Amarok is being installed and tested by new
users.

![](http://www.researchut.com/blog/images/games.jpg)

My favorite games. KAtomic and Kshisen. Both work perfectly. The SVG rendering
is also perfect.

![](http://www.researchut.com/blog/images/kate.jpg)

Here's Mr. Editor.

![](http://www.researchut.com/blog/images/dolphin.jpg)

And the fish.

![](http://www.researchut.com/blog/images/konqueror.jpg)

And my love, my biggest killer app for KDE. The Konqueror

