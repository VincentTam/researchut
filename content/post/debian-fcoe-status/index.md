{
    "title": "Fibre Channel over Ethernet",
    "date": "2014-06-29T06:24:57-04:00",
    "lastmod": "2014-06-29T06:24:57-04:00",
    "draft": "false",
    "tags": [
        "fcoe"
    ],
    "categories": [
        "Debian-Blog"
    ],
    "url": "/blog/debian-fcoe-status"
}

**Fibre Channel over Ethernet** ( **FCoE** ) is a [computer
network](http://en.wikipedia.org/wiki/Computer_network "Computer network")
technology that encapsulates [Fibre
Channel](http://en.wikipedia.org/wiki/Fibre_Channel "Fibre Channel") frames
over [Ethernet](http://en.wikipedia.org/wiki/Ethernet "Ethernet") networks.
This allows Fibre Channel to use [10 Gigabit
Ethernet](http://en.wikipedia.org/wiki/10_Gigabit_Ethernet "10 Gigabit
Ethernet") networks (or higher speeds) while preserving the Fibre Channel
protocol. The specification was part of the [International Committee for
Information Technology
Standards](http://en.wikipedia.org/wiki/International_Committee_for_Information_Technology_Standards
"International Committee for Information Technology Standards") T11 FC-BB-5
standard published in 2009 (As descripted on Wikipedia)

I just orphaned the FCoE packages for Debian. I don't really have the time and
enthusiasm to maintain FCoE any more. The packages may not be in top notch
shape, but FCoE as a technology, itself did not see many takers. The popcon
stats are low.

In case anyone is interested to takeover the maintenance, there is a [pkg-
fcoe](https://alioth.debian.org/projects/pkg-fcoe/) group on alioth. There are
4 packages that build the stack:
[lldpad](http://packages.qa.debian.org/l/lldpad.html),
[libhbaapi](http://packages.qa.debian.org/libh/libhbaapi.html),
[libhbalinux](http://packages.qa.debian.org/libh/libhbalinux.html) and [fcoe-
utils](http://packages.qa.debian.org/f/fcoe-utils.html).

