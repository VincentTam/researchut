{
    "title": "Bye Bye Bootsplash",
    "date": "2006-07-13T04:28:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

Wow!

This is called freedom. **Bootsplash   **has been a great eye-candy
application in the Linux arena but now it's time to say goodbye.

I just tried **splashy** and its a charm. The great thing is that it doesn 't
need a patched kernel. So newbies or people who don't prefer to rollout their
own kernel can use **splashy** and have a clean boot up.

