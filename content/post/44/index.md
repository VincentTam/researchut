{
    "title": "Building Debian Packages",
    "date": "2008-07-17T09:02:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "packaging"
    ],
    "categories": [
        "Debian-Pages"
    ]
}

When writing packages for Debian, one of the biggest problems is determining
the packages to be put in **Build-Depends**

Sometimes, a single dependency in Build-Depends ends up building the package
successfully on your machine while it fails on other machines. This is where
pbuilder is really handy. It allows you to have bare minimal Debian OS
releases, to which you can pass the package to build. It downloads the Build-
Depends and then does a build in the clean Debian build environment. Now if
you've provided incomplete Build-Depends, your build will fail.

pbuilder really makes it neat when you want to build Debian packages.

> Here's the show of pbuilder in action.

> I: Using pkgname logfile  
>  Current time: Thu Jul 17 03:30:16 IST 2008  
>  pbuilder-time-stamp: 1216245616  
>  Obtaining the cached apt archive contents  
>  Installing the build-deps  
>   -> Attempting to satisfy build-dependencies  
>   -> Creating pbuilder-satisfydepends-dummy package  
>  Package: pbuilder-satisfydepends-dummy  
>  Version: 0.invalid.0  
>  Architecture: i386  
>  Maintainer: Debian Pbuilder Team <pbuilder-maint@lists.alioth.debian.org>  
>  Description: Dummy package to satisfy dependencies with aptitude - created
by pbuilder  
>   This package was created automatically by pbuilder and should  
>  Depends: debhelper (>= 6), autotools-dev, libdbus-qt-1-dev, libhal-dev,
kdelibs4-dev  
>  dpkg-deb: building package `pbuilder-satisfydepends-dummy' in `/tmp
/satisfydepends-aptitude/pbuilder-satisfydepends-dummy.deb'.  
>  Reading package lists...  
>  Building dependency tree...  
>  Reading state information...  
>  aptitude is already the newest version.  
>  0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.  
>  Selecting previously deselected package pbuilder-satisfydepends-dummy.  
>  (Reading database ... 10301 files and directories currently installed.)  
>  Unpacking pbuilder-satisfydepends-dummy (from .../pbuilder-satisfydepends-
dummy.deb) ...  
>  dpkg: dependency problems prevent configuration of pbuilder-satisfydepends-
dummy:  
>   pbuilder-satisfydepends-dummy depends on debhelper (>= 6); however:  
>    Package debhelper is not installed.  
>   pbuilder-satisfydepends-dummy depends on autotools-dev; however:  
>    Package autotools-dev is not installed.  
>   pbuilder-satisfydepends-dummy depends on libdbus-qt-1-dev; however:  
>    Package libdbus-qt-1-dev is not installed.  
>   pbuilder-satisfydepends-dummy depends on libhal-dev; however:  
>    Package libhal-dev is not installed.  
>   pbuilder-satisfydepends-dummy depends on kdelibs4-dev; however:  
>    Package kdelibs4-dev is not installed.  
>  dpkg: error processing pbuilder-satisfydepends-dummy (--install):  
>   dependency problems - leaving unconfigured  
>  Errors were encountered while processing:  
>   pbuilder-satisfydepends-dummy  
>  Reading package lists...  
>  Building dependency tree...  
>  Reading state information...  
>  Initializing package states...  
>  Writing extended state information...  
>  The following NEW packages will be installed:  
>    autotools-dev{a} bsdmainutils{a} comerr-dev{a} debhelper{a} defoma{a}  
>    esound-common{a} file{a} fontconfig{a} fontconfig-config{a} gettext{a}  
>    gettext-base{a} groff-base{a} hicolor-icon-theme{a} html2text{a}  
>    intltool-debian{a} kdelibs-data{a} kdelibs4-dev{a} kdelibs4c2a{a}  
>    libacl1-dev{a} libart-2.0-2{a} libart-2.0-dev{a} libarts1-dev{a}  
>    libarts1c2a{a} libartsc0{a} libartsc0-dev{a} libasound2{a}  
>    libasound2-dev{a} libaspell-dev{a} libaspell15{a} libattr1-dev{a}  
>    libaudio-dev{a} libaudio2{a} libaudiofile-dev{a} libaudiofile0{a}  
>    libavahi-client-dev{a} libavahi-client3{a} libavahi-common-data{a}  
>    libavahi-common-dev{a} libavahi-common3{a} libavahi-qt3-1{a}  
>    libavahi-qt3-dev{a} libavc1394-0{a} libbz2-dev{a} libcups2{a}  
>    libcups2-dev{a} libcupsys2-dev{a} libdbus-1-3{a} libdbus-1-dev{a}  
>    libdbus-qt-1-1c2{a} libdbus-qt-1-dev{a} libdrm2{a} libesd0{a}  
>    libesd0-dev{a} libexpat1{a} libexpat1-dev{a} libfam-dev{a} libfam0{a}  
>    libfontconfig1{a} libfontconfig1-dev{a} libfreebob0{a} libfreetype6{a}  
>    libfreetype6-dev{a} libgcrypt11-dev{a} libgl1-mesa-dev{a}  
>    libgl1-mesa-glx{a} libglib2.0-0{a} libglib2.0-dev{a} libglu1-mesa{a}  
>    libglu1-mesa-dev{a} libgnutls-dev{a} libgpg-error-dev{a} libhal-dev{a}  
>    libhal1{a} libice-dev{a} libice6{a} libidn11{a} libidn11-dev{a}  
>    libiec61883-0{a} libilmbase-dev{a} libilmbase6{a} libjack-dev{a}  
>    libjack0{a} libjack0.100.0-dev{a} libjasper-dev{a} libjasper1{a}  
>    libjpeg62{a} libjpeg62-dev{a} libkadm55{a} libkeyutils1{a} libkrb5-dev{a}  
>    libkrb53{a} liblcms1{a} liblcms1-dev{a} liblua50{a} liblua50-dev{a}  
>    liblualib50{a} liblualib50-dev{a} libmad0{a} libmad0-dev{a} libmagic1{a}  
>    libmng-dev{a} libmng1{a} libnewt0.52{a} libogg-dev{a} libogg0{a}  
>    libopenexr-dev{a} libopenexr6{a} libpcre3{a} libpcre3-dev{a}  
>    libpcrecpp0{a} libpng12-0{a} libpng12-dev{a} libpopt0{a}  
>    libpthread-stubs0{a} libpthread-stubs0-dev{a} libqt3-headers{a}  
>    libqt3-mt{a} libqt3-mt-dev{a} libraw1394-8{a} libsasl2-dev{a}  
>    libsasl2-modules{a} libsm-dev{a} libsm6{a} libssl-dev{a} libssl0.9.8{a}  
>    libtasn1-3-dev{a} libtiff4{a} libtiff4-dev{a} libtiffxx0c2{a}  
>    libvorbis-dev{a} libvorbis0a{a} libvorbisenc2{a} libvorbisfile3{a}  
>    libx11-6{a} libx11-data{a} libx11-dev{a} libxau-dev{a} libxau6{a}  
>    libxaw7{a} libxcb-xlib0{a} libxcb-xlib0-dev{a} libxcb1{a} libxcb1-dev{a}  
>    libxcursor-dev{a} libxcursor1{a} libxdamage1{a} libxdmcp-dev{a}  
>    libxdmcp6{a} libxext-dev{a} libxext6{a} libxfixes-dev{a} libxfixes3{a}  
>    libxft-dev{a} libxft2{a} libxi-dev{a} libxi6{a} libxinerama-dev{a}  
>    libxinerama1{a} libxml2{a} libxml2-dev{a} libxml2-utils{a} libxmu-dev{a}  
>    libxmu-headers{a} libxmu6{a} libxmuu1{a} libxpm4{a} libxrandr-dev{a}  
>    libxrandr2{a} libxrender-dev{a} libxrender1{a} libxslt1-dev{a}  
>    libxslt1.1{a} libxt-dev{a} libxt6{a} libxtrap6{a} libxxf86misc1{a}  
>    libxxf86vm1{a} lua50{a} makedev{a} man-db{a} menu-xdg{a}  
>    mesa-common-dev{a} module-init-tools{a} oss-compat{a} pkg-config{a}  
>    po-debconf{a} qt3-dev-tools{a} ttf-dejavu{a} ttf-dejavu-core{a}  
>    ttf-dejavu-extra{a} ucf{a} whiptail{a} x11-common{a} x11-xserver-utils{a}  
>    x11proto-core-dev{a} x11proto-fixes-dev{a} x11proto-input-dev{a}  
>    x11proto-kb-dev{a} x11proto-randr-dev{a} x11proto-render-dev{a}  
>    x11proto-xext-dev{a} x11proto-xinerama-dev{a} xauth{a} xtrans-dev{a}  
>    zlib1g-dev{a}  
>  The following partially installed packages will be configured:  
>    pbuilder-satisfydepends-dummy  
>  0 packages upgraded, 205 newly installed, 0 to remove and 0 not upgraded.  
>  Need to get 0B/71.6MB of archives. After unpacking 221MB will be used.  
>  Writing extended state information...  
>  debconf: delaying package configuration, since apt-utils is not installed  
>  Selecting previously deselected package libmagic1.  
>  (Reading database ... 10301 files and directories currently installed.)  
>  Unpacking libmagic1 (from .../libmagic1_4.24-4_i386.deb) ...  
>  Selecting previously deselected package file.  
>  Unpacking file (from .../archives/file_4.24-4_i386.deb) ...  
>  Selecting previously deselected package html2text.  
>  Unpacking html2text (from .../html2text_1.3.2a-3_i386.deb) ...  
>  Selecting previously deselected package gettext-base.  
>  Unpacking gettext-base (from .../gettext-base_0.17-2_i386.deb) ...  
>  Selecting previously deselected package gettext.  
>  Unpacking gettext (from .../gettext_0.17-2_i386.deb) ...  
>  Selecting previously deselected package intltool-debian.  
>  Unpacking intltool-debian (from .../intltool-
debian_0.35.0+20060710.1_all.deb) ...  
>  Selecting previously deselected package po-debconf.  
>  Unpacking po-debconf (from .../po-debconf_1.0.15_all.deb) ...  
>  Selecting previously deselected package groff-base.  
>  Unpacking groff-base (from .../groff-base_1.18.1.1-20_i386.deb) ...  
>  Selecting previously deselected package bsdmainutils.  
>  Unpacking bsdmainutils (from .../bsdmainutils_6.1.10_i386.deb) ...  
>  Selecting previously deselected package man-db.  
>  Unpacking man-db (from .../man-db_2.5.2-1_i386.deb) ...  
>  Selecting previously deselected package debhelper.  
>  Unpacking debhelper (from .../debhelper_7.0.10_all.deb) ...  
>  Selecting previously deselected package autotools-dev.  
>  Unpacking autotools-dev (from .../autotools-dev_20080123.1_all.deb) ...  
>  Selecting previously deselected package libdbus-1-3.  
>  Unpacking libdbus-1-3 (from .../libdbus-1-3_1.2.1-2_i386.deb) ...  
>  Selecting previously deselected package libexpat1.  
>  Unpacking libexpat1 (from .../libexpat1_2.0.1-4_i386.deb) ...  
>  Selecting previously deselected package libfreetype6.  
>  Unpacking libfreetype6 (from .../libfreetype6_2.3.7-1_i386.deb) ...  
>  Selecting previously deselected package ucf.  
>  Unpacking ucf (from .../apt/archives/ucf_3.007_all.deb) ...  
>  Moving old data out of the way  
>  Selecting previously deselected package libnewt0.52.  
>  Unpacking libnewt0.52 (from .../libnewt0.52_0.52.2-11.2_i386.deb) ...  
>  Selecting previously deselected package libpopt0.  
>  Unpacking libpopt0 (from .../libpopt0_1.14-4_i386.deb) ...  
>  Selecting previously deselected package whiptail.  
>  Unpacking whiptail (from .../whiptail_0.52.2-11.2_i386.deb) ...  
>  Selecting previously deselected package defoma.  
>  Unpacking defoma (from .../defoma_0.11.10-0.2_all.deb) ...  
>  Selecting previously deselected package ttf-dejavu-core.  
>  Unpacking ttf-dejavu-core (from .../ttf-dejavu-core_2.25-1_all.deb) ...  
>  Selecting previously deselected package ttf-dejavu-extra.  
>  Unpacking ttf-dejavu-extra (from .../ttf-dejavu-extra_2.25-1_all.deb) ...  
>  Selecting previously deselected package ttf-dejavu.  
>  Unpacking ttf-dejavu (from .../ttf-dejavu_2.25-1_all.deb) ...  
>  Selecting previously deselected package fontconfig-config.  
>  Unpacking fontconfig-config (from .../fontconfig-config_2.6.0-1_all.deb)
...  
>  Selecting previously deselected package libfontconfig1.  
>  Unpacking libfontconfig1 (from .../libfontconfig1_2.6.0-1_i386.deb) ...  
>  Selecting previously deselected package fontconfig.  
>  Unpacking fontconfig (from .../fontconfig_2.6.0-1_i386.deb) ...  
>  Selecting previously deselected package x11-common.  
>  Unpacking x11-common (from .../x11-common_1%3a7.3+12_all.deb) ...  
>  Selecting previously deselected package libice6.  
>  Unpacking libice6 (from .../libice6_2%3a1.0.4-1_i386.deb) ...  
>  Selecting previously deselected package libsm6.  
>  Unpacking libsm6 (from .../libsm6_2%3a1.0.3-2_i386.deb) ...  
>  Selecting previously deselected package libxau6.  
>  Unpacking libxau6 (from .../libxau6_1%3a1.0.3-3_i386.deb) ...  
>  Selecting previously deselected package libxdmcp6.  
>  Unpacking libxdmcp6 (from .../libxdmcp6_1%3a1.0.2-3_i386.deb) ...  
>  Selecting previously deselected package libxcb1.  
>  Unpacking libxcb1 (from .../libxcb1_1.1-1.1_i386.deb) ...  
>  Selecting previously deselected package libxcb-xlib0.  
>  Unpacking libxcb-xlib0 (from .../libxcb-xlib0_1.1-1.1_i386.deb) ...  
>  Selecting previously deselected package libx11-data.  
>  Unpacking libx11-data (from .../libx11-data_2%3a1.1.4-2_all.deb) ...  
>  Selecting previously deselected package libx11-6.  
>  Unpacking libx11-6 (from .../libx11-6_2%3a1.1.4-2_i386.deb) ...  
>  Setting up x11-common (1:7.3+12) ...  
>  ************************************  
>  All rc.d operations denied by policy  
>  ************************************  
>  Selecting previously deselected package libxt6.  
>  (Reading database ... 12178 files and directories currently installed.)  
>  Unpacking libxt6 (from .../libxt6_1%3a1.0.5-3_i386.deb) ...  
>  Selecting previously deselected package libaudio2.  
>  Unpacking libaudio2 (from .../libaudio2_1.9.1-4_i386.deb) ...  
>  Selecting previously deselected package libjpeg62.  
>  Unpacking libjpeg62 (from .../libjpeg62_6b-14_i386.deb) ...  
>  Selecting previously deselected package liblcms1.  
>  Unpacking liblcms1 (from .../liblcms1_1.16-10_i386.deb) ...  
>  Selecting previously deselected package libmng1.  
>  Unpacking libmng1 (from .../libmng1_1.0.9-1_i386.deb) ...  
>  Selecting previously deselected package libpng12-0.  
>  Unpacking libpng12-0 (from .../libpng12-0_1.2.27-1_i386.deb) ...  
>  Selecting previously deselected package libxfixes3.  
>  Unpacking libxfixes3 (from .../libxfixes3_1%3a4.0.3-2_i386.deb) ...  
>  Selecting previously deselected package libxrender1.  
>  Unpacking libxrender1 (from .../libxrender1_1%3a0.9.4-2_i386.deb) ...  
>  Selecting previously deselected package libxcursor1.  
>  Unpacking libxcursor1 (from .../libxcursor1_1%3a1.1.9-1_i386.deb) ...  
>  Selecting previously deselected package libxext6.  
>  Unpacking libxext6 (from .../libxext6_2%3a1.0.4-1_i386.deb) ...  
>  Selecting previously deselected package libxft2.  
>  Unpacking libxft2 (from .../libxft2_2.1.12-3_i386.deb) ...  
>  Selecting previously deselected package libxi6.  
>  Unpacking libxi6 (from .../libxi6_2%3a1.1.3-1_i386.deb) ...  
>  Selecting previously deselected package libxinerama1.  
>  Unpacking libxinerama1 (from .../libxinerama1_2%3a1.0.3-2_i386.deb) ...  
>  Selecting previously deselected package libxrandr2.  
>  Unpacking libxrandr2 (from .../libxrandr2_2%3a1.2.2-2_i386.deb) ...  
>  Selecting previously deselected package libqt3-mt.  
>  Unpacking libqt3-mt (from .../libqt3-mt_3%3a3.3.8b-5_i386.deb) ...  
>  Selecting previously deselected package libdbus-qt-1-1c2.  
>  Unpacking libdbus-qt-1-1c2 (from .../libdbus-
qt-1-1c2_0.62.git.20060814-2_i386.deb) ...  
>  Selecting previously deselected package libpcre3.  
>  Unpacking libpcre3 (from .../libpcre3_7.4-1_i386.deb) ...  
>  Selecting previously deselected package libglib2.0-0.  
>  Unpacking libglib2.0-0 (from .../libglib2.0-0_2.16.3-2_i386.deb) ...  
>  Selecting previously deselected package pkg-config.  
>  Unpacking pkg-config (from .../pkg-config_0.22-1_i386.deb) ...  
>  Selecting previously deselected package libdbus-1-dev.  
>  Unpacking libdbus-1-dev (from .../libdbus-1-dev_1.2.1-2_i386.deb) ...  
>  Selecting previously deselected package libaudio-dev.  
>  Unpacking libaudio-dev (from .../libaudio-dev_1.9.1-4_i386.deb) ...  
>  Selecting previously deselected package libkeyutils1.  
>  Unpacking libkeyutils1 (from .../libkeyutils1_1.2-7_i386.deb) ...  
>  Selecting previously deselected package libkrb53.  
>  Unpacking libkrb53 (from .../libkrb53_1.6.dfsg.4~beta1-3_i386.deb) ...  
>  Selecting previously deselected package libcups2.  
>  Unpacking libcups2 (from .../libcups2_1.3.7-7_i386.deb) ...  
>  Selecting previously deselected package libgpg-error-dev.  
>  Unpacking libgpg-error-dev (from .../libgpg-error-dev_1.4-2_i386.deb) ...  
>  Selecting previously deselected package libgcrypt11-dev.  
>  Unpacking libgcrypt11-dev (from .../libgcrypt11-dev_1.4.1-1_i386.deb) ...  
>  Selecting previously deselected package zlib1g-dev.  
>  Unpacking zlib1g-dev (from .../zlib1g-dev_1%3a1.2.3.3.dfsg-12_i386.deb) ...  
>  Selecting previously deselected package libtasn1-3-dev.  
>  Unpacking libtasn1-3-dev (from .../libtasn1-3-dev_1.4-1_i386.deb) ...  
>  Selecting previously deselected package libgnutls-dev.  
>  Unpacking libgnutls-dev (from .../libgnutls-dev_2.4.1-1_i386.deb) ...  
>  Selecting previously deselected package libkadm55.  
>  Unpacking libkadm55 (from .../libkadm55_1.6.dfsg.4~beta1-3_i386.deb) ...  
>  Selecting previously deselected package comerr-dev.  
>  Unpacking comerr-dev (from .../comerr-dev_2.1-1.40.11-1_i386.deb) ...  
>  Selecting previously deselected package libkrb5-dev.  
>  Unpacking libkrb5-dev (from .../libkrb5-dev_1.6.dfsg.4~beta1-3_i386.deb)
...  
>  Selecting previously deselected package libcups2-dev.  
>  Unpacking libcups2-dev (from .../libcups2-dev_1.3.7-7_i386.deb) ...  
>  Selecting previously deselected package libcupsys2-dev.  
>  Unpacking libcupsys2-dev (from .../libcupsys2-dev_1.3.7-7_all.deb) ...  
>  Selecting previously deselected package libexpat1-dev.  
>  Unpacking libexpat1-dev (from .../libexpat1-dev_2.0.1-4_i386.deb) ...  
>  Selecting previously deselected package libfreetype6-dev.  
>  Unpacking libfreetype6-dev (from .../libfreetype6-dev_2.3.7-1_i386.deb) ...  
>  Selecting previously deselected package libfontconfig1-dev.  
>  Unpacking libfontconfig1-dev (from .../libfontconfig1-dev_2.6.0-1_i386.deb)
...  
>  Selecting previously deselected package x11proto-core-dev.  
>  Unpacking x11proto-core-dev (from .../x11proto-core-dev_7.0.12-1_all.deb)
...  
>  Selecting previously deselected package libxau-dev.  
>  Unpacking libxau-dev (from .../libxau-dev_1%3a1.0.3-3_i386.deb) ...  
>  Selecting previously deselected package libxdmcp-dev.  
>  Unpacking libxdmcp-dev (from .../libxdmcp-dev_1%3a1.0.2-3_i386.deb) ...  
>  Selecting previously deselected package x11proto-input-dev.  
>  Unpacking x11proto-input-dev (from .../x11proto-input-dev_1.4.3-2_all.deb)
...  
>  Selecting previously deselected package x11proto-kb-dev.  
>  Unpacking x11proto-kb-dev (from .../x11proto-kb-dev_1.0.3-3_all.deb) ...  
>  Selecting previously deselected package xtrans-dev.  
>  Unpacking xtrans-dev (from .../xtrans-dev_1.2-2_all.deb) ...  
>  Selecting previously deselected package libpthread-stubs0.  
>  Unpacking libpthread-stubs0 (from .../libpthread-stubs0_0.1-2_i386.deb) ...  
>  Selecting previously deselected package libpthread-stubs0-dev.  
>  Unpacking libpthread-stubs0-dev (from .../libpthread-
stubs0-dev_0.1-2_i386.deb) ...  
>  Selecting previously deselected package libxcb1-dev.  
>  Unpacking libxcb1-dev (from .../libxcb1-dev_1.1-1.1_i386.deb) ...  
>  Selecting previously deselected package libxcb-xlib0-dev.  
>  Unpacking libxcb-xlib0-dev (from .../libxcb-xlib0-dev_1.1-1.1_i386.deb) ...  
>  Selecting previously deselected package libx11-dev.  
>  Unpacking libx11-dev (from .../libx11-dev_2%3a1.1.4-2_i386.deb) ...  
>  Selecting previously deselected package mesa-common-dev.  
>  Unpacking mesa-common-dev (from .../mesa-common-dev_7.0.3-4_all.deb) ...  
>  Selecting previously deselected package libdrm2.  
>  Unpacking libdrm2 (from .../libdrm2_2.3.0-4_i386.deb) ...  
>  Selecting previously deselected package libxdamage1.  
>  Unpacking libxdamage1 (from .../libxdamage1_1%3a1.1.1-4_i386.deb) ...  
>  Selecting previously deselected package libxxf86vm1.  
>  Unpacking libxxf86vm1 (from .../libxxf86vm1_1%3a1.0.1-3_i386.deb) ...  
>  Selecting previously deselected package libgl1-mesa-glx.  
>  Unpacking libgl1-mesa-glx (from .../libgl1-mesa-glx_7.0.3-4_i386.deb) ...  
>  Selecting previously deselected package libgl1-mesa-dev.  
>  Unpacking libgl1-mesa-dev (from .../libgl1-mesa-dev_7.0.3-4_all.deb) ...  
>  Selecting previously deselected package libglu1-mesa.  
>  Unpacking libglu1-mesa (from .../libglu1-mesa_7.0.3-4_i386.deb) ...  
>  Selecting previously deselected package libglu1-mesa-dev.  
>  Unpacking libglu1-mesa-dev (from .../libglu1-mesa-dev_7.0.3-4_i386.deb) ...  
>  Selecting previously deselected package libice-dev.  
>  Unpacking libice-dev (from .../libice-dev_2%3a1.0.4-1_i386.deb) ...  
>  Selecting previously deselected package libjpeg62-dev.  
>  Unpacking libjpeg62-dev (from .../libjpeg62-dev_6b-14_i386.deb) ...  
>  Selecting previously deselected package liblcms1-dev.  
>  Unpacking liblcms1-dev (from .../liblcms1-dev_1.16-10_i386.deb) ...  
>  Selecting previously deselected package libmng-dev.  
>  Unpacking libmng-dev (from .../libmng-dev_1.0.9-1_i386.deb) ...  
>  Selecting previously deselected package libpng12-dev.  
>  Unpacking libpng12-dev (from .../libpng12-dev_1.2.27-1_i386.deb) ...  
>  Selecting previously deselected package libqt3-headers.  
>  Unpacking libqt3-headers (from .../libqt3-headers_3%3a3.3.8b-5_i386.deb)
...  
>  Selecting previously deselected package libsm-dev.  
>  Unpacking libsm-dev (from .../libsm-dev_2%3a1.0.3-2_i386.deb) ...  
>  Selecting previously deselected package x11proto-render-dev.  
>  Unpacking x11proto-render-dev (from .../x11proto-render-
dev_2%3a0.9.3-2_all.deb) ...  
>  Selecting previously deselected package libxrender-dev.  
>  Unpacking libxrender-dev (from .../libxrender-dev_1%3a0.9.4-2_i386.deb) ...  
>  Selecting previously deselected package x11proto-xext-dev.  
>  Unpacking x11proto-xext-dev (from .../x11proto-xext-dev_7.0.2-6_all.deb)
...  
>  Selecting previously deselected package x11proto-fixes-dev.  
>  Unpacking x11proto-fixes-dev (from .../x11proto-fixes-
dev_1%3a4.0-3_all.deb) ...  
>  Selecting previously deselected package libxfixes-dev.  
>  Unpacking libxfixes-dev (from .../libxfixes-dev_1%3a4.0.3-2_i386.deb) ...  
>  Selecting previously deselected package libxcursor-dev.  
>  Unpacking libxcursor-dev (from .../libxcursor-dev_1%3a1.1.9-1_i386.deb) ...  
>  Selecting previously deselected package libxext-dev.  
>  Unpacking libxext-dev (from .../libxext-dev_2%3a1.0.4-1_i386.deb) ...  
>  Selecting previously deselected package libxft-dev.  
>  Unpacking libxft-dev (from .../libxft-dev_2.1.12-3_i386.deb) ...  
>  Selecting previously deselected package libxi-dev.  
>  Unpacking libxi-dev (from .../libxi-dev_2%3a1.1.3-1_i386.deb) ...  
>  Selecting previously deselected package x11proto-xinerama-dev.  
>  Unpacking x11proto-xinerama-dev (from .../x11proto-xinerama-
dev_1.1.2-5_all.deb) ...  
>  Selecting previously deselected package libxinerama-dev.  
>  Unpacking libxinerama-dev (from .../libxinerama-dev_2%3a1.0.3-2_i386.deb)
...  
>  Selecting previously deselected package libxmu-headers.  
>  Unpacking libxmu-headers (from .../libxmu-headers_2%3a1.0.4-1_all.deb) ...  
>  Selecting previously deselected package libxmu6.  
>  Unpacking libxmu6 (from .../libxmu6_2%3a1.0.4-1_i386.deb) ...  
>  Selecting previously deselected package libxt-dev.  
>  Unpacking libxt-dev (from .../libxt-dev_1%3a1.0.5-3_i386.deb) ...  
>  Selecting previously deselected package libxmu-dev.  
>  Unpacking libxmu-dev (from .../libxmu-dev_2%3a1.0.4-1_i386.deb) ...  
>  Selecting previously deselected package x11proto-randr-dev.  
>  Unpacking x11proto-randr-dev (from .../x11proto-randr-dev_1.2.1-2_all.deb)
...  
>  Selecting previously deselected package libxrandr-dev.  
>  Unpacking libxrandr-dev (from .../libxrandr-dev_2%3a1.2.2-2_i386.deb) ...  
>  Selecting previously deselected package qt3-dev-tools.  
>  Unpacking qt3-dev-tools (from .../qt3-dev-tools_3%3a3.3.8b-5_i386.deb) ...  
>  Selecting previously deselected package libqt3-mt-dev.  
>  Unpacking libqt3-mt-dev (from .../libqt3-mt-dev_3%3a3.3.8b-5_i386.deb) ...  
>  Selecting previously deselected package libdbus-qt-1-dev.  
>  Unpacking libdbus-qt-1-dev (from .../libdbus-
qt-1-dev_0.62.git.20060814-2_i386.deb) ...  
>  Selecting previously deselected package libhal1.  
>  Unpacking libhal1 (from .../libhal1_0.5.11-2_i386.deb) ...  
>  Selecting previously deselected package libhal-dev.  
>  Unpacking libhal-dev (from .../libhal-dev_0.5.11-2_i386.deb) ...  
>  Selecting previously deselected package libart-2.0-2.  
>  Unpacking libart-2.0-2 (from .../libart-2.0-2_2.3.20-2_i386.deb) ...  
>  Selecting previously deselected package libartsc0.  
>  Unpacking libartsc0 (from .../libartsc0_1.5.9-2_i386.deb) ...  
>  Selecting previously deselected package libasound2.  
>  Unpacking libasound2 (from .../libasound2_1.0.16-2_i386.deb) ...  
>  Selecting previously deselected package libaudiofile0.  
>  Unpacking libaudiofile0 (from .../libaudiofile0_0.2.6-7_i386.deb) ...  
>  Selecting previously deselected package esound-common.  
>  Unpacking esound-common (from .../esound-common_0.2.36-3_all.deb) ...  
>  Selecting previously deselected package libesd0.  
>  Unpacking libesd0 (from .../libesd0_0.2.36-3_i386.deb) ...  
>  Selecting previously deselected package makedev.  
>  Unpacking makedev (from .../makedev_2.3.1-88_all.deb) ...  
>   Removing any system startup links for /etc/init.d/makedev ...  
>  Selecting previously deselected package libraw1394-8.  
>  Unpacking libraw1394-8 (from .../libraw1394-8_1.3.0-3_i386.deb) ...  
>  Selecting previously deselected package libavc1394-0.  
>  Unpacking libavc1394-0 (from .../libavc1394-0_0.5.3-1+b1_i386.deb) ...  
>  Selecting previously deselected package libiec61883-0.  
>  Unpacking libiec61883-0 (from .../libiec61883-0_1.1.0-2_i386.deb) ...  
>  Selecting previously deselected package libxml2.  
>  Unpacking libxml2 (from .../libxml2_2.6.32.dfsg-2_i386.deb) ...  
>  Selecting previously deselected package libfreebob0.  
>  Unpacking libfreebob0 (from .../libfreebob0_1.0.7-1_i386.deb) ...  
>  Selecting previously deselected package libjack0.  
>  Unpacking libjack0 (from .../libjack0_0.109.2-3_i386.deb) ...  
>  Selecting previously deselected package libmad0.  
>  Unpacking libmad0 (from .../libmad0_0.15.1b-3_i386.deb) ...  
>  Selecting previously deselected package libogg0.  
>  Unpacking libogg0 (from .../libogg0_1.1.3-4_i386.deb) ...  
>  Selecting previously deselected package libvorbis0a.  
>  Unpacking libvorbis0a (from .../libvorbis0a_1.2.0.dfsg-3.1_i386.deb) ...  
>  Selecting previously deselected package libvorbisfile3.  
>  Unpacking libvorbisfile3 (from .../libvorbisfile3_1.2.0.dfsg-3.1_i386.deb)
...  
>  Selecting previously deselected package module-init-tools.  
>  Unpacking module-init-tools (from .../module-init-tools_3.4-1_i386.deb) ...  
>  Selecting previously deselected package oss-compat.  
>  Unpacking oss-compat (from .../oss-compat_0.0.4_all.deb) ...  
>  Selecting previously deselected package libarts1c2a.  
>  Unpacking libarts1c2a (from .../libarts1c2a_1.5.9-2_i386.deb) ...  
>  Selecting previously deselected package libaspell15.  
>  Unpacking libaspell15 (from .../libaspell15_0.60.6-1_i386.deb) ...  
>  Selecting previously deselected package libavahi-common-data.  
>  Unpacking libavahi-common-data (from .../libavahi-common-
data_0.6.22-3_i386.deb) ...  
>  Selecting previously deselected package libavahi-common3.  
>  Unpacking libavahi-common3 (from .../libavahi-common3_0.6.22-3_i386.deb)
...  
>  Selecting previously deselected package libavahi-client3.  
>  Unpacking libavahi-client3 (from .../libavahi-client3_0.6.22-3_i386.deb)
...  
>  Selecting previously deselected package libavahi-qt3-1.  
>  Unpacking libavahi-qt3-1 (from .../libavahi-qt3-1_0.6.22-3_i386.deb) ...  
>  Selecting previously deselected package libfam0.  
>  Unpacking libfam0 (from .../libfam0_2.7.0-13.3_i386.deb) ...  
>  Selecting previously deselected package libidn11.  
>  Unpacking libidn11 (from .../libidn11_1.8+20080606-1_i386.deb) ...  
>  Selecting previously deselected package libilmbase6.  
>  Unpacking libilmbase6 (from .../libilmbase6_1.0.1-2+nmu1_i386.deb) ...  
>  Selecting previously deselected package libjasper1.  
>  Unpacking libjasper1 (from .../libjasper1_1.900.1-5_i386.deb) ...  
>  Selecting previously deselected package liblua50.  
>  Unpacking liblua50 (from .../liblua50_5.0.3-3_i386.deb) ...  
>  Selecting previously deselected package liblualib50.  
>  Unpacking liblualib50 (from .../liblualib50_5.0.3-3_i386.deb) ...  
>  Selecting previously deselected package libopenexr6.  
>  Unpacking libopenexr6 (from .../libopenexr6_1.6.1-3_i386.deb) ...  
>  Selecting previously deselected package libtiff4.  
>  Unpacking libtiff4 (from .../libtiff4_3.8.2-10_i386.deb) ...  
>  Selecting previously deselected package libxslt1.1.  
>  Unpacking libxslt1.1 (from .../libxslt1.1_1.1.24-1_i386.deb) ...  
>  Selecting previously deselected package hicolor-icon-theme.  
>  Unpacking hicolor-icon-theme (from .../hicolor-icon-theme_0.10-1_all.deb)
...  
>  Selecting previously deselected package kdelibs-data.  
>  Unpacking kdelibs-data (from .../kdelibs-data_4%3a3.5.9.dfsg.1-6_all.deb)
...  
>  Selecting previously deselected package menu-xdg.  
>  Unpacking menu-xdg (from .../archives/menu-xdg_0.3_all.deb) ...  
>  Selecting previously deselected package libxpm4.  
>  Unpacking libxpm4 (from .../libxpm4_1%3a3.5.7-1_i386.deb) ...  
>  Selecting previously deselected package libxaw7.  
>  Unpacking libxaw7 (from .../libxaw7_2%3a1.0.4-2_i386.deb) ...  
>  Selecting previously deselected package libxmuu1.  
>  Unpacking libxmuu1 (from .../libxmuu1_2%3a1.0.4-1_i386.deb) ...  
>  Selecting previously deselected package libxtrap6.  
>  Unpacking libxtrap6 (from .../libxtrap6_2%3a1.0.0-5_i386.deb) ...  
>  Selecting previously deselected package libxxf86misc1.  
>  Unpacking libxxf86misc1 (from .../libxxf86misc1_1%3a1.0.1-3_i386.deb) ...  
>  Selecting previously deselected package x11-xserver-utils.  
>  Unpacking x11-xserver-utils (from .../x11-xserver-utils_7.3+3_i386.deb) ...  
>  Selecting previously deselected package xauth.  
>  Unpacking xauth (from .../xauth_1%3a1.0.3-2_i386.deb) ...  
>  Selecting previously deselected package kdelibs4c2a.  
>  Unpacking kdelibs4c2a (from .../kdelibs4c2a_4%3a3.5.9.dfsg.1-6_i386.deb)
...  
>  Selecting previously deselected package libart-2.0-dev.  
>  Unpacking libart-2.0-dev (from .../libart-2.0-dev_2.3.20-2_i386.deb) ...  
>  Selecting previously deselected package libglib2.0-dev.  
>  Unpacking libglib2.0-dev (from .../libglib2.0-dev_2.16.3-2_i386.deb) ...  
>  Selecting previously deselected package libartsc0-dev.  
>  Unpacking libartsc0-dev (from .../libartsc0-dev_1.5.9-2_i386.deb) ...  
>  Selecting previously deselected package libasound2-dev.  
>  Unpacking libasound2-dev (from .../libasound2-dev_1.0.16-2_i386.deb) ...  
>  Selecting previously deselected package libaudiofile-dev.  
>  Unpacking libaudiofile-dev (from .../libaudiofile-dev_0.2.6-7_i386.deb) ...  
>  Selecting previously deselected package libesd0-dev.  
>  Unpacking libesd0-dev (from .../libesd0-dev_0.2.36-3_i386.deb) ...  
>  Selecting previously deselected package libjack-dev.  
>  Unpacking libjack-dev (from .../libjack-dev_0.109.2-3_i386.deb) ...  
>  Selecting previously deselected package libjack0.100.0-dev.  
>  Unpacking libjack0.100.0-dev (from
.../libjack0.100.0-dev_0.109.2-3_all.deb) ...  
>  Selecting previously deselected package libmad0-dev.  
>  Unpacking libmad0-dev (from .../libmad0-dev_0.15.1b-3_i386.deb) ...  
>  Selecting previously deselected package libogg-dev.  
>  Unpacking libogg-dev (from .../libogg-dev_1.1.3-4_i386.deb) ...  
>  Selecting previously deselected package libvorbisenc2.  
>  Unpacking libvorbisenc2 (from .../libvorbisenc2_1.2.0.dfsg-3.1_i386.deb)
...  
>  Selecting previously deselected package libvorbis-dev.  
>  Unpacking libvorbis-dev (from .../libvorbis-dev_1.2.0.dfsg-3.1_i386.deb)
...  
>  Selecting previously deselected package libarts1-dev.  
>  Unpacking libarts1-dev (from .../libarts1-dev_1.5.9-2_i386.deb) ...  
>  Selecting previously deselected package libattr1-dev.  
>  Unpacking libattr1-dev (from .../libattr1-dev_1%3a2.4.43-1_i386.deb) ...  
>  Selecting previously deselected package libacl1-dev.  
>  Unpacking libacl1-dev (from .../libacl1-dev_2.2.47-2_i386.deb) ...  
>  Selecting previously deselected package libaspell-dev.  
>  Unpacking libaspell-dev (from .../libaspell-dev_0.60.6-1_i386.deb) ...  
>  Selecting previously deselected package libbz2-dev.  
>  Unpacking libbz2-dev (from .../libbz2-dev_1.0.5-0.1_i386.deb) ...  
>  Selecting previously deselected package libfam-dev.  
>  Unpacking libfam-dev (from .../libfam-dev_2.7.0-13.3_i386.deb) ...  
>  Selecting previously deselected package libidn11-dev.  
>  Unpacking libidn11-dev (from .../libidn11-dev_1.8+20080606-1_i386.deb) ...  
>  Selecting previously deselected package libjasper-dev.  
>  Unpacking libjasper-dev (from .../libjasper-dev_1.900.1-5_i386.deb) ...  
>  Selecting previously deselected package libilmbase-dev.  
>  Unpacking libilmbase-dev (from .../libilmbase-dev_1.0.1-2+nmu1_i386.deb)
...  
>  Selecting previously deselected package libopenexr-dev.  
>  Unpacking libopenexr-dev (from .../libopenexr-dev_1.6.1-3_i386.deb) ...  
>  Selecting previously deselected package libpcrecpp0.  
>  Unpacking libpcrecpp0 (from .../libpcrecpp0_7.4-1_i386.deb) ...  
>  Selecting previously deselected package libpcre3-dev.  
>  Unpacking libpcre3-dev (from .../libpcre3-dev_7.4-1_i386.deb) ...  
>  Selecting previously deselected package lua50.  
>  Unpacking lua50 (from .../lua50_5.0.3-3_i386.deb) ...  
>  Selecting previously deselected package liblua50-dev.  
>  Unpacking liblua50-dev (from .../liblua50-dev_5.0.3-3_i386.deb) ...  
>  Selecting previously deselected package liblualib50-dev.  
>  Unpacking liblualib50-dev (from .../liblualib50-dev_5.0.3-3_i386.deb) ...  
>  Selecting previously deselected package libssl0.9.8.  
>  Unpacking libssl0.9.8 (from .../libssl0.9.8_0.9.8g-10.1_i386.deb) ...  
>  Selecting previously deselected package libsasl2-modules.  
>  Unpacking libsasl2-modules (from
.../libsasl2-modules_2.1.22.dfsg1-21_i386.deb) ...  
>  Selecting previously deselected package libsasl2-dev.  
>  Unpacking libsasl2-dev (from .../libsasl2-dev_2.1.22.dfsg1-21_i386.deb) ...  
>  Selecting previously deselected package libssl-dev.  
>  Unpacking libssl-dev (from .../libssl-dev_0.9.8g-10.1_i386.deb) ...  
>  Selecting previously deselected package libtiffxx0c2.  
>  Unpacking libtiffxx0c2 (from .../libtiffxx0c2_3.8.2-10_i386.deb) ...  
>  Selecting previously deselected package libtiff4-dev.  
>  Unpacking libtiff4-dev (from .../libtiff4-dev_3.8.2-10_i386.deb) ...  
>  Selecting previously deselected package libxml2-dev.  
>  Unpacking libxml2-dev (from .../libxml2-dev_2.6.32.dfsg-2_i386.deb) ...  
>  Selecting previously deselected package libxml2-utils.  
>  Unpacking libxml2-utils (from .../libxml2-utils_2.6.32.dfsg-2_i386.deb) ...  
>  Selecting previously deselected package libxslt1-dev.  
>  Unpacking libxslt1-dev (from .../libxslt1-dev_1.1.24-1_i386.deb) ...  
>  Selecting previously deselected package libavahi-common-dev.  
>  Unpacking libavahi-common-dev (from .../libavahi-common-
dev_0.6.22-3_i386.deb) ...  
>  Selecting previously deselected package libavahi-client-dev.  
>  Unpacking libavahi-client-dev (from .../libavahi-client-
dev_0.6.22-3_i386.deb) ...  
>  Selecting previously deselected package libavahi-qt3-dev.  
>  Unpacking libavahi-qt3-dev (from .../libavahi-qt3-dev_0.6.22-3_i386.deb)
...  
>  Selecting previously deselected package kdelibs4-dev.  
>  Unpacking kdelibs4-dev (from .../kdelibs4-dev_4%3a3.5.9.dfsg.1-6_i386.deb)
...  
>  Setting up libmagic1 (4.24-4) ...  
>  Setting up file (4.24-4) ...  
>  Setting up html2text (1.3.2a-3) ...  
>  Setting up gettext-base (0.17-2) ...  
>  Setting up gettext (0.17-2) ...  
>  Setting up intltool-debian (0.35.0+20060710.1) ...  
>  Setting up po-debconf (1.0.15) ...  
>  Setting up groff-base (1.18.1.1-20) ...  
>  Setting up bsdmainutils (6.1.10) ...  
>  Setting up man-db (2.5.2-1) ...  
>  Building database of manual pages ...  
>  Setting up debhelper (7.0.10) ...  
>  Setting up autotools-dev (20080123.1) ...  
>  Setting up libdbus-1-3 (1.2.1-2) ...  
>  Setting up libexpat1 (2.0.1-4) ...  
>  Setting up libfreetype6 (2.3.7-1) ...  
>  Setting up ucf (3.007) ...  
>  Setting up libnewt0.52 (0.52.2-11.2) ...  
>  Setting up libpopt0 (1.14-4) ...  
>  Setting up whiptail (0.52.2-11.2) ...  
>  Setting up defoma (0.11.10-0.2) ...  
>  Setting up ttf-dejavu-core (2.25-1) ...  
>  Setting up ttf-dejavu-extra (2.25-1) ...  
>  Setting up ttf-dejavu (2.25-1) ...  
>  Setting up fontconfig-config (2.6.0-1) ...  
>  Setting up libfontconfig1 (2.6.0-1) ...  
>  Setting up fontconfig (2.6.0-1) ...  
>  Updating font configuration of fontconfig...  
>  Cleaning up category cid..  
>  Cleaning up category truetype..  
>  Cleaning up category type1..  
>  Updating category type1..  
>  Updating category truetype..  
>  Updating category cid..  
>  Updating fontconfig cache for /usr/share/fonts/truetype/ttf-dejavu  
>  Cleaning up old fontconfig caches... done.  
>  Regenerating fonts cache... done.  
>  Setting up libice6 (2:1.0.4-1) ...  
>  Setting up libsm6 (2:1.0.3-2) ...  
>  Setting up libxau6 (1:1.0.3-3) ...  
>  Setting up libxdmcp6 (1:1.0.2-3) ...  
>  Setting up libxcb1 (1.1-1.1) ...  
>  Setting up libxcb-xlib0 (1.1-1.1) ...  
>  Setting up libx11-data (2:1.1.4-2) ...  
>  Setting up libx11-6 (2:1.1.4-2) ...  
>  Setting up libxt6 (1:1.0.5-3) ...  
>  Setting up libaudio2 (1.9.1-4) ...  
>  Setting up libjpeg62 (6b-14) ...  
>  Setting up liblcms1 (1.16-10) ...  
>  Setting up libmng1 (1.0.9-1) ...  
>  Setting up libpng12-0 (1.2.27-1) ...  
>  Setting up libxfixes3 (1:4.0.3-2) ...  
>  Setting up libxrender1 (1:0.9.4-2) ...  
>  Setting up libxcursor1 (1:1.1.9-1) ...  
>  Setting up libxext6 (2:1.0.4-1) ...  
>  Setting up libxft2 (2.1.12-3) ...  
>  Setting up libxi6 (2:1.1.3-1) ...  
>  Setting up libxinerama1 (2:1.0.3-2) ...  
>  Setting up libxrandr2 (2:1.2.2-2) ...  
>  Setting up libqt3-mt (3:3.3.8b-5) ...  
>  Setting up libdbus-qt-1-1c2 (0.62.git.20060814-2) ...  
>  Setting up libpcre3 (7.4-1) ...  
>  Setting up libglib2.0-0 (2.16.3-2) ...  
>  Setting up pkg-config (0.22-1) ...  
>  Setting up libdbus-1-dev (1.2.1-2) ...  
>  Setting up libaudio-dev (1.9.1-4) ...  
>  Setting up libkeyutils1 (1.2-7) ...  
>  Setting up libkrb53 (1.6.dfsg.4~beta1-3) ...  
>  Setting up libcups2 (1.3.7-7) ...  
>  Setting up libgpg-error-dev (1.4-2) ...  
>  Setting up libgcrypt11-dev (1.4.1-1) ...  
>  Setting up zlib1g-dev (1:1.2.3.3.dfsg-12) ...  
>  Setting up libtasn1-3-dev (1.4-1) ...  
>  Setting up libgnutls-dev (2.4.1-1) ...  
>  Setting up libkadm55 (1.6.dfsg.4~beta1-3) ...  
>  Setting up comerr-dev (2.1-1.40.11-1) ...  
>  Setting up libkrb5-dev (1.6.dfsg.4~beta1-3) ...  
>  Setting up libcups2-dev (1.3.7-7) ...  
>  Setting up libcupsys2-dev (1.3.7-7) ...  
>  Setting up libexpat1-dev (2.0.1-4) ...  
>  Setting up libfreetype6-dev (2.3.7-1) ...  
>  Setting up libfontconfig1-dev (2.6.0-1) ...  
>  Setting up x11proto-core-dev (7.0.12-1) ...  
>  Setting up libxau-dev (1:1.0.3-3) ...  
>  Setting up libxdmcp-dev (1:1.0.2-3) ...  
>  Setting up x11proto-input-dev (1.4.3-2) ...  
>  Setting up x11proto-kb-dev (1.0.3-3) ...  
>  Setting up xtrans-dev (1.2-2) ...  
>  Setting up libpthread-stubs0 (0.1-2) ...  
>  Setting up libpthread-stubs0-dev (0.1-2) ...  
>  Setting up libxcb1-dev (1.1-1.1) ...  
>  Setting up libxcb-xlib0-dev (1.1-1.1) ...  
>  Setting up libx11-dev (2:1.1.4-2) ...  
>  Setting up mesa-common-dev (7.0.3-4) ...  
>  Setting up libdrm2 (2.3.0-4) ...  
>  Setting up libxdamage1 (1:1.1.1-4) ...  
>  Setting up libxxf86vm1 (1:1.0.1-3) ...  
>  Setting up libgl1-mesa-glx (7.0.3-4) ...  
>  Setting up libgl1-mesa-dev (7.0.3-4) ...  
>  Setting up libglu1-mesa (7.0.3-4) ...  
>  Setting up libglu1-mesa-dev (7.0.3-4) ...  
>  Setting up libice-dev (2:1.0.4-1) ...  
>  Setting up libjpeg62-dev (6b-14) ...  
>  Setting up liblcms1-dev (1.16-10) ...  
>  Setting up libmng-dev (1.0.9-1) ...  
>  Setting up libpng12-dev (1.2.27-1) ...  
>  Setting up libqt3-headers (3:3.3.8b-5) ...  
>  Setting up libsm-dev (2:1.0.3-2) ...  
>  Setting up x11proto-render-dev (2:0.9.3-2) ...  
>  Setting up libxrender-dev (1:0.9.4-2) ...  
>  Setting up x11proto-xext-dev (7.0.2-6) ...  
>  Setting up x11proto-fixes-dev (1:4.0-3) ...  
>  Setting up libxfixes-dev (1:4.0.3-2) ...  
>  Setting up libxcursor-dev (1:1.1.9-1) ...  
>  Setting up libxext-dev (2:1.0.4-1) ...  
>  Setting up libxft-dev (2.1.12-3) ...  
>  Setting up libxi-dev (2:1.1.3-1) ...  
>  Setting up x11proto-xinerama-dev (1.1.2-5) ...  
>  Setting up libxinerama-dev (2:1.0.3-2) ...  
>  Setting up libxmu-headers (2:1.0.4-1) ...  
>  Setting up libxmu6 (2:1.0.4-1) ...  
>  Setting up libxt-dev (1:1.0.5-3) ...  
>  Setting up libxmu-dev (2:1.0.4-1) ...  
>  Setting up x11proto-randr-dev (1.2.1-2) ...  
>  Setting up libxrandr-dev (2:1.2.2-2) ...  
>  Setting up qt3-dev-tools (3:3.3.8b-5) ...  
>  Setting up libqt3-mt-dev (3:3.3.8b-5) ...  
>  Setting up libdbus-qt-1-dev (0.62.git.20060814-2) ...  
>  Setting up libhal1 (0.5.11-2) ...  
>  Setting up libhal-dev (0.5.11-2) ...  
>  Setting up libart-2.0-2 (2.3.20-2) ...  
>  Setting up libartsc0 (1.5.9-2) ...  
>  Setting up libasound2 (1.0.16-2) ...  
>  Setting up libaudiofile0 (0.2.6-7) ...  
>  Setting up esound-common (0.2.36-3) ...  
>  Setting up libesd0 (0.2.36-3) ...  
>  Setting up makedev (2.3.1-88) ...  
>  Setting up libraw1394-8 (1.3.0-3) ...  
>  Creating device node /dev/raw1394... done.  
>  Setting up libavc1394-0 (0.5.3-1+b1) ...  
>  Setting up libiec61883-0 (1.1.0-2) ...  
>  Setting up libxml2 (2.6.32.dfsg-2) ...  
>  Setting up libfreebob0 (1.0.7-1) ...  
>  Setting up libjack0 (0.109.2-3) ...  
>  Setting up libmad0 (0.15.1b-3) ...  
>  Setting up libogg0 (1.1.3-4) ...  
>  Setting up libvorbis0a (1.2.0.dfsg-3.1) ...  
>  Setting up libvorbisfile3 (1.2.0.dfsg-3.1) ...  
>  Setting up module-init-tools (3.4-1) ...  
>  Setting up oss-compat (0.0.4) ...  
>  FATAL: Could not load /lib/modules/2.6.25-2-686/modules.dep: No such file
or directory  
>  FATAL: Could not load /lib/modules/2.6.25-2-686/modules.dep: No such file
or directory  
>  FATAL: Could not load /lib/modules/2.6.25-2-686/modules.dep: No such file
or directory  
>  Setting up libarts1c2a (1.5.9-2) ...  
>  Setting up libaspell15 (0.60.6-1) ...  
>  Setting up libavahi-common-data (0.6.22-3) ...  
>  Setting up libavahi-common3 (0.6.22-3) ...  
>  Setting up libavahi-client3 (0.6.22-3) ...  
>  Setting up libavahi-qt3-1 (0.6.22-3) ...  
>  Setting up libfam0 (2.7.0-13.3) ...  
>  Setting up libidn11 (1.8+20080606-1) ...  
>  Setting up libilmbase6 (1.0.1-2+nmu1) ...  
>  Setting up libjasper1 (1.900.1-5) ...  
>  Setting up liblua50 (5.0.3-3) ...  
>  Setting up liblualib50 (5.0.3-3) ...  
>  Setting up libopenexr6 (1.6.1-3) ...  
>  Setting up libtiff4 (3.8.2-10) ...  
>  Setting up libxslt1.1 (1.1.24-1) ...  
>  Setting up hicolor-icon-theme (0.10-1) ...  
>  Setting up kdelibs-data (4:3.5.9.dfsg.1-6) ...  
>  Setting up menu-xdg (0.3) ...  
>  Setting up libxpm4 (1:3.5.7-1) ...  
>  Setting up libxaw7 (2:1.0.4-2) ...  
>  Setting up libxmuu1 (2:1.0.4-1) ...  
>  Setting up libxtrap6 (2:1.0.0-5) ...  
>  Setting up libxxf86misc1 (1:1.0.1-3) ...  
>  Setting up x11-xserver-utils (7.3+3) ...  
>  Setting up xauth (1:1.0.3-2) ...  
>  Setting up kdelibs4c2a (4:3.5.9.dfsg.1-6) ...  
>  Setting up libart-2.0-dev (2.3.20-2) ...  
>  Setting up libglib2.0-dev (2.16.3-2) ...  
>  Setting up libartsc0-dev (1.5.9-2) ...  
>  Setting up libasound2-dev (1.0.16-2) ...  
>  Setting up libaudiofile-dev (0.2.6-7) ...  
>  Setting up libesd0-dev (0.2.36-3) ...  
>  Setting up libjack-dev (0.109.2-3) ...  
>  Setting up libjack0.100.0-dev (0.109.2-3) ...  
>  Setting up libmad0-dev (0.15.1b-3) ...  
>  Setting up libogg-dev (1.1.3-4) ...  
>  Setting up libvorbisenc2 (1.2.0.dfsg-3.1) ...  
>  Setting up libvorbis-dev (1.2.0.dfsg-3.1) ...  
>  Setting up libarts1-dev (1.5.9-2) ...  
>  Setting up libattr1-dev (1:2.4.43-1) ...  
>  Setting up libacl1-dev (2.2.47-2) ...  
>  Setting up libaspell-dev (0.60.6-1) ...  
>  Setting up libbz2-dev (1.0.5-0.1) ...  
>  Setting up libfam-dev (2.7.0-13.3) ...  
>  Setting up libidn11-dev (1.8+20080606-1) ...  
>  Setting up libjasper-dev (1.900.1-5) ...  
>  Setting up libilmbase-dev (1.0.1-2+nmu1) ...  
>  Setting up libopenexr-dev (1.6.1-3) ...  
>  Setting up libpcrecpp0 (7.4-1) ...  
>  Setting up libpcre3-dev (7.4-1) ...  
>  Setting up lua50 (5.0.3-3) ...  
>  Setting up liblua50-dev (5.0.3-3) ...  
>  Setting up liblualib50-dev (5.0.3-3) ...  
>  Setting up libssl0.9.8 (0.9.8g-10.1) ...  
>  Setting up libsasl2-modules (2.1.22.dfsg1-21) ...  
>  Setting up libsasl2-dev (2.1.22.dfsg1-21) ...  
>  Setting up libssl-dev (0.9.8g-10.1) ...  
>  Setting up libtiffxx0c2 (3.8.2-10) ...  
>  Setting up libtiff4-dev (3.8.2-10) ...  
>  Setting up libxml2-dev (2.6.32.dfsg-2) ...  
>  Setting up libxml2-utils (2.6.32.dfsg-2) ...  
>  Setting up libxslt1-dev (1.1.24-1) ...  
>  Setting up libavahi-common-dev (0.6.22-3) ...  
>  Setting up libavahi-client-dev (0.6.22-3) ...  
>  Setting up libavahi-qt3-dev (0.6.22-3) ...  
>  Setting up kdelibs4-dev (4:3.5.9.dfsg.1-6) ...  
>  Setting up pbuilder-satisfydepends-dummy (0.invalid.0) ...  
>  Reading package lists...  
>  Building dependency tree...  
>  Reading state information...  
>  Reading extended state information...  
>  Initializing package states...  
>  Writing extended state information...  
>   -> Finished parsing the build-deps  
>  Reading package lists...  
>  Building dependency tree...  
>  Reading state information...  
>  fakeroot is already the newest version.  
>  0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.  
>  Copying back the cached apt archive contents  
>  Copying source file  
>      -> copying [pbuilder/result/krypt_0.2-1.dsc]  
>      -> copying [pbuilder/result/krypt_0.2.orig.tar.gz]  
>      -> copying [pbuilder/result/krypt_0.2-1.diff.gz]  
>  Extracting source  
>  dpkg-source: warning: extracting unsigned source package (krypt_0.2-1.dsc)  
>  dpkg-source: extracting krypt in krypt-0.2  
>  dpkg-source: info: unpacking krypt_0.2.orig.tar.gz  
>  dpkg-source: info: applying krypt_0.2-1.diff.gz  
>   -> Building the package  
>  dpkg-buildpackage: set CFLAGS to default value: -g -O2  
>  dpkg-buildpackage: set CPPFLAGS to default value:  
>  dpkg-buildpackage: set LDFLAGS to default value:  
>  dpkg-buildpackage: set FFLAGS to default value: -g -O2  
>  dpkg-buildpackage: set CXXFLAGS to default value: -g -O2  
>  dpkg-buildpackage: source package krypt  
>  dpkg-buildpackage: source version 0.2-1  
>  dpkg-buildpackage: host architecture i386  
>   fakeroot debian/rules clean  
>  dh_testdir  
>  dh_testroot  
>  rm -f build-stamp  
>  # Add here commands to clean up after the build process.  
>  [ ! -f Makefile ] || /usr/bin/make distclean  
>  rm -f config.sub config.guess  
>  dh_clean  
>   dpkg-source -b krypt-0.2  
>  dpkg-source: info: using source format `1.0'  
>  dpkg-source: info: building krypt using existing krypt_0.2.orig.tar.gz  
>  dpkg-source: info: building krypt in krypt_0.2-1.diff.gz  
>  dpkg-source: info: building krypt in krypt_0.2-1.dsc  
>   debian/rules build  
>  dh_testdir  
>  # Add here commands to configure the package.  
>  cp -f /usr/share/misc/config.sub config.sub  
>  cp -f /usr/share/misc/config.guess config.guess  
>  ./configure --build i486-linux-gnu --prefix=/usr
--mandir=\${prefix}/share/man --infodir=\${prefix}/share/info CFLAGS="-g -O2"
LDFLAGS="-Wl,-z,defs"  
>  checking build system type... i486-pc-linux-gnu  
>  checking host system type... i486-pc-linux-gnu  
>  checking target system type... i486-pc-linux-gnu  
>  checking for a BSD-compatible install... /usr/bin/install -c  
>  checking for -p flag to install... yes  
>  checking whether build environment is sane... yes  
>  checking for a thread-safe mkdir -p... /bin/mkdir -p  
>  checking for gawk... no  
>  checking for mawk... mawk  
>  checking whether make sets $(MAKE)... yes  
>  checking for kde-config... /usr/bin/kde-config  
>  checking where to install... /usr (as requested)  
>  checking for style of include used by make... GNU  
>  checking for gcc... gcc  
>  checking for C compiler default output file name... a.out  
>  checking whether the C compiler works... yes  
>  checking whether we are cross compiling... no  
>  checking for suffix of executables...  
>  checking for suffix of object files... o  
>  checking whether we are using the GNU C compiler... yes  
>  checking whether gcc accepts -g... yes  
>  checking for gcc option to accept ISO C89... none needed  
>  checking dependency style of gcc... gcc3  
>  checking how to run the C preprocessor... gcc -E  
>  checking for g++... g++  
>  checking whether we are using the GNU C++ compiler... yes  
>  checking whether g++ accepts -g... yes  
>  checking dependency style of g++... gcc3  
>  checking whether gcc is blacklisted... no  
>  checking whether g++ supports -Wmissing-format-attribute... yes  
>  checking whether gcc supports -Wmissing-format-attribute... yes  
>  checking whether g++ supports -Wundef... yes  
>  checking whether g++ supports -Wno-long-long... yes  
>  checking whether g++ supports -Wno-non-virtual-dtor... yes  
>  checking whether g++ supports -fno-reorder-blocks... yes  
>  checking whether g++ supports -fno-exceptions... yes  
>  checking whether g++ supports -fno-check-new... yes  
>  checking whether g++ supports -fno-common... yes  
>  checking whether g++ supports -fexceptions... yes  
>  checking whether system headers can cope with -O2 -fno-inline... irrelevant  
>  checking how to run the C++ preprocessor... g++ -E  
>  checking whether g++ supports -O0... yes  
>  checking whether g++ supports -Wl,--no-undefined... yes  
>  checking whether g++ supports -Wl,--allow-shlib-undefined... yes  
>  not using lib directory suffix  
>  checking for a sed that does not truncate output... /bin/sed  
>  checking for grep that handles long lines and -e... /bin/grep  
>  checking for egrep... /bin/grep -E  
>  checking for ld used by gcc... /usr/bin/ld  
>  checking if the linker (/usr/bin/ld) is GNU ld... yes  
>  checking for /usr/bin/ld option to reload object files... -r  
>  checking for BSD-compatible nm... /usr/bin/nm -B  
>  checking whether ln -s works... yes  
>  checking how to recognise dependent libraries... pass_all  
>  checking for ANSI C header files... yes  
>  checking for sys/types.h... yes  
>  checking for sys/stat.h... yes  
>  checking for stdlib.h... yes  
>  checking for string.h... yes  
>  checking for memory.h... yes  
>  checking for strings.h... yes  
>  checking for inttypes.h... yes  
>  checking for stdint.h... yes  
>  checking for unistd.h... yes  
>  checking dlfcn.h usability... yes  
>  checking dlfcn.h presence... yes  
>  checking for dlfcn.h... yes  
>  checking for g77... no  
>  checking for xlf... no  
>  checking for f77... no  
>  checking for frt... no  
>  checking for pgf77... no  
>  checking for cf77... no  
>  checking for fort77... no  
>  checking for fl32... no  
>  checking for af77... no  
>  checking for xlf90... no  
>  checking for f90... no  
>  checking for pgf90... no  
>  checking for pghpf... no  
>  checking for epcf90... no  
>  checking for gfortran... no  
>  checking for g95... no  
>  checking for xlf95... no  
>  checking for f95... no  
>  checking for fort... no  
>  checking for ifort... no  
>  checking for ifc... no  
>  checking for efc... no  
>  checking for pgf95... no  
>  checking for lf95... no  
>  checking for ftn... no  
>  checking whether we are using the GNU Fortran 77 compiler... no  
>  checking whether  accepts -g... no  
>  checking the maximum length of command line arguments... 32768  
>  checking command to parse /usr/bin/nm -B output from gcc object... ok  
>  checking for objdir... .libs  
>  checking for ar... ar  
>  checking for ranlib... ranlib  
>  checking for strip... strip  
>  checking if gcc static flag  works... yes  
>  checking if gcc supports -fno-rtti -fno-exceptions... no  
>  checking for gcc option to produce PIC... -fPIC  
>  checking if gcc PIC flag -fPIC works... yes  
>  checking if gcc supports -c -o file.o... yes  
>  checking whether the gcc linker (/usr/bin/ld) supports shared libraries...
yes  
>  checking whether -lc should be explicitly linked in... no  
>  checking dynamic linker characteristics... GNU/Linux ld.so  
>  checking how to hardcode library paths into programs... immediate  
>  checking whether stripping libraries is possible... yes  
>  checking for shl_load... no  
>  checking for shl_load in -ldld... no  
>  checking for dlopen... no  
>  checking for dlopen in -ldl... yes  
>  checking whether a program can dlopen itself... yes  
>  checking whether a statically linked program can dlopen itself... yes  
>  checking if libtool supports shared libraries... yes  
>  checking whether to build shared libraries... yes  
>  checking whether to build static libraries... no  
>  configure: creating libtool  
>  appending configuration tag "CXX" to libtool  
>  checking for ld used by g++... /usr/bin/ld  
>  checking if the linker (/usr/bin/ld) is GNU ld... yes  
>  checking whether the g++ linker (/usr/bin/ld) supports shared libraries...
yes  
>  checking for g++ option to produce PIC... -fPIC  
>  checking if g++ PIC flag -fPIC works... yes  
>  checking if g++ supports -c -o file.o... yes  
>  checking whether the g++ linker (/usr/bin/ld) supports shared libraries...
yes  
>  checking dynamic linker characteristics... GNU/Linux ld.so  
>  checking how to hardcode library paths into programs... immediate  
>  checking whether stripping libraries is possible... yes  
>  checking for shl_load... (cached) no  
>  checking for shl_load in -ldld... (cached) no  
>  checking for dlopen... (cached) no  
>  checking for dlopen in -ldl... (cached) yes  
>  checking whether a program can dlopen itself... (cached) yes  
>  checking whether a statically linked program can dlopen itself... (cached)
yes  
>  appending configuration tag "F77" to libtool  
>  checking for msgfmt... /usr/bin/msgfmt  
>  checking for gmsgfmt... /usr/bin/msgfmt  
>  checking for xgettext... /usr/bin/xgettext  
>  checking for pkg-config... /usr/bin/pkg-config  
>  checking if C++ programs can be compiled... yes  
>  checking for strlcat... no  
>  checking if strlcat needs custom prototype... yes - in libkdefakes  
>  checking for strlcpy... no  
>  checking if strlcpy needs custom prototype... yes - in libkdefakes  
>  checking for main in -lutil... yes  
>  checking for main in -lcompat... no  
>  checking for crypt in -lcrypt... yes  
>  checking for socklen_t... yes  
>  checking for dnet_ntoa in -ldnet... no  
>  checking for dnet_ntoa in -ldnet_stub... no  
>  checking for inet_ntoa... yes  
>  checking for connect... yes  
>  checking for remove... yes  
>  checking for shmat... yes  
>  checking for sys/types.h... (cached) yes  
>  checking for stdint.h... (cached) yes  
>  checking sys/bitypes.h usability... yes  
>  checking sys/bitypes.h presence... yes  
>  checking for sys/bitypes.h... yes  
>  checking for poll in -lpoll... no  
>  checking Carbon/Carbon.h usability... no  
>  checking Carbon/Carbon.h presence... no  
>  checking for Carbon/Carbon.h... no  
>  checking CoreAudio/CoreAudio.h usability... no  
>  checking CoreAudio/CoreAudio.h presence... no  
>  checking for CoreAudio/CoreAudio.h... no  
>  checking if res_init needs -lresolv... yes  
>  checking for res_init... yes  
>  checking if res_init needs custom prototype... no  
>  checking for killpg in -lucb... no  
>  checking size of int... 4  
>  checking size of short... 2  
>  checking size of long... 4  
>  checking size of char *... 4  
>  checking for dlopen in -ldl... (cached) yes  
>  checking for shl_unload in -ldld... no  
>  checking size of size_t... 4  
>  checking size of unsigned long... 4  
>  checking sizeof size_t == sizeof unsigned long... yes  
>  checking for PIE support... yes  
>  checking if enabling -pie/fPIE support... yes  
>  checking crt_externs.h usability... no  
>  checking crt_externs.h presence... no  
>  checking for crt_externs.h... no  
>  checking for _NSGetEnviron... no  
>  checking for vsnprintf... yes  
>  checking for snprintf... yes  
>  checking for X... libraries /usr/lib, headers .  
>  checking for IceConnectionNumber in -lICE... yes  
>  checking for libXext... yes  
>  checking for pthread_create in -lpthread... yes  
>  checking for extra includes... no  
>  checking for extra libs... no  
>  checking for libz... -lz  
>  checking for libpng... -lpng -lz -lm  
>  checking for libjpeg6b... no  
>  checking for libjpeg... -ljpeg  
>  checking for perl... /usr/bin/perl  
>  checking for Qt... libraries /usr/lib, headers /usr/share/qt3/include using
-mt  
>  checking for moc... /usr/share/qt3/bin/moc  
>  checking for uic... /usr/share/qt3/bin/uic  
>  checking whether uic supports -L ... yes  
>  checking whether uic supports -nounload ... yes  
>  checking if Qt needs -ljpeg... no  
>  checking for rpath... yes  
>  checking for KDE... libraries /usr/lib, headers /usr/include/kde  
>  checking if UIC has KDE plugins available... yes  
>  checking for KDE paths... defaults  
>  checking for dcopidl... /usr/bin/dcopidl  
>  checking for dcopidl2cpp... /usr/bin/dcopidl2cpp  
>  checking for mcopidl... /usr/bin/mcopidl  
>  checking for artsc-config... /usr/bin/artsc-config  
>  checking for meinproc... /usr/bin/meinproc  
>  checking for kconfig_compiler... /usr/bin/kconfig_compiler  
>  checking for dcopidlng... /usr/bin/dcopidlng  
>  checking for xmllint... /usr/bin/xmllint  
>  checking whether byte ordering is bigendian... no  
>  checking for MAXPATHLEN... 4096  
>  checking pkg-config is at least version 0.9.0... yes  
>  checking for DBUS... yes  
>  checking for HAL... yes  
>  checking dbus/connection.h usability... yes  
>  checking dbus/connection.h presence... yes  
>  checking for dbus/connection.h... yes  
>  checking dbus/message.h usability... yes  
>  checking dbus/message.h presence... yes  
>  checking for dbus/message.h... yes  
>  checking if po should be compiled... yes  
>  checking if src should be compiled... yes  
>  configure: creating ./config.status  
>  wrong input (flag != 4) at admin/conf.change.pl line 117, <> line 1380.  
>  config.status: creating Makefile  
>  config.status: creating po/Makefile  
>  config.status: creating src/Makefile  
>  config.status: creating src/pics/Makefile  
>  config.status: creating config.h  
>  config.status: executing depfiles commands  
>  
>  Good - your configure finished. Start make now  
>  
>  dh_testdir  
>  # Add here commands to compile the package.  
>  /usr/bin/make  
>  make[1]: Entering directory `/tmp/buildd/krypt-0.2'  
>  /usr/bin/make  all-recursive  
>  make[2]: Entering directory `/tmp/buildd/krypt-0.2'  
>  Making all in po  
>  make[3]: Entering directory `/tmp/buildd/krypt-0.2/po'  
>  make[3]: Nothing to be done for `all'.  
>  make[3]: Leaving directory `/tmp/buildd/krypt-0.2/po'  
>  Making all in src  
>  make[3]: Entering directory `/tmp/buildd/krypt-0.2/src'  
>  Making all in pics  
>  make[4]: Entering directory `/tmp/buildd/krypt-0.2/src/pics'  
>  make[4]: Nothing to be done for `all'.  
>  make[4]: Leaving directory `/tmp/buildd/krypt-0.2/src/pics'  
>  make[4]: Entering directory `/tmp/buildd/krypt-0.2/src'  
>  rm -rf confdialog.h;  
>  /usr/share/qt3/bin/uic -L /usr/lib/kde3/plugins/designer -nounload
./confdialog.ui | /usr/bin/perl -pi -e "s,public QWizard,public KWizard,g;
s,#include <qwizard.h>,#include <kwizard.h>,g" >> confdialog.h ;  
>  rm -rf decryptdialog.h;  
>  /usr/share/qt3/bin/uic -L /usr/lib/kde3/plugins/designer -nounload
./decryptdialog.ui | /usr/bin/perl -pi -e "s,public QWizard,public KWizard,g;
s,#include <qwizard.h>,#include <kwizard.h>,g" >> decryptdialog.h ;  
>  rm -rf devconfdialog.h;  
>  /usr/share/qt3/bin/uic -L /usr/lib/kde3/plugins/designer -nounload
./devconfdialog.ui | /usr/bin/perl -pi -e "s,public QWizard,public KWizard,g;
s,#include <qwizard.h>,#include <kwizard.h>,g" >> devconfdialog.h ;  
>  /usr/share/qt3/bin/moc ./halbackend.h -o halbackend.moc  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT halbackend.o -MD -MP -MF .deps/halbackend.Tpo -c -o halbackend.o
halbackend.cpp  
>  mv -f .deps/halbackend.Tpo .deps/halbackend.Po  
>  /usr/share/qt3/bin/moc ./kryptapp.h -o kryptapp.moc  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT kryptapp.o -MD -MP -MF .deps/kryptapp.Tpo -c -o kryptapp.o
kryptapp.cpp  
>  kryptapp.cpp:171: warning: unused parameter 'info'  
>  mv -f .deps/kryptapp.Tpo .deps/kryptapp.Po  
>  /usr/share/qt3/bin/moc ./kryptconf.h -o kryptconf.moc  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT kryptconf.o -MD -MP -MF .deps/kryptconf.Tpo -c -o kryptconf.o
kryptconf.cpp  
>  mv -f .deps/kryptconf.Tpo .deps/kryptconf.Po  
>  /usr/share/qt3/bin/moc ./kryptdevconf.h -o kryptdevconf.moc  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT kryptdevconf.o -MD -MP -MF .deps/kryptdevconf.Tpo -c -o
kryptdevconf.o kryptdevconf.cpp  
>  mv -f .deps/kryptdevconf.Tpo .deps/kryptdevconf.Po  
>  /usr/share/qt3/bin/moc ./kryptdevice.h -o kryptdevice.moc  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT kryptdevice.o -MD -MP -MF .deps/kryptdevice.Tpo -c -o
kryptdevice.o kryptdevice.cpp  
>  mv -f .deps/kryptdevice.Tpo .deps/kryptdevice.Po  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT kryptdevicetriv.o -MD -MP -MF .deps/kryptdevicetriv.Tpo -c -o
kryptdevicetriv.o kryptdevicetriv.cpp  
>  mv -f .deps/kryptdevicetriv.Tpo .deps/kryptdevicetriv.Po  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT kryptdevitem.o -MD -MP -MF .deps/kryptdevitem.Tpo -c -o
kryptdevitem.o kryptdevitem.cpp  
>  mv -f .deps/kryptdevitem.Tpo .deps/kryptdevitem.Po  
>  /usr/share/qt3/bin/moc ./kryptdialog.h -o kryptdialog.moc  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT kryptdialog.o -MD -MP -MF .deps/kryptdialog.Tpo -c -o
kryptdialog.o kryptdialog.cpp  
>  mv -f .deps/kryptdialog.Tpo .deps/kryptdialog.Po  
>  /usr/share/qt3/bin/moc ./kryptsystray.h -o kryptsystray.moc  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT kryptsystray.o -MD -MP -MF .deps/kryptsystray.Tpo -c -o
kryptsystray.o kryptsystray.cpp  
>  mv -f .deps/kryptsystray.Tpo .deps/kryptsystray.Po  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT main.o -MD -MP -MF .deps/main.Tpo -c -o main.o main.cpp  
>  In file included from /usr/include/kde/kaboutdata.h:24,  
>                   from main.cpp:22:  
>  /usr/share/qt3/include/qimage.h: In member function 'bool
QImageTextKeyLang::operator<(const QImageTextKeyLang&) const':  
>  /usr/share/qt3/include/qimage.h:61: warning: suggest parentheses around &&
within ||  
>  mv -f .deps/main.Tpo .deps/main.Po  
>  /usr/share/qt3/bin/moc confdialog.h -o confdialog.moc  
>  rm -f confdialog.cpp  
>  echo '#include <kdialog.h>' > confdialog.cpp  
>  echo '#include <klocale.h>' >> confdialog.cpp  
>  /usr/share/qt3/bin/uic -L /usr/lib/kde3/plugins/designer -nounload -tr
tr2i18n -i confdialog.h ./confdialog.ui > confdialog.cpp.temp ; ret=$?; \  
>      /usr/bin/perl -pe "s,tr2i18n( \"\" ),QString::null,g"
confdialog.cpp.temp | /usr/bin/perl -pe "s,tr2i18n( \"\"\, \"\"
),QString::null,g" | /usr/bin/perl -pe
"s,image([0-9][0-9]*)_data,img\$1_confdialog,g" | /usr/bin/perl -pe "s,:
QWizard\\(,: KWizard(,g" >> confdialog.cpp ;\  
>      rm -f confdialog.cpp.temp ;\  
>      if test "$ret" = 0; then echo '#include "confdialog.moc"' >>
confdialog.cpp; else rm -f confdialog.cpp ; exit $ret ; fi  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT confdialog.o -MD -MP -MF .deps/confdialog.Tpo -c -o confdialog.o
confdialog.cpp  
>  mv -f .deps/confdialog.Tpo .deps/confdialog.Po  
>  /usr/share/qt3/bin/moc decryptdialog.h -o decryptdialog.moc  
>  rm -f decryptdialog.cpp  
>  echo '#include <kdialog.h>' > decryptdialog.cpp  
>  echo '#include <klocale.h>' >> decryptdialog.cpp  
>  /usr/share/qt3/bin/uic -L /usr/lib/kde3/plugins/designer -nounload -tr
tr2i18n -i decryptdialog.h ./decryptdialog.ui > decryptdialog.cpp.temp ;
ret=$?; \  
>      /usr/bin/perl -pe "s,tr2i18n( \"\" ),QString::null,g"
decryptdialog.cpp.temp | /usr/bin/perl -pe "s,tr2i18n( \"\"\, \"\"
),QString::null,g" | /usr/bin/perl -pe
"s,image([0-9][0-9]*)_data,img\$1_decryptdialog,g" | /usr/bin/perl -pe "s,:
QWizard\\(,: KWizard(,g" >> decryptdialog.cpp ;\  
>      rm -f decryptdialog.cpp.temp ;\  
>      if test "$ret" = 0; then echo '#include "decryptdialog.moc"' >>
decryptdialog.cpp; else rm -f decryptdialog.cpp ; exit $ret ; fi  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT decryptdialog.o -MD -MP -MF .deps/decryptdialog.Tpo -c -o
decryptdialog.o decryptdialog.cpp  
>  mv -f .deps/decryptdialog.Tpo .deps/decryptdialog.Po  
>  /usr/share/qt3/bin/moc devconfdialog.h -o devconfdialog.moc  
>  rm -f devconfdialog.cpp  
>  echo '#include <kdialog.h>' > devconfdialog.cpp  
>  echo '#include <klocale.h>' >> devconfdialog.cpp  
>  /usr/share/qt3/bin/uic -L /usr/lib/kde3/plugins/designer -nounload -tr
tr2i18n -i devconfdialog.h ./devconfdialog.ui > devconfdialog.cpp.temp ;
ret=$?; \  
>      /usr/bin/perl -pe "s,tr2i18n( \"\" ),QString::null,g"
devconfdialog.cpp.temp | /usr/bin/perl -pe "s,tr2i18n( \"\"\, \"\"
),QString::null,g" | /usr/bin/perl -pe
"s,image([0-9][0-9]*)_data,img\$1_devconfdialog,g" | /usr/bin/perl -pe "s,:
QWizard\\(,: KWizard(,g" >> devconfdialog.cpp ;\  
>      rm -f devconfdialog.cpp.temp ;\  
>      if test "$ret" = 0; then echo '#include "devconfdialog.moc"' >>
devconfdialog.cpp; else rm -f devconfdialog.cpp ; exit $ret ; fi  
>  g++ -DHAVE_CONFIG_H -I. -I.. -I/usr/include/dbus-1.0
-I/usr/lib/dbus-1.0/include   -DDBUS_API_SUBJECT_TO_CHANGE -I/usr/include/hal
-I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include   -I/usr/include/kde
-I/usr/share/qt3/include -I.   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-
long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-
subscripts -Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-
format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-
common  -MT devconfdialog.o -MD -MP -MF .deps/devconfdialog.Tpo -c -o
devconfdialog.o devconfdialog.cpp  
>  mv -f .deps/devconfdialog.Tpo .deps/devconfdialog.Po  
>  /bin/sh ../libtool --silent --tag=CXX   \--mode=link g++  -Wno-long-long
-Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wchar-subscripts
-Wall -W -Wpointer-arith -O2 -g -O2 -Wformat-security -Wmissing-format-
attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-common  -R
/usr/lib -R /usr/lib -R /usr/lib  -Wl,-z,defs -o krypt halbackend.o kryptapp.o
kryptconf.o kryptdevconf.o kryptdevice.o kryptdevicetriv.o kryptdevitem.o
kryptdialog.o kryptsystray.o main.o confdialog.o decryptdialog.o
devconfdialog.o -lkwalletclient -ldbus-1   -lhal -ldbus-1   -lkdeui -ldbus-
qt-1  
>  make[4]: Leaving directory `/tmp/buildd/krypt-0.2/src'  
>  make[3]: Leaving directory `/tmp/buildd/krypt-0.2/src'  
>  make[3]: Entering directory `/tmp/buildd/krypt-0.2'  
>  make[3]: Nothing to be done for `all-am'.  
>  make[3]: Leaving directory `/tmp/buildd/krypt-0.2'  
>  make[2]: Leaving directory `/tmp/buildd/krypt-0.2'  
>  make[1]: Leaving directory `/tmp/buildd/krypt-0.2'  
>  #docbook-to-man debian/krypt.sgml > krypt.1  
>  touch build-stamp  
>   fakeroot debian/rules binary  
>  dh_testdir  
>  dh_testroot  
>  dh_clean -k  
>  dh_installdirs  
>  # Add here commands to install the package into debian/krypt.  
>  /usr/bin/make DESTDIR=/tmp/buildd/krypt-0.2/debian/krypt install  
>  make[1]: Entering directory `/tmp/buildd/krypt-0.2'  
>  Making install in po  
>  make[2]: Entering directory `/tmp/buildd/krypt-0.2/po'  
>  make[3]: Entering directory `/tmp/buildd/krypt-0.2/po'  
>  make[3]: Nothing to be done for `install-exec-am'.  
>  /usr/bin/install -c -p -m 644 pl.gmo
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/locale/pl/LC_MESSAGES/krypt.mo  
>  mkdir -p --
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/locale/pl/LC_MESSAGES  
>  /usr/bin/install -c -p -m 644 de.gmo
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/locale/de/LC_MESSAGES/krypt.mo  
>  mkdir -p --
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/locale/de/LC_MESSAGES  
>  make[3]: Leaving directory `/tmp/buildd/krypt-0.2/po'  
>  make[2]: Leaving directory `/tmp/buildd/krypt-0.2/po'  
>  Making install in src  
>  make[2]: Entering directory `/tmp/buildd/krypt-0.2/src'  
>  Making install in pics  
>  make[3]: Entering directory `/tmp/buildd/krypt-0.2/src/pics'  
>  make[4]: Entering directory `/tmp/buildd/krypt-0.2/src/pics'  
>  make[4]: Nothing to be done for `install-exec-am'.  
>  test -z "/usr/share/apps/krypt/pics" || /bin/mkdir -p
"/tmp/buildd/krypt-0.2/debian/krypt/usr/share/apps/krypt/pics"  
>   /usr/bin/install -c -p -m 644 'decrypt.png'
'/tmp/buildd/krypt-0.2/debian/krypt/usr/share/apps/krypt/pics/decrypt.png'  
>   /usr/bin/install -c -p -m 644 'encrypt.png'
'/tmp/buildd/krypt-0.2/debian/krypt/usr/share/apps/krypt/pics/encrypt.png'  
>   /usr/bin/install -c -p -m 644 'mount.png'
'/tmp/buildd/krypt-0.2/debian/krypt/usr/share/apps/krypt/pics/mount.png'  
>   /usr/bin/install -c -p -m 644 'umount.png'
'/tmp/buildd/krypt-0.2/debian/krypt/usr/share/apps/krypt/pics/umount.png'  
>   /usr/bin/install -c -p -m 644 'encrypt_48.png'
'/tmp/buildd/krypt-0.2/debian/krypt/usr/share/apps/krypt/pics/encrypt_48.png'  
>   /usr/bin/install -c -p -m 644 'decrypt_48.png'
'/tmp/buildd/krypt-0.2/debian/krypt/usr/share/apps/krypt/pics/decrypt_48.png'  
>   /usr/bin/install -c -p -m 644 'ignore.png'
'/tmp/buildd/krypt-0.2/debian/krypt/usr/share/apps/krypt/pics/ignore.png'  
>  make[4]: Leaving directory `/tmp/buildd/krypt-0.2/src/pics'  
>  make[3]: Leaving directory `/tmp/buildd/krypt-0.2/src/pics'  
>  make[3]: Entering directory `/tmp/buildd/krypt-0.2/src'  
>  make[4]: Entering directory `/tmp/buildd/krypt-0.2/src'  
>  test -z "/usr/bin" || /bin/mkdir -p
"/tmp/buildd/krypt-0.2/debian/krypt/usr/bin"  
>    /bin/sh ../libtool --silent   \--mode=install /usr/bin/install -c -p
'krypt' '/tmp/buildd/krypt-0.2/debian/krypt/usr/bin/krypt'  
>  /bin/sh ../admin/mkinstalldirs
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/32x32/apps  
>  mkdir -p --
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/32x32/apps  
>  /usr/bin/install -c -p -m 644 ./hi32-app-krypt.png
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/32x32/apps/krypt.png  
>  /bin/sh ../admin/mkinstalldirs
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/22x22/apps  
>  mkdir -p --
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/22x22/apps  
>  /usr/bin/install -c -p -m 644 ./hi22-app-krypt.png
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/22x22/apps/krypt.png  
>  /bin/sh ../admin/mkinstalldirs
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/64x64/apps  
>  mkdir -p --
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/64x64/apps  
>  /usr/bin/install -c -p -m 644 ./hi64-app-krypt.png
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/64x64/apps/krypt.png  
>  /bin/sh ../admin/mkinstalldirs
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/16x16/apps  
>  mkdir -p --
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/16x16/apps  
>  /usr/bin/install -c -p -m 644 ./hi16-app-krypt.png
/tmp/buildd/krypt-0.2/debian/krypt/usr/share/icons/hicolor/16x16/apps/krypt.png  
>  test -z "/usr/share/applnk/Utilities" || /bin/mkdir -p
"/tmp/buildd/krypt-0.2/debian/krypt/usr/share/applnk/Utilities"  
>   /usr/bin/install -c -p -m 644 'krypt.desktop'
'/tmp/buildd/krypt-0.2/debian/krypt/usr/share/applnk/Utilities/krypt.desktop'  
>  make[4]: Leaving directory `/tmp/buildd/krypt-0.2/src'  
>  make[3]: Leaving directory `/tmp/buildd/krypt-0.2/src'  
>  make[2]: Leaving directory `/tmp/buildd/krypt-0.2/src'  
>  make[2]: Entering directory `/tmp/buildd/krypt-0.2'  
>  make[3]: Entering directory `/tmp/buildd/krypt-0.2'  
>  make[3]: Nothing to be done for `install-exec-am'.  
>  make[3]: Nothing to be done for `install-data-am'.  
>  make[3]: Leaving directory `/tmp/buildd/krypt-0.2'  
>  make[2]: Leaving directory `/tmp/buildd/krypt-0.2'  
>  make[1]: Leaving directory `/tmp/buildd/krypt-0.2'  
>  dh_testdir  
>  dh_testroot  
>  dh_installchangelogs ChangeLog  
>  dh_installdocs  
>  dh_installexamples  
>  dh_installmenu  
>  dh_installman  
>  dh_link  
>  dh_strip  
>  dh_compress  
>  dh_fixperms  
>  dh_installdeb  
>  dh_shlibdeps  
>  dpkg-shlibdeps: warning: dependency on libz.so.1 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libXt.so.6 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libX11.so.6 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libXft.so.2 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libXrender.so.1 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libfontconfig.so.1 could be avoided
if "debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libXext.so.6 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libdl.so.2 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libkdefx.so.4 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libfreetype.so.6 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libICE.so.6 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libaudio.so.2 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libjpeg.so.62 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libidn.so.11 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libpthread.so.0 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libgcc_s.so.1 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libXi.so.6 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libXinerama.so.1 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libSM.so.6 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libXcursor.so.1 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libpng12.so.0 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libart_lgpl_2.so.2 could be avoided
if "debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dpkg-shlibdeps: warning: dependency on libXrandr.so.2 could be avoided if
"debian/krypt/usr/bin/krypt" were not uselessly linked against it (they use
none of its symbols).  
>  dh_gencontrol  
>  dpkg-gencontrol: warning: unknown substitution variable ${misc:Depends}  
>  dh_md5sums  
>  dh_builddeb  
>  dpkg-deb: building package `krypt' in `../krypt_0.2-1_i386.deb'.  
>   dpkg-genchanges -mRitesh Raj Sarraf <rrs@researchut.com>
>../krypt_0.2-1_i386.changes  
>  dpkg-genchanges: including full source code in upload  
>  dpkg-buildpackage: full upload (original source is included)  
>  Copying back the cached apt archive contents  
>   -> unmounting dev/pts filesystem  
>   -> unmounting proc filesystem  
>   -> cleaning the build env  
>      -> removing directory /tmp/kde-rrs//32737 and its subdirectories  
>  Current time: Thu Jul 17 03:34:59 IST 2008  
>  pbuilder-time-stamp: 1216245899  
>

>  

Awesome.

