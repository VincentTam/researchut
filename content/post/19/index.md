{
    "title": "apt-offline",
    "date": "2009-10-14T16:15:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools",
        "Computing"
    ]
}

It all started long back when I worked for a giant computer manufacturing
company. Certain IT policies led fo the need for an **Offline APT Package
Manager**

While I got it working for a long time, I didn't have the aggression to polish
and push it for general usage. Thanks to my friend appaji, [apt-offline](http
://apt-offline.alioth.debian.org) (a.k.a pypt-offilne) is now part of Debian.

