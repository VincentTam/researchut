{
    "title": "Linux Desktop Usage 2019",
    "date": "2019-03-15T20:05:06+05:30",
    "lastmod": "2019-03-15T20:05:06+05:30",
    "draft": "false",
    "tags": [
        "Desktop",
        "KDE",
        "GNOME",
        "Linux",
        "Integration"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/Linux_Desktop_Usage_2019"
}

If I look back now, it must be more than 20 years since I got fascinated with ***GNU/Linux*** ecosystem and started using it.

Back then, it was more curiosity of a young teenager and the excitement to learn something. There's one thing that I have always admired/respected about Free Software's values, is: <b>Access for everyone to learn</b>. This is something I never forget and still try to do my bit.

It was perfect timing and I was lucky to be part of it. Free Software was (and still is) a great platform to learn upon, if you have the willingness and desire for it.

Over the years, a lot lot lot has changed, evolved and improved. From the days of writing down the <i>XF86Config</i> configuration file to get the **X** server running, to a new world where now everything is almost ***dynamic***, is a great milestone that we have achieved.

All through these years, I always used ***GNU/Linux*** platform as my primary computing platform. The **CLI, Shell and Tools**, have all been a great source of learning. Most of the stuff was (and to an extent, still is) standardized and focus was usually on a single project.

There was less competition on that front, rather there was more **collaboration**. For example, standard tools like: ***sed, awk, grep*** etc were single tools. Like you didn't have 2 variants of it. So, enhancements to these tools was timely and consistent and learning these tools was an incremental task.

On the other hand, on the Desktop side of things, it started and stood for a very long time, to do things their own ways. But eventually, quite a lot of those things have standardized, thankfully.

For the larger part of my desktop usage, I have mostly been a KDE user. I have used other environments like ***IceWM, Enlightenment*** briefly but always felt the need to fallback to KDE, as it provided a full and uniform solution. For quite some time, I was more of a user preferring to only use the **K\*** tools, as in if it wasn't written with ***kdelibs***, I'd try to avoid it. But, In the last 5 years, I took at detour and tried to unlearn and re-learn the other major desktop environment, GNOME.

GNOME is an equally beautiful and elegant desktop environment with a minimalistic user interface (but which at many times ends up plaguing its application's feature set too, making it "minimalistic feature set applications"). I realized that quite a lot of time and money is invested into the GNOME project, especially by the leading Linux Distribution Vendors.

But the fact is that **GNU/Linux** is still not a major player on the Desktop market. Some believe that the Desktop Market itself has faded and been replaced by the Mobile market. I think Desktop Computing still has a critical place in the near foreseeable future and the Mobile Platform is more of an extension shell to it. For example, for quickies, the Mobile platform is perfect. But for a substantial amount of work to be done, we still fallback to using our workstations. Mobile platform is good for a quick chat or email, but if you need to ***write a review report*** or ***a blog post*** or ***prepare a presentation*** or ***update an excel sheet***, you'd still prefer to use your workstation.

So.... After using GNOME platform for a couple of years, I realized that there's a lot of work and thought put into this platform too, just like the KDE platform. ***BUT*** To really be able to dream about the **"Year of the dominance of the GNU/Linux desktop platform"**, all these projects need to work together and synergise their efforts.

Pain points:

* Multiple tools, multiple efforts wasted. Could be synergised.
* Data accessiblity
* Integration and uniformity

## Multiple tools

**Kmail** used to be an awesome email client. **Evoltuion** today is an awesome email client. **Thunderbird** was an awesome email client, which from what I last remember, **Mozilla** had lack of funds to continue maintaining it. And then there's the never ending stream of new/old projects that come and go. Thankfully, email is pretty standardized in its **data format**. Otherwise, it would be a nightmare to switch between these client. But still, GNU/Linux platforms have the potential to provide a strong and viable offering if they could synergise their work together. Today, a lot of resource is just wasted and nobody wins. Definitely not the GNU/Linux platform. Who wins are: GMail, Hotmail etc.

If you even look at the browser side of things, **Google** realized the potential of the Web platform for its business. So they do have a **Web client** for GNU/Linux. But you'll never see an equivalent for Email/PIM. Not because it is obsolete. But more because it would hurt their business instead.

## Data accessibility

My biggest gripe is data accessiblity. Thankfully, for most of the stuff that we rely upon (email, documents etc), things are standardized. But there still are annoyances. For example, when KDE 4.x debacle occured, **kwallet** could not export its password database to the newer one. When I moved to GNOME, I had another very very very hard time extracting passwords from **kwallet** and feeding them to **SeaHorse**. Then, when recently, I switched back to KDE, I had to similarly struggle exporting back my data from **SeaHorse** (no, not back to **KWallet**). Over the years, I realized that critical data should be kept in its simplest format. And let the front-ends do all the bling they want to. I realized this more with Emails. **Maildir** is a good clean format to store my email in, irrespective of how I access my email. Whether it is **dovecot, Evolution, Akonadi, Kmail** etc, I still have my bare data intact.

I had burnt myself on the password front quite a bit, so on this migration back to KDE, I wanted an email like solution. So there's `pass`, a password store, which fits the bill just like the Email use case. It would make a lot more sense for all **Desktop Password Managers** to instead just be a frontend interface to `pass` and let it keep the crucial data in bare minimal format, and accessbile at all times, irrespective of the overhauling that the Desktop projects tend to do every couple of years or so.

Data is **critical**. Without retaining its compatibility (both **backwards and forward**), no battle can you win.

I honestly feel the Linux Desktop Architects from the different projects should sit together and agree on a set of interfaces/tools (yes yes there is fd.o) and **stick to it**. Too much time and energy is wasted otherwise.


## Integration and Uniformity

This is something I have always desired and I was quite impressed (and delighted) to see some progress on the KDE desktop in the UI department. On GNOME, I developed a liking for the **Evolution** email client. Infact, it is my client currently, for Email, NNTP and other PIM. And I still get to use it nicely in a KDE environment. Thank you.
![Evolution KDE/GNOME Integration](/images/linux-desktop-2019-evolution.png)
