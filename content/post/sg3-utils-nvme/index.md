{
    "title": "sg3-utils and NVMe disks",
    "date": "2018-10-02T14:30:00-04:00",
    "lastmod": "2018-10-03T03:04:00-04:00",
    "draft": "false",
    "tags": [
        "sg3-utils",
        "NVMe",
        "SSD",
        "scsi"
    ],
    "categories": [
        "Debian-Blog"
    ],
    "url": "/blog/sg3-utils-nvme"
}

sg3-utils, version 1.44, was recently
[uploaded](https://tracker.debian.org/pkg/sg3-utils) to Debian. This new
upstream release has happened almost 2.5 years after the last release. One
important feature to emphasize about is some support for NVMe disks, which are
now getting more common on latest range of laptops.

    
    
    rrs@priyasi:~$ sudo sg_inq /dev/nvme0
    [sudo] password for rrs: 
    Identify controller for /dev/nvme0:
      Model number: PC401 NVMe SK hynix 512GB               
      Serial number: ES82N092210402J4U   
      Firmware revision: 80003E00
      Version: 1.2
      Optional admin command support:
        Device self-test
        Firmware download and commit
        Format NVM
        Security send and receive
      Optional NVM command support:
        Save and Select fields non-zero
        Dataset management
        Write uncorrectable
        Compare
      PCI vendor ID VID/SSVID: 0x1c5c/0x1c5c
      IEEE OUI Identifier: 0xace42e
      Controller ID: 0x1
      Number of namespaces: 1
      Maximum data transfer size: 32 pages
      Namespace 1 (of 1):
        Namespace size/capacity: 1000215216/1000215216 blocks
        Namespace utilization: 765481168 blocks
        EUI-64: 0xace42e8170034999
        Number of LBA formats: 2
        Index LBA size: 0
        LBA format 0 support: <-- active
          Logical block size: 512 bytes
          Approximate namespace size: 512 GB
          Metadata size: 0 bytes
          Relative performance: Best [0x0]
        LBA format 1 support:
          Logical block size: 4096 bytes
          Approximate namespace size: 0 GB
          Metadata size: 0 bytes
          Relative performance: Best [0x0]
    

More details are available on the
[upstream](http://sg.danny.cz/sg/sg3_utils.html) page. Here's the snippet for
NVMe disks support.

    
    
    NVME Support
    
    On one hand NVM Express (usually written as "NVMe") doesn't have much in common with SCSI. They have different command sets and don't share many transports. The SOP/PQI project which uses the SCSI command set over PCIe (PCI Express) has not caught on, probably closely related to NVMe's success on PCIe.
    
    On the other hand both SCSI and NVMe offer alternative approaches to accessing solid state disks (SSDs), approaches that have much in common. SCSI has been around a little longer (1981 compared to 2011) . In the early days of NVMe there was a SCSI to NVMe Translation Layer (SNTL) but that was dropped in 2017 from the Linux kernel as Intel were not prepared to support it any longer. Strange, since the equivalent mechanism for (mainly) SATA disks called SCSI to ATA Translation (SAT) has been quite successful both for SATA and the SAS (SCSI serial protocol).
    
    There are some signs of the two protocols co-existing with the NVME-MI (Management Interface) group accepting the SES-3 standard for (storage) enclosure management. To this end two NVME-MI commands have been added: SES Send and SES Receive. The library underpinning sg3_utils has been extended to detect NVMe devices in Linux and FreeBSD (not yet working for Windows) has been extended and to pass-through SCSI SEND DIAGNOSTIC and RECEIVE DIAGNOSTIC RESULTS commands to those new NVME-MI commands. The sg3_utils library implements a small SNTL that emulates the mandatory SPC commands (e.g. INQUIRY and REPORT LUNS) and a few others so that the sg_ses utility can interact with a NVMe enclosure with almost no code changes to sg_ses itself. sg_inq has been modified to detect NVMe devices and partially decode the Identify Controller and Identify namespace responses. The lsscsi utility (see the [lsscsi](http://sg.danny.cz/html) package) now lists NVMe namespaces (after SCSI logical units) and NVMe controllers (after SCSI hosts (HBAs)).
    
    The sg_senddiag utility applied to a NVMe device will attempt to tunnel the associated data in NVME-MI SES Send and SES Recieve commands. That should fail unless the NVMe controller has an enclosure attached. The sg_raw command can issue NVME Admin command. The sg_inq command has two modes when applied to a NVME (namespace or controller); without options it will send Identify controller and namespace (assuming there is one) and partially decode the response; with the "--page=sinq" option ("sinq" means standard Inquiry, so for SCSI devices that is the same as the default (no option) usage) will issue a SCSI INQUIRY which the above mentioned SNTL will emulate an INQUIRY response.
    
    Since there is no standard SNTL, this package has taken the liberty of defining its own "vendor specific" VPD page: 0xde . When fetched (with sg_inq, sg_vpd or sdparm) on a NVMe device it will yield the Identify controller response which is 4096 bytes long.
    
    
     

