{
    "title": "Setting up appliances - the new way",
    "date": "2017-02-20T13:39:51-05:00",
    "lastmod": "2017-02-20T13:41:43-05:00",
    "draft": "false",
    "tags": [
        "fitbit",
        "pip",
        "python",
        "linux containers",
        "systemd-nspawn"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/fitbit-pip-nspawn"
}

I own a Fitbit Surge. But Fitibit chose to remain exclusive in terms of
interoperability. Which means to make any sense out of the data that the watch
gathers, you need to stick with what Fitbit mandates. Fair enough in today's
trends. It also is part of their business model to restrict useful aspects of
the report to **Premium Membership.  **Again, fair enough in today's business'
trends.

But a [nice human](https://github.com/praveendath92/fitbit-googlefit) chose to
write a bridge; to extract **Fitbit** data and feed into **Google Fit**. The
project is written in Python, so you can get it to work on most common
computer platforms. I never bothered to package this tool for Debian, because
I never was sure when I'd throw away the Fitbit. But until that happens, I
decided to use the tool to sync my data to Google Fit. Which led me to
requirements.txt

This project's requirement.txt lists versioned module dependencies, of which
many modules in Debian, were either older or newer than what was mentioned in
the requirements. To get the tool working, I installed it the pip way. 3
months later, something broke and I needed to revisit the installed modules.
At that point, I realized that there's no such thing as: **pip upgrade**

That further led me to dig on why anyone wouldn't add something so simple,
because today, in the days of **pip** , **snap** , **flatpak** and **dockers**
, Distributions are predicted to go obsolete and irrelevant. Users should get
the **SOURCES** directly from the developers. But just looking at the [date
the bug was filed](https://github.com/pypa/pip/issues/59), killed my
enthusiasm any further.

So, without packaging for Debian, and without installing through pip, I was
happy that my init has the ability to create confined and containerized
environments, something that I could use to get the job done.



    
    
    rrs@chutzpah:~$ sudo machinectl login fitbit
    [sudo] password for rrs:
    Connected to machine fitbit. Press ^] three times within 1s to exit session.
    
    Debian GNU/Linux 9 fitbit pts/0
    
    fitbit login: root
    Last login: Fri Feb 17 12:44:25 IST 2017 on pts/1
    
    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.
    
    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    root@fitbit:~# tail -n 25 /var/tmp/lxc/fitbit-google.log
    synced calories - 1440 data points
    
    ------------------------------   2017-02-19  -------------------------
    synced steps - 1440 data points
    synced distance - 1440 data points
    synced heart_rate - 38215 data points
    synced weight - 0 logs
    synced body_fat - 0 logs
    synced calories - 1440 data points
    
    ------------------------------   2017-02-20  -------------------------
    synced steps - 1270 data points
    synced distance - 1270 data points
    synced heart_rate - 32547 data points
    synced weight - 0 logs
    synced body_fat - 0 logs
    synced calories - 1271 data points
    
    Synced 7 exercises between : 2017-02-15 -- 2017-02-20
    
    --------------------------------------------------------------------------
                                         Like it ?
    star the repository : https://github.com/praveendath92/fitbit-googlefit
    --------------------------------------------------------------------------
    
    root@fitbit:~#
    



