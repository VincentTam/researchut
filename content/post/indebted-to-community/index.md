{
    "title": "I am so indebted to the community",
    "date": "2013-03-01T13:37:20-05:00",
    "lastmod": "2013-03-01T13:37:20-05:00",
    "draft": "false",
    "tags": [
        "audacity",
        "community",
        "Free Software",
        "LVM",
        "Device Mapper"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/indebted-to-community"
}

As someone who learned computers on his own, I always acknowledged the value
that the Free Software movement has brought. The accessibility of these
topics, which are only supposed to be part of text books and schools, is
available for anyone and everyone who has the resource and passion to do it.

But this past week, 2 things made me pretty impressed with the maturity and
quality of work that we do.

rrs@zan:/media$ sudo lvextend -r -v -L 100G /dev/BackupDisk/DATA

    Finding volume group BackupDisk

    Executing: fsadm --verbose check /dev/BackupDisk/DATA

fsadm: "ext4" filesystem found on "/dev/mapper/BackupDisk-DATA"

fsadm: Skipping filesystem check for device "/dev/mapper/BackupDisk-DATA" as
the filesystem is mounted on /media/DATA

    fsadm failed: 3

    Archiving volume group "BackupDisk" metadata (seqno 7).

  Extending logical volume DATA to 100.00 GiB

    Found volume group "BackupDisk"

    Found volume group "BackupDisk"

    Loading BackupDisk-DATA table (254:2)

    Suspending BackupDisk-DATA (254:2) with device flush

    Found volume group "BackupDisk"

    Resuming BackupDisk-DATA (254:2)

    Creating volume group backup "/etc/lvm/backup/BackupDisk" (seqno 8).

  Logical volume DATA successfully resized

    Executing: fsadm --verbose resize /dev/BackupDisk/DATA 104857600K

fsadm: "ext4" filesystem found on "/dev/mapper/BackupDisk-DATA"

fsadm: Device "/dev/mapper/BackupDisk-DATA" size is 107374182400 bytes

fsadm: Parsing tune2fs -l "/dev/mapper/BackupDisk-DATA"

fsadm: Resizing filesystem on device "/dev/mapper/BackupDisk-DATA" to
107374182400 bytes (13107200 -> 26214400 blocks of 4096 bytes)

fsadm: Executing resize2fs /dev/mapper/BackupDisk-DATA 26214400

resize2fs 1.42.5 (29-Jul-2012)

Filesystem at /dev/mapper/BackupDisk-DATA is mounted on /media/DATA; on-line
resizing required

old_desc_blocks = 4, new_desc_blocks = 7

The filesystem on /dev/mapper/BackupDisk-DATA is now 26214400 blocks long.





I didn't have much hope that this extend operation would succeed. But it did.
When I initiated this operation, in the background, I had the backups being
synced parallely (which actually made me resize my volume. :-)





The other item which made me happy yesterday was **Audacity**. Once upon a
time, when I needed to split a music file to cut a ringtone out of it, I 'd go
looking for software that could do it. Then I would hope that one of those
software vendors have a fully working version and not a demo with just 5
seconds clipping. Cowon was one media player I can recollect I've used to
split audio files.

But in this case, I had a different requirement. I needed to increase the dB
of my ringtone file so that it sounded really a ringtone (Example: Bourne
Ultimatum OST). Audacity, not only did it do the job, it did it for me in just
3-5 minutes. And all with just button clicks. For a n00b with no experience in
that domain, I was really impressed.



