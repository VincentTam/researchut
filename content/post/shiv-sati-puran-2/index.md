{
    "title": "The Holy Tale of Sati – Shiva Puran (Part-II)",
    "date": "2016-01-11T04:58:58-05:00",
    "lastmod": "2016-01-11T04:58:58-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "sati"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/shiv-sati-puran-2"
}

[![shiva-parvati](/sites/default/files/shiva-
parvati.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/shiva-
parvati.jpg)  
Back at Kailasha, in the new marital bliss Lord Shiva once confided to Sati
the nine easy ways to propitiate him and gain his grace. Those nine ways of
devotion were -

  1.  **Hearing**   _(Kathas related to him and his Puranic wisdom)._

  2.  **Singing**   _(Prayers, odes and hymns in his praise)._

  3.  **Remembering  ** _(His glory, deeds and names)._

  4.  **Serving**   _(The noble causes of others taking them as his forms)._

  5.  **Servitude**   _(Living with the spirit of   humility of devotion surrendering all to his grace)._

  6.  **Worship**   _(Him with true devotional spirit confirming to all rites, rituals, customs and regimes)._

  7.  **Obeisance**   _(Making to him in true faith and trust taking him to be the ultimate redeemer and deliverer)._

  8.  **Friendliness**   _(To him as only a true friend proves helpful in need and he as divinely true friend shall get his devotee friend across the sea of mundane woes)._

  9.  **Surrender**   _(The soul to him, all the deeds and consequences to   his grace)._

He revealed that there was a little difference between the knowledge
(Spiritual) and devotion. The latter was more rewarding as it involved direct
approach to power supreme instead of going through various channels of
regimes. For an enlightened one all the mundane attachments and exercises were
meaningless as he mentally withdraws from the physical realities. But a
devotee continues to live in mundane world in comfort and participates in its
joys and pleasures while spiritually benefitting from the grace of Lord Shiva.
Propitiated by direct prayer or worship Lord Shiva blesses his devotee more
readily as the emotion of devotion is more humble and pure here. A  _Jnani_ ,
enlightened one is more dedicated to his own exercise, its correctness and
achievements. For him his blessing is just a reward or spiritual toil. The
_Jnani_ begets some pride in his success. But his devotee is totally dependent
on his grace and no wall of pride exists between him and his devotee.

In this sense. enlightenment ( _Jnan_ ) without devotion is worthless. It also
loses value when pride or egoism poison it. The sentiment of devotion (
_Bhakti_ ) is the most important. Without it, knowledge and asceticism is as
meaningless as honey without its sweetness.

Lord Shiva admitted to Sati that his devotees were his only treasure. For
them, he would burn even God of death, Yama to ashes. His devotee was more
dear to him even than Sati herself. Lord Shiva blessed Sati with comprehension
( _Jnan_ ) of his mysticism, the most important fact of which was that Lord
Shiva and divine power source Uma were indivisible and inseparable. That was
basic truth of Lord Shiva, the very spirit of eternal joy unlimited,
unfathomable and in exhaustible.

"HAR HAR MAHADEV"



Copyright (C) Vibhor Mahajan

